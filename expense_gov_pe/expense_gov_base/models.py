# coding: utf-8
from odoo import models, fields, api

_RUTAS = [
    ('ruta1', "Ruta I"),
    ('ruta2', "Ruta II"),
    ('ruta3', "Ruta III"),
    ('ruta4', "Ruta IV"),
]


class Activity(models.Model):
    _name = "pncm.activity"

    name = fields.Char(string="Nombre")


class TypeTransport(models.Model):
    _name = 'product.type.transport'

    name = fields.Char('Nombre', required=True)


class ProductTypeTransport(models.Model):
    _inherit = 'product.product'

    type_transport_id = fields.Many2one(
        'product.type.transport',
        string=u'Tipo Transporte'
    )


class ExpenseRouterService(models.Model):
    _name = 'expense.routes'

    @api.one
    def _compute_name(self):
        name = self.origin_state_id.name
        name = name + "-{} ".format(self.origin_province_id.name)
        name = name + "-{} ".format(self.origin_district_id.name)
        name = name + "-{} ".format(self.origin_poblation_id.name)
        name = name + " --> HACIA --> {} ".format(self.dest_state_id.name)
        name = name + "-{} ".format(self.dest_province_id.name)
        name = name + "-{} ".format(self.dest_district_id.name)
        name = name + "-{} ".format(self.dest_poblation_id.name)
        self.name = name
        return name

    @api.model
    def _state_default(self):
        user_id = self.env.user.id
        employee = self.env['hr.employee'].search(
            [('user_id','=', user_id)],
            limit=1,
        )
        if employee:
            return employee.department_id.departamento_id.id

    @api.model
    def _province_default(self):
        user_id = self.env.user.id
        employee = self.env['hr.employee'].search(
            [('user_id', '=', user_id)],
            limit=1,
        )
        if employee:
            return employee.department_id.provincia_id.id

    @api.model
    def _district_default(self):
        user_id = self.env.user.id
        employee = self.env['hr.employee'].search(
            [('user_id', '=', user_id)],
            limit=1,
        )
        if employee:
            return employee.department_id.distrito_id.id


    name = fields.Char(string='Nombre', compute='_compute_name')
    # Origen
    origin_state_id = fields.Many2one(
        'res.country.state', 'Departamento Origen',
        required=True,
        domain=[('country_id.code', '=', 'PE'),
                ('state_id', '=', False),
                ('province_id', '=', False)
                ],
        default=lambda self: self._state_default(),
    )
    origin_province_id = fields.Many2one(
        'res.country.state', 'Provincia Origen',
        domain="[('state_id', '=', origin_state_id), "
               "('province_id', '=', False)]",
        default=lambda self: self._province_default(),
    )
    origin_district_id = fields.Many2one(
        'res.country.state', 'Distrito Origen',
        domain="[('state_id', '=', origin_state_id), "
               "('province_id', '=', origin_province_id),"
               "('district_id', '=', False)]",
        default=lambda self: self._district_default(),
    )
    origin_poblation_id = fields.Many2one(
        'res.country.state', 'Centro Poblado Origen',
        domain="[('state_id', '=', origin_state_id), "
               "('province_id', '=', origin_province_id), "
               "('district_id', '=', origin_district_id)]")
    # Destino
    dest_state_id = fields.Many2one(
        'res.country.state', 'Departamento Destino',
        required=True,
        domain=[('country_id.code', '=', 'PE'),
                ('state_id', '=', False),
                ('province_id', '=', False)
                ]
    )
    dest_province_id = fields.Many2one(
        'res.country.state', 'Provincia Destino',
        domain="[('state_id', '=', dest_state_id),"
               " ('province_id', '=', False)]")
    dest_district_id = fields.Many2one(
        'res.country.state', 'Distrito Destino',
        domain="[('state_id', '=', dest_state_id), "
               "('province_id', '=', dest_province_id),"
               "('district_id', '=', False)]")
    dest_poblation_id = fields.Many2one(
        'res.country.state', 'Centro Poblado Destino',
        domain="[('state_id', '=', dest_state_id), "
               "('province_id', '=', dest_province_id), "
               "('district_id', '=', dest_district_id)]")
    reouter_line = fields.One2many(
        'expense.routes.line',
        'router_id',
        string=u'Rutas'
    )


class Expenserouterservice(models.Model):
    _name = 'expense.routes.line'

    router_id = fields.Many2one(
        'expense.routes',
        string="Rutas",
        required=True
    )
    sequence = fields.Selection(
        selection=_RUTAS,
        string='# Ruta'
    )
    # Origen
    origin_state_id = fields.Many2one(
        'res.country.state', 'Departamento Origen',
        required=True,
        domain=[('country_id.code', '=', 'PE'),
                ('state_id', '=', False),
                ('province_id', '=', False)
            ],
    )
    origin_province_id = fields.Many2one(
        'res.country.state', 'Provincia Origen',
        domain="[('state_id', '=', origin_state_id), "
               "('province_id', '=', False)]")
    origin_district_id = fields.Many2one(
        'res.country.state', 'Distrito Origen',
        domain="[('state_id', '=', origin_state_id), "
               "('province_id', '=', origin_province_id),"
               "('district_id', '=', False)]")
    origin_poblation_id = fields.Many2one(
        'res.country.state', 'Centro Poblado Origen',
        domain="[('state_id', '=', origin_state_id), "
               "('province_id', '=', origin_province_id), "
               "('district_id', '=', origin_district_id)]")
    # Destino
    dest_state_id = fields.Many2one(
        'res.country.state', 'Departamento Destino',
        required=True,
        domain=[('country_id.code', '=', 'PE'),
                ('state_id', '=', False),
                ('province_id', '=', False)
                ]
    )
    dest_province_id = fields.Many2one(
        'res.country.state', 'Provincia Destino',
        domain="[('state_id', '=', dest_state_id),"
               " ('province_id', '=', False)]")
    dest_district_id = fields.Many2one(
        'res.country.state', 'Distrito Destino',
        domain="[('state_id', '=', dest_state_id), "
               "('province_id', '=', dest_province_id),"
               "('district_id', '=', False)]")
    dest_poblation_id = fields.Many2one(
        'res.country.state', 'Centro Poblado Destino',
        domain="[('state_id', '=', dest_state_id), "
               "('province_id', '=', dest_province_id), "
               "('district_id', '=', dest_district_id)]")
    type_transport = fields.Many2one(
        'product.type.transport',
        string='Tipo Transporte'
    )
    pricelist = fields.Float(string=u'Costo Individual')
    time_distance = fields.Char(string=u'Tiempo (Hr.)')
