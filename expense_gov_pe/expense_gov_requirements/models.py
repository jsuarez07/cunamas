# coding: utf-8
from odoo import models, fields


class CalendarExpensive(models.Model):
    _name = 'pncm.expensive.calendar'

    start_date = fields.Date(
        string='Comienza en',
    )
    stop_date = fields.Date(
        string='Termina',
    )

    department_id = fields.Many2one(
        'hr.department',
        string=u'Oficina / UT'
    )

    employe_id = fields.Many2one('hr.employee')
    calendar_line = fields.One2many(
        'pncm.expensive.calendar.line',
        'calendar_id',
        string=u'Lugares a visitar'
    )


class Calendarexpensive(models.Model):
    _name = 'pncm.expensive.calendar.line'

    calendar_id = fields.Many2one(
        'pncm.expensive.calendar',
        string=u'Calendario'
    )
    # Destino
    dest_state_id = fields.Many2one(
        'res.country.state', 'Departamento Destino',
        required=True,
        domain=[('country_id.code', '=', 'PE'),
                ('state_id', '=', False),
                ('province_id', '=', False)
                ]
    )
    dest_province_id = fields.Many2one(
        'res.country.state', 'Provincia Destino',
        domain="[('state_id', '=', dest_state_id),"
               " ('province_id', '=', False)]")
    dest_district_id = fields.Many2one(
        'res.country.state', 'Distrito Destino',
        domain="[('state_id', '=', dest_state_id), "
               "('province_id', '=', dest_province_id),"
               "('district_id', '=', False)]")
    dest_poblation_id = fields.Many2one(
        'res.country.state', 'Centro Poblado Destino',
        domain="[('state_id', '=', dest_state_id), "
               "('province_id', '=', dest_province_id), "
               "('district_id', '=', dest_district_id)]")
    activity = fields.Many2one(
        'pncm.activity',
        string=u'Tipo Activdad'
    )
    date = fields.Date(string='Fecha')
