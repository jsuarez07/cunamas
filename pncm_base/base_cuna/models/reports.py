# -*- coding: utf-8 -*-

import calendar
import datetime
from ...report_xlsx.report.report_xlsx import ReportXlsx
from datetime import datetime


def cantidad_meses(d1, d2):
    format_str = '%Y-%m-%d'
    d1 = datetime.strptime(d1, format_str)
    d2 = datetime.strptime(d2, format_str)
    return (d1.year - d2.year) * 12 + d1.month - d2.month


def buscar_tabla_base(name, line):
    data = line.filtered(lambda x: x.name == name)
    if len(data) == 1:
        return 1
    else:
        return 0


def buscar_discapacidad(name, line):
    if line.abrev == name:
        return 1
    else:
        return 0


def buscar_jefe_hogar(line):
    rec = line.filtered(lambda x: not x.fecha_fin and x.es_jefe_hogar)
    if len(rec) == 1:
        return 1
    else:
        return 0


def convertir_str_date(string):
    format_str = '%Y-%m-%d'
    dia = datetime.strptime(string, format_str)
    return dia


def get_month_day_range(date):
    date = datetime.strptime(date, '%Y-%m-%d')
    first_day = date.replace(day=1)
    last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
    return first_day, last_day


def verificar_fechas(obj, anio, mes):
    date_str = '%s-%s-%s' % (anio.name, str(mes), '01')
    # primero obtengo el primer y ultimo dia del mes
    primer_dia, ultimo_dia = get_month_day_range(date_str)
    # convierto la fecha del obj a datetime
    fecha_inicio = convertir_str_date(obj.fecha_ingreso)
    if fecha_inicio and not obj.fecha_retiro:
        if fecha_inicio <= primer_dia:
            return True
        else:
            if fecha_inicio <= ultimo_dia:
                return True
            else:
                return False
    else:
        fecha_fin = convertir_str_date(obj.fecha_ingreso)
        if fecha_fin <= ultimo_dia:
            return True
        else:
            if ultimo_dia >= fecha_inicio >= primer_dia:
                return True
            else:
                return False


class ActoresComunalesReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Actores Comunales')
        sheet.write(0, 0, 'ANIO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'CGREQ_UBIGEO', title_blue_format)
        sheet.write(0, 3, 'CG UNIDAD_TERRITORIAL_NUEVA', title_blue_format)
        sheet.write(0, 4, 'CORD_UNIDAD_TERRITORIAL', title_blue_format)
        sheet.write(0, 5, 'DEPARTAMENTO', title_blue_format)
        sheet.write(0, 6, 'PROVINCIA', title_blue_format)
        sheet.write(0, 7, 'DISTRITO', title_blue_format)
        sheet.write(0, 8, 'UT_PERSONA_ID', title_blue_format)
        sheet.write(0, 9, 'UT_PERSONA_NOM', title_blue_format)
        sheet.write(0, 10, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 11, 'NOMDEP', title_blue_format)
        sheet.write(0, 12, 'NOMPRO', title_blue_format)
        sheet.write(0, 13, 'NOMDIS', title_blue_format)
        sheet.write(0, 14, 'TCCPP_ID', title_blue_format)
        sheet.write(0, 15, 'CODCENPOB', title_blue_format)
        sheet.write(0, 16, 'NOMCENPOB', title_blue_format)
        sheet.write(0, 17, 'CG_ID', title_blue_format)
        sheet.write(0, 18, 'CG_COD', title_blue_format)
        sheet.write(0, 19, 'CG_NOM', title_blue_format)
        sheet.write(0, 20, 'FECINIATE', title_blue_format)
        sheet.write(0, 21, 'FECCIEATE', title_blue_format)
        sheet.write(0, 22, 'CARGO_INTEGRANTE', title_blue_format)
        sheet.write(0, 23, 'ACTOR_COD', title_blue_format)
        sheet.write(0, 24, 'ACTOR_PATERNO', title_blue_format)
        sheet.write(0, 25, 'ACTOR_MATERNO', title_blue_format)
        sheet.write(0, 26, 'ACTOR_NOMBRE', title_blue_format)
        sheet.write(0, 27, 'ACTOR_FECNAC', title_blue_format)
        sheet.write(0, 28, 'TIPO_DOCUMENTO', title_blue_format)
        sheet.write(0, 29, 'NUMDOC', title_blue_format)
        sheet.write(0, 30, 'SEXO', title_blue_format)
        sheet.write(0, 31, 'ES_JEFE_HOGAR', title_blue_format)
        sheet.write(0, 32, 'LEE_ESCRIBE', title_blue_format)
        sheet.write(0, 33, 'DISC_VISUAL', title_blue_format)
        sheet.write(0, 34, 'DISC_OIR', title_blue_format)
        sheet.write(0, 35, 'DISC_HABLAR', title_blue_format)
        sheet.write(0, 36, 'DISC_EXTREMIDADES', title_blue_format)
        sheet.write(0, 37, 'DISC_MENTAL', title_blue_format)
        sheet.write(0, 38, 'NO_TIENE_DISCAPACIDAD', title_blue_format)
        sheet.write(0, 39, 'NRO_HIJOS', title_blue_format)
        sheet.write(0, 40, 'CUNAMAS', title_blue_format)
        sheet.write(0, 41, 'QALI_WARMA', title_blue_format)
        sheet.write(0, 42, 'FONCODES', title_blue_format)
        sheet.write(0, 43, 'JUNTOS', title_blue_format)
        sheet.write(0, 44, 'PENSION_65', title_blue_format)
        sheet.write(0, 45, 'OTRO', title_blue_format)
        sheet.write(0, 46, 'NO_BENEF_PROGRAMA', title_blue_format)
        sheet.write(0, 47, 'RELIGION', title_blue_format)
        sheet.write(0, 48, 'LENGUA_MATERNA', title_blue_format)
        sheet.write(0, 49, 'NIVEL_EDUCATIVO', title_blue_format)
        sheet.write(0, 50, 'ULT_ANIO_APROBADO', title_blue_format)
        sheet.write(0, 51, 'OCUPACION', title_blue_format)
        sheet.write(0, 52, 'ESTADO_CIVIL', title_blue_format)
        sheet.write(0, 53, 'IDTCENPOB', title_blue_format)
        sheet.write(0, 54, 'FECHA_INICIO', title_blue_format)
        sheet.write(0, 55, 'FECHA_FIN', title_blue_format)
        sheet.write(0, 56, 'FUNCIONO', title_blue_format)
        sheet.write(0, 57, 'REPORTE_FECHA', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1
        ut = lines.unidad_territorial_id
        for comite in lines.unidad_territorial_id.ut_comites_gestion:
            if comite.state == 'activo':
                for actor in comite.afiliaciones_ids:
                    sheet.write(row, 0, anio, body_format)
                    sheet.write(row, 1, mes, body_format)
                    sheet.write(row, 2, ut.id, body_format)
                    sheet.write(row, 3, ut.name, body_format)
                    sheet.write(row, 4, ut.name, body_format)
                    sheet.write(row, 5, comite.departamento_id.name, body_format)
                    sheet.write(row, 6, comite.provincia_id.name, body_format)
                    sheet.write(row, 7, comite.distrito_id.name, body_format)
                    sheet.write(row, 8, actor.id, body_format)
                    sheet.write(row, 9, actor.person_id.name, body_format)
                    sheet.write(row, 10, ut.id, body_format)
                    sheet.write(row, 11, comite.departamento_id.name, body_format)
                    sheet.write(row, 12, comite.provincia_id.name, body_format)
                    sheet.write(row, 13, comite.distrito_id.name, body_format)
                    sheet.write(row, 14, comite.centro_poblado_id.id, body_format)
                    sheet.write(row, 15, comite.centro_poblado_id.name, body_format)
                    sheet.write(row, 16, comite.centro_poblado_id.name, body_format)
                    sheet.write(row, 17, comite.id, body_format)
                    sheet.write(row, 18, comite.id, body_format)
                    sheet.write(row, 19, comite.name, body_format)
                    sheet.write(row, 20, comite.fecha_inicio, body_format)
                    sheet.write(row, 21, comite.fecha_cierre or '', body_format)
                    sheet.write(row, 22, actor.cargo, body_format)
                    sheet.write(row, 23, actor.id, body_format)
                    sheet.write(row, 24, actor.person_id.firstname, body_format)
                    sheet.write(row, 25, actor.person_id.secondname, body_format)
                    sheet.write(row, 26, actor.person_id.name, body_format)
                    sheet.write(row, 27, actor.person_id.birthdate, body_format)
                    sheet.write(row, 28, actor.person_id.type_document, body_format)
                    sheet.write(row, 29, actor.person_id.document_number, body_format)
                    sheet.write(row, 30, actor.person_id.gender, body_format)
                    sheet.write(row, 31, buscar_jefe_hogar(actor.person_id.integrantes_lines_ids), body_format)
                    sheet.write(row, 32, actor.person_id.sabe_leer_y_escribir, body_format)
                    sheet.write(
                        row, 33, buscar_discapacidad('DISC_VISUAL', actor.person_id.discapacidad_id), body_format
                    )
                    sheet.write(
                        row, 34, buscar_discapacidad('DISC_OIR', actor.person_id.discapacidad_id), body_format
                    )
                    sheet.write(
                        row, 35, buscar_discapacidad('DISC_HABLAR', actor.person_id.discapacidad_id), body_format
                    )
                    sheet.write(
                        row, 36, buscar_discapacidad('DISC_EXTREMIDADES', actor.person_id.discapacidad_id), body_format
                    )
                    sheet.write(
                        row, 37, buscar_discapacidad('DISC_MENTAL', actor.person_id.discapacidad_id), body_format
                    )
                    sheet.write(
                        row, 38, buscar_discapacidad('NO_TIENE_DISCAPACIDAD', actor.person_id.discapacidad_id),
                        body_format
                    )
                    sheet.write(row, 39, actor.person_id.nro_hijos, body_format)
                    sheet.write(
                        row, 40, buscar_tabla_base(u'CunaMás', actor.person_id.programa_social_ids), body_format
                    )
                    sheet.write(
                        row, 41, buscar_tabla_base('Qali Warma', actor.person_id.programa_social_ids), body_format
                    )
                    sheet.write(
                        row, 42, buscar_tabla_base('Foncodes', actor.person_id.programa_social_ids), body_format
                    )
                    sheet.write(row, 43, buscar_tabla_base('Juntos', actor.person_id.programa_social_ids), body_format)
                    sheet.write(
                        row, 44, buscar_tabla_base(u'Pensión', actor.person_id.programa_social_ids), body_format
                    )
                    sheet.write(row, 45, buscar_tabla_base('Otro', actor.person_id.programa_social_ids), body_format)
                    sheet.write(row, 46, buscar_tabla_base('Ninguno', actor.person_id.programa_social_ids), body_format)
                    sheet.write(row, 47, actor.person_id.religion_id.name, body_format)
                    sheet.write(row, 48, actor.person_id.lengua_materna_id.name, body_format)
                    sheet.write(row, 49, actor.person_id.nivel_educativo_id.name, body_format)
                    sheet.write(row, 50, actor.person_id.ultimo_grado_apr_id.name, body_format)
                    sheet.write(row, 51, actor.person_id.ocupacion_id.name, body_format)
                    sheet.write(row, 52, actor.person_id.marital_status, body_format)
                    sheet.write(row, 53, comite.centro_poblado_id.id, body_format)
                    sheet.write(row, 54, actor.fecha_ingreso, body_format)
                    sheet.write(row, 55, actor.fecha_retiro or '', body_format)
                    sheet.write(row, 56, 0 if actor.fecha_retiro else 1, body_format)
                    sheet.write(row, 57, lines.fecha_creacion, body_format)
                    row += 1


ActoresComunalesReportXls('report.base_cuna.ActoresComunalesReportXls.xlsx', 'dynamic.report.wizard')


class ApoyoAdministrativoReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Apoyo Administrativo')
        sheet.set_column('C:C', 25)
        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOM UNIDAD_TERRITORIAL', title_blue_format)
        sheet.write(0, 3, 'NOMDEP', title_blue_format)
        sheet.write(0, 4, 'NOMPRO', title_blue_format)
        sheet.write(0, 5, 'NOMDIS', title_blue_format)
        sheet.write(0, 6, 'ID', title_blue_format)
        sheet.write(0, 7, 'NOMCOM', title_blue_format)
        sheet.write(0, 8, 'ID_PERSONA', title_blue_format)
        sheet.write(0, 9, 'APEPATERNO', title_blue_format)
        sheet.write(0, 10, 'APEMATERNO', title_blue_format)
        sheet.write(0, 11, 'NOMBRE', title_blue_format)
        sheet.write(0, 12, 'FECNAC', title_blue_format)
        sheet.write(0, 13, 'NUMDOC', title_blue_format)
        sheet.write(0, 14, 'NUM_DIAS_LABORADOS', title_blue_format)
        sheet.write(0, 15, 'MONTO', title_blue_format)
        sheet.write(0, 16, 'TIPDOC', title_blue_format)
        sheet.write(0, 17, 'ESTCIV', title_blue_format)
        sheet.write(0, 18, 'NIVEL EDUCATIVO', title_blue_format)
        sheet.write(0, 19, 'SEGURO', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1
        ut = lines.unidad_territorial_id

        for comite in lines.unidad_territorial_id.ut_comites_gestion:
            if comite.state == 'activo':
                apoyo_adm = comite.afiliaciones_ids.filtered(
                    lambda x: x.cargo == 'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'
                )
                for actor in apoyo_adm:
                    just = self.env['justificacion.actor.lines'].search([
                        ('comite_gestion_id', '=', comite.id),
                        ('anio', '=', lines.anio.id),
                        ('mes', '=', lines.mes),
                        ('afiliacion_id', '=', actor.id)
                    ])
                    if just:
                        sheet.write(row, 0, anio, body_format)
                        sheet.write(row, 1, mes, body_format)
                        sheet.write(row, 2, ut.name, body_format)
                        sheet.write(row, 3, comite.departamento_id.name, body_format)
                        sheet.write(row, 4, comite.provincia_id.name, body_format)
                        sheet.write(row, 5, comite.distrito_id.name, body_format)
                        sheet.write(row, 6, ut.id, body_format)
                        sheet.write(row, 7, comite.name, body_format)
                        sheet.write(row, 8, actor.person_id.id, body_format)
                        sheet.write(row, 9, actor.person_id.firstname, body_format)
                        sheet.write(row, 10, actor.person_id.secondname, body_format)
                        sheet.write(row, 11, actor.person_id.name, body_format)
                        sheet.write(row, 12, actor.person_id.birthdate, body_format)
                        sheet.write(row, 13, actor.person_id.document_number, body_format)
                        sheet.write(row, 14, just.dias_trabajados, body_format)
                        sheet.write(row, 15, just.monto, body_format)
                        sheet.write(row, 16, actor.person_id.type_document, body_format)
                        sheet.write(row, 17, actor.person_id.marital_status, body_format)
                        sheet.write(row, 18, actor.person_id.nivel_educativo_id.name, body_format)
                        sheet.write(row, 19, actor.person_id.seguro_id.name, body_format)
                        row += 1


ApoyoAdministrativoReportXls('report.base_cuna.ApoyoAdministrativoReportXls.xlsx', 'dynamic.report.wizard')


class PlanillaVigenteReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Planilla Vigente')
        sheet.set_column('C:C', 25)
        sheet.write(0, 0, 'MES', title_blue_format)
        sheet.write(0, 1, u'AÑO', title_blue_format)
        sheet.write(0, 2, 'NOM UNIDAD_TERRITORIAL', title_blue_format)
        sheet.write(0, 3, 'NOMDEP', title_blue_format)
        sheet.write(0, 4, 'NOMPRO', title_blue_format)
        sheet.write(0, 5, 'NOMDIS', title_blue_format)
        sheet.write(0, 6, 'ID', title_blue_format)
        sheet.write(0, 7, 'NOMCOM', title_blue_format)
        sheet.write(0, 8, 'ID_PERSONA', title_blue_format)
        sheet.write(0, 9, 'APEPATERNO', title_blue_format)
        sheet.write(0, 10, 'APEMATERNO', title_blue_format)
        sheet.write(0, 11, 'NOMBRE', title_blue_format)
        sheet.write(0, 12, 'FECNAC', title_blue_format)
        sheet.write(0, 13, 'NUMDOC', title_blue_format)
        sheet.write(0, 14, 'SEXO', title_blue_format)
        sheet.write(0, 15, 'NUM_DIAS_LABORADOS', title_blue_format)
        sheet.write(0, 16, 'MONTO', title_blue_format)
        sheet.write(0, 17, 'TIPDOC', title_blue_format)
        sheet.write(0, 18, 'ESTCIV', title_blue_format)
        sheet.write(0, 19, 'NIVEL EDUCATIVO', title_blue_format)
        sheet.write(0, 20, 'SEGURO', title_blue_format)
        sheet.write(0, 21, 'RENIEC', title_blue_format)
        sheet.write(0, 22, 'FECHA REPORTE', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1
        ut = lines.unidad_territorial_id
        cargo = [
            'PRESIDENTE DE COMITE DE GESTION',
            'PRIMER VOCAL',
            'SEGUNDO VOCAL',
            'SECRETARIO DE COMITE DE GESTION',
            'TESORERO DE COMITE DE GESTION',
            'APOYO ADMINISTRATIVO DEL COMITE DE GESTION',
            'CONSEJO DE VIGILANCIA',
            'REPARTIDOR'
        ]

        for comite in lines.unidad_territorial_id.ut_comites_gestion:
            if comite.state == 'activo':
                personas = comite.afiliaciones_ids.filtered(lambda x: x.cargo not in cargo)
                for actor in personas:
                    just = self.env['justificacion.actor.lines'].search([
                        ('comite_gestion_id', '=', comite.id),
                        ('anio', '=', lines.anio.id),
                        ('mes', '=', lines.mes),
                        ('afiliacion_id', '=', actor.id)
                    ])
                    if just:
                        sheet.write(row, 0, mes, body_format)
                        sheet.write(row, 1, anio, body_format)
                        sheet.write(row, 2, ut.name, body_format)
                        sheet.write(row, 3, comite.departamento_id.name, body_format)
                        sheet.write(row, 4, comite.provincia_id.name, body_format)
                        sheet.write(row, 5, comite.distrito_id.name, body_format)
                        sheet.write(row, 6, ut.id, body_format)
                        sheet.write(row, 7, comite.name, body_format)
                        sheet.write(row, 8, actor.person_id.id, body_format)
                        sheet.write(row, 9, actor.person_id.firstname, body_format)
                        sheet.write(row, 10, actor.person_id.secondname, body_format)
                        sheet.write(row, 11, actor.person_id.name, body_format)
                        sheet.write(row, 12, actor.person_id.birthdate, body_format)
                        sheet.write(row, 13, actor.person_id.document_number, body_format)
                        sheet.write(row, 14, actor.person_id.gender.upper(), body_format)
                        sheet.write(row, 15, just.dias_trabajados, body_format)
                        sheet.write(row, 16, just.monto, body_format)
                        sheet.write(row, 17, actor.person_id.type_document.upper(), body_format)
                        sheet.write(row, 18, actor.person_id.marital_status, body_format)
                        sheet.write(row, 19, actor.person_id.nivel_educativo_id.name, body_format)
                        sheet.write(row, 20, actor.person_id.seguro_id.name, body_format)
                        sheet.write(row, 21, actor.person_id.validado_reniec, body_format)
                        sheet.write(row, 22, lines.fecha_creacion, body_format)
                        row += 1


PlanillaVigenteReportXls('report.base_cuna.PlanillaVigenteReportXls.xlsx', 'dynamic.report.wizard')


class JuntaDirectivaReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Junta Directiva')
        sheet.set_column('C:C', 25)
        sheet.write(0, 0, 'MES', title_blue_format)
        sheet.write(0, 1, u'AÑO', title_blue_format)
        sheet.write(0, 2, 'UT', title_blue_format)
        sheet.write(0, 3, 'DEPARTAMENTO', title_blue_format)
        sheet.write(0, 4, 'PROVINCIA', title_blue_format)
        sheet.write(0, 5, 'DISTRITO', title_blue_format)
        sheet.write(0, 6, 'CG ID', title_blue_format)
        sheet.write(0, 7, 'CG_NOMBRE', title_blue_format)
        sheet.write(0, 8, 'ID_PERSONA', title_blue_format)
        sheet.write(0, 9, 'APEPATERNO', title_blue_format)
        sheet.write(0, 10, 'APEMATERNO', title_blue_format)
        sheet.write(0, 11, 'NOMBRES', title_blue_format)
        sheet.write(0, 12, 'SEXO', title_blue_format)
        sheet.write(0, 13, 'FECHA NAC', title_blue_format)
        sheet.write(0, 14, 'TIPO_OCUPACION', title_blue_format)
        sheet.write(0, 15, 'SABEE_LEER_ESCRIBIR', title_blue_format)
        sheet.write(0, 16, 'ULTIMO_GRADO', title_blue_format)
        sheet.write(0, 17, 'NUMDOC', title_blue_format)
        sheet.write(0, 18, 'USU_TIPODOC', title_blue_format)
        sheet.write(0, 19, 'ESTADO CIVIL', title_blue_format)
        sheet.write(0, 20, 'CG_CARGO_PERSONA', title_blue_format)
        sheet.write(0, 21, 'FECHA INICIO', title_blue_format)
        sheet.write(0, 22, 'FECHA FIN', title_blue_format)
        sheet.write(0, 23, 'FECHA REPORTE', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1
        ut = lines.unidad_territorial_id
        cargo = [
            'PRESIDENTE DE COMITE DE GESTION',
            'PRIMER VOCAL',
            'SEGUNDO VOCAL',
            'SECRETARIO DE COMITE DE GESTION',
            'TESORERO DE COMITE DE GESTION',
            'CONSEJO DE VIGILANCIA',
        ]

        for comite in lines.unidad_territorial_id.ut_comites_gestion:
            if comite.state == 'activo':
                personas = comite.afiliaciones_ids.filtered(lambda x: x.cargo in cargo)
                for actor in personas:
                    sheet.write(row, 0, mes, body_format)
                    sheet.write(row, 1, anio, body_format)
                    sheet.write(row, 2, ut.name, body_format)
                    sheet.write(row, 3, comite.departamento_id.name, body_format)
                    sheet.write(row, 4, comite.provincia_id.name, body_format)
                    sheet.write(row, 5, comite.distrito_id.name, body_format)
                    sheet.write(row, 6, comite.id, body_format)
                    sheet.write(row, 7, comite.name, body_format)
                    sheet.write(row, 8, actor.person_id.id, body_format)
                    sheet.write(row, 9, actor.person_id.firstname, body_format)
                    sheet.write(row, 10, actor.person_id.secondname, body_format)
                    sheet.write(row, 11, actor.person_id.name, body_format)
                    sheet.write(row, 12, actor.person_id.gender.upper(), body_format)
                    sheet.write(row, 13, actor.person_id.birthdate, body_format)
                    sheet.write(row, 14, actor.person_id.ocupacion_id.name, body_format)
                    sheet.write(row, 15, 'SI' if actor.person_id.sabe_leer_y_escribir else 'NO', body_format)
                    sheet.write(row, 16, actor.person_id.ultimo_grado_apr_id.name, body_format)
                    sheet.write(row, 17, actor.person_id.document_number, body_format)
                    sheet.write(row, 18, actor.person_id.type_document.upper(), body_format)
                    sheet.write(row, 19, actor.person_id.marital_status, body_format)
                    sheet.write(row, 20, actor.cargo, body_format)
                    sheet.write(row, 21, actor.fecha_ingreso, body_format)
                    sheet.write(row, 22, actor.fecha_retiro or '', body_format)
                    sheet.write(row, 23, lines.fecha_creacion, body_format)
                    row += 1


JuntaDirectivaReportXls('report.base_cuna.JuntaDirectivaReportXls.xlsx', 'dynamic.report.wizard')


class ListaLocalesReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Lista Locales')
        sheet.set_column('C:C', 25)
        sheet.set_column(24, 40)
        sheet.write(0, 0, 'MES', title_blue_format)
        sheet.write(0, 1, u'AÑO', title_blue_format)
        sheet.write(0, 2, 'UT', title_blue_format)
        sheet.write(0, 3, 'DEPCG', title_blue_format)
        sheet.write(0, 4, 'PROCG', title_blue_format)
        sheet.write(0, 5, 'DISCG', title_blue_format)
        sheet.write(0, 6, 'CG CODIGO', title_blue_format)
        sheet.write(0, 7, 'CG NOMBRE', title_blue_format)
        sheet.write(0, 8, 'DIRCOM', title_blue_format)
        sheet.write(0, 9, 'REFDIR', title_blue_format)
        sheet.write(0, 10, 'UBIGEO_LOCAL', title_blue_format)
        sheet.write(0, 11, 'CODCENPOB', title_blue_format)
        sheet.write(0, 12, 'COD_CCPP_LOCAL', title_blue_format)
        sheet.write(0, 13, 'NOM_CCPP_LOCAL', title_blue_format)
        sheet.write(0, 14, 'DEP_LOCAL', title_blue_format)
        sheet.write(0, 15, 'PRO_LOCAL', title_blue_format)
        sheet.write(0, 16, 'DIS_LOCAL', title_blue_format)
        sheet.write(0, 17, 'CODIGOLOCAL', title_blue_format)
        sheet.write(0, 18, 'NOMBRELOCAL', title_blue_format)
        sheet.write(0, 19, 'TIPLOC', title_blue_format)
        sheet.write(0, 20, 'DIRLOC', title_blue_format)
        sheet.write(0, 21, 'REFDIR_1', title_blue_format)
        sheet.write(0, 22, 'LATITUD_INICIO', title_blue_format)
        sheet.write(0, 23, 'LONGITUD_INICIO', title_blue_format)
        sheet.write(0, 24, 'URL', title_blue_format)
        sheet.write(0, 25, 'CA_USU', title_blue_format)
        sheet.write(0, 26, 'FECHA_REGISTRO', title_blue_format)
        sheet.write(0, 27, 'TIESERALI', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1
        ut = lines.unidad_territorial_id

        for comite in lines.unidad_territorial_id.ut_comites_gestion:
            if comite.state == 'activo':
                locales = comite.local_ids.filtered(lambda x: x.state == 'activacion')
                for local in locales:
                    url = 'http://maps.google.com/maps?q=%f,%f&ll=%f,%f&z=12' % (
                        float(local.lat), float(local.lng), float(local.lat), float(local.lng)
                    )
                    cant_usuarios = 0
                    for rec in local.modulos_ids:
                        cant_usuarios += len(rec.ninios_ids)
                    sheet.write(row, 0, mes, body_format)
                    sheet.write(row, 1, anio, body_format)
                    sheet.write(row, 2, ut.name, body_format)
                    sheet.write(row, 3, comite.departamento_id.name, body_format)
                    sheet.write(row, 4, comite.provincia_id.name, body_format)
                    sheet.write(row, 5, comite.distrito_id.name, body_format)
                    sheet.write(row, 6, comite.id, body_format)
                    sheet.write(row, 7, comite.name, body_format)
                    sheet.write(row, 8, comite.direccion or '', body_format)
                    sheet.write(row, 9, comite.referencia or '', body_format)
                    sheet.write(row, 10, comite.codigo, body_format)
                    sheet.write(row, 11, comite.centro_poblado_id.code or '', body_format)
                    sheet.write(row, 12, local.centro_poblado_id.code or '', body_format)
                    sheet.write(row, 13, local.centro_poblado_id.name or '', body_format)
                    sheet.write(row, 14, local.departamento_id.name, body_format)
                    sheet.write(row, 15, local.provincia_id.name, body_format)
                    sheet.write(row, 16, local.distrito_id.name, body_format)
                    sheet.write(row, 17, local.code, body_format)
                    sheet.write(row, 18, local.name, body_format)
                    sheet.write(row, 19, local.tipo_local, body_format)
                    sheet.write(row, 20, local.direccion or '', body_format)
                    sheet.write(row, 21, local.referencia or '', body_format)
                    sheet.write(row, 22, local.lat, body_format)
                    sheet.write(row, 23, local.lng, body_format)
                    sheet.write(row, 24, url, body_format)
                    sheet.write(row, 25, cant_usuarios, body_format)
                    sheet.write(row, 26, lines.fecha_creacion, body_format)
                    sheet.write(row, 27, len(local.sa_ids), body_format)
                    row += 1


ListaLocalesReportXls('report.base_cuna.ListaLocalesReportXls.xlsx', 'dynamic.report.wizard')


class LocalesInfraestructuraReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Locales-Infraestructura')
        sheet.set_column('C:C', 25)
        sheet.set_column('E:E', 30)
        sheet.set_column('O:Q', 20)
        sheet.set_column('AC:AI', 30)

        sheet.write(0, 0, 'UT', title_blue_format)
        sheet.write(0, 1, 'IDTCOM', title_blue_format)
        sheet.write(0, 2, 'CG NOMBRE', title_blue_format)
        sheet.write(0, 3, 'ID LOCAL', title_blue_format)
        sheet.write(0, 4, 'NOMLOC', title_blue_format)
        sheet.write(0, 5, 'ESTADO', title_blue_format)
        sheet.write(0, 6, 'CG CODIGO', title_blue_format)
        sheet.write(0, 7, 'DIRLOC', title_blue_format)
        sheet.write(0, 8, 'NUMTEL', title_blue_format)
        sheet.write(0, 9, 'PROAPEPAT', title_blue_format)
        sheet.write(0, 10, 'PROAPEMAT', title_blue_format)
        sheet.write(0, 11, 'PRONOMBRE', title_blue_format)
        sheet.write(0, 12, 'PRODNI', title_blue_format)
        sheet.write(0, 13, 'TIPO_LOCAL', title_blue_format)
        sheet.write(0, 14, 'TIENE_SERV_ALI', title_blue_format)
        sheet.write(0, 15, 'AREA_CONSTRUIDA', title_blue_format)
        sheet.write(0, 16, 'AREA_TOTAL_TERRENO', title_blue_format)
        sheet.write(0, 17, 'IDTCENPOB', title_blue_format)
        sheet.write(0, 18, 'NOMCENPOB', title_blue_format)
        sheet.write(0, 19, 'NUMERO_SALAS', title_blue_format)
        sheet.write(0, 20, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 21, 'IDTUBI', title_blue_format)
        sheet.write(0, 22, 'NOMDIS', title_blue_format)
        sheet.write(0, 23, 'NOMDEP', title_blue_format)
        sheet.write(0, 24, 'NOMPRO', title_blue_format)
        sheet.write(0, 25, 'TIPO_HOGAR', title_blue_format)
        sheet.write(0, 26, 'MOD_INTERVENCION', title_blue_format)
        sheet.write(0, 27, 'AREA_CEDIDA', title_blue_format)
        sheet.write(0, 28, u'AÑO_CONSRUCCION', title_blue_format)
        sheet.write(0, 29, 'LINDERO_MEDIDA_POR_FRENTE', title_blue_format)
        sheet.write(0, 30, 'LINDERO_MEDIDA_POR_DERECHA', title_blue_format)
        sheet.write(0, 31, 'LINDERO_MEDIDA_POR_IZQUIERDA', title_blue_format)
        sheet.write(0, 32, 'LINDERO_MEDIDA_POR_FONDO', title_blue_format)
        sheet.write(0, 33, 'POSIB_AMP_AREA_CONSTR', title_blue_format)
        sheet.write(0, 34, 'CUENTA_CONVENIO_ACTA', title_blue_format)
        sheet.write(0, 35, 'FECINICONV', title_blue_format)
        sheet.write(0, 36, 'FECFINCONV', title_blue_format)

        row = 1
        ut = lines.unidad_territorial_id

        for comite in ut.ut_comites_gestion:
            if comite.state == 'activo':
                for local in comite.local_ids:
                    sheet.write(row, 0, ut.name, body_format)
                    sheet.write(row, 1, comite.id, body_format)
                    sheet.write(row, 2, comite.name, body_format)
                    sheet.write(row, 3, local.id, body_format)
                    sheet.write(row, 4, local.name, body_format)
                    sheet.write(row, 5, local.state.upper(), body_format)
                    sheet.write(row, 6, comite.codigo, body_format)
                    sheet.write(row, 7, local.direccion or '', body_format)
                    sheet.write(row, 8, local.nro_telefono or '', body_format)
                    sheet.write(row, 9, local.propietario_id.firstname or '', body_format)
                    sheet.write(row, 10, local.propietario_id.secondname or '', body_format)
                    sheet.write(row, 11, local.propietario_id.name or '', body_format)
                    sheet.write(row, 12, local.propietario_id.document_number, body_format)
                    sheet.write(row, 13, local.tipo_local.upper(), body_format)
                    sheet.write(row, 14, 'SI' if local.sa_ids else 'NO', body_format)
                    sheet.write(row, 15, local.area_construida, body_format)
                    sheet.write(row, 16, local.area_total_terreno, body_format)
                    sheet.write(row, 17, local.centro_poblado_id.id or '', body_format)
                    sheet.write(row, 18, local.centro_poblado_id.name or '', body_format)
                    sheet.write(row, 19, len(local.salas_ids), body_format)
                    sheet.write(row, 20, local.centro_poblado_id.code, body_format)
                    sheet.write(row, 21, local.distrito_id.id, body_format)
                    sheet.write(row, 22, local.distrito_id.name, body_format)
                    sheet.write(row, 23, local.departamento_id.name, body_format)
                    sheet.write(row, 24, local.provincia_id.name, body_format)
                    sheet.write(row, 25, local.procedencia.upper(), body_format)
                    sheet.write(row, 26, local.modalidad_intervencion.upper(), body_format)
                    sheet.write(row, 27, local.area_cedida, body_format)
                    sheet.write(row, 28, local.anio_construccion, body_format)
                    sheet.write(row, 29, local.lindero_frente, body_format)
                    sheet.write(row, 30, local.lindero_derecha, body_format)
                    sheet.write(row, 31, local.lindero_izquierda, body_format)
                    sheet.write(row, 32, local.lindero_fondo, body_format)
                    sheet.write(row, 33, local.posibilid_ampliacion, body_format)
                    convenio = local.convenios.filtered(lambda x: x.state == 'activo')
                    sheet.write(row, 34, 'SI' if convenio else 'NO', body_format)
                    sheet.write(row, 35, convenio.fecha_inicio if convenio else '', body_format)
                    sheet.write(row, 36, convenio.fecha_fin if convenio else '', body_format)
                    row += 1


LocalesInfraestructuraReportXls('report.base_cuna.LocalesInfraestructuraReportXls.xlsx', 'dynamic.report.wizard')


class RequerimientosUTReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Requerimientos x UT')
        sheet.set_column('C:C', 25)

        sheet.write(0, 0, 'ANIO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'UT', title_blue_format)
        sheet.write(0, 3, 'CAN CG', title_blue_format)
        sheet.write(0, 4, 'CG SIN GEN', title_blue_format)
        sheet.write(0, 5, 'CG GENE', title_blue_format)
        sheet.write(0, 6, 'CON SCAN', title_blue_format)
        sheet.write(0, 7, 'SIN SCAN', title_blue_format)

        row = 1
        uts = self.env['hr.department'].search([('is_office', '=', True)])
        mes = lines.mes
        anio = lines.anio

        for ut in uts:
            cgs = ut.ut_comites_gestion.filtered(lambda x: x.state != 'cerrado')
            cg_gene = cg_sin_gene = con_scan = sin_scan = 0
            for cg in cgs:
                reqs = cg.requerimientos_ids.filtered(lambda x: x.anio == anio and x.mes == mes and x.servicio == 'scd')
                if reqs:
                    for req in reqs:
                        if req.estado == 'borrador':
                            cg_sin_gene += 1
                        elif req.estado == 'desembolso':
                            cg_gene += 1
                            sin_scan += 1
                        else:
                            cg_gene += 1
                            con_scan += 1
                else:
                    cg_sin_gene += 1

            sheet.write(row, 0, anio.name, body_format)
            sheet.write(row, 1, mes, body_format)
            sheet.write(row, 2, ut.name, body_format)
            sheet.write(row, 3, len(cgs), body_format)
            sheet.write(row, 4, cg_sin_gene, body_format)
            sheet.write(row, 5, cg_gene, body_format)
            sheet.write(row, 6, con_scan, body_format)
            sheet.write(row, 7, sin_scan, body_format)
            row += 1


RequerimientosUTReportXls('report.base_cuna.RequerimientosUTReportXls.xlsx', 'dynamic.report.wizard')


class Ficha9ReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        total_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 7,
            'bg_color': '#53a8ba',
            'border': True,
        })

        sheet = workbook.add_worksheet('Resumen')

        sheet.set_column('B:L', 35)
        sheet.write(0, 0, 'N°', title_blue_format)
        sheet.write(0, 1, 'UT', title_blue_format)
        sheet.write(0, 2, 'Alerta N° Niños registrados en más de 1 ficha 9', title_blue_format)
        sheet.write(0, 3, 'Alerta N° niños registrados por ficha que superan 8', title_blue_format)
        sheet.write(0, 4, 'N° de Niños menores de 6 meses', title_blue_format)
        sheet.write(0, 5, 'Niños mayores a 37 meses', title_blue_format)
        sheet.write(0, 6, 'N° de Niño con  0 días de asistencia y más de 10 días enfermo', title_blue_format)
        sheet.write(0, 7, 'N° de Niños con  0 días de asistencia y 0 días enfermo', title_blue_format)
        sheet.write(0, 8, "='data'!BL2", title_blue_format)
        sheet.write(0, 9, "='data'!BM2", title_blue_format)
        sheet.write(0, 10, 'Registros con al menos una observación', title_blue_format)
        sheet.write(0, 11, 'Cantidad de Observaciones', title_blue_format)

        row = 1
        uts = self.env['hr.department'].search([('is_office', '=', True)])
        mes = lines.mes
        anio = lines.anio

        for ut in uts:
            sheet.write(row, 0, row, body_format)
            sheet.write(row, 1, ut.name, body_format)
            sheet.write(row, 2, "=SUMIFS('data'!BF:BF,'data'!B:B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 3, "=SUMIFS('data'!BG:BG,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 4, "=SUMIFS('data'!BH:BH,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 5, "=SUMIFS('data'!BI:BI,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 6, "=SUMIFS('data'!BJ:BJ,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 7, "=SUMIFS('data'!BK:BK,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 8, "=SUMIFS('data'!BL:Bl,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 9, "=SUMIFS('data'!BM:BM,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 10, "=SUMIFS('data'!BN:BN,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 11, "=SUMIFS('data'!BO:BO,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            row += 1
        # Pagina 2
        sheet2 = workbook.add_worksheet('data')
        sheet2.set_column('BB:BQ', 35)
        sheet2.set_column('P:P', 30)
        sheet2.write(1, 0, 'FICHA ID', title_blue_format)
        sheet2.write(1, 1, 'UT', title_blue_format)
        sheet2.write(1, 2, 'NOMDEP', title_blue_format)
        sheet2.write(1, 3, 'NOMPRO', title_blue_format)
        sheet2.write(1, 4, 'NOMDIS', title_blue_format)
        sheet2.write(1, 5, 'IDCOM', title_blue_format)
        sheet2.write(1, 6, 'NOMCOM', title_blue_format)
        sheet2.write(1, 7, 'AT_CODIGO', title_blue_format)
        sheet2.write(1, 8, 'AT_PATERNO', title_blue_format)
        sheet2.write(1, 9, 'AT_MATERNO', title_blue_format)
        sheet2.write(1, 10, 'AT_NOMBRES', title_blue_format)
        sheet2.write(1, 11, 'NOMDEPLOC', title_blue_format)
        sheet2.write(1, 12, 'NOMPROLOC', title_blue_format)
        sheet2.write(1, 13, 'NOMDISLOC', title_blue_format)
        sheet2.write(1, 14, 'LOCAL ID', title_blue_format)
        sheet2.write(1, 15, 'NOMLOCAL', title_blue_format)
        sheet2.write(1, 16, 'TIPOLOCAL', title_blue_format)
        sheet2.write(1, 17, 'ID SALA', title_blue_format)
        sheet2.write(1, 18, 'NOMSALA', title_blue_format)
        sheet2.write(1, 19, 'ASISTENCIA_CONTROL_ID', title_blue_format)
        sheet2.write(1, 20, 'CUIDADOR_CODIGO', title_blue_format)
        sheet2.write(1, 21, 'CUIDADOR_PATERNO', title_blue_format)
        sheet2.write(1, 22, 'CUIDADOR_MATERNO', title_blue_format)
        sheet2.write(1, 23, 'CUIDADOR_NOMBRES', title_blue_format)
        sheet2.write(1, 24, 'CUIDADOR_FECNAC', title_blue_format)
        sheet2.write(1, 25, 'CUIDADOR_NUMDOC', title_blue_format)
        sheet2.write(1, 26, 'APEPATUSU', title_blue_format)
        sheet2.write(1, 27, 'APEMATUSU', title_blue_format)
        sheet2.write(1, 28, 'NOMUSU', title_blue_format)
        sheet2.write(1, 29, 'SEXO', title_blue_format)
        sheet2.write(1, 30, 'FECNACUSU', title_blue_format)
        sheet2.write(1, 31, 'NUMDOCUSU', title_blue_format)
        sheet2.write(1, 32, 'USU_TIPDOC', title_blue_format)
        sheet2.write(1, 33, 'N_FALTA_ENFERMO_EDA', title_blue_format)
        sheet2.write(1, 34, 'N_FALTA_ENFERMO_IRA', title_blue_format)
        sheet2.write(1, 35, 'N_FALTA_ENFERMO_OTRO', title_blue_format)
        sheet2.write(1, 36, 'N_TOTAL_FALTA', title_blue_format)
        sheet2.write(1, 37, 'N_TOTAL_ENFERMO', title_blue_format)
        sheet2.write(1, 38, 'N TOTAL ASISTIO', title_blue_format)
        sheet2.write(1, 39, 'CONSUME_MMN', title_blue_format)
        sheet2.write(1, 40, 'TRAECRED', title_blue_format)
        sheet2.write(1, 41, 'TRAEMMN', title_blue_format)
        sheet2.write(1, 42, 'SESSOC', title_blue_format)
        sheet2.write(1, 43, 'PRALAVMAN', title_blue_format)
        sheet2.write(1, 44, 'PRAHIGBUC', title_blue_format)
        sheet2.write(1, 45, 'PRACUENTO', title_blue_format)
        sheet2.write(1, 46, 'PRAJUEGOS', title_blue_format)
        sheet2.write(1, 47, 'USUCRE', title_blue_format)
        sheet2.write(1, 48, 'USUMOD', title_blue_format)
        sheet2.write(1, 49, 'Mes', title_blue_format)
        sheet2.write(1, 50, u'Año', title_blue_format)
        sheet2.write(1, 51, 'Reporte', title_blue_format)
        sheet2.write(1, 52, 'EDAD', title_blue_format)
        sheet2.write(1, 53, 'Asistencia + ENFERMO + FALTA  por niño', title_blue_format)
        sheet2.write(1, 54, '(Asistencia + ENFERMO  por niño) por ficha', title_blue_format)
        sheet2.write(1, 55, 'N° Niños por ficha', title_blue_format)
        sheet2.write(1, 56, 'CONTAR USUARIOS', title_blue_format)
        sheet2.write(1, 57, 'Alerta N° Niños registrados en más de 1 ficha 9', title_blue_format)
        sheet2.write(1, 58, 'Alerta N° niños registrados por ficha que superan 8', title_blue_format)
        sheet2.write(1, 59, 'N° de Niños menores de 6 meses', title_blue_format)
        sheet2.write(1, 60, 'Niños mayores a 37 meses', title_blue_format)
        sheet2.write(1, 61, 'N° de Niño con  0 días de asistencia y más de 10 días enfermo', title_blue_format)
        sheet2.write(1, 62, 'N° de Niños con  0 días de asistencia y 0 días enfermo', title_blue_format)
        sheet2.write(1, 63,
                     '="Suma de días de (Asistencia + Enfermo+Falta) que supera los "&BD1&" días utiles del mes"',
                     title_blue_format
                     )
        sheet2.write(1, 64,
                     '="Ficha con total de niños multiplicado por el número de días que superarn "&(8*BD1)&" '
                     'atenciones(8 niños * "&$BD$1&" días utiles)"',
                     title_blue_format
                     )
        sheet2.write(1, 65, 'Registros con al menos una observación', title_blue_format)
        sheet2.write(1, 66, 'Cantidad de Observaciones', title_blue_format)
        sheet2.write(1, 67, 'FECHA_INGRESO', title_blue_format)

        sheet2.write(0, 54, u'DÍAS UTILES', title_blue_format)
        row1 = 2
        for cg in lines.unidad_territorial_id.ut_comites_gestion:
            asis_ids = self.env['asistencia.control'].search([
                ('comite_gestion_id', '=', cg.id),
                ('mes', '=', mes),
                ('anio', '=', anio.id),
            ])
            for asis in asis_ids:
                sheet2.write(0, 55, asis.dias_utiles, body_format)

                total_54 = sum(ninio.asiste + ninio.enfermedad for ninio in asis.asistencia_menor_ids)
                total_55 = len(asis.asistencia_menor_ids)
                for ninio in asis.asistencia_menor_ids:
                    asist_menor = self.env['asistencia.menor'].search([
                        ('mes', '=', mes),
                        ('anio', '=', anio.id),
                        ('ninio_afiliacion_id', '=', ninio.ninio_afiliacion_id.id)
                    ])
                    ninio_id = ninio.ninio_afiliacion_id.ninios_id.integrante_id
                    sheet2.write(row1, 0, asis.id, body_format)
                    sheet2.write(row1, 1, cg.unidad_territorial_id.name, body_format)
                    sheet2.write(row1, 2, cg.unidad_territorial_id.departamento_id.name, body_format)
                    sheet2.write(row1, 3, cg.unidad_territorial_id.provincia_id.name, body_format)
                    sheet2.write(row1, 4, cg.unidad_territorial_id.distrito_id.name, body_format)
                    sheet2.write(row1, 5, cg.id, body_format)
                    sheet2.write(row1, 6, cg.name, body_format)
                    sheet2.write(row1, 7, asis.acomp_tecnico_id.id, body_format)
                    sheet2.write(row1, 8, asis.acomp_tecnico_id.person_id.firstname, body_format)
                    sheet2.write(row1, 9, asis.acomp_tecnico_id.person_id.secondname, body_format)
                    sheet2.write(row1, 10, asis.acomp_tecnico_id.person_id.name, body_format)
                    sheet2.write(row1, 11, asis.local_id.departamento_id.name, body_format)
                    sheet2.write(row1, 12, asis.local_id.provincia_id.name, body_format)
                    sheet2.write(row1, 13, asis.local_id.distrito_id.name, body_format)
                    sheet2.write(row1, 14, asis.local_id.id, body_format)
                    sheet2.write(row1, 15, asis.local_id.name, body_format)
                    sheet2.write(row1, 16, asis.local_id.tipo_local.upper(), body_format)
                    sheet2.write(row1, 17, asis.sala_id.id, body_format)
                    sheet2.write(row1, 18, asis.sala_id.name, body_format)
                    sheet2.write(row1, 19, asis.id, body_format)
                    sheet2.write(row1, 20, asis.mad_cuidadora_id.id, body_format)
                    sheet2.write(row1, 21, asis.mad_cuidadora_id.person_id.firstname, body_format)
                    sheet2.write(row1, 22, asis.mad_cuidadora_id.person_id.secondname, body_format)
                    sheet2.write(row1, 23, asis.mad_cuidadora_id.person_id.name, body_format)
                    sheet2.write(row1, 24, asis.mad_cuidadora_id.person_id.birthdate, body_format)
                    sheet2.write(row1, 25, asis.mad_cuidadora_id.person_id.document_number, body_format)
                    sheet2.write(row1, 26, ninio_id.firstname, body_format)
                    sheet2.write(row1, 27, ninio_id.secondname, body_format)
                    sheet2.write(row1, 28, ninio_id.name, body_format)
                    sheet2.write(row1, 29, ninio_id.gender, body_format)
                    sheet2.write(row1, 30, ninio_id.birthdate, body_format)
                    sheet2.write(row1, 31, ninio_id.document_number, body_format)
                    sheet2.write(row1, 32, ninio_id.type_document.upper(), body_format)
                    sheet2.write(row1, 33, ninio.totfaleda, body_format)
                    sheet2.write(row1, 34, ninio.totfalira, body_format)
                    sheet2.write(row1, 35, ninio.totfalotr, body_format)
                    sheet2.write(row1, 36, ninio.faltas, body_format)
                    sheet2.write(row1, 37, ninio.enfermedad, body_format)
                    sheet2.write(row1, 38, ninio.asiste, body_format)
                    sheet2.write(row1, 39, ninio.totconmn, body_format)
                    sheet2.write(row1, 40, ninio.cred, body_format)
                    sheet2.write(row1, 41, ninio.mn, body_format)
                    sheet2.write(row1, 42, 1 if ninio.familia_participa else '', body_format)
                    sheet2.write(row1, 43, 1 if ninio.lavado_manos else '', body_format)
                    sheet2.write(row1, 44, 1 if ninio.higiene_bucal else '', body_format)
                    sheet2.write(row1, 45, 1 if ninio.cuenta_cuento else '', body_format)
                    sheet2.write(row1, 46, 1 if ninio.juega_ninios else '', body_format)
                    sheet2.write(row1, 47, asis.usuario_creador_id.name or '', body_format)
                    sheet2.write(row1, 48, asis.usuario_mod_id.name or '', body_format)
                    sheet2.write(row1, 49, mes, body_format)
                    sheet2.write(row1, 50, anio.name, body_format)
                    sheet2.write(row1, 51, asis.fecha_creacion, body_format)
                    sheet2.write(row1, 52, ninio_id.months, body_format)
                    sheet2.write(row1, 53, ninio.asiste + ninio.faltas + ninio.enfermedad, body_format)
                    sheet2.write(row1, 54, total_54, body_format)
                    sheet2.write(row1, 55, total_55, body_format)
                    sheet2.write(row1, 56, len(asist_menor), body_format)
                    sheet2.write(row1, 57, '=--(BE' + str(row1 + 1) + '>1)', body_format)
                    sheet2.write(row1, 58, '=--(BD' + str(row1 + 1) + '>8)', body_format)
                    sheet2.write(row1, 59, '=--(BA' + str(row1 + 1) + '<6)', body_format)
                    sheet2.write(row1, 60, '=--(BA' + str(row1 + 1) + '>=37)', body_format)
                    sheet2.write(row1, 61, '=--(AM' + str(row1 + 1) + '=0 & AL' + str(row1 + 1) + '>10)', body_format)
                    sheet2.write(row1, 62, '=--(AL' + str(row1 + 1) + '=0 & AM' + str(row1 + 1) + '=0)', body_format)
                    sheet2.write(row1, 63, '=--(BB' + str(row1 + 1) + '>$BD$1)', body_format)
                    sheet2.write(row1, 64, '=--(BC' + str(row1 + 1) + '>(8*$BD$1))', body_format)
                    sheet2.write(row1, 65, '=--(SUM(BH' + str(row1 + 1) + ':BM' + str(row1 + 1) + ')>0)', body_format)
                    sheet2.write(row1, 66, '=SUM(BH' + str(row1 + 1) + ':BM' + str(row1 + 1) + ')', body_format)
                    sheet2.write(row1, 67, lines.fecha_creacion, body_format)
                    row1 += 1
        sheet2.write(row1, 56, 'TOTAL', title_blue_format)
        sheet2.write(row1, 57, '=SUM(BF3:BF' + str(row1), total_blue_format)
        sheet2.write(row1, 58, '=SUM(BG3:BG' + str(row1), total_blue_format)
        sheet2.write(row1, 59, '=SUM(BH3:BH' + str(row1), total_blue_format)
        sheet2.write(row1, 60, '=SUM(BI3:BI' + str(row1), total_blue_format)
        sheet2.write(row1, 61, '=SUM(BJ3:BJ' + str(row1), total_blue_format)
        sheet2.write(row1, 62, '=SUM(BK3:BK' + str(row1), total_blue_format)
        sheet2.write(row1, 63, '=SUM(BL3:BL' + str(row1), total_blue_format)
        sheet2.write(row1, 64, '=SUM(BM3:BM' + str(row1), total_blue_format)
        sheet2.write(row1, 65, '=SUM(BN3:BN' + str(row1), total_blue_format)
        sheet2.write(row1, 66, '=SUM(BO3:BO' + str(row1), total_blue_format)


Ficha9ReportXls('report.base_cuna.Ficha9ReportXls.xlsx', 'dynamic.report.wizard')


class DatasetSCDXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Dataset SCD')
        sheet.set_column('A:A', 35)
        sheet.set_column('F:F', 35)
        sheet.set_column('M:O', 35)

        sheet.write(0, 0, 'UT', title_blue_format)
        sheet.write(0, 1, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 2, 'NOMDEP', title_blue_format)
        sheet.write(0, 3, 'NOMPRO', title_blue_format)
        sheet.write(0, 4, 'NOMDIS', title_blue_format)
        sheet.write(0, 5, 'NOMCOM', title_blue_format)
        sheet.write(0, 6, 'UBIGEO LOCAL', title_blue_format)
        sheet.write(0, 7, 'NOMDEPLOC', title_blue_format)
        sheet.write(0, 8, "NOMPROLOC", title_blue_format)
        sheet.write(0, 9, "NOMDISLOC", title_blue_format)
        sheet.write(0, 10, 'CCPP_LOCAL', title_blue_format)
        sheet.write(0, 11, 'NOM_CCPP', title_blue_format)
        sheet.write(0, 12, 'NOMLOC', title_blue_format)
        sheet.write(0, 13, 'APEPATUSU', title_blue_format)
        sheet.write(0, 14, 'NOMUSU', title_blue_format)
        sheet.write(0, 15, 'SEXO', title_blue_format)
        sheet.write(0, 16, 'FECNACUSU', title_blue_format)
        sheet.write(0, 17, 'EDAD_USU', title_blue_format)

        row = 1
        ut = lines.unidad_territorial_id

        for cg in ut.ut_comites_gestion:
            for local in cg.local_ids:
                for modulo in local.modulos_ids:
                    for ninio in modulo.ninios_ids:
                        sheet.write(row, 0, ut.name, body_format)
                        sheet.write(row, 1, ut.distrito_id.code, body_format)
                        sheet.write(row, 2, ut.departamento_id.name, body_format)
                        sheet.write(row, 3, ut.provincia_id.name, body_format)
                        sheet.write(row, 4, ut.distrito_id.name, body_format)
                        sheet.write(row, 5, cg.name, body_format)
                        sheet.write(row, 6, local.distrito_id.code, body_format)
                        sheet.write(row, 7, local.departamento_id.name, body_format)
                        sheet.write(row, 8, local.provincia_id.name, body_format)
                        sheet.write(row, 9, local.distrito_id.name, body_format)
                        sheet.write(row, 10, local.centro_poblado_id.code, body_format)
                        sheet.write(row, 11, local.centro_poblado_id.name, body_format)
                        sheet.write(row, 12, local.name, body_format)
                        sheet.write(row, 13, ninio.ninios_id.integrante_id.firstname, body_format)
                        sheet.write(row, 14, ninio.ninios_id.integrante_id.name, body_format)
                        sheet.write(row, 15, ninio.ninios_id.integrante_id.gender.upper(), body_format)
                        sheet.write(row, 16, ninio.ninios_id.integrante_id.birthdate, body_format)
                        sheet.write(row, 17, ninio.ninios_id.integrante_id.months, body_format)
                        row += 1


DatasetSCDXls('report.base_cuna.DatasetSCDXls.xlsx', 'dynamic.report.wizard')


class DatasetSAFXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Dataset SCD')
        sheet.set_column('A:A', 35)
        sheet.set_column('F:F', 35)
        sheet.set_column('I:L', 35)
        sheet.write(0, 0, 'NOMUT', title_blue_format)
        sheet.write(0, 1, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 2, 'NOMDEP', title_blue_format)
        sheet.write(0, 3, 'NOMPRO', title_blue_format)
        sheet.write(0, 4, 'NOMDIS', title_blue_format)
        sheet.write(0, 5, 'NOMCOM', title_blue_format)
        sheet.write(0, 6, 'CODCENPOB', title_blue_format)
        sheet.write(0, 7, 'NOMCENPOB', title_blue_format)
        sheet.write(0, 8, 'NOMBRE', title_blue_format)
        sheet.write(0, 9, 'APELLIDO PATERNO', title_blue_format)
        sheet.write(0, 10, 'SEXO', title_blue_format)
        sheet.write(0, 11, 'TIPO BENEFICIARIO', title_blue_format)
        sheet.write(0, 12, 'FECNACUSU', title_blue_format)
        sheet.write(0, 13, 'EDAD_USU', title_blue_format)
        row = 1

        ut = lines.unidad_territorial_id
        familias = self.env['unidad.familiar'].search([
            ('services', '=', 'saf'),
            ('state', '=', 'activo'),
            ('unidad_territorial_id', '=', ut.id),
        ])
        for fam in familias:
            integrantes = fam.integrantes_ids.filtered(
                lambda x: x.integrante_id.madre_gestante or x.parentesco_id.name == u'Niño(a)'
            )
            for persona in integrantes:
                sheet.write(row, 0, ut.name, body_format)
                sheet.write(row, 1, ut.distrito_id.code, body_format)
                sheet.write(row, 2, ut.departamento_id.name, body_format)
                sheet.write(row, 3, ut.provincia_id.name, body_format)
                sheet.write(row, 4, ut.distrito_id.name, body_format)
                sheet.write(row, 5, fam.comite_gestion_id.name, body_format)
                sheet.write(row, 6, fam.centro_poblado_id.code, body_format)
                sheet.write(row, 7, fam.centro_poblado_id.name, body_format)
                sheet.write(row, 8, persona.integrante_id.name, body_format)
                sheet.write(row, 9, persona.integrante_id.firstname, body_format)
                sheet.write(row, 10, persona.integrante_id.gender.upper(), body_format)
                tipo = u'Niño(a)' if persona.parentesco_id.name == u'Niño(a)' else 'Madre Gestante'
                sheet.write(row, 11, tipo, body_format)
                sheet.write(row, 12, persona.integrante_id.birthdate, body_format)
                sheet.write(row, 13, persona.integrante_id.months, body_format)
                row += 1


DatasetSAFXls('report.base_cuna.DatasetSAFXls.xlsx', 'dynamic.report.wizard')


class ReporteFamiliaXls(ReportXlsx):

    def buscar_visitas_familia(self, integrantes, mes, anio):
        rec = integrantes.filtered(lambda x: x.parentesco_id.name == u'Niño(a)')
        if len(rec) > 0:
            for r in rec:
                ficha_rec = self.env['ficha.reconocimiento'].search([
                    ('gestante_id', '=', r.id),
                    ('visita_ids', '!=', False),
                    ('mes', '=', mes),
                    ('anio', '=', anio.id)
                ])
                if ficha_rec:
                    return 1
                ficha_fort = self.env['ficha.fortalecimiento'].search([
                    ('gestante_id', '=', r.id),
                    ('visita_ids', '!=', False),
                    ('mes', '=', mes),
                    ('anio', '=', anio.id)
                ])
                if ficha_fort:
                    return 1
        return 0

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Reporte Familia')

        sheet.set_column('A:A', 35)
        sheet.set_column('F:F', 35)
        sheet.set_column('I:L', 35)
        sheet.write(0, 0, 'UT', title_blue_format)
        sheet.write(0, 1, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 2, 'NOMDEP', title_blue_format)
        sheet.write(0, 3, 'NOMPRO', title_blue_format)
        sheet.write(0, 4, 'NOMDIS', title_blue_format)
        sheet.write(0, 5, 'CODCENPOB', title_blue_format)
        sheet.write(0, 6, 'NOMCENPOB', title_blue_format)
        sheet.write(0, 7, 'DIR', title_blue_format)
        sheet.write(0, 8, 'REF', title_blue_format)
        sheet.write(0, 9, 'AT ID', title_blue_format)
        sheet.write(0, 10, 'AT PATERNO', title_blue_format)
        sheet.write(0, 11, 'AT MATERNO', title_blue_format)
        sheet.write(0, 12, 'AT NOMBRE', title_blue_format)
        sheet.write(0, 13, 'CG ID', title_blue_format)
        sheet.write(0, 14, 'CG CODCOM', title_blue_format)
        sheet.write(0, 15, 'CG NOMCOM', title_blue_format)
        sheet.write(0, 16, 'FAC ID', title_blue_format)
        sheet.write(0, 17, 'FAC PATERNO', title_blue_format)
        sheet.write(0, 18, 'FAC MATERNO', title_blue_format)
        sheet.write(0, 19, 'FAC NOMBRE', title_blue_format)
        sheet.write(0, 20, 'COD HOGAR', title_blue_format)
        sheet.write(0, 21, 'IDTPER', title_blue_format)
        sheet.write(0, 22, 'TIPO DOCUMENTO', title_blue_format)
        sheet.write(0, 23, 'N° DOC', title_blue_format)
        sheet.write(0, 24, 'VALIDADO RENIEC', title_blue_format)
        sheet.write(0, 25, 'NOMBRE', title_blue_format)
        sheet.write(0, 26, 'APEPAT', title_blue_format)
        sheet.write(0, 27, 'APEMAT', title_blue_format)
        sheet.write(0, 28, 'GENERO', title_blue_format)
        sheet.write(0, 29, 'MESES', title_blue_format)
        sheet.write(0, 30, 'FEC NAC', title_blue_format)
        sheet.write(0, 31, 'LEE ESCRIBE', title_blue_format)
        sheet.write(0, 32, 'ES JEFE HOGAR', title_blue_format)
        sheet.write(0, 33, 'ES CUID PRINCIPAL', title_blue_format)
        sheet.write(0, 34, 'PARENTESCO', title_blue_format)
        sheet.write(0, 35, 'ESTADO CIVIL', title_blue_format)
        sheet.write(0, 36, 'NIVEL EDUCATIVO', title_blue_format)
        sheet.write(0, 37, u'ULTIMO AÑO', title_blue_format)
        sheet.write(0, 38, 'SEGURO', title_blue_format)
        sheet.write(0, 39, u'SEGURO NÚMERO', title_blue_format)
        sheet.write(0, 40, 'OCUPACION', title_blue_format)
        sheet.write(0, 41, 'VISUAL', title_blue_format)
        sheet.write(0, 42, 'OIR', title_blue_format)
        sheet.write(0, 43, 'HABLAR', title_blue_format)
        sheet.write(0, 44, 'USAR_EXTREMIDADES', title_blue_format)
        sheet.write(0, 45, 'MENTAL', title_blue_format)
        sheet.write(0, 46, 'NO_TIENE', title_blue_format)
        sheet.write(0, 47, 'NUMERO_HIJOS', title_blue_format)
        sheet.write(0, 48, 'CUNAMAS', title_blue_format)
        sheet.write(0, 49, 'QALI_WARMA', title_blue_format)
        sheet.write(0, 50, 'FONCODES', title_blue_format)
        sheet.write(0, 51, 'JUNTOS', title_blue_format)
        sheet.write(0, 52, 'PENSION_65', title_blue_format)
        sheet.write(0, 53, 'OTRO', title_blue_format)
        sheet.write(0, 54, 'NINGUNO', title_blue_format)
        sheet.write(0, 55, 'RELIGION', title_blue_format)
        sheet.write(0, 56, 'LENGUA_MATERNA', title_blue_format)
        sheet.write(0, 57, 'EDAD_ANIOS', title_blue_format)
        sheet.write(0, 58, 'EDAD_MESES', title_blue_format)
        sheet.write(0, 59, 'NINIO_CON_VISITA', title_blue_format)
        sheet.write(0, 60, 'FAMILIA_CON_ASISTENCIA', title_blue_format)
        sheet.write(0, 61, 'FAM_NIN_CON_VISITA', title_blue_format)

        row = 1
        ut = lines.unidad_territorial_id
        mes = lines.mes
        anio = lines.anio
        familias = self.env['unidad.familiar'].search([
            ('services', '=', 'saf'),
            ('state', '=', 'activo'),
            ('unidad_territorial_id', '=', ut.id),
        ])
        for fam in familias:
            visitas = self.buscar_visitas_familia(fam.integrantes_ids, mes, anio)
            socializacion = self.env['socializacion.lineas'].search([
                ('familia_id', '=', fam.id),
                ('mes', '=', mes),
                ('anio', '=', anio.id)
            ])
            for persona in fam.integrantes_ids:
                sheet.write(row, 0, ut.name, body_format)
                sheet.write(row, 1, ut.distrito_id.code, body_format)
                sheet.write(row, 2, ut.departamento_id.name, body_format)
                sheet.write(row, 3, ut.provincia_id.name, body_format)
                sheet.write(row, 4, ut.distrito_id.name, body_format)
                sheet.write(row, 5, fam.centro_poblado_id.code, body_format)
                sheet.write(row, 6, fam.centro_poblado_id.name, body_format)
                sheet.write(row, 7, ut.direccion, body_format)
                sheet.write(row, 8, ut.referencia, body_format)
                at = fam.comite_gestion_id.miembros_equipo.filtered(
                    lambda x: x.cargo == u'ACOMPAÑANTE TECNICO' and not x.fecha_fin
                )
                sheet.write(row, 9, at.id, body_format)
                sheet.write(row, 10, at.person_id.firstname, body_format)
                sheet.write(row, 11, at.person_id.secondname, body_format)
                sheet.write(row, 12, at.person_id.name, body_format)
                sheet.write(row, 13, fam.comite_gestion_id.id, body_format)
                sheet.write(row, 14, fam.comite_gestion_id.codigo, body_format)
                sheet.write(row, 15, fam.comite_gestion_id.name, body_format)
                sheet.write(row, 16, fam.facilitador_id.id, body_format)
                sheet.write(row, 17, fam.facilitador_id.person_id.firstname, body_format)
                sheet.write(row, 18, fam.facilitador_id.person_id.secondname, body_format)
                sheet.write(row, 19, fam.facilitador_id.person_id.name, body_format)
                sheet.write(row, 20, fam.id, body_format)
                sheet.write(row, 21, persona.integrante_id.id, body_format)
                sheet.write(row, 22, persona.integrante_id.type_document.upper(), body_format)
                sheet.write(row, 23, persona.integrante_id.document_number, body_format)
                sheet.write(row, 24, persona.integrante_id.validado_reniec, body_format)
                sheet.write(row, 25, persona.integrante_id.name, body_format)
                sheet.write(row, 26, persona.integrante_id.firstname, body_format)
                sheet.write(row, 27, persona.integrante_id.secondname, body_format)
                sheet.write(row, 28, persona.integrante_id.gender.upper(), body_format)
                sheet.write(row, 29, persona.integrante_id.months, body_format)
                sheet.write(row, 30, persona.integrante_id.birthdate, body_format)
                sheet.write(row, 31, persona.integrante_id.sabe_leer_y_escribir, body_format)
                sheet.write(row, 32, 'SI' if persona.es_jefe_hogar else 'NO', body_format)
                sheet.write(row, 33, 'SI' if persona.es_cuidador_principal else 'NO', body_format)
                sheet.write(row, 34, persona.parentesco_id.name.upper(), body_format)
                sheet.write(row, 35, persona.integrante_id.marital_status, body_format)
                sheet.write(row, 36, persona.integrante_id.nivel_educativo_id.name.upper(), body_format)
                sheet.write(row, 37, persona.integrante_id.ultimo_grado_apr_id.name.upper(), body_format)
                sheet.write(row, 38, persona.integrante_id.seguro_id.name.upper(), body_format)
                sheet.write(row, 39, persona.integrante_id.codigo_seguro, body_format)
                sheet.write(row, 40, persona.integrante_id.ocupacion_id.name.upper(), body_format)
                discapacidad = persona.integrante_id.discapacidad_id
                sheet.write(row, 41, buscar_discapacidad('DISC_VISUAL', discapacidad), body_format)
                sheet.write(row, 42, buscar_discapacidad('DISC_OIR', discapacidad), body_format)
                sheet.write(row, 43, buscar_discapacidad('DISC_HABLAR', discapacidad), body_format)
                sheet.write(row, 44, buscar_discapacidad('DISC_EXTREMIDADES', discapacidad), body_format)
                sheet.write(row, 45, buscar_discapacidad('DISC_MENTAL', discapacidad), body_format)
                sheet.write(row, 46, buscar_discapacidad('NO_TIENE_DISCAPACIDAD', discapacidad), body_format)
                sheet.write(row, 47, persona.integrante_id.nro_hijos, body_format)
                programa_social = persona.integrante_id.programa_social_ids
                sheet.write(row, 48, buscar_tabla_base(u'CunaMás', programa_social), body_format)
                sheet.write(row, 49, buscar_tabla_base('Qali Warma', programa_social), body_format)
                sheet.write(row, 50, buscar_tabla_base('Foncodes', programa_social), body_format)
                sheet.write(row, 51, buscar_tabla_base('Juntos', programa_social), body_format)
                sheet.write(row, 52, buscar_tabla_base(u'Pensión', programa_social), body_format)
                sheet.write(row, 53, buscar_tabla_base('Otro', programa_social), body_format)
                sheet.write(row, 54, buscar_tabla_base('Ninguno', programa_social), body_format)
                sheet.write(row, 55, persona.integrante_id.religion_id.name.upper(), body_format)
                sheet.write(row, 56, persona.integrante_id.lengua_materna_id.name.upper(), body_format)
                sheet.write(row, 57, persona.integrante_id.anios, body_format)
                sheet.write(row, 58, persona.integrante_id.months, body_format)
                sheet.write(row, 59, visitas, body_format)
                sheet.write(row, 60, 1 if socializacion else 0, body_format)
                sheet.write(row, 61, visitas, body_format)
                row += 1


ReporteFamiliaXls('report.base_cuna.ReporteFamiliaXls.xlsx', 'dynamic.report.wizard')


class TramaCGXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Trama Comite Gestion')
        sheet.set_column('A:A', 35)
        sheet.set_column('F:F', 35)
        sheet.set_column('I:L', 35)
        sheet.write(0, 0, 'CG ID', title_blue_format)
        sheet.write(0, 1, 'COMITE CODIGO', title_blue_format)
        sheet.write(0, 2, 'NOMBRE COMITE', title_blue_format)

        row = 1
        ut = lines.unidad_territorial_id
        for cg in ut.ut_comites_gestion:
            if cg.servicio != 'scd':
                sheet.write(row, 0, cg.id, body_format)
                sheet.write(row, 1, cg.codigo, body_format)
                sheet.write(row, 2, cg.name, body_format)
                row += 1


TramaCGXls('report.base_cuna.TramaCGXls.xlsx', 'dynamic.report.wizard')


class TramaFacilitadorXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Trama Facilitadores')
        sheet.write(0, 0, 'FAC ID', title_blue_format)
        sheet.write(0, 1, 'NOMBRE FACILITADOR', title_blue_format)

        row = 1
        ut = lines.unidad_territorial_id
        for cg in ut.ut_comites_gestion:
            if cg.servicio != 'scd':
                facilitadores = cg.afiliaciones_ids.filtered(lambda x: x.tipo_guia.name == 'FACILITADOR')
                for fac in facilitadores:
                    nombre = '{} {} {}'.format(fac.person_id.name, fac.person_id.firstname, fac.person_id.secondname)
                    sheet.write(row, 0, fac.id, body_format)
                    sheet.write(row, 1, nombre, body_format)
                    row += 1


TramaFacilitadorXls('report.base_cuna.TramaFacilitadorXls.xlsx', 'dynamic.report.wizard')


class TramaUsuariosXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Trama Usuarios')
        sheet.write(0, 0, 'USUARIO ID', title_blue_format)
        sheet.write(0, 1, 'USUARIO TIPO', title_blue_format)

        row = 1
        ut = lines.unidad_territorial_id
        familias = self.env['unidad.familiar'].search([
            ('services', '=', 'saf'),
            ('state', '=', 'activo'),
            ('unidad_territorial_id', '=', ut.id),
        ])
        for fam in familias:
            integrantes = fam.integrantes_ids.filtered(
                lambda x: x.integrante_id.madre_gestante or x.parentesco_id.name == u'Niño(a)'
            )
            for persona in integrantes:
                sheet.write(row, 0, persona.id, body_format)
                tipo = u'Niño(a)' if persona.parentesco_id.name == u'Niño(a)' else 'Madre Gestante'
                sheet.write(row, 1, tipo, body_format)
                row += 1


TramaUsuariosXls('report.base_cuna.TramaUsuariosXls.xlsx', 'dynamic.report.wizard')


class SesionesSocializacionXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Sesiones Socialización')

        sheet.write(0, 0, 'UT', title_blue_format)
        sheet.write(0, 1, 'DEPARTAMENTO', title_blue_format)
        sheet.write(0, 2, 'PROVINCIA', title_blue_format)
        sheet.write(0, 3, 'DISTRITO', title_blue_format)
        sheet.write(0, 4, 'ID CG', title_blue_format)
        sheet.write(0, 5, 'COD CG', title_blue_format)
        sheet.write(0, 6, 'CG', title_blue_format)
        sheet.write(0, 7, 'COD FAMILIA', title_blue_format)
        sheet.write(0, 8, 'COD CUIDADOR PRINCIPAL', title_blue_format)
        sheet.write(0, 9, 'NOMBRE CUIDADOR PRINCIPAL', title_blue_format)
        sheet.write(0, 10, 'COD FACILITADOR VISITA', title_blue_format)
        sheet.write(0, 11, 'FACILITADOR VISITA', title_blue_format)
        sheet.write(0, 12, 'FECHA DE LA SESION SOCIALIZACION', title_blue_format)
        sheet.write(0, 13, u'AÑO DE EJECUCION', title_blue_format)
        sheet.write(0, 14, 'MES DE EJECUCION', title_blue_format)
        sheet.write(0, 15, 'USUARIO QUE REALIZA EL REGISTRO', title_blue_format)
        sheet.write(0, 16, 'CARGO DE PERSONA QUE REALIZA REGISTRO', title_blue_format)
        sheet.write(0, 17, 'FECHA DE REGISTRO', title_blue_format)
        sheet.write(0, 18, 'Reporte Fecha', title_blue_format)

        row = 1
        anio = lines.anio
        mes = lines.mes
        ut = lines.unidad_territorial_id

        lineas = self.env['socializacion.lineas'].search([
            ('unidad_territorial_id', '=', ut.id),
            ('anio', '=', anio.id),
            ('mes', '=', mes),
        ])
        sheet.set_column('I:Q', 35)
        for sesion in lineas:
            sheet.write(row, 0, ut.name, body_format)
            sheet.write(row, 1, ut.departamento_id.name, body_format)
            sheet.write(row, 2, ut.provincia_id.name, body_format)
            sheet.write(row, 3, ut.distrito_id.name, body_format)
            sheet.write(row, 4, sesion.familia_id.comite_gestion_id.id, body_format)
            sheet.write(row, 5, sesion.familia_id.comite_gestion_id.codigo, body_format)
            sheet.write(row, 6, sesion.familia_id.comite_gestion_id.name, body_format)
            sheet.write(row, 7, sesion.familia_id.id, body_format)
            cuid = sesion.familia_id.integrantes_ids.filtered(lambda x: x.es_cuidador_principal)
            sheet.write(row, 8, cuid.id if cuid else '', body_format)
            sheet.write(row, 9, cuid.integrante_id.name if cuid else '', body_format)
            sheet.write(row, 10, sesion.familia_id.facilitador_id.id, body_format)
            sheet.write(row, 11, sesion.familia_id.facilitador_id.person_id.name, body_format)
            sheet.write(row, 12, sesion.ficha_id.fecha_sesion, body_format)
            sheet.write(row, 13, anio.name, body_format)
            sheet.write(row, 14, mes, body_format)
            cargo = ''
            if sesion.ficha_id.usuario_creador_id:
                cargo = sesion.ficha_id.usuario_creador_id.partner_id.tipo_persona.name
            sheet.write(row, 15, sesion.ficha_id.usuario_creador_id.name, body_format)
            sheet.write(row, 16, cargo, body_format)
            sheet.write(row, 17, sesion.ficha_id.fecha_creacion, body_format)
            sheet.write(row, 18, lines.fecha_creacion, body_format)

            row += 1


SesionesSocializacionXls('report.base_cuna.SesionesSocializacionXls.xlsx', 'dynamic.report.wizard')


class LongitudinalSafXls(ReportXlsx):

    def buscar_fichas(self, persona, anio, mes):
        fichas = self.env['survey.user_input'].search([
            ('persona_ficha_id', '=', persona.id),
            ('mes', '=', mes),
            ('anio', '=', anio.id)
        ])
        if len(fichas) == 0:
            return ''
        else:
            return len(fichas)

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 3
        ut = lines.unidad_territorial_id
        anio = lines.anio
        sheet = workbook.add_worksheet('Longitudinal Saf')

        sheet.write(2, 0, 'USUARIO_ID', title_blue_format)
        sheet.write(2, 1, 'TPER_PATERNO', title_blue_format)
        sheet.write(2, 2, 'TPER_MATERNO', title_blue_format)
        sheet.write(2, 3, 'TPER_NOMBRES', title_blue_format)
        sheet.write(2, 4, 'TPER_FECHANAC', title_blue_format)
        sheet.write(2, 5, 'TPER_SEXO', title_blue_format)
        sheet.write(2, 6, 'TPER_TIPODOC', title_blue_format)
        sheet.write(2, 7, 'TPER_NUMDOC', title_blue_format)
        sheet.write(0, 8, 'VISITAS', title_blue_format)
        sheet.write(1, 8, 'USUARIOS', title_blue_format)
        sheet.write(2, 8, 'USUARIO', title_blue_format)
        sheet.write(2, 9, '%s01_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 10, '%s02_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 11, '%s03_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 12, '%s04_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 13, '%s05_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 14, '%s06_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 15, '%s07_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 16, '%s08_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 17, '%s09_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 18, '%s10_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 19, '%s11_VISITA_TOTAL' % anio.name, title_blue_format)
        sheet.write(2, 20, '%s12_VISITA_TOTAL' % anio.name, title_blue_format)

        familias = self.env['unidad.familiar'].search([
            ('services', '=', 'saf'),
            ('state', '=', 'activo'),
            ('unidad_territorial_id', '=', ut.id),
        ])
        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)
        for fam in familias:
            integrantes = fam.integrantes_ids.filtered(
                lambda x: x.integrante_id.madre_gestante or x.parentesco_id.name == u'Niño(a)'
            )
            for persona in integrantes:
                sheet.write(row, 0, persona.id, body_format)
                sheet.write(row, 1, persona.integrante_id.firstname, body_format)
                sheet.write(row, 2, persona.integrante_id.secondname, body_format)
                sheet.write(row, 3, persona.integrante_id.name, body_format)
                sheet.write(row, 4, persona.integrante_id.birthdate, body_format)
                sheet.write(row, 5, persona.integrante_id.gender.upper(), body_format)
                sheet.write(row, 6, persona.integrante_id.type_document, body_format)
                sheet.write(row, 7, persona.integrante_id.document_number, body_format)
                tipo = u'Niño(a)' if persona.parentesco_id.name == u'Niño(a)' else 'Madre Gestante'
                sheet.write(row, 8, tipo, body_format)
                sheet.write(row, 9, self.buscar_fichas(persona, anio, '01'), body_format)
                sheet.write(row, 10, self.buscar_fichas(persona, anio, '02'), body_format)
                sheet.write(row, 11, self.buscar_fichas(persona, anio, '03'), body_format)
                sheet.write(row, 12, self.buscar_fichas(persona, anio, '04'), body_format)
                sheet.write(row, 13, self.buscar_fichas(persona, anio, '05'), body_format)
                sheet.write(row, 14, self.buscar_fichas(persona, anio, '06'), body_format)
                sheet.write(row, 15, self.buscar_fichas(persona, anio, '07'), body_format)
                sheet.write(row, 16, self.buscar_fichas(persona, anio, '08'), body_format)
                sheet.write(row, 17, self.buscar_fichas(persona, anio, '09'), body_format)
                sheet.write(row, 18, self.buscar_fichas(persona, anio, '10'), body_format)
                sheet.write(row, 19, self.buscar_fichas(persona, anio, '11'), body_format)
                sheet.write(row, 20, self.buscar_fichas(persona, anio, '12'), body_format)
                row += 1
        sheet.write(0, 9, '=SUBTOTAL(9,J4:J%d)' % row, body_format)
        sheet.write(0, 10, '=SUBTOTAL(9,K4:K%d)' % row, body_format)
        sheet.write(0, 11, '=SUBTOTAL(9,L4:L%d)' % row, body_format)
        sheet.write(0, 12, '=SUBTOTAL(9,M4:M%d)' % row, body_format)
        sheet.write(0, 13, '=SUBTOTAL(9,N4:N%d)' % row, body_format)
        sheet.write(0, 14, '=SUBTOTAL(9,O4:O%d)' % row, body_format)
        sheet.write(0, 15, '=SUBTOTAL(9,P4:P%d)' % row, body_format)
        sheet.write(0, 16, '=SUBTOTAL(9,Q4:Q%d)' % row, body_format)
        sheet.write(0, 17, '=SUBTOTAL(9,R4:R%d)' % row, body_format)
        sheet.write(0, 18, '=SUBTOTAL(9,S4:S%d)' % row, body_format)
        sheet.write(0, 19, '=SUBTOTAL(9,T4:T%d)' % row, body_format)
        sheet.write(0, 20, '=SUBTOTAL(9,U4:U%d)' % row, body_format)
        
        sheet.write(1, 9, '=SUMPRODUCT((J4:J%d<>"")*1)' % row, body_format)
        sheet.write(1, 10, '=SUMPRODUCT((K4:K%d<>"")*1)' % row, body_format)
        sheet.write(1, 11, '=SUMPRODUCT((L4:L%d<>"")*1)' % row, body_format)
        sheet.write(1, 12, '=SUMPRODUCT((M4:M%d<>"")*1)' % row, body_format)
        sheet.write(1, 13, '=SUMPRODUCT((N4:N%d<>"")*1)' % row, body_format)
        sheet.write(1, 14, '=SUMPRODUCT((O4:O%d<>"")*1)' % row, body_format)
        sheet.write(1, 15, '=SUMPRODUCT((P4:P%d<>"")*1)' % row, body_format)
        sheet.write(1, 16, '=SUMPRODUCT((Q4:Q%d<>"")*1)' % row, body_format)
        sheet.write(1, 17, '=SUMPRODUCT((R4:R%d<>"")*1)' % row, body_format)
        sheet.write(1, 18, '=SUMPRODUCT((S4:S%d<>"")*1)' % row, body_format)
        sheet.write(1, 19, '=SUMPRODUCT((T4:T%d<>"")*1)' % row, body_format)
        sheet.write(1, 20, '=SUMPRODUCT((U4:U%d<>"")*1)' % row, body_format)


LongitudinalSafXls('report.base_cuna.LongitudinalSafXls.xlsx', 'dynamic.report.wizard')


class LongitudinalSCDXls(ReportXlsx):

    def buscar_asistencia(self, persona, anio, mes):
        asis = self.env['asistencia.menor'].search([
            ('afi_ninio_id', '=', persona.id),
            ('mes', '=', mes),
            ('anio', '=', anio.id)
        ])
        if len(asis) <= 0:
            return ''
        else:
            asiste = sum(line.asiste for line in asis)
            return asiste

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 2
        ut = lines.unidad_territorial_id
        anio = lines.anio
        sheet = workbook.add_worksheet('Longitudinal SCD')

        sheet.write(1, 0, 'CODIGO DE NIÑO', title_blue_format)
        sheet.write(1, 1, 'TPER_PATERNO', title_blue_format)
        sheet.write(1, 2, 'TPER_MATERNO', title_blue_format)
        sheet.write(1, 3, 'TPER_NOMBRES', title_blue_format)
        sheet.write(1, 4, 'TPER_FECHANAC', title_blue_format)
        sheet.write(1, 5, 'TPER_SEXO', title_blue_format)
        sheet.write(1, 6, 'TIPO DOCUMENTO', title_blue_format)
        sheet.write(0, 7, 'Cantidad de niños que asistieron', title_blue_format)
        sheet.write(1, 7, 'NUMERO DE DOCUMENTO', title_blue_format)
        sheet.write(1, 8, '%s01 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 9, '%s02 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 10, '%s03 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 11, '%s04 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 12, '%s05 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 13, '%s06 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 14, '%s07 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 15, '%s08 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 16, '%s09 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 17, '%s10 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 18, '%s11 ASISTIO' % anio.name, title_blue_format)
        sheet.write(1, 19, '%s12 ASISTIO' % anio.name, title_blue_format)

        familias = self.env['unidad.familiar'].search([
            ('services', '=', 'scd'),
            ('state', '=', 'activo'),
            ('unidad_territorial_id', '=', ut.id),
        ])
        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)
        for fam in familias:
            integrantes = fam.integrantes_ids.filtered(lambda x: x.parentesco_id.name == u'Niño(a)')
            for persona in integrantes:
                sheet.write(row, 0, persona.id, body_format)
                sheet.write(row, 1, persona.integrante_id.firstname, body_format)
                sheet.write(row, 2, persona.integrante_id.secondname, body_format)
                sheet.write(row, 3, persona.integrante_id.name, body_format)
                sheet.write(row, 4, persona.integrante_id.birthdate, body_format)
                sheet.write(row, 5, persona.integrante_id.gender.upper(), body_format)
                sheet.write(row, 6, persona.integrante_id.type_document.upper(), body_format)
                sheet.write(row, 7, persona.integrante_id.document_number, body_format)
                sheet.write(row, 8, self.buscar_asistencia(persona, anio, '01'), body_format)
                sheet.write(row, 9, self.buscar_asistencia(persona, anio, '02'), body_format)
                sheet.write(row, 10, self.buscar_asistencia(persona, anio, '03'), body_format)
                sheet.write(row, 11, self.buscar_asistencia(persona, anio, '04'), body_format)
                sheet.write(row, 12, self.buscar_asistencia(persona, anio, '05'), body_format)
                sheet.write(row, 13, self.buscar_asistencia(persona, anio, '06'), body_format)
                sheet.write(row, 14, self.buscar_asistencia(persona, anio, '07'), body_format)
                sheet.write(row, 15, self.buscar_asistencia(persona, anio, '08'), body_format)
                sheet.write(row, 16, self.buscar_asistencia(persona, anio, '09'), body_format)
                sheet.write(row, 17, self.buscar_asistencia(persona, anio, '10'), body_format)
                sheet.write(row, 18, self.buscar_asistencia(persona, anio, '11'), body_format)
                sheet.write(row, 19, self.buscar_asistencia(persona, anio, '12'), body_format)
                row += 1
        sheet.write(0, 9, '=SUMPRODUCT((I3:I%d<>"")*1)' % row, body_format)
        sheet.write(0, 10, '=SUMPRODUCT((J3:J%d<>"")*1)' % row, body_format)
        sheet.write(0, 11, '=SUMPRODUCT((K3:K%d<>"")*1)' % row, body_format)
        sheet.write(0, 12, '=SUMPRODUCT((L3:L%d<>"")*1)' % row, body_format)
        sheet.write(0, 13, '=SUMPRODUCT((M3:M%d<>"")*1)' % row, body_format)
        sheet.write(0, 14, '=SUMPRODUCT((N3:N%d<>"")*1)' % row, body_format)
        sheet.write(0, 15, '=SUMPRODUCT((O3:O%d<>"")*1)' % row, body_format)
        sheet.write(0, 16, '=SUMPRODUCT((P3:P%d<>"")*1)' % row, body_format)
        sheet.write(0, 17, '=SUMPRODUCT((Q3:Q%d<>"")*1)' % row, body_format)
        sheet.write(0, 18, '=SUMPRODUCT((R3:R%d<>"")*1)' % row, body_format)
        sheet.write(0, 19, '=SUMPRODUCT((S3:S%d<>"")*1)' % row, body_format)
        sheet.write(0, 20, '=SUMPRODUCT((T3:T%d<>"")*1)' % row, body_format)


LongitudinalSCDXls('report.base_cuna.LongitudinalSCDXls.xlsx', 'dynamic.report.wizard')


class RequerimientosCGReportXls(ReportXlsx):

    def buscar_asistencia(self, persona, anio, mes):
        asis = self.env['asistencia.menor'].search([
            ('afi_ninio_id', '=', persona.id),
            ('mes', '=', mes),
            ('anio', '=', anio.id)
        ])
        if len(asis) <= 0:
            return ''
        else:
            asiste = sum(line.asiste for line in asis)
            return asiste

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1
        ut = lines.unidad_territorial_id
        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('Requerimientos CG')

        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOMDEP', title_blue_format)
        sheet.write(0, 3, 'NOMPRO', title_blue_format)
        sheet.write(0, 4, 'NOMDIS', title_blue_format)
        sheet.write(0, 5, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 6, 'UT', title_blue_format)
        sheet.write(0, 7, 'ID', title_blue_format)
        sheet.write(0, 8, 'NOMCOM', title_blue_format)
        sheet.write(0, 9, 'NUM_CIAI', title_blue_format)
        sheet.write(0, 10, 'NUM_HOGAR_FAMILIAR', title_blue_format)
        sheet.write(0, 11, 'NUM_COMUNALES', title_blue_format)
        sheet.write(0, 12, 'TOTAL_LOC', title_blue_format)
        sheet.write(0, 13, 'NUM_GUIA_FAMILIAR', title_blue_format)
        sheet.write(0, 14, 'ASIG_SUBMGFAM', title_blue_format)
        sheet.write(0, 15, 'NUM_MADRE_GUIA', title_blue_format)
        sheet.write(0, 16, 'NUM_MADRE_CUIDADORA_A', title_blue_format)
        sheet.write(0, 17, 'NUM_MADRE_CUIDADORA_B', title_blue_format)
        sheet.write(0, 18, 'TOTAL_CUI', title_blue_format)
        sheet.write(0, 19, 'NUM_SOCIA_COCINA', title_blue_format)
        sheet.write(0, 20, 'NUM_VIG_LIMPIEZA', title_blue_format)
        sheet.write(0, 21, 'NUM_APOYO_ADM', title_blue_format)
        sheet.write(0, 22, 'AMPLIACION_META', title_blue_format)
        sheet.write(0, 23, 'TOTAL_USU', title_blue_format)
        sheet.write(0, 24, 'NDIAS', title_blue_format)
        sheet.write(0, 25, 'MONTO_TOTAL', title_blue_format)
        sheet.write(0, 26, 'SALDO_ANTERIOR', title_blue_format)
        sheet.write(0, 27, 'DEPOSITO', title_blue_format)
        sheet.write(0, 28, 'TIENE_SCAN', title_blue_format)
        sheet.write(0, 29, 'REPORTE_FECHA', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)

        for cg in ut.ut_comites_gestion:
            req = self.env['requerimiento.lines'].search([
                ('servicio', '=', 'scd'),
                ('comite_gestion_id', '=', cg.id),
                ('unidad_territorial_id', '=', ut.id),
                ('anio', '=', anio.id),
                ('mes', '=', mes)
            ])
            if req:
                sheet.write(row, 0, anio.name, body_format)
                sheet.write(row, 1, mes, body_format)
                sheet.write(row, 2, ut.departamento_id.name, body_format)
                sheet.write(row, 3, ut.provincia_id.name, body_format)
                sheet.write(row, 4, ut.distrito_id.name, body_format)
                sheet.write(row, 5, ut.distrito_id.code, body_format)
                sheet.write(row, 6, ut.name, body_format)
                sheet.write(row, 7, ut.id, body_format)
                sheet.write(row, 8, cg.name, body_format)
                ciais = cg.local_ids.filtered(lambda x: x.tipo_local in ['ciai', 'ambos'])
                sheet.write(row, 9, len(ciais) if ciais else 0, body_format)
                sheet.write(row, 10, len(cg.familias_ids), body_format)
                actores_comunales = cg.afiliaciones_ids.filtered(lambda x: x.cargo in [
                    'PRESIDENTE DE COMITE DE GESTION',
                    'PRIMER VOCAL',
                    'SEGUNDO VOCAL',
                    'SECRETARIO DE COMITE DE GESTION',
                    'TESORERO DE COMITE DE GESTION',
                    'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'
                ])
                sheet.write(row, 11, len(actores_comunales), body_format)
                sheet.write(row, 12, len(cg.local_ids), body_format)

                sheet.write(row, 13, req.desembolso_id.guias_familiares, body_format)
                sheet.write(row, 14, req.desembolso_id.incentivo_mad_guia_base, body_format)
                sheet.write(row, 15, req.desembolso_id.madres_guias, body_format)
                sheet.write(row, 16, req.desembolso_id.mad_cuid_a, body_format)
                sheet.write(row, 17, req.desembolso_id.mad_cuid_b, body_format)
                sheet.write(row, 18, req.desembolso_id.mad_cuid_a + req.desembolso_id.mad_cuid_b, body_format)
                sheet.write(row, 19, req.desembolso_id.socias_cocina, body_format)
                sheet.write(row, 20, req.desembolso_id.nro_vigilantes, body_format)
                sheet.write(row, 21, req.desembolso_id.apoyo_adm, body_format)
                sheet.write(row, 22, req.desembolso_id.meta_cg, body_format)
                sheet.write(row, 23, req.desembolso_id.nro_usuarios_atendidos, body_format)
                sheet.write(row, 24, req.desembolso_id.dias_atender, body_format)
                sheet.write(row, 25, req.desembolso_id.total_deposito, body_format)
                sheet.write(row, 26, req.desembolso_id.saldo_anterior, body_format)
                sheet.write(row, 27, req.desembolso_id.total_anterior, body_format)
                sheet.write(row, 28, '1' if req.adjuntar_desembolso else '0', body_format)
                sheet.write(row, 29, lines.fecha_creacion, body_format)
                row += 1


RequerimientosCGReportXls('report.base_cuna.RequerimientosCGReportXls.xlsx', 'dynamic.report.wizard')


class SaldosCGReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1
        ut = lines.unidad_territorial_id
        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('Saldos CG')

        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOM', title_blue_format)
        sheet.write(0, 3, 'NOMDEP', title_blue_format)
        sheet.write(0, 4, 'NOMPRO', title_blue_format)
        sheet.write(0, 5, 'NOMDIS', title_blue_format)
        sheet.write(0, 6, 'ID_COMITE', title_blue_format)
        sheet.write(0, 7, 'NOMCOM', title_blue_format)
        sheet.write(0, 8, 'SALDO_ALIMENTOS', title_blue_format)
        sheet.write(0, 9, 'SALDO_ACTORES', title_blue_format)
        sheet.write(0, 10, 'SALDO', title_blue_format)
        sheet.write(0, 11, 'ESTADO_F3', title_blue_format)
        sheet.write(0, 12, 'REPORTE_FECHA', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)

        for cg in ut.ut_comites_gestion:
            gastos = self.env['justificacion.gastos'].search([
                ('servicio', '=', 'scd'),
                ('comite_gestion_id', '=', cg.id),
                ('anio', '=', anio.id),
                ('mes', '=', mes)
            ])
            if gastos:
                sheet.write(row, 0, anio.name, body_format)
                sheet.write(row, 1, mes, body_format)
                sheet.write(row, 2, ut.name, body_format)
                sheet.write(row, 3, ut.departamento_id.name, body_format)
                sheet.write(row, 4, ut.provincia_id.name, body_format)
                sheet.write(row, 5, ut.distrito_id.name, body_format)
                sheet.write(row, 6, cg.id, body_format)
                sheet.write(row, 7, cg.name, body_format)
                sheet.write(row, 8, gastos.atencion_alimentaria_recibido - gastos.alimento, body_format)
                saldo_actores = gastos.incentivo_cuid_a_saldo + gastos.incentivo_cuid_b_saldo + \
                    gastos.incentivo_mad_guia_saldo + gastos.incentivo_guias_fam_saldo + \
                    gastos.incentivo_socio_cocina_saldo + gastos.incentivo_adm_saldo + \
                    gastos.incentivo_vigilancia_saldo
                sheet.write(row, 9, saldo_actores, body_format)
                sheet.write(row, 10, gastos.saldo, body_format)
                sheet.write(row, 11, gastos.estado.upper(), body_format)
                sheet.write(row, 12, lines.fecha_creacion, body_format)
                row += 1


SaldosCGReportXls('report.base_cuna.SaldosCGReportXls.xlsx', 'dynamic.report.wizard')


class SociasSAPlanillaReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1
        ut = lines.unidad_territorial_id
        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('SOCIA SA PLANILLA')

        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOM', title_blue_format)
        sheet.write(0, 3, 'NOMDEP', title_blue_format)
        sheet.write(0, 4, 'NOMPRO', title_blue_format)
        sheet.write(0, 5, 'NOMDIS', title_blue_format)
        sheet.write(0, 6, 'ID_COMITE', title_blue_format)
        sheet.write(0, 7, 'NOMCOM', title_blue_format)
        sheet.write(0, 8, 'IDTSERALI', title_blue_format)
        sheet.write(0, 9, 'NOMSERALI', title_blue_format)
        sheet.write(0, 10, 'TIPO_INTEGRANTE_COM', title_blue_format)
        sheet.write(0, 11, 'ID_INTEGRANTE_COM', title_blue_format)
        sheet.write(0, 12, 'APEPAT', title_blue_format)
        sheet.write(0, 13, 'APEMAT', title_blue_format)
        sheet.write(0, 14, 'NOMPER', title_blue_format)
        sheet.write(0, 15, 'FECNAC', title_blue_format)
        sheet.write(0, 16, 'NUMDOC', title_blue_format)
        sheet.write(0, 17, 'NUM_DIAS_LABORADOS', title_blue_format)
        sheet.write(0, 18, 'MONTO', title_blue_format)
        sheet.write(0, 19, 'ID_AFILIACION', title_blue_format)
        sheet.write(0, 20, 'FECFINSER', title_blue_format)
        sheet.write(0, 21, 'TIEALQ', title_blue_format)
        sheet.write(0, 22, 'FECHA_INICIO_ALQUILER', title_blue_format)
        sheet.write(0, 23, 'FECHA_FIN_ALQUILER', title_blue_format)
        sheet.write(0, 24, 'MONTO_ALQUILER', title_blue_format)
        sheet.write(0, 25, 'REPORTE_FECHA', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)
        tipo = self.env['tipo.persona'].search([('name', '=', 'SOCIO DE COCINA')])
        for cg in ut.ut_comites_gestion:
            for sa in cg.servicio_alimentario_ids.filtered(lambda x: x.state == 'activacion'):
                for socia in sa.socias_cocina_ids:
                    gastos = self.env['justificacion.actor.lines'].search([
                        ('afiliacion_id', '=', socia.id),
                        ('comite_gestion_id', '=', cg.id),
                        ('tipo_persona', '=', tipo.id),
                        ('anio', '=', anio.id),
                        ('mes', '=', mes)
                    ])
                    sheet.write(row, 0, anio.name, body_format)
                    sheet.write(row, 1, mes, body_format)
                    sheet.write(row, 2, ut.name, body_format)
                    sheet.write(row, 3, ut.departamento_id.name, body_format)
                    sheet.write(row, 4, ut.provincia_id.name, body_format)
                    sheet.write(row, 5, ut.distrito_id.name, body_format)
                    sheet.write(row, 6, cg.id, body_format)
                    sheet.write(row, 7, cg.name, body_format)
                    sheet.write(row, 8, sa.id, body_format)
                    sheet.write(row, 9, sa.name, body_format)
                    sheet.write(row, 10, tipo.id, body_format)
                    sheet.write(row, 11, socia.id, body_format)
                    sheet.write(row, 12, socia.person_id.firstname, body_format)
                    sheet.write(row, 13, socia.person_id.secondname, body_format)
                    sheet.write(row, 14, socia.person_id.name, body_format)
                    sheet.write(row, 15, socia.person_id.birthdate, body_format)
                    sheet.write(row, 16, socia.person_id.document_number, body_format)
                    sheet.write(row, 17, gastos.dias_trabajados, body_format)
                    sheet.write(row, 18, gastos.monto, body_format)
                    sheet.write(row, 19, socia.id, body_format)
                    sheet.write(row, 20, sa.fecha_baja or '', body_format)
                    sheet.write(row, 21, 1 if sa.es_alquilado else 0, body_format)
                    sheet.write(row, 22, sa.fecha_inicio_alquiler or '', body_format)
                    sheet.write(row, 23, sa.fecha_fin_alquiler or '', body_format)
                    sheet.write(row, 24, sa.monto_alquiler or '', body_format)
                    sheet.write(row, 25, lines.fecha_creacion, body_format)
                    row += 1


SociasSAPlanillaReportXls('report.base_cuna.SociasSAPlanillaReportXls.xlsx', 'dynamic.report.wizard')


class GuiasPlanillaReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1
        ut = lines.unidad_territorial_id
        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('GUIAS PLANILLA')

        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOM', title_blue_format)
        sheet.write(0, 3, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 4, 'NOMDEP', title_blue_format)
        sheet.write(0, 5, 'NOMPRO', title_blue_format)
        sheet.write(0, 6, 'NOMDIS', title_blue_format)
        sheet.write(0, 7, 'ID', title_blue_format)
        sheet.write(0, 8, 'NOMCOM', title_blue_format)
        sheet.write(0, 9, 'CODMADCUI', title_blue_format)
        sheet.write(0, 10, 'APEPAT', title_blue_format)
        sheet.write(0, 11, 'APEMAT', title_blue_format)
        sheet.write(0, 12, 'NOMPER', title_blue_format)
        sheet.write(0, 13, 'CATEGORIA', title_blue_format)
        sheet.write(0, 14, 'FECNAC', title_blue_format)
        sheet.write(0, 15, 'NUMDOC', title_blue_format)
        sheet.write(0, 16, 'TIPDOC', title_blue_format)
        sheet.write(0, 17, 'ESTCIV', title_blue_format)
        sheet.write(0, 18, 'DES', title_blue_format)
        sheet.write(0, 19, 'SEG', title_blue_format)
        sheet.write(0, 20, 'CAN_MESES', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)
        cargos = ['MADRE CUIDADORA', 'MADRE GUIA', 'GUIA FAMILIAR']
        tipo = self.env['tipo.persona'].search([('name', 'in', cargos)])

        for cg in ut.ut_comites_gestion:
            gastos = self.env['justificacion.actor.lines'].search([
                ('comite_gestion_id', '=', cg.id),
                ('tipo_persona', 'in', tipo.ids),
                ('anio', '=', anio.id),
                ('mes', '=', mes)
            ])
            for mc in gastos:
                sheet.write(row, 0, anio.name, body_format)
                sheet.write(row, 1, mes, body_format)
                sheet.write(row, 2, ut.name, body_format)
                sheet.write(row, 3, ut.distrito_id.code, body_format)
                sheet.write(row, 4, ut.departamento_id.name, body_format)
                sheet.write(row, 5, ut.provincia_id.name, body_format)
                sheet.write(row, 6, ut.distrito_id.name, body_format)
                sheet.write(row, 7, cg.id, body_format)
                sheet.write(row, 8, cg.name, body_format)
                sheet.write(row, 9, mc.afiliacion_id.id, body_format)
                sheet.write(row, 10, mc.afiliacion_id.person_id.firstname, body_format)
                sheet.write(row, 11, mc.afiliacion_id.person_id.secondname, body_format)
                sheet.write(row, 12, mc.afiliacion_id.person_id.name, body_format)
                sheet.write(row, 13, mc.afiliacion_id.tipo_guia.abrev, body_format)
                sheet.write(row, 14, mc.afiliacion_id.person_id.birthdate, body_format)
                sheet.write(row, 15, mc.afiliacion_id.person_id.document_number, body_format)
                sheet.write(row, 16, mc.afiliacion_id.person_id.type_document, body_format)
                sheet.write(row, 17, mc.afiliacion_id.person_id.marital_status, body_format)
                sheet.write(row, 18, mc.afiliacion_id.person_id.nivel_educativo_id.name, body_format)
                sheet.write(row, 19, mc.afiliacion_id.person_id.seguro_id.name, body_format)
                sheet.write(row, 20, cantidad_meses(lines.fecha_creacion, mc.afiliacion_id.fecha_ingreso), body_format)
                row += 1


GuiasPlanillaReportXls('report.base_cuna.GuiasPlanillaReportXls.xlsx', 'dynamic.report.wizard')


class CuidadorasAsistenciaReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1
        ut = lines.unidad_territorial_id
        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('CUIDADORAS ASISTENCIA')

        sheet.write(0, 0, 'NOM', title_blue_format)
        sheet.write(0, 1, 'CODUBIGEOCG', title_blue_format)
        sheet.write(0, 2, 'NOMDEPCG', title_blue_format)
        sheet.write(0, 3, 'NOMPROCG', title_blue_format)
        sheet.write(0, 4, 'NOMDISCG', title_blue_format)
        sheet.write(0, 5, 'IDCG', title_blue_format)
        sheet.write(0, 6, 'NOMCOM', title_blue_format)
        sheet.write(0, 7, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 8, 'NOMDEP', title_blue_format)
        sheet.write(0, 9, 'NOMPRO', title_blue_format)
        sheet.write(0, 10, 'NOMDIS', title_blue_format)
        sheet.write(0, 11, 'IDLOC', title_blue_format)
        sheet.write(0, 12, 'NOMLOC', title_blue_format)
        sheet.write(0, 13, 'TIPLOC', title_blue_format)
        sheet.write(0, 14, 'IDTPRISALCUI', title_blue_format)
        sheet.write(0, 15, 'APEPAT', title_blue_format)
        sheet.write(0, 16, 'APEMAT', title_blue_format)
        sheet.write(0, 17, 'NOMPER', title_blue_format)
        sheet.write(0, 18, 'FECNAC', title_blue_format)
        sheet.write(0, 19, 'NUMDOC', title_blue_format)
        sheet.write(0, 20, 'IDTTIPSEX', title_blue_format)
        sheet.write(0, 21, 'EST_CIVIL', title_blue_format)
        sheet.write(0, 22, 'NIVEL_EDUCA', title_blue_format)
        sheet.write(0, 23, 'EDAD_CUI', title_blue_format)
        sheet.write(0, 24, 'CAN_MESES_PROG', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)

        for cg in ut.ut_comites_gestion:
            for local in cg.local_ids:
                for mad in local.madres_cuidadoras_ids:
                    date_str = '%s-%s-%s' % (anio.name, str(mes), '01')
                    cant = cantidad_meses(date_str, mad.fecha_ingreso)
                    if cant >= 0:
                        sheet.write(row, 0, ut.name, body_format)
                        sheet.write(row, 1, cg.distrito_id.code, body_format)
                        sheet.write(row, 2, cg.departamento_id.name, body_format)
                        sheet.write(row, 3, cg.provincia_id.name, body_format)
                        sheet.write(row, 4, cg.distrito_id.name, body_format)
                        sheet.write(row, 5, cg.id, body_format)
                        sheet.write(row, 6, cg.name, body_format)
                        sheet.write(row, 7, local.distrito_id.code, body_format)
                        sheet.write(row, 8, local.departamento_id.name, body_format)
                        sheet.write(row, 9, local.provincia_id.name, body_format)
                        sheet.write(row, 10, local.distrito_id.name, body_format)
                        sheet.write(row, 11, local.id, body_format)
                        sheet.write(row, 12, local.name, body_format)
                        sheet.write(row, 13, local.tipo_local.upper(), body_format)
                        sheet.write(row, 14, mad.salas_id.id, body_format)
                        sheet.write(row, 15, mad.person_id.firstname, body_format)
                        sheet.write(row, 16, mad.person_id.secondname, body_format)
                        sheet.write(row, 17, mad.person_id.name, body_format)
                        sheet.write(row, 18, mad.person_id.birthdate, body_format)
                        sheet.write(row, 19, mad.person_id.document_number, body_format)
                        sheet.write(row, 20, mad.person_id.gender.upper(), body_format)
                        sheet.write(row, 21, mad.person_id.marital_status.upper(), body_format)
                        sheet.write(row, 22, mad.person_id.nivel_educativo_id.name.upper(), body_format)
                        sheet.write(row, 23, mad.person_id.age, body_format)
                        sheet.write(row, 24, cant, body_format)
                    row += 1


CuidadorasAsistenciaReportXls('report.base_cuna.CuidadorasAsistenciaReportXls.xlsx', 'dynamic.report.wizard')


class RNUReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1
        ut = lines.unidad_territorial_id
        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('RNU')

        sheet.write(0, 0, 'CO_PROGRAMA_SOCIAL', title_blue_format)
        sheet.write(0, 1, 'CO_GENERADO', title_blue_format)
        sheet.write(0, 2, 'TI_DOI_BENEFICIARIO', title_blue_format)
        sheet.write(0, 3, 'NU_DOI_BENEFICIARIO', title_blue_format)
        sheet.write(0, 4, u'AÑO', title_blue_format)
        sheet.write(0, 5, 'MES', title_blue_format)
        sheet.write(0, 6, 'CO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 7, 'AP_PATERNO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 8, 'AP_MATERNO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 9, 'NO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 10, 'TI_SEXO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 11, 'FE_NACIMIENTO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 12, 'UN_UBIGEO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 13, 'DI_BENEFICIARIO', title_blue_format)
        sheet.write(0, 14, 'NU_LATITUD', title_blue_format)
        sheet.write(0, 15, 'NU_LONGITUD', title_blue_format)
        sheet.write(0, 16, 'CO_CENTRO_POBLADO', title_blue_format)
        sheet.write(0, 17, 'NO_CENTRO_POBLADO', title_blue_format)
        sheet.write(0, 18, 'ES_CIVIL_BENEFICIARIO', title_blue_format)
        sheet.write(0, 19, 'TI_TIENE_APODERADO', title_blue_format)
        sheet.write(0, 20, 'TI_DOI_APODERADO', title_blue_format)
        sheet.write(0, 21, 'NU_DOI_APODERADO', title_blue_format)
        sheet.write(0, 22, 'AP_PATERNO_APODERADO', title_blue_format)
        sheet.write(0, 23, 'AP_MATERNO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'NO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'TI_SEXO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'FE_NACIMIENTO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'NU_UBIGEO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'TI_RELACION_BENEFICIARIO', title_blue_format)
        sheet.write(0, 24, 'ES_CIVIL_APODERADO', title_blue_format)
        sheet.write(0, 24, 'FE_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'TI_TRANSFERENCIA', title_blue_format)
        sheet.write(0, 24, 'TI_FINANCIAMIENTO', title_blue_format)
        sheet.write(0, 24, 'CO_CENTROAFILIACION', title_blue_format)
        sheet.write(0, 24, 'NO_CENTRO_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'ES_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'NU_UBIGEO_CENTRO_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'CO_CENTRO_POBLADO_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'NO_CENTRO_POBLADO_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'TI_DESAFILIACION', title_blue_format)
        sheet.write(0, 24, 'FE_DESAFILIACION', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)

        for cg in ut.ut_comites_gestion:
            for local in cg.local_ids:
                for mad in local.madres_cuidadoras_ids:
                    date_str = '%s-%s-%s' % (anio.name, str(mes), '01')
                    cant = cantidad_meses(date_str, mad.fecha_ingreso)
                    if cant >= 0:
                        sheet.write(row, 0, ut.name, body_format)
                        sheet.write(row, 1, cg.distrito_id.code, body_format)
                        sheet.write(row, 2, cg.departamento_id.name, body_format)
                        sheet.write(row, 3, cg.provincia_id.name, body_format)
                        sheet.write(row, 4, cg.distrito_id.name, body_format)
                        sheet.write(row, 5, cg.id, body_format)
                        sheet.write(row, 6, cg.name, body_format)
                        sheet.write(row, 7, local.distrito_id.code, body_format)
                        sheet.write(row, 8, local.departamento_id.name, body_format)
                        sheet.write(row, 9, local.provincia_id.name, body_format)
                        sheet.write(row, 10, local.distrito_id.name, body_format)
                        sheet.write(row, 11, local.id, body_format)
                        sheet.write(row, 12, local.name, body_format)
                        sheet.write(row, 13, local.tipo_local.upper(), body_format)
                        sheet.write(row, 14, mad.salas_id.id, body_format)
                        sheet.write(row, 15, mad.person_id.firstname, body_format)
                        sheet.write(row, 16, mad.person_id.secondname, body_format)
                        sheet.write(row, 17, mad.person_id.name, body_format)
                        sheet.write(row, 18, mad.person_id.birthdate, body_format)
                        sheet.write(row, 19, mad.person_id.document_number, body_format)
                        sheet.write(row, 20, mad.person_id.gender.upper(), body_format)
                        sheet.write(row, 21, mad.person_id.marital_status.upper(), body_format)
                        sheet.write(row, 22, mad.person_id.nivel_educativo_id.name.upper(), body_format)
                        sheet.write(row, 23, mad.person_id.age, body_format)
                        sheet.write(row, 24, cant, body_format)
                    row += 1


RNUReportXls('report.base_cuna.RNUReportXls.xlsx', 'dynamic.report.wizard')


class FacilitadorasVisitasXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 3
        ut = lines.unidad_territorial_id
        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('FACILITADORAS - VISITAS')

        sheet.merge_range('C2:G2', 'UBICACIÓN CG SEGÚN REQUERIMIENTOS', title_blue_format)
        sheet.merge_range('H2:M2', 'UBICACIÓN DE FACILITADORAS ACTUALMENTE', title_blue_format)
        sheet.merge_range('N2:P2', 'DATOS DEL COMITÉ DE GESTIÓN QUE REALIZÓ REQUERIMIENTO', title_blue_format)
        sheet.merge_range('N2:P2', 'DATOS DEL COMITÉ DE GESTIÓN QUE REALIZÓ REQUERIMIENTO', title_blue_format)
        sheet.merge_range('Q2:X2', 'DATOS PERSONALES', title_blue_format)
        sheet.merge_range('Y2:AD2', 'DISCAPACIDAD', title_blue_format)
        sheet.merge_range('AE2:AK2', 'BENEFICIARIA DE ALGUN PROGRAMA SOCIAL', title_blue_format)
        sheet.merge_range('AL2:AT2', 'INFORMACIÓN COMPLEMENTARIA', title_blue_format)
        sheet.merge_range('Q1:AT1', 'DATOS DE LA FACILITADORA', title_blue_format)

        sheet.write(2, 0, u'AÑO', title_blue_format)
        sheet.write(2, 1, 'MES', title_blue_format)
        sheet.write(2, 2, 'CGREQ_UBIGEO', title_blue_format)
        sheet.write(2, 3, 'UNIDAD_TERRITORIAL_NUEVA', title_blue_format)
        sheet.write(2, 4, 'DEPARTAMENTO', title_blue_format)
        sheet.write(2, 5, 'PROVINCIA', title_blue_format)
        sheet.write(2, 6, 'DISTRITO', title_blue_format)

        sheet.write(2, 7, 'FAC_ACTUAL_UBI', title_blue_format)
        sheet.write(2, 8, 'FAC_ACTUAL_DEP', title_blue_format)
        sheet.write(2, 9, 'FAC_ACTUAL_PROV', title_blue_format)
        sheet.write(2, 10, 'FAC_ACTUAL_DIST', title_blue_format)
        sheet.write(2, 11, 'FAC_ACTUAL_CCPP', title_blue_format)
        sheet.write(2, 12, 'FAC_ACTUAL_NOMCEPOB', title_blue_format)

        sheet.write(2, 13, 'CG_ID', title_blue_format)
        sheet.write(2, 14, 'CODCOM', title_blue_format)
        sheet.write(2, 15, 'COMITE_GESTION', title_blue_format)

        sheet.write(2, 16, 'FAC_ID_VIS', title_blue_format)
        sheet.write(2, 17, 'FAC_PATERNO', title_blue_format)
        sheet.write(2, 18, 'FAC_MATERNO', title_blue_format)
        sheet.write(2, 19, 'FAC_NOMBRES', title_blue_format)
        sheet.write(2, 20, 'FAC_NACIMIENTO', title_blue_format)
        sheet.write(2, 21, 'TIPO_DOCUMENTO', title_blue_format)
        sheet.write(2, 22, 'NUMDOC', title_blue_format)
        sheet.write(2, 23, 'FAC_SEXO', title_blue_format)

        sheet.write(2, 24, 'DISC_VISUAL', title_blue_format)
        sheet.write(2, 25, 'DISC_OIR', title_blue_format)
        sheet.write(2, 26, 'DISC_HABLAR', title_blue_format)
        sheet.write(2, 27, 'DISC_EXTREMIDADES', title_blue_format)
        sheet.write(2, 28, 'DISC_MENTAL', title_blue_format)
        sheet.write(2, 29, 'NO_TIENE_DISCAPACIDAD', title_blue_format)

        sheet.write(2, 30, 'CUNAMAS', title_blue_format)
        sheet.write(2, 31, 'QALI_WARMA', title_blue_format)
        sheet.write(2, 32, 'FONCODES', title_blue_format)
        sheet.write(2, 33, 'JUNTOS', title_blue_format)
        sheet.write(2, 34, 'PENSION_65', title_blue_format)
        sheet.write(2, 35, 'OTRO', title_blue_format)
        sheet.write(2, 36, 'NO_BENEF_PROGRAMA', title_blue_format)

        sheet.write(2, 37, 'ESTADO_CIVIL', title_blue_format)
        sheet.write(2, 38, 'ES_JEFE_HOGAR', title_blue_format)
        sheet.write(2, 39, 'LEE_ESCRIBE', title_blue_format)
        sheet.write(2, 40, 'NRO_HIJOS', title_blue_format)
        sheet.write(2, 41, 'RELIGION', title_blue_format)
        sheet.write(2, 42, 'LENGUA_MATERNA', title_blue_format)
        sheet.write(2, 43, 'NIVEL_EDUCATIVO', title_blue_format)
        sheet.write(2, 44, 'ULT_ANIO_APROBADO', title_blue_format)
        sheet.write(2, 45, 'OCUPACION', title_blue_format)

        sheet.write(2, 46, 'FECHA_REPORTE', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)

        for cg in ut.ut_comites_gestion:
            facilitadores = cg.afiliaciones_ids.filtered(lambda x: x.cargo == 'FACILITADOR')
            req = cg.requerimientos_ids.filtered(
                lambda x: x.anio == anio and x.mes == mes and x.servicio == 'saf' and x.comite_gestion_id == cg
            )
            if req:
                for fac in facilitadores:
                    validado = verificar_fechas(fac, anio, mes)  # valida facilitador activo dentro del mes/año
                    if validado:
                        sheet.write(row, 0, anio.name, body_format)
                        sheet.write(row, 1, mes, body_format)
                        sheet.write(row, 2, req.distrito_id.code, body_format)
                        sheet.write(row, 3, req.unidad_territorial_id.name, body_format)
                        sheet.write(row, 4, req.departamento_id.name, body_format)
                        sheet.write(row, 5, req.provincia_id.name, body_format)
                        sheet.write(row, 6, req.distrito_id.name, body_format)
                        sheet.write(row, 7, cg.distrito_id.code, body_format)
                        sheet.write(row, 8, cg.departamento_id.name, body_format)
                        sheet.write(row, 9, cg.provincia_id.name, body_format)
                        sheet.write(row, 10, cg.distrito_id.name, body_format)
                        sheet.write(row, 11, cg.centro_poblado_id.code, body_format)
                        sheet.write(row, 12, cg.centro_poblado_id.name, body_format)
                        sheet.write(row, 13, cg.id, body_format)
                        sheet.write(row, 14, cg.codigo, body_format)
                        sheet.write(row, 15, cg.name, body_format)
                        sheet.write(row, 16, fac.id, body_format)
                        sheet.write(row, 17, fac.person_id.firstname, body_format)
                        sheet.write(row, 18, fac.person_id.secondname, body_format)
                        sheet.write(row, 19, fac.person_id.name, body_format)
                        sheet.write(row, 20, fac.person_id.birthdate, body_format)
                        sheet.write(row, 21, fac.person_id.type_document, body_format)
                        sheet.write(row, 22, fac.person_id.document_number, body_format)
                        sheet.write(row, 23, fac.person_id.gender.upper(), body_format)
                        sheet.write(
                            row, 24, buscar_discapacidad('DISC_VISUAL', fac.person_id.discapacidad_id), body_format
                        )
                        sheet.write(
                            row, 25, buscar_discapacidad('DISC_OIR', fac.person_id.discapacidad_id), body_format
                        )
                        sheet.write(
                            row, 26, buscar_discapacidad('DISC_HABLAR', fac.person_id.discapacidad_id), body_format
                        )
                        sheet.write(
                            row, 27, buscar_discapacidad('DISC_EXTREMIDADES', fac.person_id.discapacidad_id),
                            body_format
                        )
                        sheet.write(
                            row, 28, buscar_discapacidad('DISC_MENTAL', fac.person_id.discapacidad_id), body_format
                        )
                        sheet.write(
                            row, 29, buscar_discapacidad('NO_TIENE_DISCAPACIDAD', fac.person_id.discapacidad_id),
                            body_format
                        )

                        sheet.write(
                            row, 30, buscar_tabla_base(u'CunaMás', fac.person_id.programa_social_ids), body_format
                        )
                        sheet.write(
                            row, 31, buscar_tabla_base('Qali Warma', fac.person_id.programa_social_ids), body_format
                        )
                        sheet.write(
                            row, 32, buscar_tabla_base('Foncodes', fac.person_id.programa_social_ids), body_format
                        )
                        sheet.write(row, 33, buscar_tabla_base('Juntos', fac.person_id.programa_social_ids),
                                    body_format)
                        sheet.write(
                            row, 34, buscar_tabla_base(u'Pensión', fac.person_id.programa_social_ids), body_format
                        )
                        sheet.write(row, 35, buscar_tabla_base('Otro', fac.person_id.programa_social_ids),
                                    body_format)
                        sheet.write(row, 36, buscar_tabla_base('Ninguno', fac.person_id.programa_social_ids),
                                    body_format)
                        sheet.write(row, 37, fac.person_id.marital_status, body_format)
                        sheet.write(row, 38, buscar_jefe_hogar(fac.person_id.integrantes_lines_ids), body_format)
                        sheet.write(row, 39, 'SI' if fac.person_id.sabe_leer_y_escribir else 'NO', body_format)
                        sheet.write(row, 40, fac.person_id.nro_hijos, body_format)
                        sheet.write(row, 41, fac.person_id.religion_id.name, body_format)
                        sheet.write(row, 42, fac.person_id.lengua_materna_id.name, body_format)
                        sheet.write(row, 43, fac.person_id.nivel_educativo_id.name, body_format)
                        sheet.write(row, 44, fac.person_id.ultimo_grado_apr_id.name, body_format)
                        sheet.write(row, 45, fac.person_id.ocupacion_id.name, body_format)
                        sheet.write(row, 46, lines.fecha_creacion, body_format)
                        row += 1


FacilitadorasVisitasXls('report.base_cuna.FacilitadorasVisitasXls.xlsx', 'dynamic.report.wizard')


class DatosObservadosXls(ReportXlsx):

    def cantidad_visitas(self, lines):
        mas_5 = ante_fec_nac = 0
        for line in lines:
            if len(line.visita_ids) > 5:
                mas_5 += 1
            for visita in line.visita_ids:
                if visita.fecha_visita < line.fecha_nac_ges:
                    ante_fec_nac += 1
        return mas_5, ante_fec_nac

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('DATOS OBSERVADOS')

        uts = self.env['hr.department'].search([('is_office','=',True)])

        sheet.write(0, 0, 'N°', title_blue_format)
        sheet.write(0, 1, 'UT', title_blue_format)
        sheet.write(0, 2, 'N_CG', title_blue_format)
        sheet.write(0, 3, 'N_FAMILIAS', title_blue_format)
        sheet.write(0, 4, 'N_NINIO', title_blue_format)
        sheet.write(0, 5, 'N_REGISTROS', title_blue_format)
        sheet.write(0, 6, 'GEST_12_50', title_blue_format)
        sheet.write(0, 7, 'NIN0_FUERA_EDAD', title_blue_format)
        sheet.write(0, 8, 'SIN VISITAS', title_blue_format)
        sheet.write(0, 9, 'MAS 5 VISITAS', title_blue_format)
        sheet.write(0, 10, 'NAC ANT ATENC', title_blue_format)
        sheet.write(0, 11, 'TIENE NO TIENE DISCA', title_blue_format)
        sheet.write(0, 12, 'DISCA MAYOR 3', title_blue_format)
        sheet.write(0, 13, 'FAM_MAS_5_VIS_RECO', title_blue_format)
        sheet.write(0, 14, 'VER_OBS', title_blue_format)

        i = 1
        for ut in uts:
            familias = ninios = visita_ninio = no_visita_ninio = madre_gestante = ninio_fuera_edad = mas_5visitas = \
                ante_fec_nac = tiene_no_tiene_dis = dis_mayor_3 = mas_5visitas_rec = 0
            for cg in ut.ut_comites_gestion:
                familias += len(cg.familias_ids)
                for familia in cg.familias_ids:
                    for integrante in familia.integrantes_ids:
                        if integrante.parentesco_id.name == 'Niño(a)':
                            ninios += 1
                            if integrante.integrante_id.months > 36:
                                ninio_fuera_edad += 1
                            if integrante.fichas_rec_ids or integrante.fichas_fort_ids or integrante.ficha_gestante_ids:
                                mas_5visitas_a, ante_fec_nac_a = self.cantidad_visitas(integrante.fichas_rec_ids)
                                mas_5visitas_b, ante_fec_nac_b = self.cantidad_visitas(integrante.fichas_fort_ids)
                                mas_5visitas_c, ante_fec_nac_c = self.cantidad_visitas(integrante.ficha_gestante_ids)
                                mas_5visitas_rec += mas_5visitas_a
                                mas_5visitas += mas_5visitas_a + mas_5visitas_b + mas_5visitas_c
                                ante_fec_nac += ante_fec_nac_a + ante_fec_nac_b + ante_fec_nac_c
                                visita_ninio += 1
                            else:
                                no_visita_ninio += 1
                        if integrante.integrante_id.discapacidad_id:
                            disc_no = integrante.integrante_id.discapacidad_id.filtered(
                                lambda x: x.abrev == 'NO_TIENE_DISCAPACIDAD'
                            )
                            if len(integrante.integrante_id.discapacidad_id) > 3:
                                dis_mayor_3 += 1
                            if disc_no and len(integrante.integrante_id.discapacidad_id) > 1:
                                tiene_no_tiene_dis += 1
                        if integrante.madre_gestante and 12 <= integrante.integrante_id.months / 12 <= 50:
                            madre_gestante += 1


            sheet.write(i, 0, i, body_format)
            sheet.write(i, 1, ut.name, body_format)
            sheet.write(i, 2, len(ut.ut_comites_gestion), body_format)
            sheet.write(i, 3, familias, body_format)
            sheet.write(i, 4, ninios, body_format)
            sheet.write(i, 5, visita_ninio, body_format)
            sheet.write(i, 6, madre_gestante, body_format)
            sheet.write(i, 7, ninio_fuera_edad, body_format)
            sheet.write(i, 8, ninio_fuera_edad, body_format)
            sheet.write(i, 9, no_visita_ninio, body_format)
            sheet.write(i, 10, mas_5visitas, body_format)
            sheet.write(i, 11, tiene_no_tiene_dis, body_format)
            sheet.write(i, 12, dis_mayor_3, body_format)
            sheet.write(i, 13, mas_5visitas_rec, body_format)
            sheet.write(i, 14, '=SUM(G%d:N%d)' % (i + 1, i + 1), body_format)
            i += 1

DatosObservadosXls('report.base_cuna.DatosObservadosXls.xlsx', 'dynamic.report.wizard')


class SaludSafXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('DATOS')

        row = 1
        ut = lines.unidad_territorial_id
        anio = lines.anio
        mes = lines.mes

        sheet.write(0, 0, 'UNIDAD TERRITORIAL', title_blue_format)
        sheet.write(0, 1, 'CODIGO CENTRO POBLADO', title_blue_format)
        sheet.write(0, 2, 'CODIGO COMITE', title_blue_format)
        sheet.write(0, 3, 'NOMBRE COMITE', title_blue_format)
        sheet.write(0, 4, 'NINIO_SAF_ID', title_blue_format)
        sheet.write(0, 5, 'APELLIDO_PAT', title_blue_format)
        sheet.write(0, 6, 'APELLIDO_MAT', title_blue_format)
        sheet.write(0, 7, 'NOMBRE_NINIO', title_blue_format)
        sheet.write(0, 8, 'NRO DNI', title_blue_format)
        sheet.write(0, 9, 'FECHA NAC', title_blue_format)
        sheet.write(0, 10, 'NAC EDADNNMES', title_blue_format)
        sheet.write(0, 11, 'CODIGO_FACILITADOR', title_blue_format)
        sheet.write(0, 12, 'CODIGO_FAMILIA', title_blue_format)
        sheet.write(0, 13, 'USUARIO_REGISTRO', title_blue_format)
        sheet.write(0, 14, 'FECHA_EXPORTACION', title_blue_format)
        sheet.write(0, 15, 'ESTABLECIMIENTO_SALUD', title_blue_format)
        sheet.write(0, 16, 'PESO_AL_NACER', title_blue_format)
        sheet.write(0, 17, 'TALLA_AL_NACER', title_blue_format)
        sheet.write(0, 18, 'TIPO_ESTABLECIMIENTO', title_blue_format)
        sheet.write(0, 19, 'FECHA_RECIBIO_MICRONUTRIENTES', title_blue_format)
        sheet.write(0, 20, 'NRO_MESES_MICRONUTRIENTES', title_blue_format)
        sheet.write(0, 21, 'TIENE_ALERGFIA', title_blue_format)
        sheet.write(0, 22, 'ALERGIA', title_blue_format)
        sheet.write(0, 23, 'TIENE_ENFERMEDAD_NACIMIENTO', title_blue_format)
        sheet.write(0, 24, 'ENFERMEDAD_NACIMIENTO', title_blue_format)
        sheet.write(0, 25, 'DIAGNOSTICO_NUTRICIONAL_TE', title_blue_format)
        sheet.write(0, 26, 'DIAGNOSTICO_NUTRICIONAL_PT', title_blue_format)
        sheet.write(0, 27, 'DIAGNOSTICO_NUTRICIONAL_PE', title_blue_format)
        sheet.write(0, 28, 'TALLA', title_blue_format)
        sheet.write(0, 29, 'PARASITOSIS_EXAMEN', title_blue_format)
        sheet.write(0, 30, 'PARASITOSIS_RESULTADO_TIENE', title_blue_format)
        sheet.write(0, 31, 'PARASITOSIS_TRATAMIENTO', title_blue_format)
        sheet.write(0, 32, 'RECIBE_MICRONUTRIENTES_EESS', title_blue_format)
        sheet.write(0, 33, 'RECIBE_SULFATO_FERROSO_EESS', title_blue_format)
        sheet.write(0, 34, 'PESO', title_blue_format)
        sheet.write(0, 35, 'TIPO_RIESGO_NUTRICIONAL_RT', title_blue_format)
        sheet.write(0, 36, 'TIPO_RIESGO_NUTRICIONAL_RP', title_blue_format)
        sheet.write(0, 37, 'TIPO_RIESGO_NUTRICIONAL_RD', title_blue_format)
        sheet.write(0, 38, 'PENULTIMOCONTROL', title_blue_format)
        sheet.write(0, 39, 'ULTIMOCONTROL', title_blue_format)
        sheet.write(0, 40, 'BCG', title_blue_format)
        sheet.write(0, 41, 'HVB', title_blue_format)
        sheet.write(0, 42, 'ANTIAMARILICA', title_blue_format)
        sheet.write(0, 43, 'INFLUENZA_1', title_blue_format)
        sheet.write(0, 44, 'INFLUENZA_2', title_blue_format)
        sheet.write(0, 45, 'INFLUENZA_3', title_blue_format)
        sheet.write(0, 46, 'IPV_APO_1', title_blue_format)
        sheet.write(0, 47, 'IPV_APO_2', title_blue_format)
        sheet.write(0, 48, 'NEUMOCOCO_1', title_blue_format)
        sheet.write(0, 49, 'NEUMOCOCO_2', title_blue_format)
        sheet.write(0, 50, 'NEUMOCOCO_3', title_blue_format)
        sheet.write(0, 51, 'NEUMOCOCO_4', title_blue_format)
        sheet.write(0, 52, 'PENTAVALENTE_1', title_blue_format)
        sheet.write(0, 53, 'PENTAVALENTE_2', title_blue_format)
        sheet.write(0, 54, 'PENTAVALENTE_3', title_blue_format)
        sheet.write(0, 55, 'REFUERZO_APO_1', title_blue_format)
        sheet.write(0, 56, 'REFUERZO_APO_2', title_blue_format)
        sheet.write(0, 57, 'REFUERZO_DPT', title_blue_format)
        sheet.write(0, 58, 'ROTAVIRUS_1', title_blue_format)
        sheet.write(0, 59, 'ROTAVIRUS_2', title_blue_format)
        sheet.write(0, 60, 'SPR_1', title_blue_format)
        sheet.write(0, 61, 'SPR_2', title_blue_format)
        sheet.write(0, 62, 'ANIO', title_blue_format)
        sheet.write(0, 63, 'MES', title_blue_format)
        sheet.write(0, 64, 'FECHA REPORTE', title_blue_format)

        row = 1

        fichas_salud_saf = self.env['fichas.salud'].search([
            ('anio', '=', anio.id),
            ('mes', '=', mes),
        ])
    
        for rec in fichas_salud_saf:
            sheet.write(row, 0, rec.unidad_territorial_id.name, body_format)
            sheet.write(row, 1, rec.centro_poblado_id.name,body_format)
            sheet.write(row, 2, rec.comite_gestion_id.codigo, body_format)
            sheet.write(row, 3, rec.comite_gestion_id.name, body_format)
            sheet.write(row, 4, rec.ninio_id.id, body_format)
            sheet.write(row, 5, rec.ninio_id.integrante_id.firstname, body_format)
            sheet.write(row, 6, rec.ninio_id.integrante_id.secondname, body_format)
            sheet.write(row, 7, rec.ninio_id.integrante_id.name, body_format)
            sheet.write(row, 8, rec.ninio_id.integrante_id.document_number, body_format)
            sheet.write(row, 9, rec.ninio_id.integrante_id.birthdate, body_format)
            sheet.write(row, 10, rec.ninio_id.integrante_id.months, body_format)
            sheet.write(row, 11, rec.facilitador_id.name, body_format)
            sheet.write(row, 12, rec.familia_id.id, body_format)
            sheet.write(row, 13, 'USUARIO_REGISTRO', body_format)
            sheet.write(row, 14, 'FECHA_EXPORTACION', body_format)
            sheet.write(row, 15, 'ESTABLECIMIENTO_SALUD', body_format)
            sheet.write(row, 16, 'PESO_AL_NACER', body_format)
            sheet.write(row, 17, 'TALLA_AL_NACER', body_format)
            sheet.write(row, 18, 'TIPO_ESTABLECIMIENTO', body_format)
            sheet.write(row, 19, 'FECHA_RECIBIO_MICRONUTRIENTES', body_format)
            sheet.write(row, 20, 'NRO_MESES_MICRONUTRIENTES', body_format)
            sheet.write(row, 21, 'TIENE_ALERGFIA', body_format)
            sheet.write(row, 22, 'ALERGIA', body_format)
            sheet.write(row, 23, 'TIENE_ENFERMEDAD_NACIMIENTO', body_format)
            sheet.write(row, 24, 'ENFERMEDAD_NACIMIENTO', body_format)
            sheet.write(row, 25, 'DIAGNOSTICO_NUTRICIONAL_TE', body_format)
            sheet.write(row, 26, 'DIAGNOSTICO_NUTRICIONAL_PT', body_format)
            sheet.write(row, 27, 'DIAGNOSTICO_NUTRICIONAL_PE', body_format)
            sheet.write(row, 28, 'TALLA', body_format)
            sheet.write(row, 29, 'PARASITOSIS_EXAMEN', body_format)
            sheet.write(row, 30, 'PARASITOSIS_RESULTADO_TIENE', body_format)
            sheet.write(row, 31, 'PARASITOSIS_TRATAMIENTO', body_format)
            sheet.write(row, 32, 'RECIBE_MICRONUTRIENTES_EESS', body_format)
            sheet.write(row, 33, 'RECIBE_SULFATO_FERROSO_EESS', body_format)
            sheet.write(row, 34, 'PESO', body_format)
            sheet.write(row, 35, 'TIPO_RIESGO_NUTRICIONAL_RT', body_format)
            sheet.write(row, 36, 'TIPO_RIESGO_NUTRICIONAL_RP', body_format)
            sheet.write(row, 37, 'TIPO_RIESGO_NUTRICIONAL_RD', body_format)
            sheet.write(row, 38, 'PENULTIMOCONTROL', body_format)
            sheet.write(row, 39, 'ULTIMOCONTROL', body_format)
            sheet.write(row, 40, 'BCG', body_format)
            sheet.write(row, 41, 'HVB', body_format)
            sheet.write(row, 42, 'ANTIAMARILICA', body_format)
            sheet.write(row, 43, 'INFLUENZA_1', body_format)
            sheet.write(row, 44, 'INFLUENZA_2', body_format)
            sheet.write(row, 45, 'INFLUENZA_3', body_format)
            sheet.write(row, 46, 'IPV_APO_1', body_format)
            sheet.write(row, 47, 'IPV_APO_2', body_format)
            sheet.write(row, 48, 'NEUMOCOCO_1', body_format)
            sheet.write(row, 49, 'NEUMOCOCO_2', body_format)
            sheet.write(row, 50, 'NEUMOCOCO_3', body_format)
            sheet.write(row, 51, 'NEUMOCOCO_4', body_format)
            sheet.write(row, 52, 'PENTAVALENTE_1', body_format)
            sheet.write(row, 53, 'PENTAVALENTE_2', body_format)
            sheet.write(row, 54, 'PENTAVALENTE_3', body_format)
            sheet.write(row, 55, 'REFUERZO_APO_1', body_format)
            sheet.write(row, 56, 'REFUERZO_APO_2', body_format)
            sheet.write(row, 57, 'REFUERZO_DPT', body_format)
            sheet.write(row, 58, 'ROTAVIRUS_1', body_format)
            sheet.write(row, 59, 'ROTAVIRUS_2', body_format)
            sheet.write(row, 60, 'SPR_1', body_format)
            sheet.write(row, 61, 'SPR_2', body_format)
            sheet.write(row, 62, 'ANIO', body_format)
            sheet.write(row, 63, 'MES', body_format)
            sheet.write(row, 64, 'FECHA REPORTE', body_format)
            # i += 1

SaludSafXls('report.base_cuna.SaludSafXls.xlsx', 'dynamic.report.wizard')


#sheet.write(i, 14, '=SUM()', body_format)
            #  '=SUM(BF3:BF' + str(row1), total_blue_format)

        # sheet.write(0, 0, 'CGREQ_UBIGEO', title_blue_format)
        # sheet.write(0, 1, 'UNIDAD_TERRITORIAL', title_blue_format)
        # sheet.write(0, 2, 'CORD_UNIDAD_TERRITORIAL', title_blue_format)
        # sheet.write(0, 3, 'DEPARTAMENTO', title_blue_format)
        # sheet.write(0, 4, 'PROVINCIA', title_blue_format)
        # sheet.write(0, 5, 'DISTRITO', title_blue_format)
        # sheet.write(0, 6, 'ID_COMITE', title_blue_format)
        # sheet.write(0, 7, 'NUMERO_FACILITADOR', title_blue_format)
        # sheet.write(0, 8, 'NUMERO_FAMILIAS', title_blue_format)
        # sheet.write(0, 9, 'NOMUNI', title_blue_format)
        # sheet.write(0, 10, 'CODUBIGEO', title_blue_format)
        # sheet.write(0, 11, 'NOMDEP', title_blue_format)
        # sheet.write(0, 12, 'NOMPRO', title_blue_format)
        # sheet.write(0, 13, 'NOMDIS', title_blue_format)
        # sheet.write(0, 14, 'COD_AT', title_blue_format)
        # sheet.write(0, 15, 'DNI_AT', title_blue_format)
        # sheet.write(0, 16, 'NOM_AT', title_blue_format)
        # sheet.write(0, 17, 'APEPAT_AT', title_blue_format)
        # sheet.write(0, 18, 'APEMAT_AT', title_blue_format)
        # sheet.write(0, 19, 'SEXO_AT', title_blue_format)
        # sheet.write(0, 20, 'FECNAC_AT', title_blue_format)
        # sheet.write(0, 21, 'COD_AC', title_blue_format)
        # sheet.write(0, 22, 'DNI_AC', title_blue_format)
        # sheet.write(0, 23, 'NOM_AC', title_blue_format)
        # sheet.write(0, 24, 'APEPAT_AC', title_blue_format)
        # sheet.write(0, 25, 'APEMAT_AC', title_blue_format)
        # sheet.write(0, 26, 'SEXO_AC', title_blue_format)
        # sheet.write(0, 27, 'FECNAC_AC', title_blue_format)
        # sheet.write(0, 28, 'CG_ID', title_blue_format)
        # sheet.write(0, 29, 'CODCOM', title_blue_format)
        # sheet.write(0, 30, 'NOMCOM', title_blue_format)
        # sheet.write(0, 31, 'IDTCENPOB', title_blue_format)
        # sheet.write(0, 32, 'CODCENPOB', title_blue_format)
        # sheet.write(0, 33, 'NOMCENPOB', title_blue_format)
        # sheet.write(0, 34, 'ANIO', title_blue_format)
        # sheet.write(0, 35, 'MES', title_blue_format)
        # sheet.write(0, 36, 'CODIGO_HOGAR', title_blue_format)
        # sheet.write(0, 37, 'DIR', title_blue_format)
        # sheet.write(0, 38, 'CODFACILITADOR', title_blue_format)
        # sheet.write(0, 39, 'NOMBREFACILITADOR', title_blue_format)
        # sheet.write(0, 40, 'CODFACILITADORVISITA', title_blue_format)
        # sheet.write(0, 41, 'NOMBREFACILITADORVISITA', title_blue_format)
        # sheet.write(0, 42, 'TIPDOC_FACVISITA', title_blue_format)
        # sheet.write(0, 43, 'NUMDOC_FACVISITA', title_blue_format)
        # sheet.write(0, 44, 'SEX_FACVISITA', title_blue_format)
        # sheet.write(0, 45, 'FECNAC_FACVISITA', title_blue_format)
        # sheet.write(0, 46, 'EDAD_FACVISITA', title_blue_format)
        # sheet.write(0, 47, 'CODCUIPRI', title_blue_format)
        # sheet.write(0, 48, 'NOMPER', title_blue_format)
        # sheet.write(0, 49, 'APEPAT', title_blue_format)
        # sheet.write(0, 50, 'APEMAT', title_blue_format)
        # sheet.write(0, 51, 'NOMBRECUIPRI', title_blue_format)
        # sheet.write(0, 52, 'SEXOCUI', title_blue_format)
        # sheet.write(0, 53, 'PARENTESCO', title_blue_format)
        # sheet.write(0, 54, 'GESTANTE', title_blue_format)
        # sheet.write(0, 55, 'TIPODOCUMENTOCUIP', title_blue_format)
        # sheet.write(0, 56, 'DNICUIPRINICIPAL', title_blue_format)
        # sheet.write(0, 57, 'CUIDPRI_VALIDADORENIEC', title_blue_format)
        # sheet.write(0, 58, 'TIPOSEGAPOD', title_blue_format)
        # sheet.write(0, 59, 'CUIPRIN_ESTCIVIL', title_blue_format)
        # sheet.write(0, 60, 'FECHANACIMIENTOCUIPRI', title_blue_format)
        # sheet.write(0, 61, 'FECHAREGCUI', title_blue_format)
        # sheet.write(0, 62, 'EDADCUIDADOR', title_blue_format)
        # sheet.write(0, 63, 'CODNIN', title_blue_format)
        # sheet.write(0, 64, 'NOMBRENNCOMPLETO', title_blue_format)
        # sheet.write(0, 65, 'NOMBRENN', title_blue_format)
        # sheet.write(0, 66, 'APEPATNN', title_blue_format)
        # sheet.write(0, 67, 'APEMATNN', title_blue_format)
        # sheet.write(0, 68, 'SEXO', title_blue_format)
        # sheet.write(0, 69, 'FECCRE', title_blue_format)
        # sheet.write(0, 70, 'TIPOBENEFICIARIO', title_blue_format)
        # sheet.write(0, 71, 'BENEFICIARIO_ESTCIV', title_blue_format)
        # sheet.write(0, 72, 'TIPODOCUMENTO', title_blue_format)
        # sheet.write(0, 73, 'DNI', title_blue_format)
        # sheet.write(0, 74, 'BENEFICIARIO_VALIDADORENIEC', title_blue_format)
        # sheet.write(0, 75, 'SEGURO', title_blue_format)
        # sheet.write(0, 76, 'CODAFISEG', title_blue_format)
        # sheet.write(0, 77, 'FECNAC', title_blue_format)
        # sheet.write(0, 78, 'EDADNNMESVISITA', title_blue_format)
        # sheet.write(0, 79, 'EDADNNMESVISITA_ENT', title_blue_format)
        # sheet.write(0, 80, 'FCH_PRIM_VIS', title_blue_format)
        # sheet.write(0, 81, 'FCH_ULT_VIS', title_blue_format)
        # sheet.write(0, 82, 'EDAD_USUGEST', title_blue_format)
        # sheet.write(0, 83, 'CATEGORIA_EDAD_NIN', title_blue_format)
        # sheet.write(0, 84, 'F5', title_blue_format)
        # sheet.write(0, 85, 'NVF5', title_blue_format)
        # sheet.write(0, 86, 'NVEF5', title_blue_format)
        # sheet.write(0, 87, 'MXMGF5', title_blue_format)
        # sheet.write(0, 88, 'FECHA_VISITASF5', title_blue_format)
        #
        # sheet.write(0, 92, 'ASISTECSF5', title_blue_format)
        # sheet.write(0, 93, 'RECIBIOSFCSF5', title_blue_format)
        # sheet.write(0, 94, 'TIENE_SULFATO_HOGAR_F5', title_blue_format)
        # sheet.write(0, 95, 'CONSUMINO_SULFATO_VISITA_F5', title_blue_format)
        # sheet.write(0, 96, 'F6', title_blue_format)
        # sheet.write(0, 97, 'NVF6', title_blue_format)
        # sheet.write(0, 98, 'NVEF6', title_blue_format)
        # sheet.write(0, 99, 'MMNF6', title_blue_format)
        # sheet.write(0, 100, 'MXMGF6', title_blue_format)
        # sheet.write(0, 101, 'MMN_HOGARF6', title_blue_format)
        # sheet.write(0, 102, 'FECHA_VISITASF6', title_blue_format)
        # sheet.write(0, 103, 'ASISTECSF6', title_blue_format)
        # sheet.write(0, 104, 'RECIBIOSFCSF6', title_blue_format)
        # sheet.write(0, 105, 'TIENE_SULFATO_HOGAR_F6', title_blue_format)
        # sheet.write(0, 106, 'CONSUMINO_SULFATO_VISITA_F6', title_blue_format)
        # sheet.write(0, 107, 'CARTILLAS', title_blue_format)
        # sheet.write(0, 108, 'TOTALV', title_blue_format)
        # sheet.write(0, 109, 'CANT_VIST_CODHOGAR', title_blue_format)
        # sheet.write(0, 110, 'FAM_X_FAC', title_blue_format)
        # sheet.write(0, 111, 'MESGESUSU', title_blue_format)
        # sheet.write(0, 112, 'TOTAL_VMMN', title_blue_format)
        # sheet.write(0, 113, 'TOTAL_MMN_HOGAR', title_blue_format)
        # sheet.write(0, 113, 'PORC_C_MMN_6_18', title_blue_format)
        # sheet.write(0, 114, 'DISP_C_MMN_6_18', title_blue_format)
        # sheet.write(0, 115, 'TOVALV_ASISTE_CS', title_blue_format)
        # sheet.write(0, 116, 'TMMN_HOGAR', title_blue_format)
        # sheet.write(0, 117, 'ASISTE_CS', title_blue_format)
        # sheet.write(0, 118, 'TOVALV_RECIBIO_SFCS', title_blue_format)
        # sheet.write(0, 119, 'RECIBIO_SFCS', title_blue_format)
        #
        # # EA
        #
        # sheet.set_column('B:H', 20)
        # sheet.set_column('I:U', 30)
        # fichas = []
        # fichas.extend(self.env['ficha.reconocimiento'].search([
        #     ('mes', '=', mes),
        #     ('anio', '=', anio.id),
        #     ('unidad_territorial_id', '=', ut.id)
        # ]).mapped(lambda x: x))
        # fichas.extend(self.env['ficha.fortalecimiento'].search([
        #     ('mes', '=', mes),
        #     ('anio', '=', anio.id),
        #     ('unidad_territorial_id', '=', ut.id)
        # ]).mapped(lambda x: x))
        # fichas.extend(self.env['ficha.gestante'].search([
        #     ('mes', '=', mes),
        #     ('anio', '=', anio.id),
        #     ('unidad_territorial_id', '=', ut.id)
        # ]).mapped(lambda x: x))
        #
        # for ficha in fichas:
        #     sheet.write(row, 0, ficha.comite_gestion_id.distrito_id.code, body_format)
        #     sheet.write(row, 1, ficha.comite_gestion_id.name, body_format)
        #     sheet.write(row, 2, ficha.comite_gestion_id.name, body_format)
        #     sheet.write(row, 3, ficha.comite_gestion_id.departamento_id.name, body_format)
        #     sheet.write(row, 4, ficha.comite_gestion_id.provincia_id.name, body_format)
        #     sheet.write(row, 5, ficha.comite_gestion_id.distrito_id.name, body_format)
        #     sheet.write(row, 6, ficha.comite_gestion_id.id, body_format)
        #     facilitador = ficha.comite_gestion_id.afiliaciones_ids.filtered(lambda x: x.cargo == 'FACILITADOR')
        #     facilitador.filtered(lambda x: verificar_fechas(x, anio, mes) == True)
        #     sheet.write(row, 7, len(facilitador), body_format)
        #     sheet.write(row, 8, len(ficha.comite_gestion_id.familias_ids), body_format)
        #     sheet.write(row, 9, ficha.unidad_territorial_id.name, body_format)
        #     sheet.write(row, 10, ficha.unidad_territorial_id.distrito_id.code, body_format)
        #     sheet.write(row, 11, ficha.unidad_territorial_id.departamento_id.name, body_format)
        #     sheet.write(row, 12, ficha.unidad_territorial_id.provincia_id.name, body_format)
        #     sheet.write(row, 13, ficha.unidad_territorial_id.distrito_id.name, body_format)
        #     at = ficha.comite_gestion_id.miembros_equipo.filtered(
        #         lambda x: x.cargo == u'ACOMPAÑANTE TECNICO' and not x.fecha_fin
        #     )
        #     sheet.write(row, 14, at[0].id if at else '', body_format)
        #     sheet.write(row, 15, at[0].document_number if at else '', body_format)
        #     sheet.write(row, 16, at[0].firstname if at else '', body_format)
        #     sheet.write(row, 17, at[0].lastname if at else '', body_format)
        #     sheet.write(row, 18, at[0].secondname if at else '', body_format)
        #     sheet.write(row, 19, at[0].gender.upper() if at else '', body_format)
        #     sheet.write(row, 20, at[0].birthday if at else '', body_format)
        #     sheet.write(row, 21, at[1].id if len(at) > 1 else '', body_format)
        #     sheet.write(row, 22, at[1].document_number if len(at) > 1 else '', body_format)
        #     sheet.write(row, 23, at[1].firstname if len(at) > 1 else '', body_format)
        #     sheet.write(row, 24, at[1].lastname if len(at) > 1 else '', body_format)
        #     sheet.write(row, 25, at[1].secondname if len(at) > 1 else '', body_format)
        #     sheet.write(row, 26, at[1].gender.upper() if len(at) > 1 else '', body_format)
        #     sheet.write(row, 27, at[1].birthday if len(at) > 1 else '', body_format)
        #     sheet.write(row, 28, ficha.comite_gestion_id.id, body_format)
        #     sheet.write(row, 29, ficha.comite_gestion_id.codigo, body_format)
        #     sheet.write(row, 30, ficha.comite_gestion_id.name, body_format)
        #     sheet.write(row, 31, ficha.comite_gestion_id.centro_poblado_id.id, body_format)
        #     sheet.write(row, 32, ficha.comite_gestion_id.centro_poblado_id.code, body_format)
        #     sheet.write(row, 33, ficha.comite_gestion_id.centro_poblado_id.name, body_format)
        #     sheet.write(row, 34, anio.name, body_format)
        #     sheet.write(row, 35, mes, body_format)
        #     sheet.write(row, 36, ficha.cod_familia, body_format)
        #     dir = ficha.familia_id.vivienda_ids.filtered(lambda x: not x.fecha_fin)
        #     sheet.write(row, 37, dir.direccion, body_format)
        #     sheet.write(row, 38, ficha.cod_facilitador, body_format)
        #     sheet.write(row, 39, ficha.facilitador_id.person_id.display_name, body_format)
        #     sheet.write(row, 40, ficha.cod_facilitador, body_format)
        #     sheet.write(row, 41, ficha.facilitador_id.person_id.display_name, body_format)
        #     sheet.write(row, 42, ficha.facilitador_id.person_id.type_document, body_format)
        #     sheet.write(row, 43, ficha.facilitador_id.person_id.document_number, body_format)
        #     sheet.write(row, 44, ficha.facilitador_id.person_id.gender.upper(), body_format)
        #     sheet.write(row, 45, ficha.facilitador_id.person_id.birthdate, body_format)
        #     sheet.write(row, 46, ficha.facilitador_id.person_id.age, body_format)
        #     sheet.write(row, 47, ficha.cuid_principal_id.id, body_format)
        #     sheet.write(row, 48, ficha.cuid_principal_id.integrante_id.name, body_format)
        #     sheet.write(row, 49, ficha.cuid_principal_id.integrante_id.firstname, body_format)
        #     sheet.write(row, 50, ficha.cuid_principal_id.integrante_id.secondname, body_format)
        #     sheet.write(row, 51, ficha.cuid_principal_id.integrante_id.display_name, body_format)
        #     sheet.write(row, 52, ficha.cuid_principal_id.integrante_id.gender.upper(), body_format)
        #     sheet.write(row, 53, ficha.cuid_principal_id.parentesco_id.name, body_format)
        #     sheet.write(row, 54,'Gestante' if ficha._name == 'ficha.gestante' else 'No es Gestante', body_format)
        #     sheet.write(row, 55, ficha.cuid_principal_id.integrante_id.type_document.upper(), body_format)
        #     sheet.write(row, 56, ficha.cuid_principal_id.integrante_id.document_number, body_format)
        #     sheet.write(row, 57, ficha.cuid_principal_id.integrante_id.validado_reniec, body_format)
        #     sheet.write(row, 58, ficha.cuid_principal_id.integrante_id.seguro_id.name, body_format)
        #     sheet.write(row, 59, ficha.cuid_principal_id.integrante_id.marital_status.upper(), body_format)
        #     sheet.write(row, 60, ficha.cuid_principal_id.integrante_id.birthdate, body_format)
        #     sheet.write(row, 61, ficha.cuid_principal_id.fecha_inicio, body_format)
        #     sheet.write(row, 62, ficha.cuid_principal_id.integrante_id.age, body_format)
        #
        #     sheet.write(row, 63, ficha.gestante_id.integrante_id.id, body_format)
        #     sheet.write(row, 64, ficha.gestante_id.integrante_id.display_name, body_format)
        #     sheet.write(row, 65, ficha.gestante_id.integrante_id.name, body_format)
        #     sheet.write(row, 66, ficha.gestante_id.integrante_id.firstname, body_format)
        #     sheet.write(row, 67, ficha.gestante_id.integrante_id.secondname, body_format)
        #     sheet.write(row, 68, ficha.gestante_id.integrante_id.gender.upper(), body_format)
        #     sheet.write(row, 69, ficha.gestante_id.fecha_inicio, body_format)
        #     if ficha._name == 'ficha.gestante':
        #         tipo_benef = 'Gestante'
        #     else:
        #         if ficha.gestante_id.integrante_id.gender == 'm':
        #             tipo_benef = 'Niño'
        #         else:
        #             tipo_benef = 'Niña'
        #     sheet.write(row, 70, tipo_benef, body_format)
        #     sheet.write(row, 71, ficha.gestante_id.integrante_id.marital_status.upper(), body_format)
        #     sheet.write(row, 72, ficha.gestante_id.integrante_id.type_document.upper(), body_format)
        #     sheet.write(row, 73, ficha.gestante_id.integrante_id.document_number, body_format)
        #     sheet.write(row, 74, ficha.gestante_id.integrante_id.validado_reniec, body_format)
        #     sheet.write(row, 75, ficha.gestante_id.integrante_id.seguro_id.name, body_format)
        #     sheet.write(row, 76, ficha.gestante_id.integrante_id.codigo_seguro, body_format)
        #     sheet.write(row, 77, ficha.gestante_id.integrante_id.birthdate, body_format)
        #     sheet.write(row, 78, ficha.gestante_id.integrante_id.months, body_format)
        #     sheet.write(row, 79, ficha.gestante_id.integrante_id.months, body_format)
        #     fecha_pr_visita = ''
        #     fecha_ult_visita = ''
        #     if ficha.visita_ids and len(ficha.visita_ids) == 1:
        #         fecha_pr_visita = ficha.visita_ids[0].fecha_visita
        #     elif ficha.visita_ids and len(ficha.visita_ids) > 1:
        #         fecha_pr_visita = ficha.visita_ids[0].fecha_visita
        #         fecha_ult_visita = ficha.visita_ids[len(ficha.visita_ids) - 1].fecha_visita
        #     sheet.write(row, 80, fecha_pr_visita, body_format)
        #     sheet.write(row, 81, fecha_ult_visita, body_format)
        #     sheet.write(row, 82, ficha.gestante_id.integrante_id.years if tipo_benef == 'Gestante' else '', body_format)
        #     cat_ninio = dict(ficha.gestante_id.integrante_id._fields['cat_ninio'].selection).get(
        #         ficha.gestante_id.integrante_id.cat_ninio)
        #     sheet.write(row, 83, cat_ninio or '', body_format)
        #     sheet.write(row, 84, 1 if ficha._name in ['ficha.reconocimiento', 'ficha.fortalecimiento'] else '',
        #                 body_format)
        #     sheet.write(row, 85, len(
        #         ficha.visita_ids) if ficha._name in ['ficha.reconocimiento', 'ficha.fortalecimiento'] else '',
        #                 body_format)
        #     sheet.write(row, 86, len(
        #         ficha.visita_ids) if ficha._name in ['ficha.reconocimiento', 'ficha.fortalecimiento'] else '',
        #                 body_format)
        #     sheet.write(row, 87, len(ficha.visita_ids) if ficha._name in ['ficha.gestante'] else '', body_format)
        #     fechas_visita = ' '.join(x.fecha_visita for x in ficha.visita_ids)
        #     sheet.write(row, 88, fechas_visita , body_format)
        #     # CV
        #     row += 1

