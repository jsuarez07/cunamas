# -*- coding: utf-8 -*-

from dateutil.relativedelta import relativedelta
from odoo import fields, models, api
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime


def _drop_records(self, model, domain):
    self.env[model].search(domain).sudo().unlink()


def _set_fields_lines(self, lines, obj_id, type_name=None):
    """
        Completa la informacion de las afiliaciones dependiendo el tipo de cargo
    """
    for i in lines:
        if obj_id._name == 'afiliacion.organizacion':
            i.afiliacion_id = obj_id
            i.person_id = obj_id.person_id
            i.unidad_territorial_id = obj_id.unidad_territorial_id
        """Servicio alimentario"""
        if obj_id._name == 'pncm.infra.local':
            i.comite_gestion_id = obj_id.comite_id
            i.unidad_territorial_id = obj_id.ut_id
        if i.tipo_guia:
            type_name = i.tipo_guia.name
        tipo = self.env['tipo.persona'].search([('name', '=', type_name)])
        i.cargo = tipo.name
        i.tipo_guia = tipo


def accion_agregar(nombre, form_id, model, context):
    return {
        'type': 'ir.actions.act_window',
        'name': nombre,
        'view_type': 'form',
        'view_mode': 'form',
        'res_model': model,
        'views': [(form_id, 'form')],
        'view_id': form_id,
        'target': 'new',
        'context': context
    }


def filtrar_en_vistas(self, name, model, modo):
    """
        modo:
        # filtrar
        1 = unidad_territorial
        2 = servicios
        0 = ambos

    """
    action = {
        'name': name,
        'type': "ir.actions.act_window",
        'res_model': model,
        'view_type': "form",
        'view_mode': "tree,form",
    }
    user = self.env['res.users'].browse(self._uid)
    if modo == 0:
        if user.unidad_territorial_id and user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('unidad_territorial_id', '=', %d), ('services', '=', '%s')]" % \
                               (user.unidad_territorial_id.id, user.servicio)
        elif user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d)]" % user.unidad_territorial_id.id
        elif user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('services', '=', '%s')]" % user.servicio

    elif modo == 1:
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d)]" % user.unidad_territorial_id.id

    elif modo == 2:
        if user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('services', '=', '%s')]" % user.servicio
    return action


def validar_fechas(fecha_inicio, fecha_fin):
    if fecha_fin and fecha_inicio:
        if fecha_inicio >= fecha_fin:
            raise ValidationError('La fecha de fin no puede ser menor o igual a la fecha de inicio')


estado_servicio = [
    ('deteriorado', 'Deteriorado'),
    ('conservado', 'Conservado')
]

estados = [
    ('activo', 'Activo'),
    ('cerrado', 'Cerrado'),
]


class ActorComunal(models.Model):
    _inherit = 'pncm.comitegestion.actorcomunal'

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Integrante',
        required=True
    )


class UnidadFamiliar(models.Model):
    _name = 'unidad.familiar'
    _inherit = 'tabla.base'

    facilitador_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        string='Facilitador/a',
        domain="[('comite_gestion_id', '=', comite_gestion_id),"
               "('cargo','=', 'FACILITADOR'),"
               "('fecha_retiro', '=', False)]"
    )
    departamento_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento',
        compute='_compute_provincia_vivienda_dpt',
        store=True
    )
    provincia_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia',
        compute='_compute_provincia_vivienda_dpt',
        store=True
    )
    distrito_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Distrito',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', provincia_id)]",
        compute='_compute_provincia_vivienda_dpt',
        store=True
    )
    centro_poblado_id = fields.Many2one(
        comodel_name='res.country.state',
        string=u'Centro Poblado',
        required=True
    )
    vivienda_ids = fields.One2many(
        comodel_name='vivienda.familia',
        inverse_name='familia_id',
        string='Vivienda'
    )
    services = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar')
    ],
        string=u'Servicio',
        default=lambda self: False if self.env.user.servicio == 'ambos' else self.env.user.servicio,
        required=True
    )
    servicio_log = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
        ('ambos', u'Ambos'),
    ],
        string=u'Servicio usuario logueado',
        default=lambda self: self.env.user.servicio
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id,
        readonly=True
    )
    unidad_territorial_id_log = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='familias_ids',
        domain="[('unidad_territorial_id','=',unidad_territorial_id),"
               "('state', '=', 'activo'),"
               "('servicio', '=', services)]",
        string=u'Comité de Gestión',
        required=True
    )
    migraciones_ids = fields.Many2many(
        comodel_name='migracion.familia',
        string='Familia'
    )
    integrantes_ids = fields.One2many(
        comodel_name='integrante.unidad.familiar',
        inverse_name='familia_id',
        string='Integrantes'
    )
    state = fields.Selection(
        selection=[
            ('borrador', 'Borrador'),
            ('activo', 'Activo'),
            ('cerrado', 'Cerrado'),
        ],
        string='Estado',
        track_visibility='always',
        default='borrador'
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        default=fields.Date.today(),
        readonly=True
    )
    fecha_fin = fields.Date(
        string='Fecha Cierre',
        readonly=True
    )
    hogar_activo = fields.Boolean(
        string='tiene algun hogar activo?',
        compute='compute_hogar_activo',
        store=True
    )
    fecha_activo = fields.Date(
        string='Fecha Activacion'
    )
    fecha_cierre = fields.Date(
        string='Fecha Activacion'
    )

    @api.onchange('comite_gestion_id')
    def _al_cambiar_distrito(self):
        self.centro_poblado_id = False
        if self.comite_gestion_id:
            intervenciones = self.comite_gestion_id.intervenciones
            centro_poblado_ids = [obj.centro_poblado_id.id for obj in intervenciones]
            return {'domain': {'centro_poblado_id': [('id', 'in', centro_poblado_ids)]}}

    @api.multi
    @api.depends('comite_gestion_id')
    def _compute_provincia_vivienda_dpt(self):
        for rec in self:
            if rec.comite_gestion_id:
                rec.departamento_id = rec.comite_gestion_id.departamento_id
                rec.provincia_id = rec.comite_gestion_id.provincia_id
                rec.distrito_id = rec.comite_gestion_id.distrito_id

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.id)) for obj in self]

    @api.multi
    @api.depends('vivienda_ids')
    def compute_hogar_activo(self):
        for rec in self:
            line = rec.vivienda_ids.filtered(lambda x: x.state == 'activo')
            if line:
                rec.hogar_activo = True
            else:
                rec.hogar_activo = False

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)

    @api.multi
    def accion_agregar_integrante(self):
        form_id = self.env.ref('base_cuna.form_view_integrante_familia2').id
        context = {
            'default_familia_id': self.id,
            'default_unidad_territorial_id': self.unidad_territorial_id.id,
            'default_services': self.services,
        }
        return accion_agregar('Integrante Familia', form_id, 'integrante.unidad.familiar', context)

    @api.multi
    def accion_vivienda(self):
        if self.vivienda_ids:
            activos = self.vivienda_ids.filtered(lambda x: x.state == 'activo')
            if activos:
                raise ValidationError('No puede agregar otra vivienda mientras otra este activa.')

        form_id = self.env.ref('base_cuna.form_view_vivienda_familia_crear').id
        context ={
            'default_familia_id': self.id,
            'default_unidad_territorial_id': self.unidad_territorial_id.id,
            'default_comite_gestion_id': self.comite_gestion_id.id,
            'default_services': self.services,
            'default_departamento_id': self.departamento_id.id,
            'default_provincia_id': self.provincia_id.id,
            'default_distrito_id': self.distrito_id.id,
            'default_centro_poblado_id': self.centro_poblado_id.id,

        }
        return accion_agregar('Vivienda', form_id, 'vivienda.familia', context)

    @api.multi
    def estado_activo(self):
        self.validar_integrantes_de_familia()
        self.fecha_activo = fields.Date.today()
        self.state = 'activo'
        for line in self.integrantes_ids:
            line.activo = True

    @api.multi
    def estado_cierre(self):
        for line in self.integrantes_ids:
            line.fecha_fin = fields.Date.today()
            line.activo = False
            if line.parentesco_id.name == u'Niño(a)':
                if line.integrante_id.modulos_ninios_ids:
                    rec = line.integrante_id.modulos_ninios_ids.filtered(lambda x: not x.fecha_fin)
                    rec.fecha_fin = fields.Date.today()
                    rec.state = 'cerrado'
                    line.permitir_modulo = False
                    line.permitir_familia = False
        vivienda = self.vivienda_ids.filtered(lambda x: x.state == 'activo')
        if vivienda:
            vivienda.state = 'cerrado'
            vivienda.fecha_fin = fields.Date.today()
        self.fecha_cierre = fields.Date.today()
        self.state = 'cerrado'
        self.hogar_activo = False

    """
        Verifica que al crear o modificar el registro de vivienda:
        - Debe tener integrantes,
        - No debe haber unico miembro un niño
        - Debe tener solo un cuidador principal
    """
    def validar_integrantes_de_familia(self):
        if not self.integrantes_ids or not self.vivienda_ids:
            raise ValidationError('Debe agregar integrantes y vivienda al registro.')
        else:
            if len(self.integrantes_ids) == 1:
                if self.integrantes_ids[0].es_ninio:
                    raise ValidationError(u'No se puede crear registro si el único miembro es un niño.')
            else:
                cuidador = len(self.integrantes_ids.filtered(lambda x: x.es_cuidador_principal))
                if cuidador != 1:
                    raise ValidationError(u'Debe haber un cuidador principal en la familia.')

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas(self, 'Unidad Familiar', 'unidad.familiar', 0)


class IntegranteUnidadFamiliar(models.Model):
    _name = 'integrante.unidad.familiar'
    _inherit = 'tabla.base'

    activo = fields.Boolean()
    integrante_id = fields.Many2one(
        comodel_name='res.partner',
        inverse_name='integrantes_lines_ids',
        string='Integrante',
        domain="[('familia_id', '=', False)])"
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        default=fields.Date.today(),
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )
    es_jefe_hogar = fields.Boolean(
        string='¿Es jefe de hogar?',
    )
    es_cuidador_principal = fields.Boolean(
        string='¿Es cuidador principal?'
    )
    es_cuidador_sec = fields.Boolean(
        string='¿Es cuidador secundario?'
    )
    parentesco_id = fields.Many2one(
        comodel_name='tipo.parentesco',
        string='Parentesco',
        required=True
    )
    tipo_parentesco = fields.Char(
        string='Tipo Parentesco',
        compute='compute_parentesco_id',
        store=True
    )
    services = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
    ],
        string=u'Servicio'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]"
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comité de Gestión',
        related='familia_id.comite_gestion_id',
        store=True
    )
    vivienda_id = fields.Many2one(
        comodel_name='vivienda.familia',
        string='Vivienda'
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        inverse_name='integrantes_ids',
        string='Familia'
    )
    permitir_modulo = fields.Boolean(
        string='Esta activo en algun modulo?',
        related='integrante_id.permitir_modulo',
        store=True
    )
    es_ninio = fields.Boolean(
        string='es ninio?',
        compute='compute_parentesco_id',
        store=True
    )
    madre_gestante = fields.Boolean(
        string='Madre Gestante',
        compute='compute_madre_gestante',
        store=True
    )
    name = fields.Char(
        string='Nombres y Apellidos',
        related='integrante_id.display_name'
    )

    @api.multi
    @api.depends('integrante_id')
    def compute_madre_gestante(self):
        for rec in self:
            if rec.integrante_id:
                rec.madre_gestante = rec.integrante_id.madre_gestante

    @api.multi
    def afiliar_ninio(self):
        if self.services == 'saf':
            raise ValidationError('No se puede afiliar a miembros del servicio SAF')
        if not self.fecha_fin:
            form_id = self.env.ref('base_cuna.wizard_mantenimiento_ninios_view').id
            context = {
                'default_unidad_territorial_id': self.unidad_territorial_id.id,
                'default_comite_gestion_id': self.comite_gestion_id.id,
                'default_ninio_ids': [self.id]
            }
            if self.integrante_id.modulos_ninios_ids:
                rec = self.integrante_id.modulos_ninios_ids.filtered(lambda r: not r.fecha_fin)
                if rec:
                    cerrar_context = {
                        'default_modulo_ninios_ids': [rec.id],
                        'default_editar': True
                    }
                    context.update(cerrar_context)
        else:
            raise ValidationError('No se puede afiliar cuando el integrante tiene fecha fin.')

        return accion_agregar(u'Afiliar Niños', form_id, 'mantenimiento.ninios.wizard', context)

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)

    @api.onchange('es_ninio')
    def _onchange_parentesco_id(self):
        self.es_jefe_hogar = self.es_cuidador_principal = self.es_cuidador_sec = self.integrante_id = False

    @api.multi
    @api.depends('parentesco_id')
    def compute_parentesco_id(self):
        for rec in self:
            rec.tipo_parentesco = rec.parentesco_id.name
            if rec.parentesco_id:
                if rec.parentesco_id.name == u'Niño(a)':
                    rec.es_ninio = True
                else:
                    rec.es_ninio = False

    @api.multi
    def create_record(self):
        self.ensure_one()


class Vivienda(models.Model):
    _name = 'vivienda.familia'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id,
        readonly=True
    )
    services = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
        ('ambos', u'Ambos'),
    ],
        string=u'Servicio',
        readonly=True
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id),"
               "('servicio', '=', services)]",
        string=u'Comite Gestión',
        readonly=True
    )
    departamento_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento',
        readonly=True,
        domain="[('country_id', '=', pais_id), "
               "('state_id', '=', False), "
               "('province_id', '=', False)]"
    )
    provincia_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia',
        readonly=True
    )
    distrito_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Distrito',
        readonly=True,
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', provincia_id)]"
    )
    centro_poblado_id = fields.Many2one(
        comodel_name='res.country.state',
        string=u'Centro Poblado',
        readonly=True
    )
    empadronador_id = fields.Many2one(
        comodel_name='res.partner',
        string='Empadronador'
    )
    tipo_via = fields.Selection(
        selection=[
            ('avenida', 'Avenida'),
            ('jiron', 'Jiron'),
            ('calle', 'Calle'),
            ('pasaje', 'Pasaje'),
            ('carretera', 'Carretera'),
        ],
        string=u'Tipo de Vía:',
        required=True,
        default='avenida'
    )
    direccion = fields.Char(
        string=u'Dirección'
    )
    referencia = fields.Char(
        string='Referencia'
    )
    state = fields.Selection(
        selection=estados,
        string='Estado',
        track_visibility='always',
        default='activo'
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        inverse_name='vivienda_ids',
        string='Familia',
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        default=fields.Date.today(),
        readonly=True
    )
    fecha_fin = fields.Date(
        string='Fecha Cierre',
        readonly=True
    )

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)

    @api.onchange('services')
    def _onchange_services(self):
        self.comite_gestion_id = False

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas(self, 'Viviendas', 'vivienda.familia', 0)


class AfiliacionOrganizacion(models.Model):

    _name = 'afiliacion.organizacion'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id,
        readonly=True
    )
    unidad_territorial_id_log = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    person_code = fields.Char(
        string=u'Código:'
    )
    person_id = fields.Many2one(
        comodel_name='res.partner',
        string='Persona'
    )
    nombre_completo = fields.Char(
        string='Nombre:'
    )
    fecha_nacimiento = fields.Date(
        string='Fecha Nacimiento:'
    )
    mc_afiliacion_lines_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        relation='mc_afiliacion_lines'
    )
    guia_afiliacion_lines_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        relation='guia_afiliacion_lines'
    )
    educ_afiliacion_lines_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        relation='educ_afiliacion_lines'
    )
    state = fields.Selection(selection=[
        ('borrador', 'Borrador'),
        ('activo', u'Activo'),
        ('Retiro', u'Retiro')
    ],
        string='Estado',
        default='borrador'
    )

    @api.model
    def create(self, values):
        obj = super(AfiliacionOrganizacion, self).create(values)
        self._search_in_afiliacion_lines(obj.id)
        return obj

    @api.multi
    def write(self, values):
        r = super(AfiliacionOrganizacion, self).write(values)
        id = self.read(['id'])
        self._search_in_afiliacion_lines(id[0]['id'])
        return r

    def _search_in_afiliacion_lines(self, obj_id):
        afiliacion = self.search([('id', '=', obj_id)])
        if afiliacion.mc_afiliacion_lines_ids:
            _set_fields_lines(self, afiliacion.mc_afiliacion_lines_ids, afiliacion, 'MADRE CUIDADORA')
        if afiliacion.guia_afiliacion_lines_ids:
            _set_fields_lines(self, afiliacion.guia_afiliacion_lines_ids, afiliacion)


class AfiliacionOrganizacionLineas(models.Model):

    _name = 'afiliacion.organizacion.lines'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]"
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='facilitador_ids',
        string=u'Comite Gestión'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        inverse_name='afiliaciones_ids',
        string='Local',
        domain="[('comite_id', '=', comite_gestion_id)]"
    )
    ambiente_id = fields.Many2one(
        comodel_name='ambientes.local',
        string='Ambiente',
        domain="[('local_id', '=', local_id)]"
    )
    salas_id = fields.Many2one(
        comodel_name='salas.local',
        string='Salas',
        domain="[('ambientes_id', '=', ambiente_id)]",
    )
    modulo_id = fields.Many2one(
        comodel_name='modulo.local',
        string=u'Módulo',
        domain="[('sala_id', '=', salas_id)]"
    )
    state = fields.Selection(
        selection=[
            ('active', 'Activo'),
            ('retire', 'Retiro'),
        ],
        string='Estado',
        default='active',
        compute='compute_state',
        store=True
    )
    tipo_guia = fields.Many2one(
        comodel_name='tipo.persona',
        string=u'Cargo'
    )
    fecha_ingreso = fields.Date(
        string='Fecha Ingreso'
    )
    fecha_retiro = fields.Date(
        string='Fecha Retiro'
    )
    person_id = fields.Many2one(
        comodel_name='res.partner',
        inverse_name='afiliacion_ids',
        string='Persona'
    )
    afiliacion_id = fields.Many2one(
        comodel_name='afiliacion.organizacion',
        string=u'Afiliación'
    )
    cargo = fields.Char(
        string='Cargo de la Persona',
        readonly=True,
        compute='_compute_cargo',
        store=True
    )
    cargo_activo = fields.Boolean(
        sring='el mismo cargo esta activo?'
    )
    mensaje = fields.Text(
        string='error',
        compute='_compute_mensaje'
    )
    fecha_inicio_cert = fields.Date(
        string=u'Fecha Inicio Certificación'
    )
    fecha_fin_cert = fields.Date(
        string=u'Fecha Inicio Certificación'
    )
    junta_directiva = fields.Boolean(
        string='Miembro junta directiva',
        compute='_compute_junta_directiva',
        store=True
    )
    resolucion_id = fields.Many2one(
        comodel_name='resoluciones.comite',
        string=u'Resolución',
        domain="[('comite_gestion_id', '=', comite_gestion_id),"
               " ('estado', '!=', 'vencido')]"
    )
    resol_fecha_inicio = fields.Date(
        string='Res. Fecha Inicio',
        related='resolucion_id.fecha_inicio',
        store=True
    )
    resol_fecha_fin = fields.Date(
        string='Res. Fecha fin',
        related='resolucion_id.fecha_fin',
        store=True
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
        ('ambos', u'Ambos'),
    ])

    @api.multi
    @api.depends('cargo')
    def _compute_junta_directiva(self):
        cargo = [
            'PRESIDENTE DE COMITE DE GESTION',
            'SECRETARIO DE COMITE DE GESTION',
            'TESORERO DE COMITE DE GESTION',
            'PRIMER VOCAL',
            'SEGUNDO VOCAL'
        ]
        for rec in self:
            if rec.cargo in cargo:
                rec.junta_directiva = True

    @api.constrains('fecha_inicio_cert', 'fecha_fin_cert')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio_cert, self.fecha_fin_cert)

    @api.multi
    @api.depends('tipo_guia')
    def _compute_cargo(self):
        for rec in self:
            if rec.tipo_guia:
                rec.cargo = rec.tipo_guia.name

    @api.onchange('cargo')
    def _onchange_tipo_guia(self):
        if self.cargo:
            if self.cargo != 'MADRE CUIDADORA':
                self.local_id = self.ambiente_id = self.salas_id = self.modulo_id = False
            cargo = [
                'PRESIDENTE DE COMITE DE GESTION',
                'PRIMER VOCAL',
                'SEGUNDO VOCAL',
                'SECRETARIO DE COMITE DE GESTION',
                'TESORERO DE COMITE DE GESTION',
                'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'
            ]
            if self.cargo in cargo:
                rec = self.comite_gestion_id.afiliaciones_ids.filtered(
                    lambda x: x.cargo == self.cargo and not x.fecha_retiro and self.id != x.id
                )
                if rec:
                    self.cargo_activo = True
                else:
                    self.cargo_activo = False
            else:
                self.cargo_activo = False
        else:
            self.cargo_activo = False

    @api.depends('cargo_activo')
    def _compute_mensaje(self):
        if self.cargo_activo:
            self.mensaje = 'Existe otra persona con el mismo cargo %s activo en el comite.' % self.cargo
        else:
            self.mensaje = False

    @api.multi
    def create_record(self):
        self.ensure_one()
        self.person_id.tipo_persona = self.tipo_guia

    @api.multi
    def create_record_socias_cocina(self):
        self.ensure_one()
        self.person_id.tipo_persona = self.tipo_guia
        self.local_id.socias_cocina_ids = [(4, self.id)]

    @api.multi
    @api.depends('fecha_retiro')
    def compute_state(self):
        for rec in self:
            if rec.fecha_retiro:
                rec.state = 'retire'
            else:
                rec.state = 'active'

    @api.model
    def create(self, values):
        obj = super(AfiliacionOrganizacionLineas, self).create(values)
        self.verified_active_afiliacion_lines_in_partner()
        return obj

    @api.multi
    def write(self, values):
        r = super(AfiliacionOrganizacionLineas, self).write(values)
        self.verified_active_afiliacion_lines_in_partner()
        return r

    # verifica que no crear otro afiliacion mientras otra se encuentre activa
    def verified_active_afiliacion_lines_in_partner(self):
        recs = self.search([
            ('person_id', '=', self.person_id.id),
            ('state', '=', 'active'),
        ])
        if len(recs) == 2:
            raise ValidationError(u'No se puede activar otro afiliación mientras exista otra activa.')


class ResolucionesComite(models.Model):

    _name = 'resoluciones.comite'

    nro_resolucion = fields.Char(
        string=u'N° Resolución',
        required=True
    )
    fecha_publicacion = fields.Date(
        string=u'Fecha Publicación',
        required=True
    )
    fecha_inicio = fields.Date(
        string=u'Fecha Inicio',
        required=True
    )
    tiempo_rde = fields.Integer(
        string='Tiempo de RDE',
        default=24,
        readonly=True
    )
    fecha_fin = fields.Date(
        string=u'Fecha Fin',
        compute='_compute_fecha_fin',
        store=True
    )
    motivo = fields.Selection(
        selection=[
            ('reconocimiento', 'Reconocimiento inicial del CG y junta directiva'),
            ('cambio_parcial_jd', 'Cambio de JD parcial'),
            ('cambio_total_jd', 'Cambio de JD total'),
            ('fe_erratas', u'Fe de Erratas(Rectificación)')
        ],
        string=u'Motivo resolución',
        required=True,
        default='reconocimiento'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='resoluciones_ids',
        string='Comité de Gestión'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        related='comite_gestion_id.unidad_territorial_id',
        store=True
    )
    adjuntar_archivo = fields.Binary(
        string="Archivo Adjunto",
        required=True
    )
    file_name = fields.Char(
        string="Adjunto"
    )
    convenio_ids = fields.One2many(
        comodel_name='convenios.comite',
        inverse_name='resolucion_id',
        string='Convenios',
        readonly=True
    )
    estado = fields.Selection(
        selection=[
            ('vigente', 'Vigente'),
            ('por_vencer', 'Por vencer'),
            ('vencido', 'Vencido')
        ],
        string='Estado',
        default='vigente'
    )
    name = fields.Char(
        string='Nombre',
        compute='_compute_name',
        store=True
    )
    obs = fields.Text(
        string=u'Observación'
    )

    @api.multi
    @api.depends('nro_resolucion')
    def _compute_name(self):
        for rec in self:
            if rec.nro_resolucion:
                anio = datetime.strptime(rec.fecha_inicio, "%Y-%m-%d").year
                rec.name = u"RESOLUCIÓN Nº {} - {} / MIDISPNCM“ ".format(rec.nro_resolucion, anio)

    @api.depends('fecha_inicio', 'tiempo_rde')
    def _compute_fecha_fin(self):
        if self.fecha_inicio:
            fecha_aux = datetime.strptime(self.fecha_inicio, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_fin = fecha_aux + relativedelta(months=self.tiempo_rde)

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Resoluciones',
            'type': "ir.actions.act_window",
            'res_model': 'resoluciones.comite',
            'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)

        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d)]" % user.unidad_territorial_id.id
        return action


class ConvenioComite(models.Model):

    _name = 'convenios.comite'

    name = fields.Char(
        string='Nombre',
        compute='_compute_name',
        store=True
    )
    resolucion_id = fields.Many2one(
        comodel_name='resoluciones.comite',
        inverse_name='convenio_ids',
        string='Resoluciones',
        domain="[('comite_gestion_id','=', comite_gestion_id)]",
        required=True
    )
    nro_convenio = fields.Char(
        string='N° Convenio',
        required=True
    )
    fecha_publicacion = fields.Date(
        string=u'Fecha Publicación',
        compute='_compute_fecha_publicacion',
        store=True
    )
    fecha_inicio = fields.Date(
        string=u'Fecha Inicio',
        required=True
    )
    tiempo_convenio = fields.Integer(
        string='Tiempo de convenio',
        default=24,
        required=True
    )
    fecha_fin = fields.Date(
        string=u'Fecha Fin',
        compute='_compute_fecha_fin',
        store=True
    )
    motivo = fields.Selection(
        selection=[
            ('convenio_inicial', 'Convenio Inicial'),
            ('ampliacion_vigencia', u'Ampliación de Vigencia de convenio')
        ],
        string=u'Motivo resolución',
        required=True,
        default='convenio_inicial'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='convenios_ids',
        string='Comité de Gestión'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        related='comite_gestion_id.unidad_territorial_id',
        store=True
    )
    adenda_ids = fields.One2many(
        comodel_name='adendas.comite',
        inverse_name='convenio_id',
        string='Adendas',
        readonly=True
    )
    adjuntar_archivo = fields.Binary(
        string="Archivo Adjunto",
        required=True
    )
    file_name = fields.Char(
        string="Adjunto"
    )
    obs = fields.Text(
        string=u'Observación'
    )
    estado = fields.Selection(
        selection=[
            ('vigente', 'Vigente'),
            ('por_vencer', 'Por vencer'),
            ('vencido', 'Vencido')
        ],
        string='Estado',
        default='vigente'
    )

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Convenios',
            'type': "ir.actions.act_window",
            'res_model': 'convenios.comite',
            'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)

        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d)]" % user.unidad_territorial_id.id
        return action

    @api.depends('resolucion_id')
    def _compute_fecha_publicacion(self):
        if self.resolucion_id:
            self.fecha_publicacion = self.resolucion_id.fecha_publicacion

    @api.onchange('resolucion_id')
    def _onchange_resolucion_id(self):
        self.fecha_inicio = False

    @api.constrains('fecha_inicio')
    def _constraints_fecha_inicio(self):
        if self.fecha_inicio:
            if self.resolucion_id.fecha_inicio > self.fecha_inicio:
                raise ValidationError(
                    u'Fecha Inicio del convenio no puede ser menor a la Fecha de Inicio de la resolución %s'
                    % self.resolucion_id.fecha_inicio
                )

    @api.multi
    @api.depends('fecha_inicio', 'nro_convenio')
    def _compute_name(self):
        for rec in self:
            if rec.fecha_inicio and rec.nro_convenio:
                anio = datetime.strptime(rec.fecha_inicio, "%Y-%m-%d").year
                servicio = rec.comite_gestion_id.servicio.upper()
                rec.name = "CONVENIO N° {}-{}-MIDIS/PNCM/UTAI/{}/{} ".format(
                    rec.nro_convenio, anio, servicio, rec.comite_gestion_id.unidad_territorial_id.code
                )

    @api.depends('fecha_inicio', 'tiempo_convenio')
    def _compute_fecha_fin(self):
        if self.fecha_inicio and self.tiempo_convenio:
            fecha_aux = datetime.strptime(self.fecha_inicio, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_fin = fecha_aux + relativedelta(months=self.tiempo_convenio)

    @api.multi
    def create_record(self):
        self.ensure_one()


class Adendas(models.Model):
    _name = 'adendas.comite'

    name = fields.Char(
        string='Nombre',
        compute='_compute_name',
        store=True
    )
    nro_adenda = fields.Char(
        string='N° Adenda',
        required=True
    )
    convenio_id = fields.Many2one(
        comodel_name='convenios.comite',
        inverse_name='adenda_ids',
        string='Convenios',
        domain="[('comite_gestion_id','=', comite_gestion_id)]",
        required=True
    )
    fecha_publicacion = fields.Date(
        string=u'Fecha Publicación',
        required=True
    )
    fecha_inicio = fields.Date(
        string=u'Fecha Inicio',
        compute='_compute_fecha_inicio',
        store=True
    )
    tiempo_adenda = fields.Integer(
        string='Tiempo de adendas',
        default=24,
        required=True
    )
    fecha_fin = fields.Date(
        string=u'Fecha Fin',
        compute='_compute_fecha_fin',
        store=True
    )
    motivo = fields.Selection(
        selection=[
            ('ampliacion_vigencia', u'Ampliación de Vigencia de convenio'),
            ('fe_erratas', u'Fe de Erratas(Rectificación)')
        ],
        string=u'Motivo',
        required=True,
        default='ampliacion_vigencia'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='adendas_ids',
        string='Comité de Gestión'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        related='comite_gestion_id.unidad_territorial_id',
        store=True
    )
    adjuntar_archivo = fields.Binary(
        string="Archivo Adjunto",
        required=True
    )
    file_name = fields.Char(
        string="Adjunto"
    )
    obs = fields.Text(
        string=u'Observación'
    )
    estado = fields.Selection(
        selection=[
            ('vigente', 'Vigente'),
            ('por_vencer', 'Por vencer'),
            ('vencido', 'Vencido')
        ],
        string='Estado',
        default='vigente'
    )

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Adendas',
            'type': "ir.actions.act_window",
            'res_model': 'adendas.comite',
            'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)

        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d)]" % user.unidad_territorial_id.id
        return action

    @api.onchange('tiempo_adenda')
    def _onchange_tiempo_adenda(self):
        if self.tiempo_adenda > 48:
            self.tiempo_adenda = 48

    @api.depends('fecha_inicio', 'nro_adenda')
    def _compute_name(self):
        if self.fecha_inicio and self.nro_adenda and self.convenio_id:
            anio = datetime.strptime(self.fecha_inicio, "%Y-%m-%d").year
            servicio = self.comite_gestion_id.servicio.upper()
            self.name = "ADENDA N° {} AL CONVENIO N° {}-{}-MIDIS/PNCM/UTAI/{}/{} ".format(
                self.nro_adenda, self.convenio_id.nro_convenio,
                anio, servicio, self.comite_gestion_id.unidad_territorial_id.code
            )

    @api.depends('convenio_id')
    def _compute_fecha_inicio(self):
        if self.convenio_id:
            if self.convenio_id.fecha_fin:
                fecha_aux = datetime.strptime(self.convenio_id.fecha_fin, DEFAULT_SERVER_DATE_FORMAT)
                self.fecha_inicio = fecha_aux + relativedelta(days=1)

    @api.depends('fecha_inicio', 'tiempo_adenda')
    def _compute_fecha_fin(self):
        if self.fecha_inicio and self.tiempo_adenda:
            fecha_aux = datetime.strptime(self.fecha_inicio, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_fin = fecha_aux + relativedelta(months=self.tiempo_adenda)

    @api.multi
    def create_record(self):
        self.ensure_one()


class ComiteGestion(models.Model):
    _inherit = 'pncm.comitegestion'

    centro_poblado_ids = fields.Many2many(
        comodel_name='res.country.state',
        string='Centro Poblado',
        compute='_compute_centro_poblado_activos',
        store=True
    )
    servicio_alimentario_ids = fields.Many2many(
        comodel_name='pncm.infra.local',
        string='Servicios Alimentarios',
        compute='_compute_servicio_alimentario_ids',
    )
    convenios_ids = fields.One2many(
        comodel_name='convenios.comite',
        inverse_name='comite_gestion_id',
        string='Convenios'
    )
    resoluciones_ids = fields.One2many(
        comodel_name='resoluciones.comite',
        inverse_name='comite_gestion_id',
        string='Resoluciones'
    )
    adendas_ids = fields.One2many(
        comodel_name='adendas.comite',
        inverse_name='comite_gestion_id',
        string='Convenios'
    )
    afiliaciones_ids = fields.One2many(
        comodel_name='afiliacion.organizacion.lines',
        inverse_name='comite_gestion_id',
        string='Afiliaciones'
    )
    familias_ids = fields.One2many(
        comodel_name='unidad.familiar',
        inverse_name='comite_gestion_id',
        string='Familias'
    )
    state = fields.Selection(selection=[
        ('constitucion', u'En Formacion'),
        ('activo', 'Activo'),
        ('cerrado', 'Cerrado'),
    ],
        string='Estados',
        default='constitucion'
    )
    presidente_actual = fields.Char(
        string=u'Presidente',
        compute='_compute_presidente',
        store=True
    )
    tesorero_actual = fields.Char(
        string=u'Tesorero',
        compute='_compute_tesorero_actual',
        store=True
    )
    secretario_actual = fields.Char(
        string=u'Secretario',
        compute='_compute_secretario',
        store=True
    )
    primer_vocal_actual = fields.Char(
        string=u'Primer Vocal',
        compute='_compute_primer_vocal',
        store=True
    )
    segundo_vocal_actual = fields.Char(
        string=u'Segundo Vocal',
        compute='_compute_segundo_vocal',
        store=True
    )
    meta_actual = fields.Char(
        string='Meta',
        compute='_compute_meta_actual',
        store=True
    )
    fecha_inicio = fields.Date(
        string=u'Fecha de Inicio',
        compute='_compute_fecha_ini_cier_resolucion',
        store=True
    )
    fecha_cierre = fields.Date(
        string=u'Fecha de Cierre',
        compute='_compute_fecha_ini_cier_resolucion',
        store=True
    )
    resolucion = fields.Char(
        string=u'Resolución Activa',
        compute='_compute_fecha_ini_cier_resolucion',
        store=True
    )

    @api.multi
    def estado_activo(self):
        if not self.presidente_actual or not self.tesorero_actual or not self.secretario_actual \
                or not self.primer_vocal_actual or not self.segundo_vocal_actual:
            raise ValidationError("La junta directiva esta incompleta.")
        if not self.resoluciones_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Se debe ingresar una resolución vigente.")
        if not self.convenios_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Se debe ingresar una convenio vigente.")
        if not self.adendas_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Se debe ingresar una adenda vigente.")
        self.state = 'activo'

    @api.multi
    def estado_cerrado(self):
        if self.afiliaciones_ids.filtered(lambda x: not x.fecha_retiro):
            raise ValidationError('Todos los Actores Comunitarios deben tener fecha de retiro')
        if self.miembros_equipo.filtered(lambda x: not x.fecha_fin):
            raise ValidationError('Todos los integrantes del equipo tecnico deben tener fecha fin')
        if self.resoluciones_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Las resoluciones deben estar vencidas.")
        if self.convenios_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Los convenios deben estar vencidas.")
        if self.adendas_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Las adendas deben estar vencidas.")

    @api.multi
    @api.depends('resoluciones_ids', 'resoluciones_ids.estado',
                 'resoluciones_ids.fecha_inicio', 'resoluciones_ids.fecha_fin')
    def _compute_fecha_ini_cier_resolucion(self):
        for rec in self:
            resolucion = rec.resoluciones_ids.filtered(lambda x: x.estado == 'vigente')
            if resolucion:
                rec.resolucion = resolucion.name
                rec.fecha_inicio = resolucion.fecha_inicio
                rec.fecha_cierre = resolucion.fecha_fin

    @api.multi
    @api.depends('metas', 'metas.fecha_fin')
    def _compute_meta_actual(self):
        fecha_actual = datetime.now().date()
        for rec in self:
            meta = rec.metas.filtered(
                lambda x: datetime.strptime(x.fecha_fin, DEFAULT_SERVER_DATE_FORMAT).date() > fecha_actual
            )
            rec.meta_actual = meta.meta_fisica

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_primer_vocal(self):
        self.primer_vocal_actual = self.obtener_actores_comunales_activos('PRIMER VOCAL')

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_segundo_vocal(self):
        self.segundo_vocal_actual = self.obtener_actores_comunales_activos('SEGUNDO VOCAL')

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_secretario(self):
        self.secretario_actual = self.obtener_actores_comunales_activos('SECRETARIO DE COMITE DE GESTION')

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_presidente(self):
        self.presidente_actual = self.obtener_actores_comunales_activos('PRESIDENTE DE COMITE DE GESTION')

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_tesorero_actual(self):
        self.tesorero_actual = self.obtener_actores_comunales_activos('TESORERO DE COMITE DE GESTION')

    def obtener_actores_comunales_activos(self, cargo):
        registro = self.afiliaciones_ids.filtered(
            lambda x: x.cargo == cargo and not x.fecha_retiro)
        if registro:
            return registro.person_id.display_name

    @api.multi
    @api.depends('intervenciones', 'intervenciones.fecha_fin')
    def _compute_centro_poblado_activos(self):
        for rec in self:
            list_ccpp = []

            for line in rec.intervenciones:
                if line.fecha_fin:
                    list_ccpp.append(line.centro_poblado_id.id)
            rec.centro_poblado_ids = list_ccpp

    @api.multi
    def accion_agregar_ac(self):
        form_id = self.env.ref('base_cuna.form_view_afiliacion_organizacion_lines_persona').id
        context = {
            'default_unidad_territorial_id': self.unidad_territorial_id.id,
            'default_comite_gestion_id': self.id,
            'default_state': 'active',
            'default_servicio': self.servicio,
        }
        return accion_agregar('Actor Comunal', form_id, 'afiliacion.organizacion.lines', context)

    @api.multi
    def agregar_apoyo_adm_activo(self):
        form_id = self.env.ref('base_cuna.form_view_afiliacion_organizacion_lines_apoyo_adm').id
        tipo = self.env['tipo.persona'].search([('name', '=', 'APOYO ADMINISTRATIVO DEL COMITE DE GESTION')])
        context = {
            'default_unidad_territorial_id': self.unidad_territorial_id.id,
            'default_comite_gestion_id': self.id,
            'default_state': 'active',
            'default_tipo_guia': tipo.id,
        }
        return accion_agregar('Apoyo Administrativo', form_id, 'afiliacion.organizacion.lines', context)

    def eliminar_lineas(self, tipo_persona):
        afiliaciones = self.afiliaciones_ids.filtered(lambda x: x.cargo == tipo_persona)
        if not afiliaciones:
            raise ValidationError('No hay registro de %s' % tipo_persona)
        form_id = self.env.ref('base_cuna.form_view_eliminar_afiliaciones').id
        context = {
            'default_afiliacion_ids': afiliaciones.ids,
            'default_comite_gestion_id': self.id,
            'default_nombre_tipo': tipo_persona
        }
        return accion_agregar('Afiliaciones', form_id, 'eliminar.afiliaciones', context)

    @api.multi
    def accion_agregar_convenio(self):
        form_id = self.env.ref('base_cuna.form_view_convenios_comite').id
        return self.crear_convenio_resoluciones_adendas(form_id, 'Convenios', 'convenios.comite')

    @api.multi
    def accion_agregar_resolucion(self):
        form_id = self.env.ref('base_cuna.form_view_resolucion_comite').id
        return self.crear_convenio_resoluciones_adendas(form_id, 'Resolución', 'resoluciones.comite')

    @api.multi
    def accion_agregar_adenda(self):
        form_id = self.env.ref('base_cuna.form_view_adendas_comite').id
        return self.crear_convenio_resoluciones_adendas(form_id, 'Adenda', 'adendas.comite')

    def crear_convenio_resoluciones_adendas(self, form_id, nombre, modelo):
        context = {'default_comite_gestion_id': self.id}
        return accion_agregar(nombre, form_id, modelo, context)

    @api.multi
    def _compute_servicio_alimentario_ids(self):
        for r in self:
            if r.id:
                recs = r.env['pncm.infra.local'].search([
                    ('tipo_local', '!=', 'ciai'),
                    ('comite_id', '=', r.id),
                ])
                r.servicio_alimentario_ids = recs

    @api.model
    def enviar_correos_convenios_vencer(self):
        fecha_actual = datetime.now().date()
        template = self.env['ir.model.data'].sudo().get_object('base_cuna', 'convenio_a_vencer_template')

        list_to = self.integrantes_correo()
        if list_to:
            for rec in self:
                for convenio in rec.convenios_ids:
                    fecha_vencer = datetime.strptime(convenio.fecha_fin, DEFAULT_SERVER_DATE_FORMAT) - \
                                   relativedelta(days=90)

                    if fecha_vencer.date() == fecha_actual:
                        body = '<p>Estimado,</p>' \
                                '<p>'\
                                'El siguiente convenio "${object.name}" esta próximo a vercer:'\
                                '- Fecha fin: ${object.fecha_fin}'\
                                'Darle el siguimiento correspondiente.'\
                                '</p>'\
                                '<p>Saludos</p>'
                        mail_values = {
                            'subject': template.subject,
                            'body_html': body,
                            'email_to': ';'.join(map(lambda x: x, list_to)),
                            #'email_from': template_obj.email_from,
                        }
                        create_and_send_email = self.env['mail.mail'].create(mail_values).send()
                    #self.env['mail.template'].browse(template.id).sudo().send_mail(mail_values, force_send=True)

    def integrantes_correo(self):
        juts = self.env['res.groups'].search('name', '=', 'Jefe de Unidad Territorial')
        coords = self.env['res.groups'].search('name', '=', 'Coordinador')
        esps = self.env['res.groups'].search('name', '=', 'Especialista Integrales')
        list_to = []
        for j in juts.users:
            list_to.append(j.login)
        for c in coords.users:
            list_to.append(c.login)
        for e in esps.users:
            list_to.append(e.login)
        return list_to

    @api.model
    def resoluciones_vencer(self):
        fecha_actual = datetime.now().date()
        for rec in self:
            for resolucion in rec.resoluciones_ids:
                self.cambiar_estado_res_conv_adenda(resolucion, fecha_actual)
            for convenio in rec.convenios_ids:
                self.cambiar_estado_res_conv_adenda(convenio, fecha_actual)
            for adenda in rec.adendas_ids:
                self.cambiar_estado_res_conv_adenda(adenda, fecha_actual)

    def cambiar_estado_res_conv_adenda(self, doc, fecha_actual):
        fecha_fin = datetime.strptime(doc.fecha_fin, DEFAULT_SERVER_DATE_FORMAT)
        if fecha_actual > fecha_fin.date():
            doc.estado = 'vencido'
        else:
            fecha_vencer = fecha_fin - relativedelta(days=90)

            if fecha_vencer.date() == fecha_actual:
                doc.estado = 'por_vencer'
            else:
                doc.estado = 'vigente'

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Comite Gestión',
            'type': "ir.actions.act_window",
            'res_model': 'pncm.comitegestion',
            'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)

        if user.unidad_territorial_id and user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('unidad_territorial_id', '=', %d), ('servicio', '=', '%s')]" % \
                               (user.unidad_territorial_id.id, user.servicio)
        elif user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d)]" % user.unidad_territorial_id.id
        elif user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('servicio', '=', '%s')]" % user.servicio
        return action

    @api.multi
    def write(self, values):
        r = super(ComiteGestion, self).write(values)
        self.resoluciones_vencer()
        return r


class MetasHistoricas(models.Model):
    _inherit = 'pncm.comite.metas.historicas'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        related='comite_gestion_id.unidad_territorial_id',
        store=True
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='metas',
        string='Comité de Gestión',
        required=True
    )

    @api.model
    def create(self, vals):
        metas = []
        for meta_periodo in self._obtener_periodos(
                vals['fecha_inicio'],
                vals['fecha_fin']):
            metas.append((0, 0, {
                'meta_id': self.id,
                'meta_presupuestal_planeada': vals['meta_presupuestal'],
                'meta_presupuestal_ejecutada': 0,
                'comite_gestion_id': vals['comite_gestion_id'],
                'meta_fisica_planeada': vals['meta_fisica'],
                'meta_fisica_ejecutada': 0,
                'periodo': meta_periodo,
                'meta': vals['codigo'],

            }))
        vals['metas_programadas'] = metas
        return super(MetasHistoricas, self).create(vals)

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Metas',
            'type': "ir.actions.act_window",
            'res_model': 'pncm.comite.metas.historicas',
            'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d)]" % user.unidad_territorial_id.id
        return action


class CMIntervencion(models.Model):
    _inherit = 'pncm.comitegestion.intervencion'

    ut_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office', '=', True)]",
        required=True
    )
    centro_poblado_ids = fields.Many2many(
        comodel_name='res.country.state',
        string='Centro Poblado',
        compute='_compute_centro_poblado_ids',
        store=True
    )

    @api.multi
    @api.depends('comite_gestion_id', 'comite_gestion_id.intervenciones')
    def _compute_centro_poblado_ids(self):
        for rec in self:
            rec.centro_poblado_ids = rec.comite_gestion_id.centro_poblado_ids

    @api.onchange('centro_poblado_ids')
    def onchange_domain_centro_poblado_ids(self):
        res = {}
        ids = []
        for centro in self.centro_poblado_ids:
            ids.append(centro.id)
        res['domain'] = {
            'centro_poblado_id': [('id', 'not in', ids)],
        }
        return res


class CMAmbitoIntervencion(models.Model):
    _inherit = 'pncm.ambito.intervencion'

    name = fields.Char(
        string='Nombre',
        compute='_compute_name',
        store=True
    )

    @api.multi
    @api.depends('departamento_id', 'provincia_id', 'distrito_id')
    def _compute_name(self):
        for obj in self:
            obj.name = u'{}/{}/{}'.format(obj.departamento_id.name, obj.provincia_id.name, obj.distrito_id.name)


class AmbientesLocal(models.Model):
    _name = 'ambientes.local'

    name = fields.Char(
        string=u'Descripción',
        required=True
    )
    area = fields.Float(
        string=u'Área(m2)',
        required=True
    )
    largo = fields.Float(
        string='Largo(ml)',
        required=True
    )
    ancho = fields.Float(
        string='Ancho(ml)',
        required=True
    )
    cap_max = fields.Integer(
        string=u'Cap. Max. Niñas y Niños',
        required=True
    )
    salas_ids = fields.One2many(
        comodel_name='salas.local',
        inverse_name='ambientes_id',
        string='Salas'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        inverse_name='ambientes_ids',
        string='Local',
        readonly=True
    )

    @api.multi
    def create_record(self):
        self.ensure_one()


class SalasLocal(models.Model):
    _name = 'salas.local'
    _inherit = 'tabla.base'

    name = fields.Char(
        string=u'Descripción',
        required=True
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        inverse_name='salas_ids',
        string='Local',
        readonly=True
    )
    ambientes_id = fields.Many2one(
        comodel_name='ambientes.local',
        inverse_name='salas_ids',
        string='Ambiente',
        domain="[('local_id', '=', local_id)]",
        required=True
    )
    modulos_ids = fields.One2many(
        comodel_name='modulo.local',
        inverse_name='sala_id',
        string=u'Módulos'
    )

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)


class ModuloLocal(models.Model):
    _name = 'modulo.local'

    name = fields.Char(
        string=u'Nombre',
        required=True
    )
    nro_ninios = fields.Integer(
        string=u'N° Niños',
        required=True
    )
    ninios_ids = fields.One2many(
        comodel_name='modulo.ninios',
        inverse_name='modulo_id',
        string=u'Niños',
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        inverse_name='modulos_ids',
        string='Local'
    )
    sala_id = fields.Many2one(
        comodel_name='salas.local',
        inverse_name='modulos_ids',
        domain="[('local_id', '=', local_id)]",
        string='Sala',
        required=True
    )
    afiliacion_ids = fields.One2many(
        comodel_name='afiliacion.organizacion.lines',
        inverse_name='modulo_id',
        string='Afiliaciones'
    )

    @api.multi
    def name_get(self):
        return [(obj.id, u'{} - {}'.format(obj.local_id.name, obj.name)) for obj in self]

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.constrains('ninios_ids')
    def _check_ninios_ids(self):
        if len(self.ninios_ids) > self.nro_ninios:
            raise ValidationError(u'El números de niños supera el limete del módulo')

    @api.model
    def create(self, values):
        obj = super(ModuloLocal, self).create(values)
        self._set_fields_ninios_afiliacion(obj.id)
        return obj

    @api.multi
    def write(self, values):
        r = super(ModuloLocal, self).write(values)
        id = self.read(['id'])
        self._set_fields_ninios_afiliacion(id[0]['id'])
        return r

    def _set_fields_ninios_afiliacion(self, obj_id):
        obj = self.search([('id', '=', obj_id)])
        for ninio in obj.ninios_ids:
            ninio.local_id = self.local_id
            ninio.comite_gestion_id = self.local_id.comite_id
            ninio.unidad_territorial_id = self.local_id.ut_id
            ninio.modulo_id = self.id
            ninio.sala_id = self.sala_id
            ninio.ambiente_id = ninio.sala_id.ambientes_id


class NiniosModulo(models.Model):

    _name = 'modulo.ninios'

    ninios_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Niños',
        required=True,
        domain="[('tipo_parentesco','=', u'Niño(a)')]"
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comite Gestión',
        readoonly=True
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        readoonly=True
    )
    cod_ninio = fields.Integer(
        string=u'Código Niño',
        readoonly=True,
        related='partner_id.id',
        store=True
    )
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        inverse_name='modulos_ninios_ids',
        string='Persona',
        related='ninios_id.integrante_id',
        store=True
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        string='Local',
        readoonly=True
    )
    modulo_id = fields.Many2one(
        comodel_name='modulo.local',
        inverse_name='ninios_ids',
        string='Modulo'
    )
    sala_id = fields.Many2one(
        comodel_name='salas.local',
        string='Sala'
    )
    ambiente_id = fields.Many2one(
        comodel_name='ambientes.local',
        string='Ambiente',
        domain="[('local_id', '=', local_id)]"
    )
    state = fields.Selection(
        selection=estados,
        string='Estado',
        default='activo'
    )
    historial_modulo_ids = fields.Many2many(
        comodel_name='modulo.ninios',
        relation='ninios_modulo_rel',
        column1='col_name',
        column2='col_name_2',
        compute='_compute_historial_modulo_ids',
        string='Historial'
    )

    @api.depends('partner_id')
    def _compute_historial_modulo_ids(self):
        historial = self.search([('partner_id', '=', self.partner_id.id)])
        self.historial_modulo_ids = historial

    @api.onchange('fecha_fin')
    def _onchange_fecha_fin(self):
        if self.fecha_fin:
            self.state = 'cerrado'
        else:
            self.state = 'activo'

    @api.model
    def create(self, values):
        obj = super(NiniosModulo, self).create(values)
        obj.ninios_id.integrante_id.permitir_modulo = True
        return obj

    @api.multi
    def write(self, values):
        r = super(NiniosModulo, self).write(values)
        if self.fecha_fin:
            self.ninios_id.integrante_id.permitir_modulo = False
        else:
            self.ninios_id.integrante_id.permitir_modulo = True
        return r

    @api.multi
    def name_get(self):
        return [(obj.id, u'{} - {}'.format(obj.modulo_id.name, obj.ninios_id.integrante_id.name)) for obj in self]

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)


class InfraLocal(models.Model):

    _inherit = 'pncm.infra.local'

    tipo_local = fields.Selection(
        selection=[
            ('hcd', 'HCD'),
            ('ccd', 'CCD'),
            ('ciai', u'CIAI'),
            ('sa', u'SA'),
            ('ambos', u'Ambos (CIAI y SA)'),
        ],
        required=True
    )
    ambientes_ids = fields.One2many(
        comodel_name='ambientes.local',
        inverse_name='local_id',
        string='Ambientes'
    )
    salas_ids = fields.One2many(
        comodel_name='salas.local',
        inverse_name='local_id',
        string='Salas'
    )
    modulos_ids = fields.One2many(
        comodel_name='modulo.local',
        inverse_name='local_id',
        string=u'Módulos'
    )
    afiliaciones_ids = fields.One2many(
        comodel_name='afiliacion.organizacion.lines',
        inverse_name='local_id',
        string='Todas la afiliaciones'
    )
    madres_cuidadoras_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        relation='mdcuid_lines',
        compute='_compute_afiliacion_lines',
        store=True
    )
    socias_cocina_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        string='Socias de Cocina'
    )
    sa_ids = fields.Many2many(
        comodel_name='pncm.infra.local',
        relation='sa_rel_local',
        column1='sa',
        column2='local',
        string='Servicio Alimentario',
        domain="[('id', '!=', id), "
               "('state', '=', 'activacion')]"
    )
    nro_telefono = fields.Char(
        string='N° Telefónico'
    )

    @api.multi
    def accion_agregar_socias_cocinas(self):
        form_id = self.env.ref('base_cuna.form_view_afiliacion_organizacion_lines_socias_cocina').id
        tipo = self.env['tipo.persona'].search([('name', '=', 'SOCIO DE COCINA')])
        context = {
            'default_unidad_territorial_id': self.ut_id.id,
            'default_comite_gestion_id': self.comite_id.id,
            'default_local_id': self.id,
            'default_state': 'active',
            'default_tipo_guia': tipo.id
        }
        return accion_agregar('Agrega Socias de Cocina', form_id, 'afiliacion.organizacion.lines', context)

    @api.multi
    def dar_de_baja(self):
        if self.es_activado:
            if self.socias_cocina_ids.filtered(lambda x: x.state == 'activo'):
                raise ValidationError('No se pueda dar de baja mientras tenga socias de cocina en estado "Activo".')
            if self.state == 'suspension':
                if len(self.suspensiones) > 0:
                    self.write(dict(state='baja',
                                    no_validar_suspension=True))
                else:
                    raise ValidationError(u'No ha registrado ninguna '
                                          u'suspensión')
            elif self.state == 'activacion':
                self.write(dict(state='baja'))
        else:
            raise ValidationError(u'El local debe estar activo para que '
                                  u'se pueda dar de baja')

    @api.multi
    @api.depends('afiliaciones_ids')
    def _compute_afiliacion_lines(self):
        for rec in self:
            if rec.afiliaciones_ids:
                rec.madres_cuidadoras_ids = rec.afiliaciones_ids.filtered(lambda x: x.cargo == 'MADRE CUIDADORA')

    @api.model
    def create(self, values):
        _drop_records(self, 'salas.local', [('local_id', '=', False)])
        _drop_records(self, 'modulo.local', [('sala_id', '=', False)])
        obj = super(InfraLocal, self).create(values)
        self._search_in_afiliacion_lines(obj.id)
        return obj

    @api.multi
    def write(self, values):
        _drop_records(self, 'salas.local', [('local_id', '=', False)])
        _drop_records(self, 'modulo.local', [('sala_id', '=', False)])
        r = super(InfraLocal, self).write(values)
        id = self.read(['id'])
        self._search_in_afiliacion_lines(id[0]['id'])
        return r

    def _search_in_afiliacion_lines(self, obj_id):
        servicio_obj = self.search([('id', '=', obj_id)])
        if servicio_obj.socias_cocina_ids:
            _set_fields_lines(self, servicio_obj.socias_cocina_ids, servicio_obj, 'SOCIO DE COCINA')

    @api.multi
    def action_crear_ambientes(self):
        form_id = self.env.ref('base_cuna.form_view_ambientes_local_crear').id
        context = {'default_local_id': self.id}
        return accion_agregar('Ambientes', form_id, 'ambientes.local', context)


    @api.multi
    def action_crear_salas(self):
        form_id = self.env.ref('base_cuna.form_view_salas_local_crear').id
        context = {'default_local_id': self.id}
        return accion_agregar('Salas', form_id, 'salas.local', context)

    @api.multi
    def action_crear_modulos(self):
        form_id = self.env.ref('base_cuna.form_view_modulo_local_crear').id
        context = {'default_local_id': self.id}
        return accion_agregar(u'Módulos', form_id, 'modulo.local', context)

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': 'Locales',
            'type': "ir.actions.act_window",
            'res_model': 'pncm.infra.local',
            'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)
        if not user.rol_admin:
            desired_group_name = self.env['res.groups'].search([('name', '=', u'Especialista en Nutrición')])
            is_desired_group = user.id in desired_group_name.users.ids

            if not is_desired_group:
                if user.unidad_territorial_id:
                    action['domain'] = "[('ut_id', '=', %d)]" % user.unidad_territorial_id.id
            else:
                if user.unidad_territorial_id:
                    action['domain'] = "[('ut_id', '=', %d),('tipo_local', 'in', ['sa', 'ambos'])]" % \
                                       user.unidad_territorial_id.id
                else:
                    action['domain'] = "[('tipo_local', 'in', ['sa', 'ambos'])]"
        else:
            if user.unidad_territorial_id:
                action['domain'] = "[('ut_id', '=', %d)]" % user.unidad_territorial_id.id

        return action


class MigracionFamilias(models.Model):

    _name = 'migracion.familia'
    _inherit = 'tabla.base'

    cambiar_ut = fields.Boolean(
        string=u'¿Cambiar de unidad territorial?',
        default=True
    )
    unidad_territorial_id_log = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    unidad_territorial_origen_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial Origen',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id,
        readonly=True
    )
    unidad_territorial_destino_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial Destino',
        domain="[('is_office','=', True),"
               "('id','!=', unidad_territorial_origen_id)]"
    )
    comite_gestion_origen_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_origen_id)]",
        string=u'Comite Gestión Origen',
        required=True
    )
    comite_gestion_destino_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_destino_id),"
               "('id', '!=', comite_gestion_origen_id)]",
        string=u'Comite Gestión Destino',
        required=True
    )
    familias_ids = fields.Many2many(
        comodel_name='unidad.familiar',
        domain="[('comite_gestion_id','=', comite_gestion_origen_id),"
               "('state', '=', 'activo'),"
               "('hogar_activo', '=', True)]",
        string='Familias',
        required=True
    )
    fecha_migracion = fields.Date(
        string=u'Fecha Migración',
        default=fields.Date.today(),
        readonly=True
    )

    @api.onchange('comite_gestion_origen_id')
    def _onchange_comite_gestion_origen_id(self):
        self.comite_gestion_destino_id = False
        self.familias_ids = False

    @api.onchange('cambiar_ut')
    def _onchange_cambiar_ut(self):
        self.unidad_territorial_destino_id = False
        self.comite_gestion_origen_id = False
        self.comite_gestion_destino_id = False
        self.familias_ids = False
        if not self.cambiar_ut:
            self.unidad_territorial_destino_id = self.unidad_territorial_origen_id

    @api.multi
    def create_record(self):
        self.ensure_one()
        for rec in self.familias_ids:
            rec.comite_gestion_id = self.comite_gestion_destino_id
            rec.unidad_territorial_id = self.unidad_territorial_destino_id
            rec.migraciones_ids = [(4, self.id)]
            for line in rec.vivienda_ids:
                line.state = 'cerrado'
                line.fecha_fin = self.fecha_migracion


class UT(models.Model):

    _inherit = 'hr.department'

    @api.multi
    def filtrar_vistas(self):
        search_id = self.env.ref('hr.view_department_ut_filter').id
        context ={"search_default_is_office": 1, 'default_is_office':1}
        action = {
            'name': 'Unidades Territoriales',
            'type': "ir.actions.act_window",
            'res_model': 'hr.department',
            'view_type': "form",
            'view_mode': "tree,form",
            'search_view_id': search_id,
            'context': context
        }
        user = self.env['res.users'].browse(self._uid)
        if user.unidad_territorial_id and user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('id', '=', %d), ('services', '=', '%s')]" % \
                               (user.unidad_territorial_id.id, user.servicio)
        elif user.unidad_territorial_id:
            action['domain'] = "[('id', '=', %d)]" % user.unidad_territorial_id.id
        elif user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('services', '=', '%s')]" % user.servicio
        return action

class CMEquipoTecnico(models.Model):

    _inherit = 'pncm.comite.equipo.tecnico'

    cargo = fields.Char(
        string=u'Cargo',
        compute='_compute_cargo',
        store=True
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        domain="[('is_office','=',True)]"
    )

    @api.onchange('empleado_cunamas_id')
    def _complete_fields(self):
        if self.empleado_cunamas_id:
            nombres = self.empleado_cunamas_id.name
            cargo = ''
            if self.empleado_cunamas_id.position_id:
                cargo = self.empleado_cunamas_id.position_id.name
            return {'value': {'nombres': nombres, 'cargo': cargo}}

    @api.multi
    @api.depends('empleado_cunamas_id')
    def _compute_cargo(self):
        for rec in self:
            rec.cargo = rec.empleado_cunamas_id.position_id.name

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.empleado_cunamas_id.display_name)) for obj in self]
