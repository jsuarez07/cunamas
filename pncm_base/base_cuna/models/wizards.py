# -*- coding: utf-8 -*-

from .base import months
from datetime import datetime
from collections import Counter
from odoo import fields, models, api
from odoo.exceptions import ValidationError
import pytz


_opttipodocumento = [
    ('dni', 'DNI'),
    ('ext', u'Carnet de Extranjería'),
    ('pas', 'Pasaporte')
]


def _get_report(context, report_name, name):
    if context.get('xls_export'):
        report_name = 'base_cuna.{}.xlsx'.format(report_name)
        return {
            'type': 'ir.actions.report.xml',
            'report_name': report_name,
            'name': name
        }


def _obtener_tiempo(self, time):
    date_now = datetime.now(tz=pytz.timezone('America/Lima')).replace(tzinfo=None)
    if time == 'mes':
        return months[date_now.month - 1]
    else:
        rec = self.env['tabla.anios'].search([
            ('name', '=', date_now.year)
        ])
        return rec.id


def clean_checkboxes(values):
    if values.get('is_code'):
        values['is_document'] = values['is_full_name'] = False
    if values.get('is_document'):
        values['is_code'] = values['is_full_name'] = False
    if values.get('is_full_name'):
        values['is_document'] = values['is_code'] = False


class BusquedaOrganizacionWizard(models.TransientModel):

    _name = 'busqueda.afiliacion.wizard'

    person_code = fields.Char(
        string='Código Persona'
    )
    is_code = fields.Boolean()
    document_number = fields.Char(
        string='Documento'
    )
    is_document = fields.Boolean()
    type_document = fields.Selection(
        selection=_opttipodocumento,
        string="Tipo de Documento"
    )
    is_full_name = fields.Boolean()
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Nombres y Apellidos'
    )
    verified = fields.Boolean(
        string='Aceptar'
    )
    partners_ids = fields.Many2many(
        comodel_name='res.partner',
        string='Persona'
    )
    mod_ninios_ids = fields.Many2many(
        comodel_name='modulo.ninios',
        string='Niños'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        string='Local',
        domain="[('comite_id', '=', comite_gestion_id)]"
    )
    salas_id = fields.Many2one(
        comodel_name='salas.local',
        string='Salas',
        domain="[('local_id', '=', local_id)]"
    )
    modulo_id = fields.Many2one(
        comodel_name='modulo.local',
        string=u'Módulo',
        domain="[('sala_id', '=', salas_id)]"
    )

    def _delete_data_fields(self):
        self.type_document = False
        self.document_number = False
        self.is_document = False
        self.person_code = False
        self.is_code = False
        self.is_full_name = False
        self.partner_id = False

    @api.multi
    def search_people(self):
        if self.is_full_name:
            obj_persona = self.partner_id
        else:
            obj_persona = self.env['res.partner']
            if self.is_code:
                domain = [('id', '=', self.person_code)]
            else:
                domain = [
                    ('type_document', '=', self.type_document),
                    ('document_number', '=', self.document_number)
                ]
            domain.append(('es_ninio', '=', False))
            domain.append(('gender', '=', 'f'))
            domain.append(('months', '>=', 216))
            obj_persona = obj_persona.search(domain)
        if not obj_persona:
            self.verified = False
            self.partners_ids = False
            raise ValidationError('No se encontro coincidencias')
        else:
            self.verified = True
            self.partners_ids = obj_persona

        self._delete_data_fields()
        return {
            "type": "ir.actions.do_nothing",
        }

    @api.multi
    def save_data(self):
        if len(self.partners_ids) == 1:
            obj_persona = self.env['afiliacion.organizacion'].search([('person_id', '=', self.partners_ids[0].id)])
            form_id = self.env.ref('base_cuna.form_view_afiliacion_organizacion').id
            context = {
                'default_person_code': self.partners_ids.id,
                'default_nombre_completo': self.partners_ids.name,
                'default_fecha_nacimiento': self.partners_ids.birthdate,
                'default_person_id': self.partners_ids.id,
            }
            return {
                'type': 'ir.actions.act_window',
                'name': 'Afiliar Persona',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'afiliacion.organizacion',
                'views': [(form_id, 'form')],
                'view_id': form_id,
                'context': False if obj_persona else context,
                'res_id': obj_persona.id if obj_persona else False,
            }
        else:
            raise ValidationError(
                'Debe seleccionar solo una persona'
            )

    @api.model
    def create(self, values):
        clean_checkboxes(values)
        return super(BusquedaOrganizacionWizard, self).create(values)

    @api.multi
    def write(self, values):
        clean_checkboxes(values)
        return super(BusquedaOrganizacionWizard, self).write(values)


class MantenimientoNinios(models.TransientModel):

    _name = 'mantenimiento.ninios.wizard'

    editar = fields.Boolean(
        string=u'¿Cerrar Afiliación?'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión'
    )
    state = fields.Selection(selection=[
        ('activo', 'Activo'),
        ('inactivo', 'Inactivo'),
    ],
        string='Estado'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        string='Local',
        domain="[('comite_id', '=', comite_gestion_id)]"
    )
    ambiente_id = fields.Many2one(
        comodel_name='ambientes.local',
        string='Ambiente',
        domain="[('local_id', '=', local_id)]"
    )
    sala_id = fields.Many2one(
        comodel_name='salas.local',
        inverse_name='modulos_ids',
        domain="[('local_id', '=', local_id),"
               "('ambientes_id', '=', ambiente_id)]",
        string='Sala'
    )
    modulo_id = fields.Many2one(
        comodel_name='modulo.local',
        string=u'Módulo',
        domain="[('sala_id', '=', sala_id)]"
    )
    person_code = fields.Char(
        string='Código Persona'
    )
    is_code = fields.Boolean()
    document_number = fields.Char(
        string='Documento'
    )
    is_document = fields.Boolean()
    type_document = fields.Selection(
        selection=_opttipodocumento,
        string="Tipo de Documento"
    )
    is_full_name = fields.Boolean()
    partner_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string='Nombres y Apellidos'
    )
    ninio_ids = fields.Many2many(
        comodel_name='integrante.unidad.familiar',
        string=u'Niños'
    )
    modulo_ninios_ids = fields.Many2many(
        comodel_name='modulo.ninios',
        string='Afiliacion - Niños'
    )
    filtrado = fields.Boolean(
        string='Se filtro datos?'
    )

    def delete_data_fields(self):
        self.type_document = False
        self.document_number = False
        self.is_document = False
        self.person_code = False
        self.is_code = False
        self.is_full_name = False
        self.partner_id = False
        self.ninio_ids = False
        self.modulo_ninios_ids = False

    @api.multi
    def clean_fields(self):
        self.delete_data_fields()
        return {
            "type": "ir.actions.do_nothing",
        }

    @api.onchange('is_code', 'is_document', 'is_full_name')
    def _onchange_selected_fields(self):
        if self.is_code:
            self.is_document = self.type_document = self.document_number = self.is_full_name = self.partner_id = False
        if self.is_document:
            self.is_code = self.person_code = self.partner_id = False
        if self.is_full_name:
            self.type_document = self.document_number = self.is_code = self.person_code = False

    """
        Primero se busca el registro en el modelo res.partner si es que no se filtra desde nombres y apellidos
        para luego buscarlo en los integrantes de las familias
    """
    @api.multi
    def buscar_persona(self):
        if not self.is_full_name and not self.is_code and not self.is_document:
            raise ValidationError(u'Debe seleccionar una de las opciones para comenzar la búsqueda.')
        if self.is_full_name:
            obj_persona = self.partner_id
        else:
            if self.is_code:
                obj_persona = self.env['integrante.unidad.familiar'].search([
                    ('id', '=', int(self.person_code)),
                    ('activo', '=', True),
                    ('es_ninio', '=', True)
                ])
            else:
                obj_persona = self.env['res.partner'].search([
                    ('type_document', '=', self.type_document),
                    ('document_number', '=', self.document_number),
                    ('es_ninio', '=', True)
                ])
                if obj_persona:
                    obj_persona = self.env['integrante.unidad.familiar'].search([
                        ('integrante_id', '=', obj_persona.id),
                        ('activo', '=', True),
                        ('es_ninio', '=', True)
                    ])
            if obj_persona:
                if obj_persona.permitir_modulo and not self.editar:
                    raise ValidationError(u'Ya se encuentra registrado en un módulo.')
                if not obj_persona.permitir_modulo and self.editar:
                    raise ValidationError('No se encuentran afiliaciones para esta persona.')
            else:
                raise ValidationError('No se encontro coincidencias.')

        self.delete_data_fields()
        if obj_persona:
            if obj_persona.permitir_modulo and not self.editar:
                raise ValidationError(u'Ya se encuentra registrado en un módulo.')
            if not obj_persona.permitir_modulo and self.editar:
                raise ValidationError('No se encuentran afiliaciones para esta persona.')
            self.ninio_ids = obj_persona
            self.comite_gestion_id = obj_persona.comite_gestion_id
        else:
            raise ValidationError('No se encontro coincidencias.')
        if self.editar:
            self.buscar_afiliacion()
        return {
            "type": "ir.actions.do_nothing",
        }

    @api.multi
    def create_record(self):
        self.ensure_one()
        if self.modulo_ninios_ids:
            for rec in self.modulo_ninios_ids:
                if rec.fecha_fin:
                    rec.ninios_id.integrante_id.permitir_modulo = False
                    rec.ninios_id.permitir_modulo = False

    """
        Se limpia los campos para evitar errores en la busqueda con el raise ValidationError
    """
    @api.multi
    def salir_record(self):
        if self.modulo_ninios_ids:
            for rec in self.modulo_ninios_ids:
                rec.fecha_fin = False
                rec.state = 'activo'

    @api.model
    def create(self, values):
        clean_checkboxes(values)
        return super(MantenimientoNinios, self).create(values)

    @api.multi
    def write(self, values):
        clean_checkboxes(values)
        return super(MantenimientoNinios, self).write(values)

    @api.multi
    def crear_afiliacion_ninio(self):
        if not self.local_id or not self.ambiente_id or not self.sala_id or not self.modulo_id:
            raise ValidationError('Debe completar todos los campos para crear una afiliación.')
        if self.modulo_ninios_ids:
            raise ValidationError('Ya se genero afiliación.')
        obj_ninio_afiliacion = self.env['modulo.ninios'].create({
            'unidad_territorial_id': self.unidad_territorial_id.id,
            'ninios_id': self.ninio_ids[0].id,
            'fecha_inicio': fields.Date.today(),
            'comite_gestion_id': self.comite_gestion_id.id,
            'local_id': self.local_id.id,
            'ambiente_id': self.ambiente_id.id,
            'sala_id': self.sala_id.id,
            'modulo_id': self.modulo_id.id
        })
        form_id = self.env.ref('base_cuna.form_view_modulo_ninios_readonly').id
        return {
            'type': 'ir.actions.act_window',
            'name': 'Afiliación Niño',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'modulo.ninios',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'target': 'new',
            'res_id': obj_ninio_afiliacion.id
        }

    def buscar_afiliacion(self):
        obj_ninio_afiliacion = self.env['modulo.ninios'].search([
            ('ninios_id', '=', self.ninio_ids[0].id,),
            ('state', '=', 'activo')
        ])
        if obj_ninio_afiliacion:
            self.modulo_ninios_ids = obj_ninio_afiliacion
        else:
            raise ValidationError('No se encuentran afiliaciones para esta persona.')
        return {
            "type": "ir.actions.do_nothing",
        }


class EliminarAfiliaciones(models.TransientModel):

    _name = 'eliminar.afiliaciones'

    afiliacion_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        string='Afiliaciones'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comite Gestión'
    )
    nombre_tipo = fields.Char(
        string=u'Tipo de Afiliación'
    )

    @api.multi
    def actualizar_lineas_afiliacion(self):
        original = self.comite_gestion_id.afiliaciones_ids.filtered(lambda x: x.cargo == self.nombre_tipo)
        c1 = Counter(original)
        c2 = Counter(self.afiliacion_ids)
        diff = list((c1 - c2).elements())
        if diff:
            for rec in diff:
                rec.unlink()


class EmpadronamientoActores(models.TransientModel):

    _name = 'empadronamiento.actores'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id,
        readonly=True
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión',
        required=True
    )


class DynamicReportWizard(models.TransientModel):
    _name = "dynamic.report.wizard"

    servicio = fields.Selection(
        selection=[
            ('saf', 'SAF'),
            ('scd', 'SCD'),
        ],
        string='Tipos',
        required=True
    )
    scd_reportes = fields.Selection(
        selection=[
            ('apo_adm', 'Apoyo Administrativo'),
            ('asistencia', 'Asistencia'),
            ('av_planilla', 'Avance Planillas'),
            ('req_cg', 'Requerimientos CG'),
            ('planilla_vig', 'Reporte Planilla Vigente'),
            ('junta_dir', 'Reporte Junta Directiva'),
            ('saldos_cg', 'Reporte Saldos CG'),
            ('socias_pl', 'Reporte Socias SA Planilla'),
            ('guias_pl', u'Guías según Planilla'),
            ('cuid_asis', 'Lista Cuidadoras Asistencia'),
            ('lista_locales', 'Lista Locales'),
            ('lista_locales_infra', 'Lista Locales - Infraestructura'),
            ('ficha_9', 'Reporte Ficha N°9'),
            ('req_asis', 'Requerimientos vs Asistencia'),
            ('req_ut', 'Requerimientos por UT'),
            ('longitudinal_scd', 'Longitudinal SCD'),
            ('salud_scd', 'Salud SCD'),
            ('dataset_scd', 'Dataset SCD'),
        ],
        string='Reportes SCD'
    )
    saf_reportes = fields.Selection(
        selection=[
            ('actor_comunal', 'Actor Comunal'),
            ('datos_obs', 'Datos Observados'),
            ('fac_visitas', 'Facilitadoras con visitas'),
            ('rep_fam', 'Reporte Familias'),
            ('rep_vis', 'Reporte Visitas'),
            ('req_vis', 'Requerimientos vs visitas'),
            ('seg_fac', 'Seguimiento facilitador'),
            ('sesion_soc', u'Sesiones Socialización'),
            ('trama_cg', 'Trama CG'),
            ('trama_fac', 'Trama Facilitadoras'),
            ('trama_usu', 'Trama Usuarios'),
            ('longitudinal_saf', 'Longitudinal SAF'),
            ('rnu', 'RNU'),
            ('salud_saf', 'Salud SAF'),
            ('dataset_saf', 'Dataset SAF'),
        ],
        string='Reportes SAF'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today(),
        readonly=True
    )
    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes',
        default=lambda self: _obtener_tiempo(self, 'mes')
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        required=True,
        string=u'Año',
        default=lambda self: _obtener_tiempo(self, 'anio')
    )

    @api.onchange('servicio')
    def _onchange_servicio(self):
        if self.servicio == 'scd':
            self.saf_reportes = False
        if self.servicio == 'saf':
            self.scd_reportes = False

    @api.multi
    def excel_report_saf(self):
        context = self._context
        if self.saf_reportes == 'actor_comunal':
            return _get_report(context, 'ActoresComunalesReportXls', 'Actores Comunales')
        if self.saf_reportes == 'datos_obs':
            return _get_report(context, 'DatosObservadosXls', 'Datos Observados')
        if self.saf_reportes == 'fac_visitas':
            return _get_report(context, 'FacilitadorasVisitasXls', 'Facilitadoras - Visitas')
        if self.saf_reportes == 'rep_fam':
            return _get_report(context, 'ReporteFamiliaXls', 'Reporte Familias')
        # if self.saf_reportes == 'rep_vis':
        #     return _get_report(context, 'ActoresComunalesReportXls', 'Actores Comunales')
        # if self.saf_reportes == 'req_vis':
        #     return _get_report(context, 'ActoresComunalesReportXls', 'Actores Comunales')
        # if self.saf_reportes == 'seg_fac':
        #     return _get_report(context, 'ActoresComunalesReportXls', 'Actores Comunales')
        if self.saf_reportes == 'sesion_soc':
            return _get_report(context, 'SesionesSocializacionXls', 'Sesiones Socializacion')
        if self.saf_reportes == 'trama_cg':
            return _get_report(context, 'TramaCGXls', 'Trama CG')
        if self.saf_reportes == 'trama_fac':
            return _get_report(context, 'TramaFacilitadorXls', 'Trama Facilitadores')
        if self.saf_reportes == 'trama_usu':
            return _get_report(context, 'TramaUsuariosXls', 'Trama Usuarios')
        if self.saf_reportes == 'longitudinal_saf':
            return _get_report(context, 'LongitudinalSafXls', 'Longitudinal Saf')
        if self.saf_reportes == 'rnu':
            return _get_report(context, 'RNUReportXls', 'RNU')
        if self.saf_reportes == 'salud_saf':
            return _get_report(context, 'SaludSafXls', 'Salud SAF')
        if self.saf_reportes == 'dataset_saf':
            return _get_report(context, 'DatasetSAFXls', 'Dataset SAF')

    @api.multi
    def excel_report_scd(self):
        context = self._context
        if self.scd_reportes == 'apo_adm':
            return _get_report(context, 'ApoyoAdministrativoReportXls', 'Apoyo Administrativo')
        # if self.scd_reportes == 'asistencia':
            # return _get_report(context, 'RequerimientosCGReportXls', 'Requerimientos CG')
        # if self.scd_reportes == 'av_planilla':
            # return _get_report(context, 'RequerimientosCGReportXls', 'Requerimientos CG')
        if self.scd_reportes == 'req_cg':
            return _get_report(context, 'RequerimientosCGReportXls', 'Requerimientos CG')
        if self.scd_reportes == 'planilla_vig':
            return _get_report(context, 'PlanillaVigenteReportXls', 'Planilla Vigente')
        if self.scd_reportes == 'junta_dir':
            return _get_report(context, 'JuntaDirectivaReportXls', 'Junta Directiva')
        if self.scd_reportes == 'saldos_cg':
            return _get_report(context, 'SaldosCGReportXls', 'Saldos CG')
        if self.scd_reportes == 'socias_pl':
            return _get_report(context, 'SociasSAPlanillaReportXls', 'Socias SA - Planilla')
        if self.scd_reportes == 'guias_pl':
            return _get_report(context, 'GuiasPlanillaReportXls', 'Guias - Planilla')
        if self.scd_reportes == 'cuid_asis':
            return _get_report(context, 'CuidadorasAsistenciaReportXls', 'Cuidadoras Asistencia')
        if self.scd_reportes == 'lista_locales':
            return _get_report(context, 'ListaLocalesReportXls', 'Lista Locales')
        if self.scd_reportes == 'lista_locales_infra':
            return _get_report(context, 'LocalesInfraestructuraReportXls', 'Lista Locales Infraestructura')
        if self.scd_reportes == 'ficha_9':
            return _get_report(context, 'Ficha9ReportXls', 'Reporte Ficha N° 9')
        # if self.scd_reportes == 'req_asis':
            # return _get_report(context, 'SociasSAPlanillaReportXls', 'Saldos CG')
        if self.scd_reportes == 'req_ut':
            return _get_report(context, 'RequerimientosUTReportXls', 'Requerimientos por UT')
        if self.scd_reportes == 'longitudinal_scd':
            return _get_report(context, 'LongitudinalSCDXls', 'Longitudinal SCD')
        # if self.scd_reportes == 'salud_scd':
            # return _get_report(context, 'SociasSAPlanillaReportXls', 'Saldos CG')
        if self.scd_reportes == 'dataset_scd':
            return _get_report(context, 'DatasetSCDXls', 'Dataset SCD')
