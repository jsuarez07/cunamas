# -*- coding: utf-8 -*-


from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from suds.client import Client
from suds.sudsobject import asdict
from suds.transport.https import WindowsHttpAuthenticated
from pysimplesoap.client import SoapClient, SoapFault


months = [
    ('01', 'Enero'),
    ('02', 'Febrero'),
    ('03', 'Marzo'),
    ('04', 'Abril'),
    ('05', 'Mayo'),
    ('06', 'Junio'),
    ('07', 'Julio'),
    ('08', 'Agosto'),
    ('09', 'Setiembre'),
    ('10', 'Octubre'),
    ('11', 'Noviembre'),
    ('12', 'Diciembre')
]


def _validar_wsdl(url):
    wsdl1 = url
    try:
        SoapClient(wsdl=wsdl1, cache=None, ns="ser", soap_ns="soapenv", trace=True)
    except Exception:
        raise ValidationError('Error en consultar DNI, intente nuevamente.', )


def suds2dict(d):
    out = {'__class__': d.__class__.__name__}
    for k, v in asdict(d).iteritems():
        if hasattr(v, '__keylist__'):
            out[k] = suds2dict(v)
        elif isinstance(v, list):
            out[k] = []
            for item in v:
                if hasattr(item, '__keylist__'):
                    out[k].append(suds2dict(item))
                else:
                    out[k].append(item)
        else:
            out[k] = v
    return out


def verificar_lineas_activas(rec):
    """
        Verifica si el usuario tiene alguna linea de afiliacion a familia, modulo activa.
    """
    if rec:
        total = len(rec)
        lineas = rec.filtered(lambda x: x.fecha_fin)
        if total == len(lineas):
            return False
        else:
            return True
    else:
        return False


class ResPartner(models.Model):
    _inherit = 'res.partner'

    cat_ninio = fields.Selection(
        selection=[
            ('0_5', u'Niños 0-5 meses'),
            ('6_12', u'Niños 6-12 meses'),
            ('13_18', u'Niños 13-18 meses'),
            ('19_24', u'Niños 19-24 meses'),
            ('25_32', u'Niños 25-32 meses'),
        ],
        string='Categoria ninio',
        compute='_compute_cat_ninio',
        store=True
    )
    marital_status = fields.Selection(
        selection=[
            ('Soltero', 'Solter@'),
            ('Casado', 'Casad@'),
            ('Viudo', 'Viud@'),
            ('Divorciado', 'Divorciad@'),
            ('Separado', 'Separad@'),
            ('Conviviente', 'Conviviente')
        ],
        string='Estado Civil'
    )
    fecha_ingreso_pncm = fields.Date(
        string='Fecha de Ingreso al PNCM'
    )
    nivel_educativo_id = fields.Many2one(
        comodel_name='nivel.educativo',
        string='Nivel Educativo',
    )
    ultimo_grado_apr_id = fields.Many2one(
        comodel_name='ultimo.grado.aprobado',
        string=u'Último Grado Aprobado'
    )
    seguro_id = fields.Many2one(
        comodel_name='tipo.seguro',
        string='Tipo de Seguro'
    )
    ocupacion_id = fields.Many2one(
        comodel_name='tipo.ocupacion',
        string=u'Tipo de Ocupación'
    )
    discapacidad_id = fields.Many2many(
        comodel_name='tipo.discapacidad',
        string='Tipo de Discapacidad'
    )
    lengua_materna_id = fields.Many2one(
        comodel_name='lengua.materna',
        string='Lengua Materna'
    )
    nro_hijos = fields.Integer(
        string=u'Número de Hijos'
    )
    programa_social_ids = fields.Many2many(
        comodel_name='programa.social',
        string='Programa Social'
    )
    religion_id = fields.Many2one(
        comodel_name='tipo.religion',
        string=u'Religión'
    )
    person_code = fields.Char(
        string='Código Persona'
    )
    sabe_leer_y_escribir = fields.Boolean(
        string='¿Sabe leer y escribir?'
    )
    codigo_seguro = fields.Char(
        string=u'Código Seguro'
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        string='Familia'
    )
    tipo_persona = fields.Many2one(
        comodel_name='tipo.persona',
        string='Tipo de Cargo',
        compute='compute_afiliacion',
        store=True
    )
    nombre_tipo_persona = fields.Char(
        string='Tipo de Cargo-Nombre',
        compute='compute_afiliacion',
        store=True
    )
    es_ninio = fields.Boolean(
        string='Es ninio?',
        compute='_compute_months',
        store=True
    )
    months = fields.Integer(
        string='Meses',
        compute='_compute_months',
        store=True
    )
    anios = fields.Integer(
        string=u'Años',
        compute='_compute_months',
        store=True
    )
    permitir_familia = fields.Boolean(
        string='Esta activo en otra familia?',
        compute='compute_permitir_familia',
        store=True
    )
    integrantes_lines_ids = fields.One2many(
        comodel_name='integrante.unidad.familiar',
        inverse_name='integrante_id',
        string='Familia',
        store=True
    )
    modulos_ninios_ids = fields.One2many(
        comodel_name='modulo.ninios',
        inverse_name='partner_id',
        string=u'Afiliación a módulos',
        readonly=True
    )
    permitir_modulo = fields.Boolean(
        string=u'Esta activo en algún módulo?',
        readonly=True
    )
    afiliacion_ids = fields.One2many(
        comodel_name='afiliacion.organizacion.lines',
        inverse_name='person_id',
        string='Afiliaciones',
        readonly=True
    )
    permitir_afiliacion = fields.Boolean(
        string='Tiene activo alguna afiliacion?',
        compute='compute_afiliacion',
        store=True
    )
    madre_gestante = fields.Boolean(
        string='Madre Gestante'
    )
    validado_reniec = fields.Boolean(
        string='Validado por reniec'
    )
    mes_gestacion = fields.Selection(
        selection=[
            ('1_mes', '1 mes'),
            ('2_meses', '2 meses'),
            ('3_meses', '3 meses'),
            ('4_meses', '4 meses'),
            ('5_meses', '5 meses'),
            ('6_meses', '6 meses'),
            ('7_meses', '7 meses'),
            ('8_meses', '8 meses'),
            ('9_meses', '9 meses'),
        ],
        string=u'Mes Gestación'
    )
    semana_gestacion = fields.Integer(
        string=u'Semana de Gestación',
    )
    reniec_activo = fields.Boolean(
        default=lambda self: self._validar_reniec_activo()
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento',
    )
    province_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia',
    )
    district_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Distrito',
    )
    poblation_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Centro Poblado',
    )

    @api.constrains('fecha_ingreso_pncm', 'birthdate')
    def constrains_fecha_ingreso_pncm(self):
        if self.fecha_ingreso_pncm and self.birthdate:
            if self.fecha_ingreso_pncm < self.birthdate:
                raise ValidationError('Fecha de Ingreso al sistema no puede ser menor a fecha de nacimiento')

    @api.onchange('country_id')
    def validar_firm_oga_user(self):
        if self.country_id:
            return {'domain': {
                'state_id': [
                    ('country_id', '=', self.country_id.id),
                    ('state_id', '=', False),
                    ('province_id', '=', False)
                ],
            }}

    @api.multi
    @api.depends('es_ninio', 'months')
    def _compute_cat_ninio(self):
        for rec in self:
            if 0 <= rec.months <= 5:
                rec.cat_ninio = '0_5'
            elif 6 <= rec.months <= 12:
                rec.cat_ninio = '6_12'
            elif 13 <= rec.months <= 18:
                rec.cat_ninio = '13_18'
            elif 19 <= rec.months <= 24:
                rec.cat_ninio = '19_24'
            elif 25 <= rec.months <= 32:
                rec.cat_ninio = '25_32'

    def _validar_reniec_activo(self):
        url = 'http://192.168.252.7/ServiceShared/ConsultasReniec.svc?wsdl'
        try:
            SoapClient(wsdl=url, cache=None, ns="ser", soap_ns="soapenv", trace=True)
        except Exception:
            return True

    @api.onchange('semana_gestacion')
    def _onchange_semana_gestacion(self):
        if self.semana_gestacion:
            if self.semana_gestacion > 40:
                self.semana_gestacion = 40

    @api.multi
    def consulta_reniec(self):
        if self.type_document == 'dni':
            url = 'http://192.168.252.7/ServiceShared/ConsultasReniec.svc?wsdl'
            _validar_wsdl(url)
            ntlm = WindowsHttpAuthenticated(username='xxxxx', password='yyyyy')
            client = Client(url, transport=ntlm)
            response = client.service.ObtenerDatosPersonaDefault(dniConsulta=self.document_number)
            data = suds2dict(response)

            if data.get('Nombre') is not None:
                values = ({
                    'birthdate': data.get('FechaNacimiento'),
                    'gender': 'f' if data.get('Genero') == 49 else 'm',
                    'name': data.get('Nombre'),
                    'firstname': data.get('ApellidoPaterno'),
                    'secondname': data.get('ApellidoMaterno'),
                    'street': data.get('Direccion'),
                    'validado_reniec': True
                })
                self.write(values)
            else:
                raise ValidationError('N° de DNI inválido')
        else:
            raise ValidationError('El tipo de documento debe ser DNI')

    """
        Dependiendo a la afiliación activa se filtra el tipo de cargo de la persona y
        ademas cuando el campo permitir_afiliacion este activo no te permitira filtrar en otra afiliación
    """
    @api.multi
    @api.depends('afiliacion_ids', 'afiliacion_ids.state', 'afiliacion_ids.fecha_retiro')
    def compute_afiliacion(self):
        for rec in self:
            afiliacion = rec.afiliacion_ids.filtered(lambda x: x.state == 'active')
            if afiliacion:
                rec.permitir_afiliacion = True
                rec.tipo_persona = afiliacion[0].tipo_guia
                rec.nombre_tipo_persona = afiliacion[0].tipo_guia.name

    @api.multi
    @api.depends('integrantes_lines_ids', 'integrantes_lines_ids.fecha_fin')
    def compute_permitir_familia(self):
        for rec in self:
            rec.permitir_familia = verificar_lineas_activas(rec.integrantes_lines_ids)

    """
        Dependiendo a la fecha de nacimiento calcula cuantos meses tiene, ademas valida si es niño
        que este en el rango de 3 años como max. para afiliarlo a algun modulo o servicio.
    """
    @api.multi
    @api.depends('birthdate')
    def _compute_months(self):
        now = datetime.now()
        years = 0
        month = 0
        for rec in self:
            if rec.birthdate:
                birthdate = fields.Datetime.from_string(
                    rec.birthdate,
                )
                delta = relativedelta(now, birthdate)
                years = int(delta.years)
                month = int(delta.months)
            if years < 3:
                rec.es_ninio = True
            else:
                rec.es_ninio = False
            rec.months = years * 12 + month
            rec.anios = years


class ResUsers(models.Model):

    _inherit = 'res.users'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        domain="[('is_office','=',True)]"
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
    ],
        string='Servicio'
    )
    rol_admin = fields.Boolean(
        string='Vista general'
    )

    @api.multi
    def rol_admin_check(self):
        self.rol_admin = False if self.rol_admin else True


class MesesDias(models.Model):

    _name = 'meses.dias'

    month = fields.Selection(
        selection=months,
        required=True,
        string='Mes'
    )
    dias_utiles = fields.Integer(
        string=u'Días Utiles',
        required=True,
        default=30
    )
    dias_utiles_especial = fields.Integer(
        string=u'Días Utiles Especial',
        default=30
    )
    comite_gestion_ids = fields.Many2many(
        comodel_name='pncm.comitegestion',
        string=u'Comite Gestión',
    )

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.multi
    def agregar_cg(self):
        form_id = self.env.ref('base_cuna.form_view_meses_dias_create').id
        return {
            'type': 'ir.actions.act_window',
            'name': 'Dias Especiales',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'meses.dias',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'target': 'new',
            'res_id': self.id
        }


class Anios(models.Model):
    _name = 'tabla.anios'

    name = fields.Char(
        string='Nombre'
    )
    month_days_ids = fields.Many2many(
        comodel_name='meses.dias',
        default=lambda self: self._default_deposit_lines_ids(),
        string='Meses'
    )

    @api.model
    def _default_deposit_lines_ids(self):
        result = [
            dict(month=value) for value in
            ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
        ]
        return result

    @api.model
    def get_actual_year(self):
        obj_year = self.search([('name', '=', str(datetime.now().year))])
        if not obj_year:
            self.create({'name': str(datetime.now().year)})

    @api.constrains('month_days_ids')
    def _constraints_month_days_ids(self):
        if len(self.month_days_ids) != 12:
            raise ValidationError('Solo debe existir 12 registros, uno por cada mes')


class TablasBase(models.Model):
    _name = 'tabla.base'

    name = fields.Char(
        string='Nombre'
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    activo = fields.Boolean(
        string='Activo',
        default=True
    )

    @api.multi
    def write(self, vals):
        vals['usuario_mod_id'] = self.env.user.id
        vals['fecha_mod'] = fields.Date.today()
        return super(TablasBase, self).write(vals)


class TipoSeguro(models.Model):
    _name = 'tipo.seguro'
    _inherit = 'tabla.base'


class Discapacidad(models.Model):
    _name = 'tipo.discapacidad'
    _inherit = 'tabla.base'

    abrev = fields.Char()


class Parentesco(models.Model):
    _name = 'tipo.parentesco'
    _inherit = 'tabla.base'


class ProgramaSocial(models.Model):
    _name = 'programa.social'
    _inherit = 'tabla.base'


class Religion(models.Model):
    _name = 'tipo.religion'
    _inherit = 'tabla.base'


class NivelEducativo(models.Model):
    _name = 'nivel.educativo'
    _inherit = 'tabla.base'


class UltimoGradoAprobado(models.Model):
    _name = 'ultimo.grado.aprobado'
    _inherit = 'tabla.base'


class TipoOcupacion(models.Model):
    _name = 'tipo.ocupacion'
    _inherit = 'tabla.base'


class LenguaMaterna(models.Model):
    _name = 'lengua.materna'
    _inherit = 'tabla.base'


class TipoPersona(models.Model):
    _name = 'tipo.persona'
    _inherit = 'tabla.base'

    afiliacion = fields.Boolean(
        string='Filtrar en afiliacion?'
    )
    escala_subvencion = fields.Boolean(
        string='Filtrar en escala de subvencion?'
    )
    justificacion = fields.Boolean(
        string='Filtrar en el proceso de justificacion?'
    )
    abrev = fields.Char(
        string='Abreviatura'
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
        ('ambos', u'Ambos'),
    ])