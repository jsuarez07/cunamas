odoo.define('base_cuna.SurveyWidget', function(require) {
"use strict";

var core = require('web.core');
var form_common = require('web.form_common');
var Widget = require('web.Widget');
var Model = require('web.Model');
var QWeb = core.qweb;

var SurveyWidget = form_common.AbstractField.extend({
    template: 'SurveyWidget',

    events: {
//        "click #agregar-visita-btn": "addColumn",
//        "click #eliminar-visita-btn": "deleteSelectedColumns",
    },
    init: function() {
        var self = this;
        this._super.apply(this, arguments);
        //this.set("value", "");
        this.FortalecimientoModel = new Model('ficha.fortalecimiento');
        this.ReconocimientoModel = new Model('ficha.reconocimiento');
        this.field_manager.on("view_content_has_changed", this, function () {
            this.record_id = self.get_record_id();
            self.get_plantilla(this.record_id);
        });
    },
    start: function() {
        var self = this;
//        this.on("change:effective_readonly", this, function() {
//            this.display_field();
//            this.render_value();
//        });
//        this.display_field();
        return this._super();
    },
    display_field: function() {
        var self = this;
        this.$el.html(QWeb.render('SurveyWidget', {widget: this}));
        if (! this.get("effective_readonly")) {
            this.$("input").change(function() {
                self.internal_set_value(self.$("input").val());
            });
        }
    },
    render_value: function() {
        if (this.get("effective_readonly")) {
            this.$el.text(this.get("value"));
        } else {
            this.$("input").val(this.get("value"));
        }
    },
    //_render: function () {
    //    this.$el.html(qWeb.render('SurveyWidget', {widget: this}));
    //},
    _loadData: function () {
        var self = this;
    },
    get_record_id: function () {
        var active_id = this.view.datarecord.id;
        return active_id;
    },
    get_plantilla: function(record){
        var res;
        this.ReconocimientoModel.call("get_record",[record]).then(function(result){
            res = result[0];
            $('#tablas-visita-div').empty().append(res.plantilla);
            $('#tablas-visita-preg-div').empty().append(res.preguntas);
            if(res.nro_visitas > 0){
                for (var y = 0; y < res.nro_visitas; y++) {
                    var tblBodyObj = document.getElementById('tbl_visitas').tBodies[0];
                    var tblBodyObj1 = document.getElementById('tbl_visitas_respuestas').tBodies[0];
                    var row = tblBodyObj1.rows.length;
                    for (var x = 0; x < row; x++) {
                        var newchkbxcell = tblBodyObj.rows[x].insertCell();
                        var $cloned = $(tblBodyObj1.rows[x]).clone();

                        if($cloned.attr('id') == "cabecera"){
                            $cloned.attr('id', `visita_${y}`);
                            $cloned.html(`VISITA N° ${y+1}&nbsp;`);
                        }
                        if($cloned.find('input[type=radio]')){
                            var $radio = $cloned.find('input[type=radio]')
                            var name = $radio.attr('name');

                            $radio.attr('name', `${name}${y}`);

                            console.log($radio);
                            if($radio.text() == 'Sí'){
                                $radio.bind('click', function() {
                                  alert('User clicked on "foo."');
                                });

                                //$radio.addEventListener("click", function() {
                                  //  console.log('aaa');
                                //});
                            }
                        }

                        $cloned.appendTo(newchkbxcell);
                    }
                }
            }
        });
    },
});
core.form_widget_registry.add('survey', SurveyWidget);
return {
    SurveyWidget: SurveyWidget,
};
});

//$("#s").change(function() {
//        if (!this.checked) {
//            $("#sr").attr('disabled', 'disabled');
//            $("#sr").attr('readonly', 'true');
//        }
//        else {
//            $("#sr").removeAttr('disabled');
//            $("#sr").removeAttr('readonly');
//        }
//    });
//
//    //triger change event in case checkbox checked when user accessed page
//    $("#s").trigger("change")
//    addColumn: function(){
//        var record = this.get_record_id();
//        this.ReconocimientoModel.call("action_agregar_visita_widget",[record]);
////        var tblBodyObj = document.getElementById('tbl_visitas').tBodies[0];
////        var tblBodyObj1 = document.getElementById('tbl_visitas_respuestas').tBodies[0];
////        var row = tblBodyObj1.rows.length;
////        for (var x = 0; x < row; x++) {
////            var newchkbxcell = tblBodyObj.rows[x].insertCell();
////            $(tblBodyObj1.rows[x]).clone().appendTo(newchkbxcell);
////        }
//    },
//    deleteSelectedColumns: function() {
//        var tb = document.getElementById('tbl_visitas'); //html table
//        var NoOfcolumns = tb.rows[0].cells.length; //no. of columns in table
//        for (var clm = 1; clm < NoOfcolumns; clm++) {
//            var rw = tb.rows[0]; //0th row with checkboxes
//            var chkbox = rw.cells[clm].childNodes[0];
//            console.log(clm, NoOfcolumns, chkbox);
//            if (null != chkbox && true == chkbox.checked) {
//                console.log('entre');
//                var lastrow = tb.rows;
//                for (var x = 0; x < lastrow.length; x++) {
//                    tb.rows[x].deleteCell(clm);
//                    console.log('entr1');
//                }
//                NoOfcolumns--;
//                clm--;
//            } else {
//            //alert("not selected");
//            }
//        }
//    },
//    registros_visitas : function(record){
//        var id_visitas = [];
//        var ficha = new Model('ficha.reconocimiento').query(['survey_id'])
//                              .filter([
//                                    ['id', '=', record],
//                              ])
//                              .all()
//                              .then(function (result) {
//                                 _.each(result, function(item) {
//
//                                 });
//                              });
//
//        console.log(visitas);
//    },
