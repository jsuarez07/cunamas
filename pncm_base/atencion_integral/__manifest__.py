# -*- coding: utf-8 -*-

{
    "name": u"Atención Integral",
    "version": "1.0",
    "author": "Ganemo",
    "website": "http://www.ganemocorp.com/",
    "description": """
        Agrega asistencia, visitas y Fichas
    """,
    "depends": [
        'base_cuna',
    ],
    "data": [
        'security/ir.model.access.csv',
        'views/actions.xml',
        'views/forms.xml',
        'views/menus.xml',
        'views/trees.xml',
    ],
    'installable': True,
    'active': False
}
