# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import fields, models, api
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import pytz


def _obtener_tiempo(self, time):
        date_now = datetime.now(tz=pytz.timezone('America/Lima')).replace(tzinfo=None)
        if time == 'mes':
            return months[date_now.month - 1]
        else:
            rec = self.env['tabla.anios'].search([
                ('name', '=', date_now.year)
            ])
            return rec.id


def validar_fechas(fecha_inicio, fecha_fin):
    if fecha_fin and fecha_inicio:
        if fecha_inicio >= fecha_fin:
            raise ValidationError('La fecha de fin no puede ser menor o igual a la fecha de inicio')


def get_month_day_range(date):
    date = datetime.strptime(date, '%Y-%m-%d')
    first_day = date.replace(day=1)
    return first_day


def obtener_diagnostico_tamizaje(edad_examen, resultado):
    diagnostico = ''
    if edad_examen >= 0 and edad_examen < 6:
        diagnostico = "A" if resultado < 9.5 else "N"
    elif edad_examen >= 6:
        if resultado >= 0 and resultado < 7:
            diagnostico = "AS"
        elif resultado >= 7 and resultado < 10:
            diagnostico = "AM"
        elif resultado >= 10 and resultado < 11:
            diagnostico = "AL"
        elif resultado >= 11 and resultado <= 20:
            diagnostico = "N"
    return diagnostico


def filtrar_en_vistas(self, name, model, modo):
    """
        modo:
        # filtrar
        1 = unidad_territorial
        2 = servicios
        0 = ambos

    """
    action = {
        'name': name,
        'type': "ir.actions.act_window",
        'res_model': model,
        'view_type': "form",
        'view_mode': "tree,form",
    }
    user = self.env['res.users'].browse(self._uid)
    if modo == 0:
        if user.unidad_territorial_id and user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('unidad_territorial_id', '=', %d), ('servicio', '=', '%s')]" % \
                               (user.unidad_territorial.id, user.servicio)
        elif user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d)]" % user.unidad_territorial_id.id
        elif user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('servicio', '=', '%s')]" % user.servicio

    elif modo == 1:
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d)]" % user.unidad_territorial_id.id

    elif modo == 2:
        if user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('servicio', '=', '%s')]" % user.servicio
    return action


months = [
    ('01', 'Enero'),
    ('02', 'Febrero'),
    ('03', 'Marzo'),
    ('04', 'Abril'),
    ('05', 'Mayo'),
    ('06', 'Junio'),
    ('07', 'Julio'),
    ('08', 'Agosto'),
    ('09', 'Setiembre'),
    ('10', 'Octubre'),
    ('11', 'Noviembre'),
    ('12', 'Diciembre')
]


class SurveyLabel(models.Model):

    _inherit = 'survey.label'

    survey_id = fields.Many2one(
        comodel_name='survey.survey',
        string='Survey',
        related='question_id.page_id.survey_id',
        store=True
    )
    page_id = fields.Many2one(
        comodel_name='survey.page',
        related='question_id.page_id',
        string='Pagina',
        store=True
    )
    question_conditional_id = fields.Many2one(
        comodel_name='survey.question',
        string='Preguntas',
    )
    answer_id = fields.Many2one(
        comodel_name='survey.label',
        string='Respuesta'
    )


class Survey(models.Model):

    _inherit = 'survey.survey'

    model_ids = fields.Many2many(
        comodel_name='ir.model',
        string="Modelos donde se filtrara la encuesta"
    )
    es_visita = fields.Boolean(
        string='¿Es ficha de visita?'
    )

    @api.multi
    def action_start_survey2(self):
        return self.action_formulario('fill')

    @api.multi
    def action_mostrar_respuestas(self):
        return self.action_formulario('print')

    """
        El paremetro "word" indica:
         - fill : Se inicia automaticamente la encuesta.
         - print: Se muestra los resultados.
    """
    def action_formulario(self, word):
        """ Open the website page with the survey form """
        self.ensure_one()
        token = self.env.context.get('survey_token')
        trail = "/%s" % token if token else ""
        url = self.with_context(relative_url=True).public_url
        url = url.replace('/start/', '/%s/') % word

        return {
            'type': 'ir.actions.act_url',
            'name': "Respuestas",
            'target': 'self',
            'url': url + trail
        }


class SurveyInput(models.Model):

    _inherit = 'survey.user_input'

    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    fecha_visita = fields.Date(
        string='Fecha Visita',
        compute='_compute_fechas',
        store=True
    )
    fecha_prox_visita = fields.Date(
        string=u'Fecha Próxima Visita',
        compute='_compute_fechas',
        store=True
    )
    ficha_reconocimiento_id = fields.Many2one(
        comodel_name='ficha.reconocimiento',
        inverse_name='visita_ids',
        string='Ficha Reconocimiento'
    )
    ficha_fortalecimiento_id = fields.Many2one(
        comodel_name='ficha.fortalecimiento',
        inverse_name='visita_ids',
        string='Ficha Fortalecimiento'
    )
    ficha_gestante_id = fields.Many2one(
        comodel_name='ficha.gestante',
        inverse_name='visita_ids',
        string='Ficha Gestante'
    )
    persona_ficha_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Gestante/Niño',
        compute='_compute_anio_mes_persona_id',
        store=True
    )
    mes = fields.Selection(
        selection=months,
        string='Mes',
        compute='_compute_anio_mes_persona_id',
        store=True
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año',
        compute='_compute_anio_mes_persona_id',
        store=True
    )

    @api.model
    def create(self, values):
        if values.get('fecha_visita'):
            if values.get('ficha_reconocimiento_id'):
                recs = self.env['ficha.fortalecimiento'].search([
                    ('fecha_visita', '=', values['fecha_visita'])
                ])
                if recs:
                    raise ValidationError(
                        'Ya existe una visita con la fecha % en la Ficha de Fortalecimiento.' % values['fecha_visita']
                    )
            if values.get('ficha_fortalecimiento_id'):
                recs = self.env['ficha.reconocimiento'].search([
                    ('fecha_visita', '=', values['fecha_visita'])
                ])
                if recs:
                    raise ValidationError(
                        'Ya existe una visita con la fecha % en la Ficha de Reconocimiento.' % values['fecha_visita']
                    )
        obj = super(SurveyInput, self).create(values)
        return obj

    @api.multi
    def write(self, values):
        if values.get('fecha_visita'):
            id = self.read(['id'])
            rec = self.search([('id', '=', id[0]['id'])])

            if rec.ficha_reconocimiento_id:
                recs = self.env['ficha.fortalecimiento'].search([
                    ('fecha_visita', '=', values['fecha_visita'])
                ])
                if recs:
                    raise ValidationError(
                        'Ya existe una visita con la fecha % en la Ficha de Fortalecimiento.' % values['fecha_visita']
                    )
            if rec.ficha_fortalecimiento_id:
                recs = self.env['ficha.reconocimiento'].search([
                    ('fecha_visita', '=', values['fecha_visita'])
                ])
                if recs:
                    raise ValidationError(
                        'Ya existe una visita con la fecha % en la Ficha de Reconocimiento.' % values['fecha_visita']
                    )
        r = super(SurveyInput, self).write(values)
        return r

    @api.multi
    @api.depends('ficha_reconocimiento_id', 'ficha_fortalecimiento_id', 'ficha_gestante_id')
    def _compute_anio_mes_persona_id(self):
        for rec in self:
            if rec.ficha_reconocimiento_id:
                rec.persona_ficha_id = rec.ficha_reconocimiento_id.gestante_id
                rec.mes = rec.ficha_reconocimiento_id.mes
                rec.anio = rec.ficha_reconocimiento_id.anio
            if rec.ficha_fortalecimiento_id:
                rec.persona_ficha_id = rec.ficha_fortalecimiento_id.gestante_id
                rec.mes = rec.ficha_fortalecimiento_id.mes
                rec.anio = rec.ficha_fortalecimiento_id.anio
            if rec.ficha_gestante_id:
                rec.persona_ficha_id = rec.ficha_gestante_id.gestante_id
                rec.mes = rec.ficha_gestante_id.mes
                rec.anio = rec.ficha_gestante_id.anio

    @api.multi
    @api.depends('user_input_line_ids')
    def _compute_fechas(self):
        for rec in self:
            for line in rec.user_input_line_ids:
                if line.question_id.type == 'datetime' and line.question_id.fecha_visita:
                    rec.fecha_visita = line.value_date
                if line.question_id.type == 'datetime' and line.question_id.fecha_prox_visita:
                    rec.fecha_prox_visita = line.value_date

    @api.multi
    def accion_revisar_visita(self):
        self.ensure_one()
        response = self
        if self.state == 'done':
            return self.survey_id.with_context(survey_token=response.token).action_mostrar_respuestas()
        else:
            return self.survey_id.with_context(survey_token=response.token).action_start_survey2()


class SurveyQuestion(models.Model):

    _inherit = 'survey.question'

    fecha_visita = fields.Boolean(
        string='Fecha Visita'
    )
    fecha_prox_visita = fields.Boolean(
        string=u'Fecha Próxima Visita'
    )

    @api.multi
    @api.onchange('type')
    def _onchange_type(self):
        for rec in self:
            if rec.type != 'datetime':
                rec.fecha_visita = rec.fecha_prox_visita = False

    @api.multi
    def accion_agregar_horas(self):
        if self.labels_ids:
            raise ValidationError(u'Ya existen registros en las líneas de respuestas.')
        hora = 0.0
        while hora < 24.00:
            result = '{0:02.0f}:{1:02.0f}'.format(*divmod(hora * 60, 60))
            hora += 0.25
            self.write({
                'labels_ids': [(0, 0, {
                    'value': result,
                    'question_id': self.id
                })]
            })


class IntegranteUnidadFamiliar(models.Model):
    _inherit = 'integrante.unidad.familiar'

    fichas_rec_ids = fields.One2many(
        comodel_name='ficha.reconocimiento',
        inverse_name='gestante_id',
        string='Reconocimiento'
    )
    fichas_fort_ids = fields.One2many(
        comodel_name='ficha.fortalecimiento',
        inverse_name='gestante_id',
        string='Fortalecimiento'
    )
    ficha_gestante_ids = fields.One2many(
        comodel_name='ficha.reconocimiento',
        inverse_name='gestante_id',
        string='Ficha gestante'
    )
    ficha_salud_ids = fields.One2many(
        comodel_name='ficha.salud',
        inverse_name='ninio_id',
        string='Ficha Salud'
    )


class FichaBase(models.Model):

    _name = 'ficha.base'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id,
        required=True
    )
    unidad_territorial_id_log = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=', unidad_territorial_id),"
               "('servicio', '!=', 'scd')]",
        string=u'Comite Gestión',
        required=True
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        string='Familia',
        domain="[('comite_gestion_id','=', comite_gestion_id),"
               "('services', '!=', 'scd')]",
    )
    pais_id = fields.Many2one(
        comodel_name='res.country',
        string='País',
        default=lambda self: self.env.user.company_id.country_id
    )
    departamento_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento',
        compute='_compute_facilitador',
        store=True
    )
    provincia_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia',
        compute='_compute_facilitador',
        store=True
    )
    distrito_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Distrito',
        compute='_compute_facilitador',
        store=True
    )
    centro_poblado_id = fields.Many2one(
        comodel_name='res.country.state',
        compute='_compute_facilitador',
        store=True,
        string='Centro Poblado'
    )
    cod_familia = fields.Char(
        string=u'Codigo Familia',
        compute='_compute_facilitador',
        store=True
    )
    facilitador_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        string='Facilitador/a',
        compute='_compute_facilitador',
        store=True
    )
    cod_facilitador = fields.Char(
        string=u'Código Facilitador',
        compute='_compute_facilitador',
        store=True
    )
    acomp_tecnico_id = fields.Many2one(
        comodel_name='pncm.comite.equipo.tecnico',
        string=u'Acompañante Técnico',
        domain="[('comite_gestion_id', '=', comite_gestion_id),"
               "('cargo','=', u'ACOMPAÑANTE TECNICO'),"
               "('fecha_retiro', '=', False)]",
    )
    cod_acomp_tecnico = fields.Char(
        string=u'Codigo Acompañante Técnico',
        compute='_compute_code_acomp_tecnico_id',
        store=True
    )
    gestante_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Gestante/Niño',
        domain="[('familia_id', '=', familia_id),"
               "('es_ninio','=', True),"
               "('fecha_fin', '=', False)]"
    )
    cod_gestante = fields.Char(
        string=u'Código Gestante',
        compute='_compute_code_gestante_id',
        store=True
    )
    fecha_nac_ges = fields.Date(
        string='Fecha Nacimiento',
        compute='_compute_code_gestante_id',
        store=True
    )
    edad_ges = fields.Integer(
        string='Edad',
        compute='_compute_code_gestante_id',
        store=True
    )
    genero = fields.Selection(
        selection=[
            ('m', 'Masculino'),
            ('f', 'Femenino')],
        string='Sexo',
        compute='_compute_code_gestante_id',
        store=True
    )
    cuid_principal_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string='Cuidador Principal',
        compute='_compute_facilitador',
        store=True
    )
    cod_cuid_principal = fields.Char(
        string=u'Código Cuidador Principal',
        compute='_compute_facilitador',
        store=True
    )
    parentesco_id = fields.Many2one(
        comodel_name='tipo.parentesco',
        string=u'Parentesco con el niño',
        compute='_compute_code_gestante_id',
        store=True
    )
    survey_id = fields.Many2one(
        comodel_name='survey.survey',
        default=lambda self: self._get_survey(True, 0),
        string="Survey"
    )
    response_id = fields.Many2one(
        comodel_name='survey.user_input',
        string="Response",
        ondelete="set null",
        oldname="response"
    )
    visita_ids = fields.Many2many(
        comodel_name='survey.user_input',
        string="Visitas"
    )
    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes',
        default=lambda self: _obtener_tiempo(self, 'mes')
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        required=True,
        string=u'Año',
        default=lambda self: _obtener_tiempo(self, 'anio')
    )
    nro_visitas = fields.Integer(
        string='N° Visitas',
        compute='_compute_nro_visitas',
        store=True
    )
    fecha_visita = fields.Date(
        string='Fecha de Visita',
        default=fields.Date.today()
    )
    visitas_html = fields.Html(
        string='Visitas',
        default=lambda self: self._get_survey(True, 1),
        invisible=True
    )
    tipo_preguntas_2 = fields.Html(
        string='Visitas',
        default=lambda self: self._get_survey(True, 2),
        invisible=True
    )
    nro_visita_eliminar = fields.Char(
        string='N° visita a eliminar'
    )

    @api.multi
    def eliminar_visita(self):
        if self.visita_ids and self.nro_visita_eliminar > 0:
            self.visita_ids[self.nro_visita_eliminar - 1].unlink()
            self.nro_visita_eliminar = 0

    @api.model
    def get_record(self, id):
        if not id:
            survey_id = self._get_survey(True, 0)
            rec = [{
                'visitas': True,
                'plantilla': self.dibujar_visitas(survey_id),
                'preguntas': self.dibujar_respuestas_visitas(survey_id),
                'nro_visitas': 0
            }]
            return rec
        else:
            rec = self.search([('id', '=', id)])
            if rec.visita_ids:
                return rec.mapped(lambda x: {
                    'visitas': True,
                    'plantilla': self.dibujar_visitas(rec.survey_id),
                    'preguntas': self.dibujar_respuestas_visitas(rec.survey_id),
                    'nro_visitas': len(rec.visita_ids),
                })
            else:
                return rec.mapped(lambda x: {
                    'visitas': False,
                    'plantilla': self.dibujar_visitas(rec.survey_id),
                    'preguntas': self.dibujar_respuestas_visitas(rec.survey_id),
                    'nro_visitas': 0,
                    #'date':datetime.strptime(x.date,"%Y-%m-%d").date().strftime("%d/%m/%Y")if x.date else '',
                })

    def dibujar_visitas(self, survey_id):
        tabla = '''
                    <table cellspacing="0" border="1" id="tbl_visitas">
                        <tbody>
                            <tr indice="cabecera">
                                <td align="center">SOBRE LA VISITA AL HOGAR PARA SER RELLENADO CADA VISITA
                                </td>
                            </tr>
                            %s
                        </tbody>
                    </table>
        '''
        body = ''
        for pagina in survey_id.page_ids:
            for preguntas in pagina.question_ids:
                temp = '<td>' + preguntas.question + '</td>'
                if preguntas.type != 'simple_choice':
                    body += '<tr>%s</tr>' % (temp)
                else:
                    if len(preguntas.labels_ids) == 2:

                        body += '<tr>%s</tr>' % (temp )
                    else:
                        body += '<tr>%s</tr>' % (temp)
                        for label in preguntas.labels_ids:
                            if preguntas.display_mode == 'columns':
                                temp = '<td>' + label.value + '</td>'
                                body += '<tr>%s</tr>' % (temp)
        tabla = tabla % body
        return tabla

    def dibujar_respuestas_visitas(self, survey_id):
        tabla = '''
                    <table cellspacing="0" border="1" id="tbl_visitas_respuestas">
                        <tbody>
                            <tr id="cabecera">
                                <td align="center">VISITA N° 2&nbsp;
                                    <button id="eliminar-visita-btn" type="button">Eliminar Visita</button>
                                </td>
                            </tr>
                            %s
                        </tbody>
                    </table>
        '''
        body = ''
        for pagina in survey_id.page_ids:
            for preguntas in pagina.question_ids:
                if preguntas.type != 'simple_choice':
                    temp2 = '''
                            <td>
                                <div style="width: 100px;">
                                    %s
                                </div>
                            </td>
                            '''
                    if preguntas.type == 'datetime':
                        temp2 = temp2 % '<input type="date"/>'
                    elif preguntas.type == 'numerical_box':
                        temp2 = temp2 % '<input type="text" pattern="[0-9]+"/>'
                    else:
                        temp2 = temp2 % "<input type='text'/>"
                    body += '<tr>%s</tr>' % (temp2)

                else:
                    if len(preguntas.labels_ids) == 2:
                        temp2 = '''
                            <td align="center">
                                <input name="radio_%s" type="radio">Sí&nbsp;&nbsp;
                                <input name="radio_%s" type="radio">No
                            </td>
                        ''' % (preguntas.question, preguntas.question)
                        body += '<tr>%s</tr>' % (temp2)
                    else:
                        body += '<tr>%s</tr>' % ('<td></td>')
                        for label in preguntas.labels_ids:
                            if preguntas.display_mode == 'columns':
                                temp2 = '<td><input name="radio_%s" type="radio"/></td>' % preguntas.question
                                body += '<tr>%s</tr>' % (temp2)
        tabla = tabla % body
        return tabla

    def _get_survey(self, visita, tipo):
        """
            tipo:
            0 = retorna ficha visita
            1 = dibujar visitas
            2 = dibjua respuestas visitas
        """
        self_model = self.env['ir.model'].search([('name', '=', self._name)]).id
        survey_state = self.env['survey.stage'].search([('sequence', '=', 2)]).id
        domain = [
            ('model_ids', 'in', self_model),
            ('stage_id', '=', survey_state),
            ('es_visita', '=', visita)
        ]
        rec = self.env['survey.survey'].search(domain)

        if len(rec) > 1:
            raise ValidationError('Existen activas más de una encuesta para este modelo.')
        if not rec:
            return False
            #raise ValidationError('No existen visitas activas relacionadas al modelo.')
        if tipo == 1:
            return self.dibujar_visitas(rec)
        elif tipo == 2:
            return self.dibujar_respuestas_visitas(rec)
        elif tipo == 0:
            return rec

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.id)) for obj in self]

    @api.depends('cuid_principal_id')
    def _compute_code_cuid_principal(self):
        if self.gestante_id:
            self.cod_cuid_principal = self.cuid_principal_id.id

    @api.depends('gestante_id')
    def _compute_code_gestante_id(self):
        if self.gestante_id:
            self.cod_gestante = self.gestante_id.id
            self.fecha_nac_ges = self.gestante_id.integrante_id.birthdate
            self.edad_ges = self.gestante_id.integrante_id.anios
            self.genero = self.gestante_id.integrante_id.gender
            self.parentesco_id = self.gestante_id.parentesco_id

    @api.depends('familia_id')
    def _compute_facilitador(self):
        if self.familia_id:
            self.facilitador_id = self.familia_id.facilitador_id
            self.cod_facilitador = self.familia_id.facilitador_id.id
            self.cod_familia = self.familia_id.id
            cuid = self.familia_id.integrantes_ids.filtered(lambda x: x.es_cuidador_principal)
            self.cuid_principal_id = cuid
            self.cod_cuid_principal = cuid.id
            self.departamento_id = self.familia_id.departamento_id
            self.provincia_id = self.familia_id.provincia_id
            self.distrito_id = self.familia_id.distrito_id
            self.centro_poblado_id = self.familia_id.centro_poblado_id

    @api.depends('acomp_tecnico_id')
    def _compute_code_acomp_tecnico_id(self):
        if self.acomp_tecnico_id:
            self.cod_acomp_tecnico = self.acomp_tecnico_id.id
    
    @api.depends('visita_ids')
    def _compute_nro_visitas(self):
        if self.visita_ids:
            self.nro_visitas = len(self.visita_ids)

    @api.multi
    def action_start_survey(self):
        self.ensure_one()
        # create a response and link it to this applicant
        if not self.survey_id:
            self.survey_id = self._get_survey(True, 0)
        if not self.response_id:
            response = self.env['survey.user_input'].create({'survey_id': self.survey_id.id})
            self.response_id = response.id
            return self.survey_id.with_context(survey_token=response.token).action_start_survey2()
        else:
            response = self.response_id
            return self.survey_id.with_context(survey_token=response.token).action_mostrar_respuestas()


class FichaGestante(models.Model):

    _name = 'ficha.gestante'
    _inherit = 'ficha.base'

    survey_id = fields.Many2one(
        comodel_name='survey.survey',
        default=lambda self: self._get_survey(True, 0),
        string="Survey"
    )
    etapa = fields.Selection(
        selection=[
            ('fortalecimiento', 'Fortalecimiento'),
            ('reconocimiento', 'Reconocimiento')],
        string='Etapa',
        required=True
    )
    visita_ids = fields.One2many(
        comodel_name='survey.user_input',
        inverse_name='ficha_gestante_id',
        string="Visitas",
        ondelete="set null",
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        string='Familia',
        compute='_compute_familia_id',
        store=True
    )
    cod_familia = fields.Char(
        string=u'Codigo Familia',
        compute='_compute_familia_id',
        store=True
    )
    mes_gestacion = fields.Selection(
        selection=[
            ('1_mes', '1 mes'),
            ('2_meses', '2 meses'),
            ('3_meses', '3 meses'),
            ('4_meses', '4 meses'),
            ('5_meses', '5 meses'),
            ('6_meses', '6 meses'),
            ('7_meses', '7 meses'),
            ('8_meses', '8 meses'),
            ('9_meses', '9 meses')
        ],
        string='Mes Gestación',
        required=True
    )
    gestante_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        inverse_name='ficha_gestante_ids',
        string=u'Gestante/Niño',
        domain="[('familia_id', '=', familia_id),"
               "('es_ninio','=', True),"
               "('fecha_fin', '=', False)]"
    )

    @api.multi
    @api.depends('gestante_id')
    def _compute_familia_id(self):
        for rec in self:
            if rec.gestante_id:
                rec.familia_id = rec.gestante_id.familia_id
                rec.cod_familia = rec.gestante_id.familia_id.id

    @api.multi
    def action_agregar_visita(self):
        self.ensure_one()
        # create a response and link it to this applicant
        if self.visita_ids:
            record = self.visita_ids.filtered(lambda x: x.state != 'done')
            if record:
                raise ValidationError('Debe terminar las visitas ya registradas.')
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        response = self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_gestante_id': self.id
        })
        return survey.with_context(survey_token=response.token).action_start_survey2()

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas(self, 'Ficha de Visista a la Gestante', 'ficha.gestante', 1)


class FichaReconocimiento(models.Model):

    _name = 'ficha.reconocimiento'
    _inherit = 'ficha.base'

    visita_ids = fields.One2many(
        comodel_name='survey.user_input',
        inverse_name='ficha_reconocimiento_id',
        string="Visitas",
        ondelete="set null",
    )
    gestante_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        inverse_name='fichas_rec_ids',
        string=u'Gestante/Niño',
        domain="[('familia_id', '=', familia_id),"
               "('es_ninio','=', True),"
               "('fecha_fin', '=', False)]"
    )

    @api.multi
    def action_agregar_visita(self):
        self.ensure_one()
        # create a response and link it to this applicant
        if self.visita_ids:
            record = self.visita_ids.filtered(lambda x: x.state != 'done')
            if record:
                raise ValidationError('Debe terminar las visitas ya registradas.')
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        response = self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_reconocimiento_id': self.id
        })
        return survey.with_context(survey_token=response.token).action_start_survey2()

    @api.model
    def get_record(self, id):
        if not id:
            survey_id = self._get_survey(True, 0)
            rec = [{
                'visitas': True,
                'plantilla': self.dibujar_visitas(survey_id),
                'preguntas': self.dibujar_respuestas_visitas(survey_id),
                'nro_visitas': 0
            }]
            return rec
        else:
            rec = self.search([('id', '=', id)])
            if rec.visita_ids:
                return rec.mapped(lambda x: {
                    'visitas': True,
                    'plantilla': self.dibujar_visitas(rec.survey_id),
                    'preguntas': self.dibujar_respuestas_visitas(rec.survey_id),
                    'nro_visitas': len(rec.visita_ids),
                })
            else:
                return rec.mapped(lambda x: {
                    'visitas': False,
                    'plantilla': self.dibujar_visitas(rec.survey_id),
                    'preguntas': self.dibujar_respuestas_visitas(rec.survey_id),
                    'nro_visitas': 0,
                    # 'date':datetime.strptime(x.date,"%Y-%m-%d").date().strftime("%d/%m/%Y")if x.date else '',
                })

    @api.multi
    def action_agregar_visita_widget(self):
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_reconocimiento_id': self.id
        })

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas(self, 'Ficha Reconocimiento', 'ficha.reconocimiento', 1)


class FichaFortalecimiento(models.Model):

    _name = 'ficha.fortalecimiento'
    _inherit = 'ficha.base'

    visita_ids = fields.One2many(
        comodel_name='survey.user_input',
        inverse_name='ficha_fortalecimiento_id',
        string="Visitas",
        ondelete="set null",
    )
    gestante_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        inverse_name='fichas_fort_ids',
        string=u'Gestante/Niño',
        domain="[('familia_id', '=', familia_id),"
               "('es_ninio','=', True),"
               "('fecha_fin', '=', False)]"
    )

    @api.multi
    def action_agregar_visita(self):
        self.ensure_one()
        # create a response and link it to this applicant
        if self.visita_ids:
            record = self.visita_ids.filtered(lambda x: x.state != 'done')
            if record:
                raise ValidationError('Debe terminar las visitas ya registradas.')
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        
        response = self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_fortalecimiento_id': self.id
        })
        return survey.with_context(survey_token=response.token).action_start_survey2()

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas(self, 'Ficha Fortalecimiento', 'ficha.fortalecimiento', 1)


class FichaAsistenciaSocializacion(models.Model):

    _name = 'ficha.socializacion'
    _inherit = 'tabla.base'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id
    )
    unidad_territorial_id_log = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión',
        required=True
    )
    fecha_sesion = fields.Date(
        string=u'Fecha de Sesión',
        required=True
    )
    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes',
        default=lambda self: _obtener_tiempo(self, 'mes')
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        required=True,
        string=u'Año',
        default=lambda self: _obtener_tiempo(self, 'anio')
    )
    familias_ids = fields.Many2many(
        comodel_name='socializacion.lineas',
        string='Familias'
    )
    nro_familias = fields.Integer(
        string='N° Familias Asistentes',
        compute='_compute_nro_familias',
        store=True
    )

    @api.depends('familias_ids')
    def _compute_nro_familias(self):
        if self.familias_ids:
            self.nro_familias = len(self.familias_ids.filtered(lambda x: x.asistio))

    @api.multi
    def buscar_visitas_familias(self):
        dia = get_month_day_range(self.fecha_sesion)
        visitas = self.env['survey.user_input'].search([
            ('fecha_visita', '>=', dia),
            ('fecha_visita', '<=', self.fecha_sesion)
        ])

        lista_familia = []
        for line in visitas:
            if line.ficha_reconocimiento_id:
                ficha = self.crear_linea(line.ficha_reconocimiento_id)
                ficha.ficha_id = self.id
                lista_familia.append(ficha.id)
            if line.ficha_fortalecimiento_id:
                ficha = self.crear_linea(line.ficha_fortalecimiento_id)
                ficha.ficha_id = self.id
                lista_familia.append(ficha.id)
        self.familias_ids = [(6,0, lista_familia)]
    
    def crear_linea(self, line):
        rec = self.env['socializacion.lineas'].create({
            'familia_id': line.familia_id.id,
            'cod_familia': line.familia_id.id,
            'cuid_principal_id': line.cuid_principal_id.id,
            'cod_cuid_principal': line.cuid_principal_id.id,
            'dni_cuid_principal': line.cuid_principal_id.integrante_id.document_number,
            'facilitador_id': line.facilitador_id.id,
            'cod_facilitador': line.facilitador_id.id,
            'dni_facilitador': line.facilitador_id.person_id.document_number,
            'unidad_territorial_id': line.unidad_territorial_id.id,
            'mes': line.mes,
            'anio': line.anio.id
        })
        return rec

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas(self, u'Ficha de Asistencia de Sesiones de Socialización', 'ficha.socializacion', 1)


class SocializacionLineas(models.Model):

    _name = 'socializacion.lineas'

    ficha_id = fields.Many2one(
        comodel_name='ficha.socializacion',
        string='Ficha Socializacion'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial'
    )
    mes = fields.Selection(
        selection=months,
        string='Mes',
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año',
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        readonly=True
    )
    cod_familia = fields.Integer(
        string=u'Código Familia',
        readonly=True
    )
    cuid_principal_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string='Cuidador Principal',
        readonly=True
    )
    cod_cuid_principal = fields.Integer(
        string=u'Cód. Cuidador Principal',
        readonly=True
    )
    dni_cuid_principal = fields.Char(
        string='DNI Cuidador Principal',
        readonly=True
    )
    facilitador_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        string='Facilitador/a',
        readonly=True
    )
    cod_facilitador = fields.Char(
        string=u'Código Facilitador',
        readonly=True
    )
    dni_facilitador = fields.Char(
        string='DNI Facilitador',
        readonly=True
    )
    asistio = fields.Boolean(
        string=u'Asistió'
    )


class FichaSalud(models.Model):

    _name = 'ficha.salud'

    fecha_visita = fields.Date(
        string='Fecha de Visita',
        required=True,
        default=fields.Date.today()
    )
    mes = fields.Selection(
        selection=months,
        string='Mes',
        compute='_compute_anio_mes',
        store=True
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año',
        compute='_compute_anio_mes',
        store=True
    )

    @api.multi
    @api.depends('fecha_visita')
    def _compute_anio_mes(self):
        for rec in self:
            if rec.fecha_visita:
                fecha_visita = datetime.strptime(rec.fecha_visita, '%Y-%m-%d').date()
                mes = str(fecha_visita.month - 1)
                if len(mes) == 1:
                    mes = '0' + mes
                rec.mes = mes
                rec.anio = self.env['tabla.anios'].search([
                    ('name', '=', fecha_visita.year)
                ])


    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id
    )
    unidad_territorial_id_log = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión',
        required=True
    )
    centro_poblado_id = fields.Many2one(
        comodel_name='res.country.state',
        string=u'Centro Poblado',
        related='comite_gestion_id.centro_poblado_id',
        store=True,
        readonly=True
    )
    cod_familia = fields.Integer(
        string=u'Código Familia',
        compute='_compute_familia',
        store=True
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        string='Familia',
        compute='_compute_familia',
        store=True
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar')
    ],
        string='Servicio'
    )
    ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        inverse_name='ficha_salud_ids',
        string=u'Niño',
        domain="[('es_ninio','=', True),"
               "('comite_gestion_id', '=', comite_gestion_id),"
               "('services', '=', servicio),"
               "('fecha_fin', '=', False)]",
        required=True
    )
    facilitador_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        string='Facilitador/a',
        compute='_compute_familia',
        store=True
    )
    tipo_establecimiento = fields.Selection(
        selection=[
            ('puesto_salud', 'PUESTO DE SALUD'),
            ('hospital_essalud', 'HOSPITAL ESSALUD'),
            ('ubap_essalud', 'UBAP ESSALUD')
        ],
        string='Tipo de establecimiento de salud',
        required=True
    )
    nombre_establecimiento = fields.Char(
        string='Nombre de establecimiento según juridiscción',
        required=True
    )
    fecha_nac = fields.Date(
        string='Fecha de Nacimiento',
        compute='_compute_familia',
        store=True
    )
    fecha_ingreso_pncm = fields.Date(
        string='Fecha de Ingreso al PNCM',
        compute='_compute_familia',
        store=True
    )
    seguro_id = fields.Many2one(
        comodel_name='tipo.seguro',
        string='Tipo de Seguro de salud',
        required=True
    )
    discapacidad_id = fields.Many2many(
        comodel_name='tipo.discapacidad',
        string='Tipo de Discapacidad',
        required=True
    )
    peso_nacer = fields.Float(
        string='Peso al nacer(en kilogramos)'
    )
    talla_nacer = fields.Float(
        string=u'Talla al nacer(en centímetros)'
    )
    fecha_ini_micro = fields.Date(
        string='Fecha de Inicio Consumo de Micronutrientes(Previo al ingreso)'
    )
    nro_meses_mcn = fields.Integer(
        string='Nro. Meses Consumo Micronutrientes(Previo al ingreso)'
    )
    alergia = fields.Boolean(
        string=u'Sufre alguna alergía(Medicamentos/Alimentos)'
    )
    enfermedad_nac = fields.Boolean(
        string='Sufre alguna enfermedad de Nacimiento'
    )
    control_cred_ids = fields.One2many(
        comodel_name='control.cred',
        inverse_name='ficha_salud_id',
        string='Controles CRED'
    )
    inmunizacion_ids = fields.One2many(
        comodel_name='inmunizacion.control',
        inverse_name='ficha_salud_id',
        string='Inmunizaciones'
    )
    tamizaje_ids = fields.One2many(
        comodel_name='tamizaje.anemia',
        inverse_name='ficha_salud_id',
        string='Tamizajes Anemia'
    )

    @api.depends('ninio_id')
    def _compute_familia(self):
        if self.ninio_id:
            self.fecha_nac = self.ninio_id.integrante_id.birthdate
            self.fecha_ingreso_pncm = self.ninio_id.integrante_id.fecha_ingreso_pncm
            self.familia_id = self.ninio_id.familia_id
            self.cod_familia = self.ninio_id.familia_id.id
            self.facilitador_id = self.ninio_id.familia_id.facilitador_id

    @api.multi
    def accion_agregar_tamizaje_anemia(self):
        form_id = self.env.ref('atencion_integral.form_view_tamizaje_anemia').id
        context = {
            'default_ninio_id': self.ninio_id.id,
            'default_fecha_nac': self.fecha_nac,
            'default_ficha_salud_id': self.id
        }
        return {
            'name': 'Tamizaje',
            'type': 'ir.actions.act_window',
            'res_model': 'tamizaje.anemia',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'context': context
        }

    @api.multi
    def filtra_saf_vista(self):
        context = {'default_servicio': 'saf'}
        action = {
            'name': 'Ficha Salud SAF',
            'type': "ir.actions.act_window",
            'res_model': 'ficha.salud',
            'view_type': "form",
            'view_mode': "tree,form",
            'context': context
        }
        user = self.env['res.users'].browse(self._uid)
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d), ('servicio', '=', 'saf')]" % \
                                   user.unidad_territorial_id.id
        else:
            action['domain'] = "[('servicio', '=', 'saf')]"
        return action

    @api.multi
    def filtra_scd_vista(self):
        context = {'default_servicio': 'scd'}
        action = {
            'name': 'Ficha Salud SCD',
            'type': "ir.actions.act_window",
            'res_model': 'ficha.salud',
            'view_type': "form",
            'view_mode': "tree,form",
            'context': context
        }
        user = self.env['res.users'].browse(self._uid)
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', %d), ('servicio', '=', 'scd')]" % \
                               user.unidad_territorial_id.id
        else:
            action['domain'] = "[('servicio', '=', 'scd')]"
        return action


class ControlCRED(models.Model):

    _name = 'control.cred'

    ficha_salud_id = fields.Many2one(
        comodel_name='ficha.salud',
        inverse_name='control_cred_ids',
        string='Ficha Salud'
    )
    ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Niño',
        domain="[('es_ninio','=', True),"
               "('comite_gestion_id', '=', comite_gestion_id),"
               "('services', '=', 'saf'),"
               "('fecha_fin', '=', False)]",
        readonly=True
    )
    fecha_nac = fields.Date(
        string='Fecha Nacimiento',
        compute='_compute_fecha_nac',
        store=True
    )
    edad_control = fields.Selection(
        selection=[
            ('1', '1 mes'),
            ('2', '2 meses'),
            ('3', '3 meses'),
            ('4', '4 meses'),
            ('5', '5 meses'),
            ('6', '6 meses'),
            ('7', '7 meses'),
            ('8', '8 meses'),
            ('9', '9 meses'),
            ('10', '10 meses'),
            ('11', '11 meses'),
            ('12', u'1 año'),
            ('14', u'1 año 2 meses'),
            ('16', u'1 año 4 meses'),
            ('18', u'1 año 6 meses'),
            ('20', u'1 año 8 meses'),
            ('22', u'1 año 10 meses'),
            ('24', u'2 años'),
            ('27', u'2 años 3 meses'),
            ('30', u'2 años 6 meses'),
            ('33', u'2 años 9 meses'),
            ('36', u'3 años'),
        ],
        string='Edad de control',
        required=True
    )
    fecha_limite_inicial = fields.Date(
        string=u'Fecha Límite Inicial',
        compute='_compute_fecha_limite_inicio',
        store=True
    )
    fecha_limite_final = fields.Date(
        string=u'Fecha Límite Final',
        compute='_compute_fecha_limite_final',
        store=True
    )
    fecha_control = fields.Date(
        string='Fecha Control',
        required=True
    )
    peso_kg = fields.Float(
        string='Peso(kg)',
        required=True
    )
    talla_cm = fields.Float(
        string='Talla(cm)',
        required=True
    )
    talla_edad = fields.Char(
        string='Talla/Edad',
        compute='_compute_calculo_oms',
        store=True
    )
    peso_talla = fields.Char(
        string='Peso/Talla',
        compute='_compute_calculo_oms',
        store=True
    )
    peso_edad = fields.Char(
        string='Peso/Edad',
        compute='_compute_calculo_oms',
        store=True
    )
    riesgo_talla = fields.Boolean(
        string='Riesgo Talla'
    )
    riesgo_peso = fields.Boolean(
        string='Riesgo Peso'
    )
    riesgo_diarrea = fields.Boolean(
        string='Riego Diarrea'
    )
    examen_descarte = fields.Boolean(
        string=u'¿Realizo Exámen de descarte?'
    )
    tiene_parasitosis = fields.Boolean(
        string='¿Tiene parasitosis?'
    )
    recibe_tratamiento = fields.Boolean(
        string='¿Recibe tratamiento?'
    )

    @api.depends('peso_kg', 'talla_cm')
    def _compute_calculo_oms(self):
        oms = self.env['tipo.oms'].search([
            ('tipo', '=', 'omspe'),
            ('sexo', '=', self.ninio_id.integrante_id.gender),
            ('valor', '=', self.ninio_id.integrante_id.months)
        ])
        if oms:
            if oms.dpos2 >= self.peso_kg >= oms.dneg2:
                self.peso_edad = 'NORMAL'
            elif self.peso_kg < oms.dneg2:
                self.peso_edad = 'DESNUTRICION'
            elif self.peso_kg > oms.dpos2:
                self.peso_edad = 'SOBREPESO'

            ds = oms.do - oms.dneg1
            zte = (self.talla_cm - oms.do) / ds
            if zte >= -2 and zte <= 2:
                self.talla_edad = 'NORMAL'
            elif zte >= -5 and zte <= -2:
                self.talla_edad = 'TALLA BAJA'
            elif zte >= 2 and zte <= 5:
                self.talla_edad = 'ALTO'

    @api.onchange('fecha_control', 'fecha_limite_final', 'fecha_limite_inicial')
    def _onchange_fecha_control(self):
        if self.fecha_control:
            if self.fecha_limite_final < self.fecha_control:
                self.fecha_control = self.fecha_limite_final
            if self.fecha_limite_inicial > self.fecha_control:
                self.fecha_control = self.fecha_limite_inicial

    @api.depends('fecha_nac', 'edad_control')
    def _compute_fecha_limite_inicio(self):
        if self.edad_control and self.fecha_nac:
            fecha_aux = datetime.strptime(self.fecha_nac, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_limite_inicial = fecha_aux + relativedelta(months=int(self.edad_control)) - \
                relativedelta(days=1)

    @api.depends('fecha_limite_inicial')
    def _compute_fecha_limite_final(self):
        if self.fecha_limite_inicial:
            fecha_aux = datetime.strptime(self.fecha_limite_inicial, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_limite_final = fecha_aux + relativedelta(months=1) - relativedelta(days=1)

    @api.onchange('examen_descarte', 'tiene_parasitosis')
    def _onchange_parasitosis(self):
        if not self.examen_descarte:
            self.tiene_parasitosis = self.recibe_tratamiento = False
        if not self.tiene_parasitosis:
            self.recibe_tratamiento = False

    @api.depends('ninio_id')
    def _compute_fecha_nac(self):
        if self.ninio_id:
            self.fecha_nac = self.ninio_id.integrante_id.birthdate


class TipoOMS(models.Model):
    _name = 'tipo.oms'

    tipo = fields.Selection(
        selection=[
            ('omspe', 'OMSPE'),
            ('omste', 'OMSTE'),
        ],
        string='Tipo'
    )
    sexo = fields.Selection(
        selection=[
            ('h', 'H'),
            ('m', 'M'),
        ],
        string='Sexo'
    )
    valor = fields.Integer(
        string='Valor'
    )
    dneg4 = fields.Float(
        string='DNEG4'
    )
    dneg3 = fields.Float(
        string='DNEG3'
    )
    dneg2 = fields.Float(
        string='DNEG2'
    )
    dneg1 = fields.Float(
        string='DNEG1'
    )
    do = fields.Float(
        string='DO'
    )
    dpos1 = fields.Float(
        string='DPOS1'
    )
    dpos2 = fields.Float(
        string='DPOS2'
    )
    dpos3 = fields.Float(
        string='DPOS3'
    )
    dpos4 = fields.Float(
        string='DPOS4'
    )
    valor_tipo_l = fields.Float(
        string='VALOR_TIPO_L'
    )
    valor_tipo_m = fields.Float(
        string='VALOR_TIPO_M'
    )
    valor_tipo_s = fields.Float(
        string='VALOR_TIPO_S'
    )


class Inmunizaciones(models.Model):

    _name = 'inmunizacion.control'

    ficha_salud_id = fields.Many2one(
        comodel_name='ficha.salud',
        inverse_name='inmunizacion_ids',
        string='Ficha Salud'
    )
    ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Niño',
        domain="[('es_ninio','=', True),"
               "('comite_gestion_id', '=', comite_gestion_id),"
               "('services', '=', 'saf'),"
               "('fecha_fin', '=', False)]",
        readonly=True
    )
    vacunas = fields.Selection(
        selection=[
            ('bcg', 'BCG'),
            ('hvb', 'HVB'),
            ('apo', 'REFUERZO APO'),
            ('pentavalente', 'PENTAVALENTE'),
            ('neumococo', 'NEUMOCOCO'),
            ('influenza', 'INFLUENZA'),
            ('rotavirus', 'ROTAVIRUS'),
            ('spr', 'SPR'),
            ('antiamarilica', u'ANTIAMARÍLICA'),
            ('dpt', u'REFUERZO DPT'),
            ('ipv', u'IPV/APO'),
        ],
        string='Vacunas',
        required=True
    )
    periodo = fields.Selection(
        selection=[
            ('1_mes', '1 mes'),
            ('2_mes', '2 mes'),
            ('4_mes', '4 mes'),
            ('6_mes', '6 mes'),
            ('7_mes', '7 mes'),
            ('8_mes', '8 mes'),
            ('12_mes', '12 mes'),
            ('15_mes', '15 mes'),
            ('18_mes', '18 mes'),
            ('24_36_mes', '24-36 mes')
        ],
        string='Meses',
        required=True
    )
    fecha_vacuna = fields.Date(
        string='Fecha',
        required=True
    )


class TamizajeAnemia(models.Model):

    _name = 'tamizaje.anemia'

    ficha_salud_id = fields.Many2one(
        comodel_name='ficha.salud',
        inverse_name='tamizaje_ids',
        string='Ficha Salud'
    )
    ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Niño',
        domain="[('es_ninio','=', True),"
               "('comite_gestion_id', '=', comite_gestion_id),"
               "('services', '=', 'saf'),"
               "('fecha_fin', '=', False)]",
        readonly=True
    )
    fecha_nac = fields.Date(
        string='Fecha Nacimiento',
        readonly=True
    )
    edad_control = fields.Integer(
        string='Edad de control'
    )
    fecha_examen = fields.Date(
        string=u'Fecha Exámen',
        required=True,
        default=fields.Date.today()
    )
    resultado = fields.Float(
        string='Resultado q/dL',
        required=True
    )
    edad_examen = fields.Integer(
        string=u'Edad Exámen',
        required=True
    )
    diagnostico = fields.Selection(
        selection=[
            ('A', 'Anemia'),
            ('N', 'Normal'),
            ('AS', 'Anemia Severa'),
            ('AM', 'Anemia Moderada'),
            ('AL', 'Anemia Leve'),
        ],
        string='Diagnostico',
        readonly=True
    )
    es_control = fields.Boolean(
        string='Es tamizaje de control?'
    )
    tamizaje_control_ids = fields.Many2many(
        comodel_name='tamizaje.anemia.control',
        string='Tamizaje Anemia Control'
    )
    fue_validado = fields.Boolean(
        string='Fue validado?'
    )

    # @api.depends('fecha_nac', 'edad_control')
    # def _compute_fecha_limite_inicio(self):
    #     if self.edad_control and self.fecha_nac:
    #         fecha_aux = datetime.strptime(self.fecha_nac, DEFAULT_SERVER_DATE_FORMAT)
    #         self.fecha_limite_inicial = fecha_aux + relativedelta(months=int(self.edad_control)) - \
    #                                     relativedelta(days=1)
    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.multi
    def delete_record(self):
        self.unlink()

    @api.onchange('edad_examen')
    def _onchange_edad_examen(self):
        if self.edad_examen > 36:
            self.edad_examen = 36
        elif self.edad_examen < 0:
            self.edad_examen = 0

    @api.onchange('resultado')
    def _onchange_resultado(self):
        if self.resultado > 20:
            self.resultado = 20
        elif self.resultado < 0:
            self.resultado = 0

    @api.multi
    def validar_diagnostico(self):
        self.diagnostico = obtener_diagnostico_tamizaje(self.edad_examen, self.resultado)
        self.fue_validado = True
        return {
            "type": "ir.actions.do_nothing",
        }


class TamizajeAnemiaControl(models.Model):

    _name = 'tamizaje.anemia.control'
    _inherit = 'tamizaje.anemia'

    diagnostico = fields.Selection(
        selection=[
            ('A', 'Anemia'),
            ('N', 'Normal'),
            ('AS', 'Anemia Severa'),
            ('AM', 'Anemia Moderada'),
            ('AL', 'Anemia Leve'),
        ],
        string='Diagnostico',
        compute='_compute_diagnostico',
        store=True
    )
    tamizaje_control_ids = fields.Char(
        string='Tamizaje'
    )
    ficha_salud_id = fields.Char(
        string='Ficha Salud'
    )

    @api.multi
    @api.depends('edad_examen', 'resultado')
    def _compute_diagnostico(self):
        for rec in self:
            rec.diagnostico = obtener_diagnostico_tamizaje(rec.edad_examen, rec.resultado)

    @api.model
    def create(self, values):
        diagnostico = obtener_diagnostico_tamizaje(values['edad_examen'], values['resultado'])
        values['diagnostico'] = diagnostico
        return super(TamizajeAnemiaControl, self).create(values)
