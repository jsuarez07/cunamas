# -*- coding: utf-8 -*-
from openerp import fields, models, api
from odoo.addons.pncm_comite_gestion.models import utils


class CMEditarIdentificacion(models.TransientModel):
    _name = 'pncm.infra.wizard.local.editar_identificacion'

    ut_id = fields.Char(
        string=u'Unidad Territorial',
        readonly=True
    )
    comite_id = fields.Integer(
        readonly=True
    )
    comite_name = fields.Char(
        string=u'Comité de gestión',
        readonly=True
    )
    code = fields.Char(
        string=u'Código de Local',
        readonly=True
    )
    name = fields.Char(
        string=u'Nombre',
        size=250,
        required=True
    )
    tipo_local = fields.Selection(
        selection=[
            ('ciai', u'CIAI'),
            ('sa', u'SA'),
            ('ambos', u'Ambos (CIAI y SA)')
        ],
        required=True
    )
    modalidad_intervencion = fields.Selection(
        selection=[
            ('amplicacion_cob', u'Amplicación de Cobertura'),
            ('migracion', u'Migración'),
            ('emergencia', u'Emergencia'),
        ],
        required=True,
        string=u'Modalidad de Intervención'
    )
    procedencia = fields.Selection(
        selection=[
            ('municipal', u'Municipal'),
            ('comunal', u'Comunal'),
            ('privado', u'Privado'),
            ('parroquial', u'Parroquial'),
            ('educativo', u'Educativo'),
            ('pncm', u'PNCM'),
            ('otro', u'Otro')
        ],
        required=True
    )
    propietario_id = fields.Many2one(
        'res.partner',
        string=u'Propietario/Poseedor del Local',
        required=True)

    pais_id = fields.Many2one(
        'res.country',
        'País',
        default=lambda self: self.env.user.company_id.country_id
    )
    departamento_id = fields.Many2one(
        'res.country.state', 'Departamento',
        required=True
    )
    provincia_id = fields.Many2one(
        'res.country.state', 'Provincia',
        required=True,
        domain="[('state_id', '=', departamento_id), ('province_id', '=', "
               "False)]"
    )
    distrito_id = fields.Many2one(
        'res.country.state', 'Distrito',
        required=True,
        domain="[('state_id', '=', departamento_id), ('province_id', '=', "
               "provincia_id)]"
    )
    centro_poblado_id = fields.Many2one(
        'res.country.state',
        u'Centro Poblado',
        domain="[('state_id', '=', departamento_id), ('province_id', '=', "
               "provincia_id), ('district_id', '=', distrito_id)]",
        required=True
    )
    direccion = fields.Char(string=u'Dirección', required=True, size=255)
    referencia = fields.Text(string=u'Referencia')
    lat = fields.Char(string=u'Latitud')
    lng = fields.Char(string=u'Longitud')

    @api.multi
    def guardar_local(self):
        obj = self.env['pncm.infra.local'].\
            search([('id', '=', self._get_value('active_id'))])

        mhelper = utils.ModelHelper()
        update_context = {}
        mhelper.set_values_for_update(self, update_context, [
            'name', 'tipo_local', 'modalidad_intervencion', 'procedencia',
            'propietario_id', 'direccion', 'referencia', 'tipo_local',
            'departamento_id', 'provincia_id', 'distrito_id',
            'centro_poblado_id', 'lat', 'lng'
        ])

        obj.update(update_context)

    @api.onchange('comite_id')
    def _al_cambiar_comite(self):
        if self.comite_id:
            intervenciones_comite = \
                self.env['pncm.comitegestion.intervencion'].search(
                    [('comite_gestion_id', '=', self.comite_id)])
            departamentos_ids = [obj.departamento_id.id for
                                 obj in intervenciones_comite]
            return {'domain': {'departamento_id': ['&',
                                                   ('country_id.code',
                                                    '=', 'PE'),
                                                   ('id', 'in',
                                                    departamentos_ids)]}}

    @api.onchange('departamento_id')
    def _al_cambiar_departamento(self):
        if self.departamento_id:
            intervenciones = self.env['pncm.comitegestion.intervencion']. \
                search([('comite_gestion_id', '=', self.comite_id)])
            provincia_ids = [obj.provincia_id.id for obj in intervenciones]
            return {'domain': {'provincia_id': [('id', 'in', provincia_ids)]}}

    @api.onchange('provincia_id')
    def _al_cambiar_provincia(self):
        if self.provincia_id:
            intervenciones = self.env['pncm.comitegestion.intervencion']. \
                search([('comite_gestion_id', '=', self.comite_id)])
            desitritos_ids = [obj.distrito_id.id for obj in intervenciones]
            return {'domain': {'distrito_id': [('id', 'in', desitritos_ids)]}}

    @api.onchange('distrito_id')
    def _al_cambiar_distrito(self):
        if self.distrito_id:
            intervenciones = self.env['pncm.comitegestion.intervencion']. \
                search([('comite_gestion_id', '=', self.comite_id)])
            centro_poblado_ids = [obj.centro_poblado_id.id
                                  for obj in intervenciones]
            return {'domain': {'centro_poblado_id': [('id', 'in',
                                                      centro_poblado_ids)]}}

    def _get_value(self, key):
        return self._context.get(key, False)
