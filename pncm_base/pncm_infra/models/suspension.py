# -*- coding: utf-8 -*-
from openerp import fields, models


class CMSuspensionLocal(models.Model):
    _name = 'pncm.infra.suspension.local'

    motivo = fields.Char(
        string=u'Motivo',
        required=True,
        size=250
    )
    fecha_inicio = fields.Date(
        string=u'Fecha de inicio suspensión',
        required=True
    )
    fecha_fin = fields.Date(
        string=u'Fecha de fin suspensión'
    )
    local_id = fields.Many2one(
        'pncm.infra.local',
        string=u'Local',
        required=True
    )
