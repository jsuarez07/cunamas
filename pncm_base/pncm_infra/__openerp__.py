# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014 Consultoria YarosLab (<http://www.yaroslab.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Modulo de Infraestructura",
    "version": "1.0",
    "author": "CUNAMAS",
    "website": "http://www.cunamas.gob.pe/",
    "category": "Localization",
    "description": u"""
    Modulo para la gestión de infraestructura de locales Cuna Más
    """,
    "depends": [
        "web",
        "base",
        "hr",
        "pncm_comite_gestion"
    ],
    "init_xml": [],
    "data": [
    ],
    "demo_xml": [],
    "update_xml": [
        'views/infra_base.xml',
        'views/local.xml',
        'views/saneamiento.xml',
        'views/identificacion.xml',
        'views/editar_identificacion.xml',
        'views/presupuesto.xml',
        'views/activacion.xml',
        'views/suspension.xml',
        'views/baja.xml'
    ],
    'installable': True,
    'active': False
}
