# -*- coding: utf-8 -*-

{
    "name": "Creando las Unidades Territoriales",
    "version": "1.0",
    "author": "CUNAMAS",
    "website": "http://www.cunamas.gob.pe/",
    "category": "Localization",
    "description": """
    Extendiento el Catalogo de hr.department para usarla
    como Unidad Territoriales
    """,
    "depends": [
        'base',
        'web',
        'hr',
        'pncm_comite_gestion',
        'hr_positions'
    ],
    "init_xml": [],
    "data": [
        'views/hr_department_views.xml',
        'views/pncm_unidad_territorial_base.xml'
    ],
    "demo_xml": [],
    "update_xml": [
    ],
    'installable': True,
    'active': False
}
