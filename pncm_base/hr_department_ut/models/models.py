# -*- coding: utf-8 -*-

from openerp import fields, models, api
from openerp.exceptions import ValidationError


class CMUT(models.Model):
    _inherit = 'hr.department'

    code = fields.Char(
        string=u'Código',
        required=True,
        store=True
    )
    esCT = fields.Boolean(string=u'¿Es CT?', help=u'Coordinación Territorial')
    parentUT = fields.Many2one('hr.department',
                               string=u'UT',
                               domain="[('is_office','=',True), "
                                      "('esCT','=',False)]")
    is_office = fields.Boolean(
        string=u'Información Adicional'
    )
    services = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
        ('ambos', u'Ambos'),
    ],
        string=u'Servicio'
    )
    country_id = fields.Many2one(
        'res.country',
        u'País',
        default=lambda self: self.env.user.company_id.country_id
    )
    departamento_id = fields.Many2one(
        'res.country.state', u'Departamento',
        domain="[('country_id', '=', country_id), "
               "('state_id', '=', False), "
               "('province_id', '=', False)]")
    provincia_id = fields.Many2one(
        'res.country.state', u'Provincia',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', False)]")
    distrito_id = fields.Many2one(
        'res.country.state', u'Distrito',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', provincia_id), "
               "('district_id','=' False)]")
    direccion = fields.Char(
        string=u'Dirección',
        size=50
    )
    referencia = fields.Text('Referencia')
    ut_ambitos_intervencion = fields.One2many(
        'pncm.ambito.intervencion',
        'ut_id',
        u'Ambitos de Intervención')
    ut_comites_gestion = fields.One2many('pncm.comitegestion',
                                         'unidad_territorial_id',
                                         u'Comites de Gestion')
    ut_equipo_tecnico = fields.One2many(
        'hr.employee',
        'department_id',
        u'Equipo Tecnico',
    )
    resolucion = fields.Char(
        string=u'Resolución:',
        size=50
    )
    fecha_inicio = fields.Date(string=u'Fecha de Inicio', required=False)
    fecha_fin = fields.Date(string=u'Fecha de Fin', required=False)

    @api.model
    def create(self, vals):
        if vals['is_office']:
            state = self.env['res.country.state']\
                .search([('id', '=', vals['departamento_id'])], limit=1)
            if vals['esCT']:
                codigo = u'CT-' + '{:03}'.format((self.env['hr.department'].
                                                  search_count([('esCT', '=',
                                                                 True)]) + 1))
            else:
                codigo = u'UT-' + state.code_pe + '-' + \
                         '{:03}'.format((self.env['hr.department'].
                                         search_count([('esCT', '=',
                                                        False)]) + 1))
            vals.update({'code': codigo, 'is_office': True})
        return super(CMUT, self).create(vals)

    @api.onchange('parentUT', 'departamento_id', 'esCT')
    def _get_codigo(self):
        codigo = self._generar_codigo()
        return {'value': {'code': codigo}}

    def _generar_codigo(self):
        if self.departamento_id:
            state = self.env['res.country.state']\
                .search([('id', '=', self.departamento_id.id)], limit=1)
            if self.esCT:
                codigo = u'CT-' + '{:03}'.format((self.env['hr.department'].
                                                  search_count([('esCT', '=',
                                                                 True)]) + 1))
            else:
                if state.code_pe:
                    acronimo = state.code_pe
                    codigo = u'UT-' + acronimo + '-' + '{:03}'.format(
                        (self.env['hr.department'].search_count([
                            ('esCT', '=', False)]) + 1))
                else:
                    raise ValidationError(
                        u'Estimado usuario, comuníquese con el administrador, '
                        u'se debe configurar el acrónimo. ')
        else:
            codigo = ""
        return codigo

    @api.constrains('code')
    def _check_duplicate_code(self):
        codes = self.search([])
        if self.code:
            for c in codes:
                if c.code:
                    if self.code.lower() == c.code.lower() and \
                            self.id != c.id:
                        raise ValidationError(
                            u'Estimado usuario, el codigo se '
                            u'encuentra duplicado. ')
        else:
            raise ValidationError(u"Error: Debe Ingresar un código")

    @api.onchange('departamento_id')
    def _change_departamento(self):
        if self.departamento_id:
            return {
                'value': {
                    'provincia_id': [''],
                    'distrito_id': ['']}}

    @api.onchange('provincia_id')
    def _change_provincia(self):
        if self.provincia_id:
            return {
                'value': {
                    'distrito_id': ['']}}


class CMAmbitoIntervencion(models.Model):
    _name = 'pncm.ambito.intervencion'

    ut_id = fields.Many2one('hr.department', u'Unidad Territorial')
    departamento_id = fields.Many2one(
        'res.country.state', u'Departamento',
        domain="[('country_id', '=', country_id), "
               "('state_id', '=', False), "
               "('province_id', '=', False)]", required=True)
    provincia_id = fields.Many2one(
        'res.country.state', u'Provincia',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', False)]", required=True)
    distrito_id = fields.Many2one(
        'res.country.state', u'Distrito',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', False)]", required=True)

    @api.multi
    def name_get(self):
        return [
            (obj.id, u'{}/{}/{}'.
             format(obj.departamento_id.name,
                    obj.provincia_id.name, obj.distrito_id.name)) for
            obj in self
        ]

    @api.onchange('departamento_id')
    def _change_departamento(self):
        if self.departamento_id:
            return {
                'value': {
                    'provincia_id': [''],
                    'distrito_id': ['']}}

    @api.onchange('provincia_id')
    def _change_provincia(self):
        if self.provincia_id:
            return {
                'value': {
                    'distrito_id': ['']}}

    _sql_constraints = [
        ('code_unique',
         'unique(ut_id, departamento_id, provincia_id, distrito_id)',
         u'Estimado usuario, no se pueden elegir dos valores iguales en el '
         u'ambito de intervención(valores repetidos)')
    ]
