# -*- coding: utf-8 -*-

{
    "name": u"Transferencias y Devoluciones",
    "version": "1.0",
    "author": "Ganemo",
    "website": "http://www.ganemocorp.com/",
    "description": """
        Agrega asistencia, visitas y Fichas
    """,
    "depends": [
        'base_cuna',
        'pncm_comite_gestion',
        'pncm_infra',
        'hr_department_ut'
    ],
    "data": [
        'security/ir.model.access.csv',
        'views/actions.xml',
        'views/forms.xml',
        'views/menus.xml',
        'views/reports.xml',
        'views/trees.xml',
    ],
    'installable': True,
    'active': False
}
