# -*- coding: utf-8 -*-


from .models import months, _obtener_tiempo
from odoo import api, fields, models
from odoo.exceptions import ValidationError


class SolicitudRequerimiento(models.TransientModel):
    _name = 'solicitud.requerimiento.wizard'

    name = fields.Char(
        string='Nombre',
        default='Solicitud de Requerimientos'
    )
    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes',
        default=lambda self: _obtener_tiempo(self, 'mes')
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        required=True,
        string=u'Año',
        default=lambda self: _obtener_tiempo(self, 'anios')
    )
    requerimiento_lines = fields.Many2many(
        comodel_name='requerimiento.lines',
        string='Requerimientos',
        relation = 'req_lines_scd'
    )
    requerimiento_saf_lines = fields.Many2many(
        comodel_name='requerimiento.lines',
        string='Requerimientos saf',
        relation='req_lines_saf'
    )
    servicio = fields.Selection(
        selection=[
            ('scd', u'Cuidado Diurno'),
            ('saf', u'Acompañamiento Familiar')
        ],
        string='Servicio',
        default=lambda self: False if self.env.user.servicio == 'ambos' else self.env.user.servicio,
    )
    servicio_log = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
        ('ambos', u'Ambos'),
    ],
        string=u'Servicio usuario logueado',
        default=lambda self: self.env.user.servicio
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    unidad_territorial_id_log = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )

    @api.multi
    def buscar_requerimientos_comitegestion(self):
        saf_list = []
        scd_list = []
        for cg in self.unidad_territorial_id.ut_comites_gestion:
            if cg.state == 'activo':
                if self.servicio == 'saf' and cg.servicio in ['saf', 'ambos']:
                    rec = self.verificar_servicio_linea('saf', cg)
                    saf_list.append(rec.id)
                if self.servicio == 'scd' and cg.servicio in ['scd', 'ambos']:
                    rec = self.verificar_servicio_linea('scd', cg)
                    scd_list.append(rec.id)
        self.requerimiento_lines = [(6, 0, scd_list)]
        self.requerimiento_saf_lines = [(6, 0, saf_list)]

    def verificar_servicio_linea(self, servicio, cg):
        rec = self.env['requerimiento.lines'].search([
            ('unidad_territorial_id', '=', self.unidad_territorial_id.id),
            ('comite_gestion_id', '=', cg.id),
            ('anio', '=', self.anio.id),
            ('mes', '=', self.mes),
            ('servicio', '=', servicio),
        ])
        if not rec:
            rec = self.env['requerimiento.lines'].create({
                'unidad_territorial_id': self.unidad_territorial_id.id,
                'comite_gestion_id': cg.id,
                'anio': self.anio.id,
                'mes': self.mes,
                'servicio': servicio,
                'departamento_id': cg.departamento_id.id,
                'provincia_id': cg.provincia_id.id,
                'distrito_id': cg.distrito_id.id,
                'centro_poblado_id': cg.centro_poblado_id.id,
            })
        else:
            rec.departamento_id = cg.departamento_id.id,
            rec.provincia_id = cg.provincia_id.id,
            rec.distrito_id = cg.distrito_id.id,
            rec.centro_poblado_id = cg.centro_poblado_id.id,
        return rec


class ProcesoJustificacion(models.TransientModel):

    _name = 'proceso.justificacion'

    name = fields.Char(
        default='Proceso de Justificación'
    )
    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes',
        default=lambda self: _obtener_tiempo(self, 'mes'),
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        required=True,
        string=u'Año',
        default=lambda self: _obtener_tiempo(self, 'anio'),
    )
    tipo_persona = fields.Many2one(
        comodel_name='tipo.persona',
        string='Tipo de Cargo',
        domain="[('justificacion','=', True)]"
    )
    jlines1_ids = fields.Many2many(
        comodel_name='justificaciones.lines',
        relation='justificacion_1',
        string='Justificaciones'
    )
    jlines2_ids = fields.Many2many(
        comodel_name='justificacion.actor',
        string='Justificaciones Planilla'
    )
    jlines3_ids = fields.Many2many(
        comodel_name='justificacion.gastos',
        string='Justificaciones Gastos'
    )
    jlines3_saf_ids = fields.Many2many(
        comodel_name='justificacion.gastos',
        relation='jlines3_saf_rel',
        string='Justificaciones Gastos'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión'
    )
    # Jstificacion fase 2
    nro_dias = fields.Integer(
        string='Número de días atendidos'
    )
    just_actores_ids = fields.Many2many(
        comodel_name='justificacion.actor.lines',
        string='Planillas'
    )
    justactor_id = fields.Many2one(
        comodel_name='justificacion.actor',
        string='Just.Planillas'
    )
    servicio = fields.Selection(
        selection=[
            ('scd', u'Cuidado Diurno'),
            ('saf', u'Acompañamiento Familiar')
        ],
        string='Servicio',
        default=lambda self: False if self.env.user.servicio == 'ambos' else self.env.user.servicio,
    )
    servicio_log = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
        ('ambos', u'Ambos'),
    ],
        string=u'Servicio usuario logueado',
        default=lambda self: self.env.user.servicio
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    unidad_territorial_id_log = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    tipo_persona_saf = fields.Many2one(
        comodel_name='tipo.persona',
        string='Tipo de Cargo',
        compute='_compute_servicio',
        store=True
    )

    @api.multi
    @api.depends('servicio')
    def _compute_servicio(self):
        for rec in self:
            if rec.servicio == 'saf':
                rec.tipo_persona_saf = rec.env['tipo.persona'].search([('name', '=', 'FACILITADOR')]).id

    @api.multi
    def buscar_etapa_1(self):
        cg_list = []
        for cg in self.unidad_territorial_id.ut_comites_gestion:
            req = self.env['requerimiento.lines'].search([
                ('unidad_territorial_id', '=', self.unidad_territorial_id.id),
                ('comite_gestion_id', '=', cg.id),
                ('anio', '=', self.anio.id),
                ('mes', '=', self.mes),
                ('estado', '=', 'adjuntado'),
                ('servicio', '=', self.servicio)
            ])
            if req:
                rec = self.env['justificaciones.lines'].search([
                    ('departamento_id', '=', cg.departamento_id.id),
                    ('provincia_id', '=', cg.provincia_id.id),
                    ('anio', '=', self.anio.id),
                    ('mes', '=', self.mes),
                    ('comite_gestion_id', '=', cg.id),
                    ('unidad_territorial_id', '=', self.unidad_territorial_id.id),
                    ('servicio', '=', self.servicio)
                ])
                if not rec:
                    rec = self.env['justificaciones.lines'].create({
                        'requerimiento_id': req.id,
                        'departamento_id': cg.departamento_id.id,
                        'provincia_id': cg.provincia_id.id,
                        'anio': self.anio.id,
                        'mes': self.mes,
                        'comite_gestion_id': cg.id,
                        'unidad_territorial_id': self.unidad_territorial_id.id,
                        'monto_solicitado': req.deposito,
                        'servicio': self.servicio
                    })
                cg_list.append(rec.id)
        self.jlines1_ids = [(6, 0, cg_list)]

    @api.multi
    def buscar_etapa_2(self):
        if self.tipo_persona:
            rec = self.env['justificaciones.lines'].search([
                ('siaf', '!=', False),
                ('comprobante', '!=', False),
                ('fecha_comprobante', '!=', False),
                ('situacion', '!=', False),
                ('mes', '=', self.mes),
                ('anio', '=', self.anio.id),
                ('servicio', '=', self.servicio)
            ])
            actor_list = []
            for r in rec:
                just_actor = self.env['justificacion.actor'].search([
                    ('justificacion_id', '=', r.id),
                    ('tipo_persona', '=', self.tipo_persona.id),
                    ('servicio', '=', self.servicio)
                ])
                if not just_actor:
                    just_actor = self.env['justificacion.actor'].create({
                        'comite_gestion_id': r.comite_gestion_id.id,
                        'unidad_territorial_id': r.unidad_territorial_id.id,
                        'justificacion_id': r.id,
                        'tipo_persona': self.tipo_persona.id,
                        'mes': r.mes,
                        'anio': r.anio.id,
                        'servicio': self.servicio,
                    })
                actor_list.append(just_actor.id)
            self.jlines2_ids = [(6, 0, actor_list)]
        if self.tipo_persona_saf:
            rec = self.env['justificaciones.lines'].search([
                ('siaf', '!=', False),
                ('comprobante', '!=', False),
                ('fecha_comprobante', '!=', False),
                ('situacion', '!=', False),
                ('mes', '=', self.mes),
                ('anio', '=', self.anio.id),
                ('servicio', '=', self.servicio)
            ])
            actor_list = []
            for r in rec:
                just_actor = self.env['justificacion.actor'].search([
                    ('justificacion_id', '=', r.id),
                    ('tipo_persona', '=', self.tipo_persona_saf.id),
                    ('servicio', '=', self.servicio)
                ])
                if not just_actor:
                    just_actor = self.env['justificacion.actor'].create({
                        'comite_gestion_id': r.comite_gestion_id.id,
                        'unidad_territorial_id': r.unidad_territorial_id.id,
                        'justificacion_id': r.id,
                        'tipo_persona': self.tipo_persona_saf.id,
                        'mes': r.mes,
                        'anio': r.anio.id,
                        'servicio': self.servicio,
                    })
                actor_list.append(just_actor.id)
            self.jlines2_ids = [(6, 0, actor_list)]


    @api.multi
    def buscar_etapa_3(self):
        rec = self.env['justificacion.gastos'].search([
            ('mes', '=', self.mes),
            ('anio', '=', self.anio.id),
            ('servicio', '=', self.servicio)
        ])
        if self.servicio == 'scd':
            if rec:
                self.jlines3_ids = [(6, 0, rec.ids)]
            else:
                list_3 = []
                rec = self.env['justificaciones.lines'].search([
                    ('siaf', '!=', False),
                    ('comprobante', '!=', False),
                    ('fecha_comprobante', '!=', False),
                    ('situacion', '!=', False),
                    ('just_actor_ids', '!=', False),
                    ('mes', '=', self.mes),
                    ('anio', '=', self.anio.id),
                    ('servicio', '=', self.servicio)
                ])

                for r in rec:
                    just = self.env['justificacion.gastos'].search([
                        ('justificacion_id', '=', r.id),
                        ('mes', '=', self.mes),
                        ('anio', '=', self.anio.id),
                        ('servicio', '=', self.servicio)
                    ])
                    if not just:
                        just = self.env['justificacion.gastos'].create({
                            'comite_gestion_id': r.comite_gestion_id.id,
                            'unidad_territorial_id': r.unidad_territorial_id.id,
                            'justificacion_id': r.id,
                            'mes': r.mes,
                            'anio': r.anio.id,
                            'servicio': self.servicio,
                        })
                    list_3.append(just.id)
                self.jlines3_ids = [(6, 0, list_3)]
        else:
            if rec:
                self.jlines3_saf_ids = [(6, 0, rec.ids)]
            else:
                list_3 = []
                rec = self.env['justificaciones.lines'].search([
                    ('siaf', '!=', False),
                    ('comprobante', '!=', False),
                    ('fecha_comprobante', '!=', False),
                    ('situacion', '!=', False),
                    ('just_actor_ids', '!=', False),
                    ('mes', '=', self.mes),
                    ('anio', '=', self.anio.id),
                    ('servicio', '=', self.servicio)
                ])

                for r in rec:
                    just = self.env['justificacion.gastos'].search([
                        ('justificacion_id', '=', r.id),
                        ('mes', '=', self.mes),
                        ('anio', '=', self.anio.id),
                        ('servicio', '=', self.servicio)
                    ])
                    if not just:
                        just = self.env['justificacion.gastos'].create({
                            'comite_gestion_id': r.comite_gestion_id.id,
                            'unidad_territorial_id': r.unidad_territorial_id.id,
                            'justificacion_id': r.id,
                            'mes': r.mes,
                            'anio': r.anio.id,
                            'servicio': self.servicio,
                        })
                    list_3.append(just.id)
                self.jlines3_saf_ids = [(6, 0, list_3)]

    @api.multi
    def create_record(self):
        self.ensure_one()
        if self.justactor_id and self.just_actores_ids:
            self.justactor_id.actor_lines_ids = [(6, 0, self.just_actores_ids.ids)]
