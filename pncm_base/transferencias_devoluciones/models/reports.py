# coding: utf-8

from odoo import api, models,fields


class ReportFormatoDesembolso(models.AbstractModel):
    _name = 'report.transferencias_devoluciones.formato_desembolso_template'

    @api.multi
    def render_html(self, req_id, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('transferencias_devoluciones.formato_desembolso_template')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self.env['requerimiento.lines'].browse(req_id),
            'get_data':self.get_data,
        }
        return report_obj.render('transferencias_devoluciones.formato_desembolso_template', docargs)

    @api.multi
    def get_data(self, rec):
        print(rec.comite_gestion_id.name)
