# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import fields, models, api
from odoo.exceptions import ValidationError
import calendar
import pytz


def _drop_records(self, model, domain):
    self.env[model].search(domain).sudo().unlink()


def get_month_day_range(date):
    date = datetime.strptime(date, '%Y-%m-%d')
    first_day = date.replace(day=1)
    last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
    return first_day, last_day


def obtener_validar_dias(obj, anio, mes, dias_utiles):
    date_str = '%s-%s-%s' % (anio.name, str(mes), '01')
    # primero obtengo el primer y ultimo dia del mes
    primer_dia, ultimo_dia = get_month_day_range(date_str)
    # convierto la fecha del obj a datetime
    fecha_inicio = convertir_str_date(obj.fecha_inicio)
    if obj.fecha_fin:
        fecha_fin = convertir_str_date(obj.fecha_fin)

        if fecha_inicio <= primer_dia:
            if fecha_fin < primer_dia:
                return False
            else:
                days = calculate_duration(primer_dia, fecha_fin)
                return days
        else:
            if fecha_inicio > ultimo_dia:
                return False
            else:
                if fecha_fin < ultimo_dia:
                    days = calculate_duration(fecha_inicio, fecha_fin)
                else:
                    days = calculate_duration(fecha_inicio, ultimo_dia)
                return days
    else:
        if fecha_inicio > ultimo_dia:
            return False
        else:
            if fecha_inicio <= primer_dia:
                days = calculate_duration(primer_dia, ultimo_dia)
            else:
                days = calculate_duration(fecha_inicio, ultimo_dia)
            return days


def search_values_from_subvencion(self, inicio, nombre_campo, tipo_persona, nivel=None):
    domain = [
        ('nombre_tipo', '=', tipo_persona),
        ('fecha_inicio', '<=', inicio),
        ('fecha_fin', '=', False)
    ]
    if nivel:
        domain.append(('nombre_nivel', '=', nivel))
    rec = self.env['escala.subvencion'].search(domain)
    if not rec or len(rec) > 1:
        raise ValidationError(
            'Por favor contacte con el administrador. No se encuentra valores para el campo: %s' % nombre_campo
        )
    if rec.monto == 0:
        raise ValidationError(
            'Por favor contacte con el administrador. El campo: %s, no puede ser igual a 0' % nombre_campo
        )
    else:
        return rec.monto


def search_values_from_gastos(self, periodo_inicio, nombre_campo, tipo_comite, tipo_gastos):
    rec = self.env['escala.gastos'].search([
        ('tipo_comite', '=', tipo_comite),
        ('tipo_gastos', '=', tipo_gastos),
        ('fecha_inicio', '<=', periodo_inicio),
        ('fecha_fin', '=', False),
    ])
    if not rec or len(rec) > 1:
        raise ValidationError(
            'Por favor contacte con el administrador. No se encuentra valores para el campo: %s' % nombre_campo
        )
    if rec.valor == 0:
        raise ValidationError(
            'Por favor contacte con el administrador. El campo: %s, no puede ser igual a 0' % nombre_campo
        )
    else:
        return rec.valor


def verificar_fechas(obj, anio, mes):
    date_str = '%s-%s-%s' % (anio.name, str(mes), '01')
    # primero obtengo el primer y ultimo dia del mes
    primer_dia, ultimo_dia = get_month_day_range(date_str)
    # convierto la fecha del obj a datetime
    fecha_inicio = convertir_str_date(obj.fecha_ingreso)
    if fecha_inicio and not obj.fecha_retiro:
        if fecha_inicio <= primer_dia:
            return True
        else:
            if fecha_inicio <= ultimo_dia:
                return True
            else:
                return False
    else:
        fecha_fin = convertir_str_date(obj.fecha_ingreso)
        if fecha_fin <= ultimo_dia:
            return True
        else:
            if ultimo_dia >= fecha_inicio >= primer_dia:
                return True
            else:
                return False


def convertir_str_date(string):
    format_str = '%Y-%m-%d'
    dia = datetime.strptime(string, format_str)
    return dia


def calculate_duration(start, end):
    daygenerator = [start + timedelta(x + 1) for x in xrange((end - start).days)]
    daygenerator += [start + timedelta(0)]
    days = sum(1 for day in daygenerator if day.weekday() < 5)
    return days


def _obtener_tiempo(self, time):
    date_now = datetime.now(tz=pytz.timezone('America/Lima')).replace(tzinfo=None)
    if time == 'mes':
        return months[date_now.month - 1]
    else:
        rec = self.env['tabla.anios'].search([
            ('name', '=', date_now.year)
        ])
        return rec.id


def _obtener_saldo_mes_anterior(self, modelo):
    anio = self.anio
    mes = int(self.mes)
    if mes == 1:
        anio = self.env['tabla.anios'].search([('name', '=', str(int(anio.name) - 1))])
        mes = '12'
    else:
        mes = str(mes - 1)
        if len(mes) == 1:
            mes = '0' + mes
    rec = self.env[modelo].search([
        ('unidad_territorial_id', '=', self.unidad_territorial_id.id),
        ('comite_gestion_id', '=', self.comite_gestion_id.id),
        ('anio', '=', anio.id),
        ('mes', '=', mes),
    ])
    saldo = 0
    if rec:
        saldo = rec.desembolso_id.total_deposito
    return saldo


def _validar_registro_unico_asistencia(self):
    obj = self.search([
        ('anio', '=', self.anio.id),
        ('mes', '=', self.mes),
        ('unidad_territorial_id', '=', self.unidad_territorial_id.id),
        ('comite_gestion_id', '=', self.comite_gestion_id.id),
        ('local_id', '=', self.local_id.id),
        ('modulo_id', '=', self.modulo_id.id)
    ])
    if len(obj) > 1:
        raise ValidationError(u'Ya existe una Ficha: "%d" con el mismo mes y año.' % obj[0].id)


def validatar_lineas_asistencia(rec):
    for obj in rec:
            total = obj.enfermedad + obj.asiste + obj.faltas
            if total > obj.total_dias_activo or total == 0:
                raise ValidationError(
                    u'Los niños resaltados en rojo no se pueden guardar ya que sus días registrados suman cero (0) o la'
                    u' suma es superior de una las asistencia es mayor la cantidad total de posibles dias asistidos. '
                    u'Puede borrar los niños o editarlos para poder guardar la ficha de asistencia'
                )


months = [
    ('01', 'Enero'),
    ('02', 'Febrero'),
    ('03', 'Marzo'),
    ('04', 'Abril'),
    ('05', 'Mayo'),
    ('06', 'Junio'),
    ('07', 'Julio'),
    ('08', 'Agosto'),
    ('09', 'Setiembre'),
    ('10', 'Octubre'),
    ('11', 'Noviembre'),
    ('12', 'Diciembre')
]

entidad = [
    ('a_tecnico', u'ACOMPAÑANTE TÉCNICO'),
    ('comite_gestion', u'CÓMITE DE GESTIÓN'),
    ('vigilancia', 'CONSEJO DE VIGILANCIA'),
    ('coordinador_scd', 'COORDINADOR DEL SERVICIO DE CUIDADO DIURNO'),
    ('madre_guia', 'MADRE GUÍA'),
    ('sede_central', 'SEDE CENTRAL'),
    ('otros', 'OTROS'),
]


class NiniosModulo(models.Model):

    _inherit = 'modulo.ninios'

    asistencia_ids = fields.One2many(
        comodel_name='asistencia.menor',
        inverse_name='ninio_afiliacion_id',
        string='Asistencias'
    )


class AsistenciaControl(models.Model):

    _name = 'asistencia.control'
    _inherit = 'tabla.base'

    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes',
        default=lambda self: _obtener_tiempo(self, 'mes')
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        default=lambda self: _obtener_tiempo(self, 'anio'),
        required=True,
        string=u'Año'
    )
    dias_utiles = fields.Integer(
        string=u'Días Útiles',
        readonly=True,
        compute='_compute_dias_utiles',
        store=True
    )
    acomp_tecnico_id = fields.Many2one(
        comodel_name='pncm.comite.equipo.tecnico',
        string=u'Acompañante Técnico',
        domain="[('cargo','=', u'ACOMPAÑANTE TECNICO'),"
               "('fecha_fin', '=', False),"
               "('unidad_territorial_id','=', unidad_territorial_id)]",
        required=True,
    )
    mad_cuidadora_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        string='Madre Cuidadora',
        domain="[('cargo', '=', 'MADRE CUIDADORA'),"
               "('comite_gestion_id','=', comite_gestion_id)]",
        required=True
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id,
    )
    unidad_territorial_id_log = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        related="acomp_tecnico_id.comite_gestion_id",
        store=True,
        string=u'Comite Gestión',
        readonly=True
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        string='Local',
        compute='_compute_local_ambiente_modulo',
        store=True
    )
    ambiente_id = fields.Many2one(
        comodel_name='ambientes.local',
        string='Ambiente',
        compute='_compute_local_ambiente_modulo',
        store=True
    )
    sala_id = fields.Many2one(
        comodel_name='salas.local',
        string='Sala',
        compute='_compute_local_ambiente_modulo',
        store=True
    )
    modulo_id = fields.Many2one(
        comodel_name='modulo.local',
        string=u'Módulo',
        compute='_compute_local_ambiente_modulo',
        store=True
    )
    asistencia_menor_ids = fields.One2many(
        comodel_name='asistencia.menor',
        inverse_name='asistencia_control_id',
        string='Asistencia Menor'
    )
    state = fields.Selection(
        selection=[
            ('opened', 'Abierto'),
            ('closed', 'Cerrado'),
        ],
        string='Estado',
        default='opened',
    )
    total_raciones = fields.Integer(
        string='Total raciones'
    )
    raciones_si = fields.Integer(
        string='Si'
    )
    raciones_no = fields.Integer(
        string='No'
    )
    visitas_mensual_ids = fields.Many2many(
        comodel_name='visita.mensual',
        string='Visitas'
    )
    total_visitas = fields.Integer(
        string='Total de Visitas',
        compute='_compute_total_visitas',
        store=True
    )

    @api.multi
    @api.depends('visitas_mensual_ids')
    def _compute_total_visitas(self):
        for rec in self:
            visitas = 0
            if rec.visitas_mensual_ids:
                visitas = sum(rec.total_visitas for rec in rec.visitas_mensual_ids)
            rec.total_visitas = visitas

    @api.multi
    @api.constrains('total_visitas')
    def _constrains_total_visitas(self):
        for rec in self:
            if rec.total_visitas > rec.dias_utiles:
                raise ValidationError(u'El total de visistas no puede ser superior al número de días útiles.')

    @api.multi
    @api.depends('mad_cuidadora_id')
    def _compute_local_ambiente_modulo(self):
        for rec in self:
            if rec.mad_cuidadora_id:
                rec.local_id = rec.mad_cuidadora_id.local_id
                rec.modulo_id = rec.mad_cuidadora_id.modulo_id
                rec.sala_id = rec.mad_cuidadora_id.salas_id
                rec.ambiente_id = rec.mad_cuidadora_id.ambiente_id

    @api.onchange(
        'comite_gestion_id',
        'local_id',
        'ambiente_id',
        'sala_id',
        'mes',
        'anio'
    )
    def onchange_asistencia_menor_ids(self):
        self.asistencia_menor_ids = False

    @api.multi
    def action_cerrar_asistencia(self):
        for rec in self:
            if rec.state == 'opened':
                rec.state = 'closed'

    @api.onchange('modulo_id', 'mes')
    def _onchange_modulo_id_mes(self):
        self.asistencia_menor_ids.unlink()

    @api.multi
    def filtrar_ninios(self):
        self.asistencia_menor_ids = False
        if self.modulo_id:
            obj_afiliacion = self.modulo_id.ninios_ids
            if obj_afiliacion:
                afiliaciones = []
                for obj in obj_afiliacion:
                    dias = obtener_validar_dias(obj, self.anio, self.mes, self.dias_utiles)
                    """
                        Valida que la fecha de inicio o fin del niño en el módulo se 
                        encuentre dentro del mes seleccionado.
                    """
                    if dias:
                        dias_validados = obj.partner_id.asistencia_ids.filtered(
                            lambda x: x.mes == self.mes and x.anio == self.anio and
                            x.validar and self.modulo_id != x.modulo_id
                        )
                        if dias_validados:
                            dias = self.dias_utiles - sum(line.total_dias_activo for line in dias_validados)

                        new_obj = obj.partner_id.asistencia_ids.filtered(
                            lambda x: x.mes == self.mes and x.anio == self.anio and self.modulo_id == x.modulo_id
                        )
                        if not new_obj:
                            new_obj = self.env['asistencia.menor'].create({
                                'name': u'{} - ({}/{})'.format(
                                    obj.ninios_id.integrante_id.name, self.mes, self.anio.name
                                ),
                                'ninio_afiliacion_id': obj.id,
                                'mes': self.mes,
                                'anio': self.anio.id,
                                'cod_ninio': obj.ninios_id.integrante_id.id or False,
                                'ninio': obj.ninios_id.integrante_id.name,
                                'total_dias_activo': dias,
                                'modulo_id': self.modulo_id.id,
                                'partner_id': obj.partner_id.id,
                                'unidad_territorial_id': self.unidad_territorial_id.id,
                                'comite_gestion_id': self.comite_gestion_id.id
                            })
                        else:
                            new_obj.total_dias_activo = dias

                        afiliaciones.append(new_obj.id)
                        self.asistencia_menor_ids = [(6, 0, afiliaciones)]

    @api.multi
    def state_closed(self):
        if not self.asistencia_menor_ids:
            raise ValidationError(u'Para cambiar a estado Cerrado, es necesario que el módulo cuente con niños')
        validatar_lineas_asistencia(self.asistencia_menor_ids)
        self.state = 'closed'

    @api.depends('anio', 'mes')
    def _compute_dias_utiles(self):
        if self.anio and self.mes:
            rec = self.anio.month_days_ids.filtered(lambda x: x.month == self.mes)
            cg = rec.comite_gestion_ids.filtered(lambda x: x.id == self.comite_gestion_id.id)
            if cg:
                self.dias_utiles = rec.dias_utiles_especial
            else:
                self.dias_utiles = rec.dias_utiles

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.id)) for obj in self]

    @api.multi
    def filter_ut(self):
        action = {
            'name': "Ficha N° 9 Asistencia y Control",
            'type': "ir.actions.act_window",
            'res_model': "asistencia.control",
            'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', '=', 'unidad_territorial_id')]"
        return action


class AsistenciaMenor(models.Model):

    _name = 'asistencia.menor'

    modulo_id = fields.Many2one(
        comodel_name='modulo.local',
        string=u'Módulo'
    )
    name = fields.Char(
        string='Nombre - Asistencia'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comite Gestión'
    )
    cod_ninio = fields.Char(
        string='Código'
    )
    ninio = fields.Char(
        string='Nombres y Apellidos',
        readonly=True
    )
    ninio_afiliacion_id = fields.Many2one(
        comodel_name='modulo.ninios',
        inverse_name='asistencia_ids',
        string=u'Niño'
    )
    afi_ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Niños',
        related='ninio_afiliacion_id.ninios_id',
        store=True
    )
    mes = fields.Selection(
        selection=months,
        string='Mes'
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año'
    )
    asistencia_control_id = fields.Many2one(
        comodel_name='asistencia.control',
        inverse_name='asistencia_menor_ids',
        string='Asistencia Padre'
    )
    cred = fields.Boolean(
        string='CRED'
    )
    mn = fields.Boolean(
        string='MN',
        help='Micronutrientes'
    )
    sf = fields.Boolean(
        string='SF',
        help='Sulfato Ferrorso'
    )
    familia_participa = fields.Boolean(
        string='Familia participa de sesiones de socialización'
    )
    lavado_manos = fields.Boolean(
        string='Lavado de Manos'
    )
    higiene_bucal = fields.Boolean(
        string='Higiene Bucal'
    )
    cuenta_cuento = fields.Boolean(
        string='Cuenta Cuento'
    )
    juega_ninios = fields.Boolean(
        string=u'Juega con niña(o)'
    )
    totfaleda = fields.Integer(
        string='EDA'
    )
    totfalira = fields.Integer(
        string='IRA'
    )
    totfalotr = fields.Integer(
        string='OTRO'
    )
    asiste = fields.Integer(
        string='Asiste'
    )
    enfermedad = fields.Integer(
        string='Enfermedad',
        compute='_compute_enfermedad',
        store=True
    )
    faltas = fields.Integer(
        string='Faltas'
    )
    totconmn = fields.Integer(
        string='MN',
        compute='_compute_totconmn_totconsf',
        store=True
    )
    totconsf = fields.Integer(
        string='SF',
        compute='_compute_totconmn_totconsf',
        store=True
    )
    total_dias_activo = fields.Integer(
        string='Días activos para asistencia?'
    )
    validar = fields.Boolean(
        string='Validado',
        compute='compute_validar',
        store=True
    )
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Nombres y Apellidos'
    )
    lacto_cuna = fields.Integer(
        string=u'¿Lactó en Cuna?'
    )

    @api.onchange('asiste')
    def _onchange_asiste(self):
        self.lacto_cuna = 0

    @api.onchange('lacto_cuna')
    def _onchange_lacto_cuna(self):
        if self.lacto_cuna > self.asiste:
            self.lacto_cuna = self.asiste

    @api.multi
    @api.depends('asiste', 'enfermedad', 'faltas')
    def compute_validar(self):
        for rec in self:
            asistencia = rec.asiste + rec.enfermedad + rec.faltas
            if asistencia != rec.total_dias_activo or asistencia == 0:
                rec.validar = False
            else:
                rec.validar = True

    @api.multi
    @api.depends('totfaleda', 'totfalira', 'totfalotr')
    def _compute_enfermedad(self):
        for rec in self:
            rec.enfermedad = rec.totfaleda + rec.totfalira + rec.totfalotr

    @api.onchange('sf', 'mn')
    def _onchange_sf_mn(self):
        if not self.sf:
            self.totconsf = 0
        if not self.mn:
            self.totconmn = 0

    @api.onchange('enfermedad', 'asiste', 'faltas')
    def _onchange_enfermedad_asiste_faltas(self):
        self.validate_total_days()

    def validate_total_days(self):
        total = self.enfermedad + self.asiste + self.faltas
        if total > self.total_dias_activo:
            error = 'El número de días reportados (asistio + falto + enfermedad): "%d". \n' \
                'No puede ser mayor al número de días habiles del niño : "%d"' % (total, self.total_dias_activo)
            raise ValidationError(error)

    @api.multi
    @api.depends('mn', 'sf', 'asiste')
    def _compute_totconmn_totconsf(self):
        for rec in self:
            if rec.mn:
                rec.totconmn = rec.asiste
            if rec.sf:
                rec.totconsf = rec.asiste


class VisitasMensuales(models.Model):

    _name = 'visita.mensual'

    entidad = fields.Selection(
        selection=entidad,
        string='Entidad'
    )
    total_visitas = fields.Integer(
        string='Total de visitas en el mes'
    )
    asistencia_control_id = fields.Many2one(
        comodel_name='asistencia.control',
        string='Asistencia Control'
    )


class SolicitudRequerimientoLines(models.Model):

    _name = 'requerimiento.lines'

    adjuntar_desembolso = fields.Binary(
        string="Archivo Adjunto"
    )
    file_name = fields.Char(
        string="File Name"
    )
    cod_comite = fields.Char(
        string=u'Código Comité',
        related='comite_gestion_id.codigo',
        store=True,
        readonly=True
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='requerimientos_ids',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión',
        readonly=True
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        readonly=True
    )
    nro_familias = fields.Integer(
        string='N° Familias',
        compute='_compute_nro_personas',
        store=True
    )
    facilitadores = fields.Integer(
        string='Facilitadores',
        compute='_compute_nro_personas',
        store=True
    )
    guias = fields.Integer(
        string=u'Guías',
        compute='_compute_nro_personas',
        store=True
    )
    cuidadoras = fields.Integer(
        string='Cuidadoras',
        compute='_compute_nro_personas',
        store=True
    )
    ninios = fields.Integer(
        string=u'Niñ@s',
        compute='_compute_nro_personas',
        store=True
    )
    total = fields.Float(
        string='Total',
        compute='_compute_nro_personas',
        store=True
    )
    saldo = fields.Float(
        string='Saldo',
        compute='_compute_nro_personas',
        store=True
    )
    deposito = fields.Float(
        string=u'Depósito',
        compute='_compute_nro_personas',
        store=True
    )
    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes'
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        required=True,
        string=u'Año'
    )
    desembolso_id = fields.One2many(
        comodel_name='formato.desembolso',
        inverse_name='requerimiento_line_id',
        string='Formato Desembolso'
    )
    estado = fields.Selection(
        selection=[
            ('borrador', 'Borrador'),
            ('desembolso', 'Desembolso Creado'),
            ('adjuntado', 'Archivo Adjuntado'),
        ],
        string='Estado',
        default='borrador',
        compute='_compute_estado',
        store=True
    )
    servicio = fields.Selection(
        selection=[
            ('scd', u'Cuidado Diurno'),
            ('saf', u'Acompañamiento Familiar')
        ],
        string='Servicio'
    )
    periodo_saldos_ids = fields.Many2many(
        comodel_name='justificacion.gastos',
        string='Saldos'
    )
    departamento_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento'
    )
    provincia_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia'
    )
    distrito_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Distrito'
    )
    centro_poblado_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Centro Poblado'
    )
    justificacion1_ids = fields.One2many(
        comodel_name='justificaciones.lines',
        inverse_name='requerimiento_id',
        string='Justificacion1'
    )

    @api.multi
    @api.depends('desembolso_id')
    def _compute_nro_personas(self):
        for rec in self:
            if rec.desembolso_id:
                rec.guias = rec.desembolso_id.madres_guias + rec.desembolso_id.guias_familiares
                rec.ninios = rec.desembolso_id.nro_usuarios_atendidos
                rec.total = rec.desembolso_id.total_anterior
                rec.deposito = rec.desembolso_id.total_deposito
                rec.saldo = rec.desembolso_id.saldo_anterior
                rec.cuidadoras = rec.desembolso_id.mad_cuid_a + rec.desembolso_id.mad_cuid_b

    @api.depends('adjuntar_desembolso', 'desembolso_id')
    def _compute_estado(self):
        if not self.desembolso_id:
            self.estado = 'borrador'
        if self.desembolso_id and not self.adjuntar_desembolso:
            self.estado = 'desembolso'
        if self.desembolso_id and self.adjuntar_desembolso:
            self.estado = 'adjuntado'

    def default_campos(self):
        afiliaciones = self.comite_gestion_id.afiliaciones_ids
        mad_a = 0
        mad_b = 0
        mad_guia = 0
        guia_fam = 0
        socio = 0
        adm = 0
        vigilante = 0
        nivel = self.env['nivel.escala']
        for afiliacion in afiliaciones:
            if afiliacion.cargo == 'MADRE CUIDADORA':
                nivel_a = nivel.search([
                    ('tipo', '=', 'A'),
                    ('tipo_persona', '=', afiliacion.tipo_guia.id),
                ])
                nivel_b = nivel.search([
                    ('tipo', '=', 'B'),
                    ('tipo_persona', '=', afiliacion.tipo_guia.id),
                ])
                if afiliacion.nivel_mc == nivel_a:
                    mad_a += 1
                if afiliacion.nivel_mc == nivel_b:
                    mad_b += 1
                if afiliacion.cargo in 'MADRE GUIA':
                    mad_guia += 1
                if afiliacion.cargo in 'GUIA FAMILIAR':
                    guia_fam += 1
                if afiliacion.cargo in 'SOCIO DE COCINA':
                    socio += 1
                if afiliacion.cargo in 'APOYO ADMINISTRATIVO DEL COMITE DE GESTION':
                    adm += 1
                if afiliacion.cargo in 'CONSEJO DE VIGILANCIA':
                    vigilante += 1
        locales = self.comite_gestion_id.local_ids.filtered(lambda x: x.state == 'activacion')
        mayor_32 = 0
        for line in locales:
            for mod in line.modulos_ids:
                if len(mod.ninios_ids) > 32:
                    mayor_32 += 1

        context = {
            'default_nro_ciai': len(
                self.comite_gestion_id.local_ids.filtered(lambda x: x.tipo_local in ['ciai', 'ambos'])),
            'default_nro_hcd': len(self.comite_gestion_id.local_ids.filtered(lambda x: x.tipo_local == 'hcd')),
            'default_nro_ccd': len(self.comite_gestion_id.local_ids.filtered(lambda x: x.tipo_local == 'ccd')),
            'default_nro_servicios_alimentarios': len(self.comite_gestion_id.servicio_alimentario_ids),
            'default_local_ninios': mayor_32,
            'default_mad_cuid_a': mad_a,
            'default_mad_cuid_b': mad_b,
            'default_madres_guias': mad_guia,
            'default_guias_familiares': guia_fam,
            'default_socias_cocina': socio,
            'default_apoyo_adm': adm,
            'default_nro_vigilantes': vigilante,
        }
        return context

    @api.multi
    def desembolso(self):
        if not self.desembolso_id:
            if not self.comite_gestion_id.coeficiente_atencion_id:
                raise ValidationError('El Comite de Gestíon no tiene asignado ningún Coeficiente de Atención')
            if not self.comite_gestion_id.resoluciones_ids.filtered(lambda x: x.estado != 'vencido'):
                raise ValidationError('El Comite de Gestíon no tiene asignado una resolución activa')
            if self.servicio == 'scd':
                form_id = self.env.ref('transferencias_devoluciones.form_view_formato_desembolso').id

                nombre_periodo = self.anio.name + self.mes

                periodos = self.env['periodo.saldocomite'].search([
                    ('name', '=', nombre_periodo),
                    ('comite_gestion_id', '=', self.comite_gestion_id.id)
                ])
                if not periodos:
                    periodos = self.env['periodo.saldo'].search([('name', '=', nombre_periodo)])
                saldo = 0
                if not periodos:
                    raise ValidationError('Es necesario llenar la tabla Periodo - Saldo primero.')
                else:
                    list_just = []
                    for r in periodos:
                        rec = self.env['justificacion.gastos'].search([
                            ('comite_gestion_id', '=',  self.comite_gestion_id.id),
                            ('mes', '=', r.periodo_saldo[4:]),
                            ('anio', '=', self.anio.id),
                            ('servicio', '=', self.servicio)
                        ])
                        list_just.append(rec.id)
                        saldo += rec.saldo
                    self.periodo_saldos_ids = list_just

                context = {
                    'default_coeficiente_atencion_id': self.comite_gestion_id.coeficiente_atencion_id.id,
                    'default_comite_gestion_id': self.comite_gestion_id.id,
                    'default_unidad_territorial_id': self.unidad_territorial_id.id,
                    'default_requerimiento_line_id': self.id,
                    'default_meta_cg': self.comite_gestion_id.meta_actual,
                    'default_mes': self.mes,
                    'default_anio': self.anio.id,
                    'default_saldo_anterior': saldo,
                    'default_servicio': self.servicio
                }
                context.update(self.default_campos())
            else:
                form_id = self.env.ref('transferencias_devoluciones.form_view_formato_desembolso_saf').id

                nro_facilitadores = len(self.comite_gestion_id.afiliaciones_ids.filtered(
                    lambda x: x.cargo == 'FACILITADOR' and not x.fecha_retiro
                ))

                nro_familias = nro_ninios = nro_mad_gestantes = 0
                familias = self.comite_gestion_id.familias_ids.filtered(
                    lambda x: x.services == 'saf' and not x.fecha_fin
                )
                if familias:
                    nro_familias = len(familias)
                    for rec in familias:
                        for integrante in rec.integrantes_ids:
                            if integrante.parentesco_id.name == u'Niño(a)':
                                nro_ninios += 1
                            if integrante.integrante_id.madre_gestante:
                                nro_mad_gestantes += 1

                context = {
                    'default_comite_gestion_id': self.comite_gestion_id.id,
                    'default_unidad_territorial_id': self.unidad_territorial_id.id,
                    'default_requerimiento_line_id': self.id,
                    'default_mes': self.mes,
                    'default_anio': self.anio.id,
                    'default_servicio': self.servicio,
                    'default_nro_facilitadores': nro_facilitadores,
                    'default_nro_familias': nro_familias,
                    'default_nro_ninios': nro_ninios,
                    'default_nro_mad_gestantes': nro_mad_gestantes,
                }
            return {
                'type': 'ir.actions.act_window',
                'name': 'Requerimiento de Desembolso',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'formato.desembolso',
                'views': [(form_id, 'form')],
                'view_id': form_id,
                'context': context,
                'target': 'new',
            }
        else:
            datas = {
                'req_id': self.id
            }
            return {
                'type': 'ir.actions.report.xml',
                'report_name': 'transferencias_devoluciones.formato_desembolso_template',
                'data': datas
            }

    @api.multi
    def eliminar_desembolso(self):
        self.desembolso_id.unlink()
        self.state = 'borrador'

    @api.multi
    def eliminar_adjunto(self):
        self.sudo().write({
            'adjuntar_desembolso': False,
            'estado': 'desembolso'
        })

    @api.multi
    def eliminar_req(self):
        if self.justificacion1_ids:
            if self.justificacion1_ids[0].situacion:
                raise ValidationError('Tiene un justificacion Formato N° 2 relacionada, no se puede eliminar.')
            self.justificacion1_ids.unlink()
        self.desembolso_id.unlink()
        self.unlink()

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.id)) for obj in self]


class FormatoDesembolso(models.Model):

    _name = 'formato.desembolso'

    servicio = fields.Selection(
        selection=[
            ('scd', u'Cuidado Diurno'),
            ('saf', u'Acompañamiento Familiar')
        ],
        string='Servicio'
    )
    coeficiente_atencion_id = fields.Many2one(
        comodel_name='coeficiente.atencion',
        string='Coeficiente Atención'
    )
    mes = fields.Selection(
        selection=months,
        string='Mes'
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año'
    )
    requerimiento_line_id = fields.Many2one(
        comodel_name='requerimiento.lines',
        inverse_name='desembolso_id',
        string='Linea Req'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Cómite de Gestión',
        readonly=True
    )
    fecha_actual = fields.Date(
        string='Fecha',
        default=datetime.now(),
        readonly=True
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        readonly=True
    )
    periodo_inicio = fields.Date(
        string=u'Periodo correspondiente entre el:',
        compute='_compute_perido_inicio_fin',
        store=True
    )
    periodo_fin = fields.Date(
        string='Hasta el:',
        compute='_compute_perido_inicio_fin',
        store=True
    )
    nro_ciai = fields.Integer(
        string=u'N° de CIAI',
        required=True
    )
    nro_hcd = fields.Integer(
        string=u'N° de HCD',
        required=True
    )
    nro_ccd = fields.Integer(
        string=u'N° de CCD',
        required=True
    )
    nro_servicios_alimentarios = fields.Integer(
        string=u'N° Servicios Alimentarios'
    )
    local_ninios = fields.Integer(
        string=u'Locales con mas de 32 niños(a)'
    )
    meta_cg = fields.Char(
        string='Meta del Comite de Gestión',
        readonly=True
    )
    nro_usuarios_atendidos = fields.Integer(
        string='N° Usuarios Atendidos'
    )
    variacion_atencion = fields.Integer(
        string=u'Variación de la atención'
    )
    dias_utiles = fields.Integer(
        string=u'N° de Días Utiles',
        compute='_compute_dias_utiles',
        store=True
    )
    dias_atender = fields.Integer(
        string=u'N° de Días a atender'
    )
    mad_cuid_a = fields.Integer(
        string=u'N° Madre Cuidadora A'
    )
    mad_cuid_b = fields.Integer(
        string=u'N° Madre Cuidadora B'
    )
    madres_guias = fields.Integer(
        string=u'N° Madres Guías'
    )
    guias_familiares = fields.Integer(
        string=u'N° Guías Familiares'
    )
    socias_cocina = fields.Integer(
        string=u'N° Socias de Cocina'
    )
    apoyo_adm = fields.Integer(
        string=u'N° Apoyo Administrativo'
    )
    nro_vigilantes = fields.Integer(
        string=u'N° Vigilancia y Limpieza'
    )
    atencion_alimentaria = fields.Float(
        string=u'Atención Alimentaria',
        compute='compute_at_alimentaria_gastos_op_adm',
        store=True
    )
    gastos_operativos = fields.Float(
        string='Gastos Operativos',
        compute='compute_at_alimentaria_gastos_op_adm',
        store=True
    )
    gastos_adm = fields.Float(
        string='Gastos Administrativos',
        compute='compute_at_alimentaria_gastos_op_adm',
        store=True
    )
    incentivo_cuid_a = fields.Float(
        string='Incentivo monetario para las cuidadoras A',
        compute='_compute_campos_planilla',
        store=True
    )
    incentivo_cuid_b = fields.Float(
        string='Incentivo monetario para las cuidadoras B',
        compute='_compute_campos_planilla',
        store=True
    )
    incentivo_mad_guia = fields.Float(
        string=u'Incentivo monetario para las madres guías',
        compute='_compute_campos_planilla',
        store=True
    )
    incentivo_guias_fam = fields.Float(
        string=u'Incentivo monetario para las Guías de Familia',
        compute='_compute_campos_planilla',
        store=True
    )
    incentivo_socio_cocina = fields.Float(
        string=u'Incentivo monetario para las Socias de cocina',
        compute='_compute_campos_planilla',
        store=True
    )
    incentivo_adm = fields.Float(
        string=u'Incentivo monetario para el administrativo',
        compute='_compute_campos_planilla',
        store=True
    )
    incentivo_vigilancia = fields.Float(
        string=u'Incentivo monetario para el apoyo de Vigilancia y Limpieza',
        compute='_compute_campos_planilla',
        store=True
    )
    atencion_alimentaria_base = fields.Float(
        string=u'Atención Alimentaria',
        compute='_compute_base_fields_data',
        store=True
    )
    gastos_operativos_base = fields.Float(
        string='Gastos Operativos',
        compute='_compute_base_fields_data',
        store=True
    )
    gastos_adm_base = fields.Float(
        string='Gastos Administrativos',
        compute='_compute_base_fields_data',
        store=True
    )
    incentivo_cuid_a_base = fields.Float(
        string='Incentivo monetario para las cuidadoras A',
        compute='_compute_base_fields_data',
        store=True
    )
    incentivo_cuid_b_base = fields.Float(
        string='Incentivo monetario para las cuidadoras B',
        compute='_compute_base_fields_data',
        store=True
    )
    incentivo_mad_guia_base = fields.Float(
        string=u'Incentivo monetario para las madres guías',
        compute='_compute_base_fields_data',
        store=True
    )
    incentivo_guias_fam_base = fields.Float(
        string=u'Incentivo monetario para las Guías de Familia',
        compute='_compute_base_fields_data',
        store=True
    )
    incentivo_socio_cocina_base = fields.Float(
        string=u'Incentivo monetario para las Socios de cocina',
        compute='_compute_base_fields_data',
        store=True
    )
    incentivo_adm_base = fields.Float(
        string=u'Incentivo monetario para el administrativo',
        compute='_compute_base_fields_data',
        store=True
    )
    incentivo_vigilancia_base = fields.Float(
        string=u'Incentivo monetario para el apoyo de Vigilancia y Limpieza',
        compute='_compute_base_fields_data',
        store=True
    )
    atencion_alimentaria_3 = fields.Integer(
        string='Atencion Alimentaria col3',
        compute='_compute_atencion_alimentaria_3',
        store=True
    )
    atencion_alimentaria_total = fields.Float(
        string=u'Atención Alimentaria',
        compute='_compute_atencion_alimentaria_total',
        store=True
    )
    gastos_operativos_total = fields.Float(
        string='Gastos Operativos',
        compute='_compute_gastos_operativos_total',
        store=True
    )
    gastos_adm_total = fields.Float(
        string='Gastos Administrativos',
        compute='_compute_gastos_adm_total',
        store=True
    )
    incentivo_cuid_a_total = fields.Float(
        string='Incentivo monetario para las cuidadoras A',
        compute='_compute_incentivo_cuid_a_total',
        store=True
    )
    incentivo_cuid_b_total = fields.Float(
        string='Incentivo monetario para las cuidadoras B',
        compute='_compute_incentivo_cuid_b_total',
        store=True
    )
    incentivo_mad_guia_total = fields.Float(
        string=u'Incentivo monetario para las madres guías',
        compute='_compute_incentivo_mad_guia_total',
        store=True
    )
    incentivo_guias_fam_total = fields.Float(
        string=u'Incentivo monetario para las Guías de Familia',
        compute='_compute_incentivo_guias_fam_total',
        store=True
    )
    incentivo_socio_cocina_total = fields.Float(
        string=u'Incentivo monetario para las Socias de cocina',
        compute='_compute_incentivo_socio_cocina_total',
        store=True
    )
    incentivo_adm_total = fields.Float(
        string=u'Incentivo monetario para el administrativo',
        compute='_compute_incentivo_adm_total',
        store=True
    )
    incentivo_vigilancia_total = fields.Float(
        string=u'Incentivo monetario para el apoyo de Vigilancia y Limpieza',
        compute='_compute_incentivo_vigilancia_total',
        store=True
    )
    total_anterior = fields.Float(
        string='Calculo total',
        compute='_compute_total_anterior',
        store=True
    )
    saldo_anterior = fields.Float(
        string='Saldo Mes Anterior S/.',
        readonly=True
    )
    total_deposito = fields.Float(
        string='Total a Depositar S/.',
        compute='_compute_total_deposito',
        store=True
    )
    mensaje = fields.Text(
        string='Mensaje de alerta',
        compute='_compute_mensaje',
        store=True
    )
    # saf
    nro_facilitadores = fields.Integer(
        string='N° Facilitadores'
    )
    nro_ninios = fields.Integer(
        string=u'N° Niño(a)s'
    )
    nro_mad_gestantes = fields.Integer(
        string='N° Madres Gestantes'
    )
    nro_familias_meta = fields.Integer(
        string='N° Familia Meta'
    )
    nro_familias = fields.Integer(
        string='N° Familias Usuarias'
    )
    estipendio_fac = fields.Integer(
        string='Estipendio a Facilitadores/as',
        compute='_compute_estipendio_fac',
        store=True
    )
    gastos_funcionamiento = fields.Integer(
        string='Gastos de funcionamiento del CG',
        compute='_compute_gastos_funcionamiento',
        store=True
    )
    monto_adicional = fields.Float(
        string='Monto Adicional Asignado'
    )
    estipendio_fac_monto = fields.Integer(
        string='Estipendio a Facilitadores/as',
        compute='_compute_estipendio_fac_monto',
        store=True,
        readonly=True
    )
    estipendio_fac_total = fields.Integer(
        string='Estipendio a Facilitadores/as',
        compute='_compute_estipendio_fac_total',
        store=True
    )
    gastos_funcionamiento_total = fields.Integer(
        string='Gastos de funcionamiento del CG',
        compute='_compute_gastos_funcionamiento_total',
        store=True
    )
    monto_adicional_total = fields.Float(
        string='Monto Adicional Asignado',
        compute='_compute_monto_adicional_total',
        store=True
    )
    total_saf = fields.Float(
        string='Total Requerido',
        compute='_compute_total_saf',
        store=True
    )

    @api.depends('monto_adicional_total', 'gastos_funcionamiento_total', 'estipendio_fac_total')
    def _compute_total_saf(self):
        self.total_saf = self.monto_adicional_total + self.gastos_funcionamiento_total + self.estipendio_fac_total

    @api.depends('monto_adicional')
    def _compute_monto_adicional_total(self):
        if self.monto_adicional:
            self.monto_adicional_total = self.monto_adicional

    @api.depends('periodo_inicio', 'periodo_fin')
    def _compute_gastos_funcionamiento_total(self):
        """
        ('gf', 'Gastos Funcionamiento')
        """
        tipo_comite = self.comite_gestion_id.tipo_comite
        self.gastos_funcionamiento_total = search_values_from_gastos(
            self, self.periodo_inicio, 'Gastos Funcionamineto del CG', tipo_comite, 'gf'
        )

    @api.depends('estipendio_fac_monto', 'estipendio_fac')
    def _compute_estipendio_fac_total(self):
        self.estipendio_fac_total = self.estipendio_fac_monto * self.estipendio_fac

    @api.depends('periodo_inicio', 'periodo_fin')
    def _compute_estipendio_fac_monto(self):
        self.estipendio_fac_monto = search_values_from_subvencion(
            self, self.periodo_inicio, 'Incentivo monetario para Facilitadores/as', 'FACILITADOR'
        )

    @api.depends('nro_familias')
    def _compute_gastos_funcionamiento(self):
        if self.nro_familias:
            self.gastos_funcionamiento = self.nro_familias

    @api.depends('nro_facilitadores')
    def _compute_estipendio_fac(self):
        if self.nro_facilitadores:
            self.estipendio_fac = self.nro_facilitadores

    @api.depends('nro_usuarios_atendidos', 'mad_cuid_a', 'mad_cuid_b')
    def _compute_mensaje(self):
        if self.nro_usuarios_atendidos > 0:
            max_a = max_b = 0
            for rec in self.coeficiente_atencion_id.coeficiente_lines_ids:
                if rec.tipo_persona.name == 'MADRE CUIDADORA' and rec.nivel.tipo == 'A':
                    max_a = self.nro_usuarios_atendidos / rec.nro_ninios
                if rec.tipo_persona.name == 'MADRE CUIDADORA' and rec.nivel.tipo == 'B':
                    max_b = self.nro_usuarios_atendidos / rec.nro_ninios
                if max_a <= self.mad_cuid_a + self.mad_cuid_b <= max_b:
                    self.mensaje = False
                else:
                    self.mensaje = 'Para %d usuarios se permite una cantidad minima de %d y ' \
                                   'un maximo de %d entre cuidadoras A y B, revise y corrija sus valores' % \
                                   (self.nro_usuarios_atendidos, max_a, max_b)

    @api.multi
    @api.depends(
        'mad_cuid_a',
        'mad_cuid_b',
        'madres_guias',
        'guias_familiares',
        'socias_cocina',
        'apoyo_adm',
        'nro_vigilantes'
    )
    def _compute_campos_planilla(self):
        for rec in self:
            rec.incentivo_cuid_a = rec.mad_cuid_a
            rec.incentivo_cuid_b = rec.mad_cuid_b
            rec.incentivo_mad_guia = rec.madres_guias
            rec.incentivo_guias_fam = rec.guias_familiares
            rec.incentivo_socio_cocina = rec.socias_cocina
            rec.incentivo_adm = rec.apoyo_adm
            rec.incentivo_vigilancia = rec.nro_vigilantes

    @api.onchange('dias_atender')
    def _onchange_dias_atender(self):
        if self.dias_atender > self.dias_utiles:
            self.dias_atender = self.dias_utiles

    @api.multi
    @api.depends('atencion_alimentaria', 'atencion_alimentaria_base', 'atencion_alimentaria_3')
    def _compute_atencion_alimentaria_total(self):
        for rec in self:
            rec.atencion_alimentaria_total = \
                rec.atencion_alimentaria * rec.atencion_alimentaria_base * rec.atencion_alimentaria_3

    @api.multi
    @api.depends('gastos_operativos', 'gastos_operativos_base')
    def _compute_gastos_operativos_total(self):
        for rec in self:
            rec.gastos_operativos_total = rec.gastos_operativos * rec.gastos_operativos_base

    @api.multi
    @api.depends('gastos_adm', 'gastos_adm_base')
    def _compute_gastos_adm_total(self):
        for rec in self:
            rec.gastos_adm_total = rec.gastos_adm * rec.gastos_adm_base

    @api.multi
    @api.depends('incentivo_cuid_a', 'incentivo_cuid_a_base')
    def _compute_incentivo_cuid_a_total(self):
        for rec in self:
            rec.incentivo_cuid_a_total = rec.incentivo_cuid_a * rec.incentivo_cuid_a_base

    @api.multi
    @api.depends('incentivo_cuid_b', 'incentivo_cuid_b_base')
    def _compute_incentivo_cuid_b_total(self):
        for rec in self:
            rec.incentivo_cuid_b_total = rec.incentivo_cuid_b * rec.incentivo_cuid_b_base

    @api.multi
    @api.depends('incentivo_mad_guia', 'incentivo_mad_guia_base')
    def _compute_incentivo_mad_guia_total(self):
        for rec in self:
            rec.incentivo_mad_guia_total = rec.incentivo_mad_guia * rec.incentivo_mad_guia_base

    @api.multi
    @api.depends('incentivo_guias_fam', 'incentivo_guias_fam_base')
    def _compute_incentivo_guias_fam_total(self):
        for rec in self:
            rec.incentivo_guias_fam_total = rec.incentivo_guias_fam * rec.incentivo_guias_fam_base

    @api.multi
    @api.depends('incentivo_socio_cocina', 'incentivo_socio_cocina_base')
    def _compute_incentivo_socio_cocina_total(self):
        for rec in self:
            rec.incentivo_socio_cocina_total = rec.incentivo_socio_cocina * rec.incentivo_socio_cocina_base

    @api.multi
    @api.depends('incentivo_adm', 'incentivo_adm_base')
    def _compute_incentivo_adm_total(self):
        for rec in self:
            rec.incentivo_adm_total = rec.incentivo_adm * rec.incentivo_adm_base

    @api.multi
    @api.depends('incentivo_vigilancia', 'incentivo_vigilancia_base')
    def _compute_incentivo_vigilancia_total(self):
        for rec in self:
            rec.incentivo_vigilancia_total = rec.incentivo_vigilancia * rec.incentivo_vigilancia_base

    @api.multi
    @api.depends('nro_usuarios_atendidos')
    def compute_at_alimentaria_gastos_op_adm(self):
        for rec in self:
            rec.atencion_alimentaria = rec.gastos_operativos = rec.gastos_adm = rec.nro_usuarios_atendidos

    @api.multi
    @api.depends(
        'incentivo_vigilancia_total',
        'incentivo_adm_total',
        'incentivo_socio_cocina_total',
        'incentivo_guias_fam_total',
        'incentivo_mad_guia_total',
        'incentivo_cuid_b_total',
        'incentivo_cuid_a_total',
        'gastos_adm_total',
        'gastos_operativos_total',
        'atencion_alimentaria_total'
    )
    def _compute_total_anterior(self):
        for rec in self:
            rec.total_anterior = rec.incentivo_vigilancia_total + rec.incentivo_adm_total + \
                rec.incentivo_socio_cocina_total + rec.incentivo_guias_fam_total + \
                rec.incentivo_mad_guia_total + rec.incentivo_cuid_b_total + \
                rec.incentivo_cuid_a_total + rec.gastos_adm_total + rec.gastos_operativos_total + \
                rec.atencion_alimentaria_total

    @api.multi
    @api.depends('total_anterior', 'saldo_anterior')
    def _compute_total_deposito(self):
        for rec in self:
            rec.total_deposito = rec.total_anterior + rec.saldo_anterior

    @api.multi
    @api.depends('mes', 'anio')
    def _compute_perido_inicio_fin(self):
        for rec in self:
            if rec.mes and rec.anio:
                date_str = '%s-%s-%s' % (rec.anio.name, rec.mes, '01')
                first_day, last_day = get_month_day_range(date_str)
                rec.periodo_inicio = datetime.date(first_day)
                rec.periodo_fin = datetime.date(last_day)

    @api.multi
    @api.depends('periodo_inicio', 'periodo_fin')
    def _compute_base_fields_data(self):
        for rec in self:
            if rec.periodo_fin and rec.periodo_inicio:
                """
                ('aa', u'Atención Alimentaria'),
                ('go', 'Gastos Operativos'),
                ('ga', 'Gastos Administrativos')
                """
                tipo_comite = rec.comite_gestion_id.tipo_comite
                rec.atencion_alimentaria_base = search_values_from_gastos(
                    rec, rec.periodo_inicio, 'Atención Alimentaria', tipo_comite, 'aa'
                )
                rec.gastos_operativos_base = search_values_from_gastos(
                    rec, rec.periodo_inicio, 'Gastos Operativos', tipo_comite, 'go'
                )
                rec.gastos_adm_base = search_values_from_gastos(
                    rec, rec.periodo_inicio, 'Gastos Administrativos', tipo_comite, 'ga'
                )
                rec.incentivo_cuid_a_base = search_values_from_subvencion(
                    rec, rec.periodo_inicio, 'Incentivo monetario para las cuidadoras A', 'MADRE CUIDADORA', 'A'
                )
                rec.incentivo_cuid_b_base = search_values_from_subvencion(
                    rec, rec.periodo_inicio, 'Incentivo monetario para las cuidadoras B', 'MADRE CUIDADORA', 'B'
                )
                rec.incentivo_mad_guia_base = search_values_from_subvencion(
                    rec, rec.periodo_inicio, 'Incentivo monetario para las madres guías', 'MADRE GUIA'
                )
                rec.incentivo_guias_fam_base = search_values_from_subvencion(
                    rec, rec.periodo_inicio, 'Incentivo monetario para las Guías de Familia', 'GUIA FAMILIAR'
                )
                rec.incentivo_socio_cocina_base = search_values_from_subvencion(
                    rec, rec.periodo_inicio, 'Incentivo monetario para las Socias de cocina', 'SOCIO DE COCINA'
                )
                rec.incentivo_adm_base = search_values_from_subvencion(
                    rec, rec.periodo_inicio, 'Incentivo monetario para el administrativo',
                    'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'
                )
                rec.incentivo_vigilancia_base = search_values_from_subvencion(
                    rec, rec.periodo_inicio, 'Incentivo monetario para el apoyo de Vigilancia y Limpieza',
                    'CONSEJO DE VIGILANCIA'
                )

    @api.multi
    @api.depends('anio', 'mes')
    def _compute_dias_utiles(self):
        for reg in self:
            if reg.anio and reg.mes:
                rec = reg.anio.month_days_ids.filtered(lambda x: x.month == reg.mes)
                cg = rec.comite_gestion_ids.filtered(lambda x: x.id == reg.comite_gestion_id.id)
                if cg:
                    reg.dias_utiles = reg.atencion_alimentaria_3 = rec.dias_utiles_especial
                else:
                    reg.dias_utiles = reg.atencion_alimentaria_3 = rec.dias_utiles

    @api.multi
    @api.depends('dias_atender')
    def _compute_atencion_alimentaria_3(self):
        for rec in self:
            rec.atencion_alimentaria_3 = rec.dias_atender

    @api.multi
    def create_record(self):
        if 0 in [self.atencion_alimentaria_base, self.gastos_adm_base, self.gastos_operativos_base]:
            raise ValidationError(
                'No se encuentra valores para algunos de estos campos: \n'
                '-Atencion Alimentaria.\n'
                '-Gastos Administrativo.\n'
                '-Gastos Operativos,\n'
                'Por favor contacte con el administrador.'
            )
        if self.mensaje:
            raise ValidationError(
                'La sumatoria de madres cuidadoras no esta dentro del rango.'
            )
        self.ensure_one()


class EscalaGastos(models.Model):

    _name = 'escala.gastos'
    _inherit = 'tabla.base'

    name = fields.Char(
        string=u'Descripción',
        compute='_compute_name',
        store=True
    )
    tipo_comite = fields.Selection([
        ('23', u'Comité Rural'),
        ('24', u'Urbano Disperso'),
        ('25', u'Comité Urbano')
    ],
        string=u'Tipo de Comité',
        required=True
    )
    tipo_gastos = fields.Selection([
        ('aa', u'Atención Alimentaria'),
        ('go', 'Gastos Operativos'),
        ('ga', 'Gastos Administrativos'),
        ('gf', 'Gastos Funcionamiento')
    ],
        string='Tipo de Gastos',
        required=True
    )
    valor = fields.Float(
        string='Valor',
        required=True
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )

    @api.multi
    @api.depends('tipo_comite', 'tipo_gastos')
    def _compute_name(self):
        for rec in self:
            if rec.tipo_comite and rec.tipo_gastos:
                tipo_gastos = dict(rec._fields['tipo_gastos'].selection).get(rec.tipo_gastos)
                tipo_comite = dict(rec._fields['tipo_comite'].selection).get(rec.tipo_comite)
                rec.name = "{} - {}".format(tipo_gastos, tipo_comite)


class EscalaSubvencion(models.Model):

    _name = 'escala.subvencion'
    _inherit = 'tabla.base'

    tipo_persona = fields.Many2one(
        comodel_name='tipo.persona',
        string='Tipo Persona',
        domain=[('escala_subvencion', '=', True)],
        required=True
    )
    nombre_tipo = fields.Char(
        string='Nombre de tipo de persona',
        related='tipo_persona.name',
        store=True
    )
    nivel = fields.Many2one(
        comodel_name='nivel.escala',
        string='Nivel',
        domain="[('tipo_persona', '=', tipo_persona), ('fecha_fin', '=', False)]",
    )
    nombre_nivel = fields.Char(
        string='Nombre Nivel',
        related='nivel.tipo',
        store=True
    )
    codcat = fields.Selection(
        selection=[
            ('mg', 'MG'),
            ('mc', 'MC'),
            ('sc', 'SC'),
            ('vl', 'VL'),
            ('aa', 'AA'),
            ('gf', 'GF')
        ],
        string=u'Código Categoría',
        compute='_compute_codcat',
        store=True
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )
    monto = fields.Float(
        string='Monto',
        required=True
    )

    @api.depends('tipo_persona')
    def _compute_codcat(self):
        if self.tipo_persona:
            if self.tipo_persona.name == 'MADRE CUIDADORA':
                self.codcat = 'mc'
            if self.tipo_persona.name == 'MADRE GUIA':
                self.codcat = 'mg'
            if self.tipo_persona.name == 'SOCIO DE COCINA':
                self.codcat = 'sc'
            if self.tipo_persona.name == 'CONSEJO DE VIGILANCIA':
                self.codcat = 'vl'
            if self.tipo_persona.name == 'APOYO ADMINISTRATIVO DEL COMITE DE GESTION':
                self.codcat = 'aa'
            if self.tipo_persona.name == 'GUIA FAMILIAR':
                self.codcat = 'gf'
            if self.tipo_persona.name == 'FACILITADOR':
                self.codcat = 'fc'

    @api.multi
    def name_get(self):
        return [
            (obj.id, u'[{}] - {} / {}'.format(obj.nivel.tipo or '', obj.tipo_persona.name, obj.monto)) for obj in self
        ]


class NivelEscalaSubvencion(models.Model):

    _name = 'nivel.escala'

    tipo = fields.Char(
        string='Nombre Escala',
        size=1
    )
    nombre_tipo = fields.Char(
        string='Nombre de tipo de persona',
        related='tipo_persona.name',
        store=True
    )
    tipo_persona = fields.Many2one(
        comodel_name='tipo.persona',
        string='Tipo Persona',
        domain=[('escala_subvencion', '=', True)],
        required=True
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )

    @api.multi
    def name_get(self):
        return [(obj.id, u'[{}] - {}'.format(obj.tipo or '', obj.tipo_persona.name)) for obj in self]

    @api.onchange('tipo')
    def _onchange_tipo(self):
        if self.tipo:
            self.tipo = self.tipo.upper()


class ComiteGestion(models.Model):

    _inherit = 'pncm.comitegestion'

    requerimientos_ids = fields.One2many(
        comodel_name='requerimiento.lines',
        inverse_name='comite_gestion_id',
        string='Solicitud de Requerimientos'
    )
    local_ids = fields.One2many(
        comodel_name='pncm.infra.local',
        inverse_name='comite_id',
        string='Locales'
    )
    coeficiente_atencion_id = fields.Many2one(
        comodel_name='coeficiente.atencion',
        inverse_name='comite_gestion_ids',
        string='Coeficiente Atención'
    )


class InfraLocal(models.Model):

    _inherit = 'pncm.infra.local'

    comite_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='local_ids',
        string=u'Comité de gestión',
        required=True,
        domain="[('unidad_territorial_id', '=', ut_id),"
               "('state', '=', 'activo')]")


"""
    PASO I Justificacion
"""


class JustificacionesLines(models.Model):

    _name = 'justificaciones.lines'

    situacion = fields.Boolean(
        string=u'Situación'
    )
    monto_solicitado = fields.Float(
        string='Monto Solicitado'
    )
    total = fields.Float(
        string='Saldo'
    )
    requerimiento_id = fields.Many2one(
        comodel_name='requerimiento.lines',
        inverse_name='justificacion1_ids',
        string='Requerimiento'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión'
    )
    siaf = fields.Char(
        string='SIAF'
    )
    comprobante = fields.Char(
        string='Comprobante'
    )
    fecha_comprobante = fields.Date(
        string='Fecha Comprobante'
    )
    departamento_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento'
    )
    provincia_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia'
    )
    mes = fields.Selection(
        selection=months,
        string='Mes'
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año'
    )
    just_actor_ids = fields.One2many(
        comodel_name='justificacion.actor',
        inverse_name='justificacion_id',
        string='Planillas'
    )
    gastos_just_ids = fields.One2many(
        comodel_name='justificacion.gastos',
        inverse_name='justificacion_id',
        string='Paso 3'
    )
    servicio = fields.Selection(
        selection=[
            ('scd', u'Cuidado Diurno'),
            ('saf', u'Acompañamiento Familiar')
        ],
        string='Servicio'
    )


"""
    PASO II Justificacion
"""


class JustificacionActor(models.Model):
    _name = 'justificacion.actor'

    justificacion_id = fields.Many2one(
        comodel_name='justificaciones.lines',
        inverse_name='just_actor_ids',
        string='Justificacion Padre'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        default=lambda self: self.env.user.unidad_territorial_id
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión'
    )
    tipo_persona = fields.Many2one(
        comodel_name='tipo.persona',
        string='Tipo de Cargo',
        domain="[('justificacion','=', True)]"
    )
    nro_persona = fields.Integer(
        string='N° Personas',
        compute='_compute_nro_persona_monto',
        store=True
    )
    monto = fields.Float(
        string='Monto',
        compute='_compute_nro_persona_monto',
        store=True
    )
    mes = fields.Selection(
        selection=months,
        string='Mes'
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año'
    )
    actor_lines_ids = fields.Many2many(
        comodel_name='justificacion.actor.lines',
        string='Planilla'
    )
    servicio = fields.Selection(
        selection=[
            ('scd', u'Cuidado Diurno'),
            ('saf', u'Acompañamiento Familiar')
        ],
        string='Servicio'
    )

    @api.depends('actor_lines_ids')
    def _compute_nro_persona_monto(self):
        self.monto = sum(self.actor_lines_ids.mapped('nuevo_monto'))
        self.nro_persona = len(self.actor_lines_ids)

    @api.multi
    def situacion(self):
        if self.servicio == 'scd':
            cargos = ['APOYO ADMINISTRATIVO DEL COMITE DE GESTION', 'SOCIO DE COCINA', 'CONSEJO DE VIGILANCIA']
            if self.tipo_persona.name not in cargos:
                cargos = ['MADRE CUIDADORA', 'MADRE GUIA', 'GUIA FAMILIAR']
                mc = self.env['tipo.persona'].search([('name', 'in', cargos)])
                rec = self.comite_gestion_id.afiliaciones_ids.filtered(lambda x: x.tipo_guia in mc)
            else:
                rec = self.comite_gestion_id.afiliaciones_ids.filtered(lambda x: x.tipo_guia == self.tipo_persona)
        else:
            rec = self.comite_gestion_id.afiliaciones_ids.filtered(lambda x: x.tipo_guia.name == 'FACILITADOR')
        list_actor = []
        for line in rec:
            verificado = verificar_fechas(line, self.anio, self.mes)
            if verificado:
                date_str = '%s-%s-%s' % (self.anio.name, str(self.mes), '01')
                inicio, fin = get_month_day_range(date_str)
                nivel = line.nivel_mc.tipo if line.nivel_mc else False
                monto = search_values_from_subvencion(
                    self, inicio, line.tipo_guia.name, line.tipo_guia.name, nivel
                )
                actor = self.env['justificacion.actor.lines'].search([
                    ('comite_gestion_id', '=', self.comite_gestion_id.id),
                    ('mes', '=', self.mes),
                    ('anio', '=', self.anio.id),
                    ('tipo_persona', '=', line.tipo_guia.id),
                    ('afiliacion_id', '=', line.id),
                ])
                actor.dias_trabajados_mes = self.justificacion_id.requerimiento_id.desembolso_id.dias_atender
                if not actor:
                    actor = self.env['justificacion.actor.lines'].create({
                        'comite_gestion_id': self.comite_gestion_id.id,
                        'mes': self.mes,
                        'anio': self.anio.id,
                        'tipo_persona': line.tipo_guia.id,
                        'afiliacion_id': line.id,
                        'monto': monto,
                        'dias_trabajados_mes': self.justificacion_id.requerimiento_id.desembolso_id.dias_atender,
                    })
                list_actor.append(actor.id)

        form_id = self.env.ref('transferencias_devoluciones.form_view_proceso_justificacion_actor').id
        context = {
            'default_unidad_territorial_id': self.unidad_territorial_id.id,
            'default_mes': self.mes,
            'default_anio': self.anio.id,
            'default_comite_gestion_id': self.comite_gestion_id.id,
            'default_just_actores_ids': [(6, 0, list_actor)],
            'default_justactor_id': self.id,
            'default_nro_dias': self.justificacion_id.requerimiento_id.desembolso_id.dias_atender,
        }
        return {
            'type': 'ir.actions.act_window',
            'name': 'Planilla',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'proceso.justificacion',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'context': context,
            'target': 'new'
        }


class JustificacionActorLines(models.Model):
    _name = 'justificacion.actor.lines'

    monto = fields.Float(
        string='Monto'
    )
    nuevo_monto = fields.Float(
        string='Monto Prorateado',
        compute='_compute_nuevo_monto',
        store=True
    )
    dias_trabajados = fields.Integer(
        string=u'Días trabajados'
    )
    dias_trabajados_mes = fields.Integer(
        string=u'Días trabajados'
    )
    afiliacion_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        string='Integrante'
    )
    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes',
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        required=True,
        string=u'Año'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión'
    )
    tipo_persona = fields.Many2one(
        comodel_name='tipo.persona',
        string='Tipo de Cargo'
    )

    @api.multi
    @api.depends('monto', 'dias_trabajados', 'dias_trabajados_mes')
    def _compute_nuevo_monto(self):
        for rec in self:
            if rec.dias_trabajados > 0:
                rec.nuevo_monto = (rec.dias_trabajados * rec.monto) / rec.dias_trabajados_mes
            else:
                rec.nuevo_monto = rec.monto

"""
    PASO III Justificacion
"""


class JustificacionGastos(models.Model):

    _name = 'justificacion.gastos'

    servicio = fields.Selection(
        selection=[
            ('scd', u'Cuidado Diurno'),
            ('saf', u'Acompañamiento Familiar')
        ],
        string='Servicio'
    )
    justificacion_id = fields.Many2one(
        comodel_name='justificaciones.lines',
        inverse_name='gastos_just_ids',
        string='Justificacion Padre'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]"
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comite Gestión'
    )
    mes = fields.Selection(
        selection=months,
        string='Mes'
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año'
    )

    total = fields.Float(
        string='TOTAL'
    )
    saldo = fields.Float(
        string='SALDO',
        compute='_compute_saldos',
        store=True
    )
    dias_no_asis = fields.Integer(
        string=u'Días no asist.'
    )
    dias_impr = fields.Integer(
        string=u'Días Imprev.'
    )
    nro_exp = fields.Char(
        string='N° Exp. Imprev.'
    )
    validacion = fields.Text(
        string='Alertas',
        compute='_compute_validacion',
        store=True
    )
    situacion = fields.Boolean(
        string=u'Situación'
    )
    estado = fields.Selection(
        selection=[
            ('borrador', 'Borrador'),
            ('situacion', 'Situacion'),
            ('cerrado', 'Cerrado'),
        ],
        string='Estado',
        default='borrador',
    )
    visto_bueno = fields.Boolean(
        string='V°B°'
    )
    monto_solicitud = fields.Float(
        string='Solicitado',
        compute='_compute_completar_campos',
        store=True
    )
    dias_atendidos = fields.Integer(
        string='Dias Atendidos',
        compute='_compute_completar_campos',
        store=True
    )
    asistencia_control_id = fields.Many2many(
        comodel_name='asistencia.control',
        string='Asistencia'
    )
    desembolso_id = fields.Many2one(
        comodel_name='formato.desembolso',
        string='Formato Desembolso',
        compute='_compute_desembolso_id',
        store=True
    )
    # Campos para imprimible
    atencion_alimentaria = fields.Float(
        string=u'Atención Alimentaria',
        compute='_compute_completar_campos',
        store=True
    )
    alimento_stock = fields.Float(
        string='Alimento en stock',
        compute='_compute_completar_campos',
        store=True
    )
    gasto_operativo = fields.Float(
        string='Gastos Operativos',
        compute='_compute_completar_campos',
        store=True
    )
    gasto_administrativo = fields.Float(
        string='Gastos Administrativos',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_cuid_a = fields.Float(
        string='Incentivo monetario para las cuidadoras A',
        readonly=True
    )
    incentivo_cuid_b = fields.Float(
        string='Incentivo monetario para las cuidadoras B',
        readonly=True
    )
    incentivo_mad_guia = fields.Float(
        string=u'Incentivo monetario para las madres guías',
        readonly=True
    )
    incentivo_guias_fam = fields.Float(
        string=u'Incentivo monetario para las Guías de Familia',
        readonly=True
    )
    incentivo_socio_cocina = fields.Float(
        string=u'Incentivo monetario para las Socias de cocina',
        readonly=True
    )
    incentivo_adm = fields.Float(
        string=u'Incentivo monetario para el administrativo',
        readonly=True
    )
    incentivo_vigilancia = fields.Float(
        string=u'Incentivo monetario para el apoyo de Vigilancia y Limpieza',
        readonly=True
    )
    # COL 2 montos
    atencion_alimentaria_b = fields.Float(
        string=u'Atención Alimentaria',
        compute='_compute_completar_campos',
        store=True
    )
    alimento_stock_b = fields.Float(
        string='Alimento en stock',
        compute='_compute_completar_campos',
        store=True
    )
    gasto_operativo_b = fields.Float(
        string='Gastos Operativos',
        compute='_compute_completar_campos',
        store=True
    )
    gasto_administrativo_b = fields.Float(
        string='Gastos Administrativos',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_cuid_a_b = fields.Float(
        string='Incentivo monetario para las cuidadoras A',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_cuid_b_b = fields.Float(
        string='Incentivo monetario para las cuidadoras B',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_mad_guia_b = fields.Float(
        string=u'Incentivo monetario para las madres guías',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_guias_fam_b = fields.Float(
        string=u'Incentivo monetario para las Guías de Familia',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_socio_cocina_b = fields.Float(
        string=u'Incentivo monetario para las Socias de cocina',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_adm_b = fields.Float(
        string=u'Incentivo monetario para el administrativo',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_vigilancia_b = fields.Float(
        string=u'Incentivo monetario para el apoyo de Vigilancia y Limpieza',
        compute='_compute_completar_campos',
        store=True
    )
    # nro dias
    alimento_stock_c = fields.Float(
        string='Alimento en stock',
        default=0.1
    )
    incentivo_cuid_a_c = fields.Float(
        string='Incentivo monetario para las cuidadoras A',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_cuid_b_c = fields.Float(
        string='Incentivo monetario para las cuidadoras B',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_mad_guia_c = fields.Float(
        string=u'Incentivo monetario para las madres guías',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_guias_fam_c = fields.Float(
        string=u'Incentivo monetario para las Guías de Familia',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_socio_cocina_c = fields.Float(
        string=u'Incentivo monetario para las Socias de cocina',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_adm_c = fields.Float(
        string=u'Incentivo monetario para el administrativo',
        compute='_compute_completar_campos',
        store=True
    )
    incentivo_vigilancia_c = fields.Float(
        string=u'Incentivo monetario para el apoyo de Vigilancia y Limpieza',
        compute='_compute_completar_campos',
        store=True
    )
    # importe recibido
    atencion_alimentaria_recibido = fields.Float(
        string=u'Atención Alimentaria',
        compute='_compute_desembolso_recibido',
        store=True
    )
    gastos_operativos_recibido = fields.Float(
        string='Gastos Operativos',
        compute='_compute_desembolso_recibido',
        store=True
    )
    gastos_adm_recibido = fields.Float(
        string='Gastos Administrativos',
        compute='_compute_desembolso_recibido',
        store=True
    )
    incentivo_cuid_a_recibido = fields.Float(
        string='Incentivo monetario para las cuidadoras A',
        compute='_compute_desembolso_recibido',
        store=True
    )
    incentivo_cuid_b_recibido = fields.Float(
        string='Incentivo monetario para las cuidadoras B',
        compute='_compute_desembolso_recibido',
        store=True
    )
    incentivo_mad_guia_recibido = fields.Float(
        string=u'Incentivo monetario para las madres guías',
        compute='_compute_desembolso_recibido',
        store=True
    )
    incentivo_guias_fam_recibido = fields.Float(
        string=u'Incentivo monetario para las Guías de Familia',
        compute='_compute_desembolso_recibido',
        store=True
    )
    incentivo_socio_cocina_recibido = fields.Float(
        string=u'Incentivo monetario para las Socias de cocina',
        compute='_compute_desembolso_recibido',
        store=True
    )
    incentivo_adm_recibido = fields.Float(
        string=u'Incentivo monetario para el administrativo',
        compute='_compute_desembolso_recibido',
        store=True
    )
    incentivo_vigilancia_recibido = fields.Float(
        string=u'Incentivo monetario para el apoyo de Vigilancia y Limpieza',
        compute='_compute_desembolso_recibido',
        store=True
    )
    # importe utilizado
    alimento = fields.Float(
        string='Alimento',
        compute='_compute_completar_campos',
        store=True
    )
    pp = fields.Float(
        string='P.P.',
        compute='_compute_completar_campos',
        store=True
    )
    gastos_ope = fields.Float(
        string='Gastos Ope.',
        compute='_compute_completar_campos',
        store=True
    )
    gastos_adm = fields.Float(
        string='Gastos Adm.',
        compute='_compute_completar_campos',
        store=True
    )
    mc_a = fields.Float(
        string='MC-A',
        compute='_compute_completar_campos',
        store=True
    )
    mc_b = fields.Float(
        string='MC-B',
        compute='_compute_completar_campos',
        store=True
    )
    mcg = fields.Float(
        string='MCG',
        compute='_compute_completar_campos',
        store=True
    )
    guia_fam = fields.Float(
        string='guia_fam',
        compute='_compute_completar_campos',
        store=True
    )
    sc = fields.Float(
        string='SC',
        compute='_compute_completar_campos',
        store=True
    )
    adm = fields.Float(
        string='ADM',
        compute='_compute_completar_campos',
        store=True
    )
    vig = fields.Float(
        string='VIG.',
        compute='_compute_completar_campos',
        store=True
    )
    # saldos
    atencion_alimentaria_saldo = fields.Float(
        string=u'Atención Alimentaria',
        compute='_compute_saldos',
        store=True
    )
    gastos_operativos_saldo = fields.Float(
        string='Gastos Operativos',
        compute='_compute_saldos',
        store=True
    )
    gastos_adm_saldo = fields.Float(
        string='Gastos Administrativos',
        compute='_compute_saldos',
        store=True
    )
    incentivo_cuid_a_saldo = fields.Float(
        string='Incentivo monetario para las cuidadoras A',
        compute='_compute_saldos',
        store=True
    )
    incentivo_cuid_b_saldo = fields.Float(
        string='Incentivo monetario para las cuidadoras B',
        compute='_compute_saldos',
        store=True
    )
    incentivo_mad_guia_saldo = fields.Float(
        string=u'Incentivo monetario para las madres guías',
        compute='_compute_saldos',
        store=True
    )
    incentivo_guias_fam_saldo = fields.Float(
        string=u'Incentivo monetario para las Guías de Familia',
        compute='_compute_saldos',
        store=True
    )
    incentivo_socio_cocina_saldo = fields.Float(
        string=u'Incentivo monetario para las Socias de cocina',
        compute='_compute_saldos',
        store=True
    )
    incentivo_adm_saldo = fields.Float(
        string=u'Incentivo monetario para el administrativo',
        compute='_compute_saldos',
        store=True
    )
    incentivo_vigilancia_saldo = fields.Float(
        string=u'Incentivo monetario para el apoyo de Vigilancia y Limpieza',
        compute='_compute_saldos',
        store=True
    )

    @api.multi
    @api.depends('justificacion_id')
    def _compute_desembolso_id(self):
        for rec in self:
            rec.desembolso_id = rec.justificacion_id.requerimiento_id.desembolso_id

    @api.multi
    @api.depends('desembolso_id')
    def _compute_desembolso_recibido(self):
        for rec in self:
            desembolso = rec.desembolso_id
            rec.atencion_alimentaria_recibido = desembolso.atencion_alimentaria_total
            rec.gastos_operativos_recibido = desembolso.gastos_operativos_total
            rec.gastos_adm_recibido = desembolso.gastos_adm_total
            rec.incentivo_cuid_a_recibido = desembolso.incentivo_cuid_a_total
            rec.incentivo_cuid_b_recibido = desembolso.incentivo_cuid_b_total
            rec.incentivo_mad_guia_recibido = desembolso.incentivo_mad_guia_total
            rec.incentivo_guias_fam_recibido = desembolso.incentivo_guias_fam_total
            rec.incentivo_socio_cocina_recibido = desembolso.incentivo_socio_cocina_total
            rec.incentivo_adm_recibido = desembolso.incentivo_adm_total
            rec.incentivo_vigilancia_recibido = desembolso.incentivo_vigilancia_total

    def buscar_asistencia(self):
        if self.servicio == 'scd':
            saldos = self.justificacion_id.requerimiento_id.periodo_saldos_ids
            if saldos:
               saldos = sorted(saldos, key=lambda x: int(x.mes), reverse=True)
               mes = saldos[0].mes
               anio = saldos[0].anio.id
            else:
                anio = self.anio.id
                mes = int(self.mes)
                if mes == 1:
                    anio = self.env['tabla.anios'].search([('name', '=', str(int(anio.name) - 1))]).id
                    mes = '12'
                else:
                    mes = str(mes - 1)
                    if len(mes) == 1:
                        mes = '0' + mes

            asistencia = self.env['asistencia.control'].search([
                ('state', '=', 'closed'),
                ('mes', '=', mes),
                ('anio', '=', anio),
                ('comite_gestion_id', '=', self.comite_gestion_id.id)
            ])

            if asistencia:
                context = {'asistencia_control_id': [(6, 0, asistencia.ids)]}
                return context
            else:
                raise ValidationError('No existe asistencia relacionada.')

    @api.multi
    @api.depends('asistencia_control_id')
    def _compute_completar_campos(self):
        for rec in self:
            nro_asistencia = 0
            nro_inasistencia = 0
            socio_cocina = 0
            mad_a = 0
            mad_b = 0
            vigilante = 0
            adm = 0
            mad_guia = 0
            guias = 0
            nro_usuarios = 0
            dias_utiles = 0
            if rec.asistencia_control_id:
                for asistencia in rec.asistencia_control_id:
                    nro_usuarios += len(asistencia.asistencia_menor_ids)
                    for asist in asistencia.asistencia_menor_ids:
                        nro_asistencia += sum(line.asiste for line in asist)
                        nro_inasistencia += sum(line.enfermedad + line.faltas for line in asist)
                    dias_utiles = asistencia.dias_utiles

                for line in rec.justificacion_id.just_actor_ids:
                    if line.tipo_persona.name == 'SOCIO DE COCINA':
                        socio_cocina = len(line.actor_lines_ids)
                    if line.tipo_persona.name == 'MADRE CUIDADORA':
                        actor = line.actor_lines_ids.filtered(lambda x: x.tipo_persona.name == 'MADRE CUIDADORA')
                        if actor:
                            mad_a = len(actor.filtered(lambda x: x.afiliacion_id.nivel_mc.tipo == 'A'))
                            mad_b = len(actor.filtered(lambda x: x.afiliacion_id.nivel_mc.tipo == 'B'))
                        guias = len(line.actor_lines_ids.filtered(lambda x: x.tipo_persona.name == 'GUIA FAMILIAR'))
                        mad_guia = len(line.actor_lines_ids.filtered(lambda x: x.tipo_persona.name == 'MADRE GUIA'))
                    if line.tipo_persona.name == 'CONSEJO DE VIGILANCIA':
                        vigilante = len(line.actor_lines_ids)
                    if line.tipo_persona.name == 'APOYO ADMINISTRATIVO DEL COMITE DE GESTION':
                        adm = len(line.actor_lines_ids)

                rec.dias_atendidos = dias_utiles
                rec.monto_solicitud = rec.justificacion_id.requerimiento_id.deposito
                # col 1
                rec.atencion_alimentaria = nro_asistencia
                rec.alimento_stock = nro_inasistencia
                rec.gasto_operativo = rec.gasto_administrativo = nro_usuarios
                rec.incentivo_cuid_a = mad_a
                rec.incentivo_cuid_b = mad_b
                rec.incentivo_guias_fam = guias
                rec.incentivo_mad_guia = mad_guia
                rec.incentivo_socio_cocina = socio_cocina
                rec.incentivo_adm = adm
                rec.incentivo_vigilancia = vigilante
                # col 2
                desembolso = rec.justificacion_id.requerimiento_id.desembolso_id
                rec.atencion_alimentaria_b = desembolso.atencion_alimentaria_base
                rec.alimento_stock_b = desembolso.atencion_alimentaria_base
                rec.gasto_operativo_b = desembolso.gastos_operativos_base
                rec.gasto_administrativo_b = desembolso.gastos_adm_base
                rec.incentivo_cuid_a_b = desembolso.incentivo_cuid_a_base / dias_utiles
                rec.incentivo_cuid_b_b = desembolso.incentivo_cuid_b_base / dias_utiles
                rec.incentivo_mad_guia_b = desembolso.incentivo_mad_guia_base / dias_utiles
                rec.incentivo_guias_fam_b = desembolso.incentivo_guias_fam_base / dias_utiles
                rec.incentivo_socio_cocina_b = desembolso.incentivo_socio_cocina_base / dias_utiles
                rec.incentivo_adm_b = desembolso.incentivo_adm_base / dias_utiles
                rec.incentivo_vigilancia_b = desembolso.incentivo_vigilancia_base / dias_utiles
                # col 3
                rec.incentivo_cuid_a_c = rec.incentivo_cuid_b_c = rec.incentivo_mad_guia_c = \
                    rec.incentivo_guias_fam_c = rec.incentivo_socio_cocina_c = rec.incentivo_adm_c = \
                    rec.incentivo_vigilancia_c = dias_utiles
                # importe utilizado
                rec.alimento = nro_asistencia * rec.atencion_alimentaria_b
                rec.pp = nro_asistencia * rec.alimento_stock_b * rec.alimento_stock_c
                rec.gastos_ope = nro_asistencia * rec.gasto_operativo_b
                rec.gastos_adm = nro_usuarios * rec.gasto_administrativo_b
                rec.mc_a = rec.incentivo_cuid_a * rec.incentivo_cuid_a_b * dias_utiles
                rec.mc_b = rec.incentivo_cuid_b * rec.incentivo_cuid_b_b * dias_utiles
                rec.mcg = rec.incentivo_mad_guia * rec.incentivo_guias_fam_b * dias_utiles
                rec.guia_fam = rec.incentivo_guias_fam * rec.incentivo_guias_fam_b * dias_utiles
                rec.sc = rec.incentivo_socio_cocina * rec.incentivo_socio_cocina_b * dias_utiles
                rec.adm = rec.incentivo_adm * rec.incentivo_adm_b * dias_utiles
                rec.vig = rec.incentivo_vigilancia * rec.incentivo_vigilancia_b * dias_utiles

    @api.multi
    @api.depends(
        'alimento', 'pp', 'gastos_ope', 'gastos_adm', 'mc_a', 'mc_b', 'mcg', 'guia_fam', 'sc', 'adm', 'vig',
        'atencion_alimentaria_recibido',
        'gastos_operativos_recibido',
        'gastos_adm_recibido',
        'incentivo_cuid_a_recibido',
        'incentivo_cuid_b_recibido',
        'incentivo_mad_guia_recibido',
        'incentivo_guias_fam_recibido',
        'incentivo_socio_cocina_recibido',
        'incentivo_adm_recibido',
        'incentivo_vigilancia_recibido'
    )
    def _compute_saldos(self):
        for rec in self:
            rec.atencion_alimentaria_saldo = rec.atencion_alimentaria_recibido - (rec.alimento + rec.pp)
            rec.gastos_operativos_saldo = rec.gastos_operativos_recibido - rec.gastos_ope
            rec.gastos_adm_saldo = rec.gastos_adm_recibido - rec.gastos_adm
            rec.incentivo_cuid_a_saldo = rec.incentivo_cuid_a_recibido - rec.mc_a
            rec.incentivo_cuid_b_saldo = rec.incentivo_cuid_b_recibido - rec.mc_b
            rec.incentivo_mad_guia_saldo = rec.incentivo_mad_guia_recibido - rec.mcg
            rec.incentivo_guias_fam_saldo = rec.incentivo_guias_fam_recibido - rec.guia_fam
            rec.incentivo_socio_cocina_saldo = rec.incentivo_socio_cocina_recibido - rec.sc
            rec.incentivo_adm_saldo = rec.incentivo_adm_recibido - rec.adm
            rec.incentivo_vigilancia_saldo = rec.incentivo_vigilancia_recibido - rec.vig
            rec.saldo = rec.atencion_alimentaria_saldo + rec.gastos_operativos_saldo + rec.gastos_adm_saldo + \
                rec.incentivo_cuid_a_saldo + rec.incentivo_cuid_b_saldo + rec.incentivo_mad_guia_saldo + \
                rec.incentivo_guias_fam_saldo + rec.incentivo_socio_cocina_saldo + rec.incentivo_adm_saldo + \
                rec.incentivo_vigilancia_saldo

    @api.multi
    def ver_alertas(self):
        if self.validacion:
            raise ValidationError(
                u'El cálculo se realizó correctamente, ¡tener en cuenta que no '
                'cuenta con registros de planillas para %s!' % self.validacion
            )

    def obtener_alertas(self):
        val_text = []
        if self.mc_a == 0:
            val_text.append('MADRE CUIDADORA A')
        if self.mc_b == 0:
            val_text.append('MADRE CUIDADORA B')
        if self.mcg == 0:
            val_text.append('MADRE GUIA')
        if self.sc == 0:
            val_text.append('SOCIO DE COCINA')
        if self.adm == 0:
            val_text.append('APOYO ADMINISTRATIVO')
        if self.vig == 0:
            val_text.append('CONSEJO DE VIGILANCIA')
        if val_text:
            val_text = ", ".join(val_text)
            return val_text
        else:
            return False

    @api.multi
    def write(self, values):
        id = self.read(['id'])
        rec = self.search([('id', '=', id[0]['id'])])
        if values.get('situacion'):
            values['estado'] = 'situacion'
            data2 = rec.buscar_asistencia()
            if data2:
                values.update(data2)
        r = super(JustificacionGastos, self).write(values)
        return r

    @api.depends('situacion', 'estado')
    def _compute_validacion(self):
        validacion = False
        if self.situacion and self.estado == 'situacion':
            validacion = self.obtener_alertas()
        self.validacion = validacion

    @api.multi
    def cerrar_justificacion(self):
        self.estado = 'cerrado'

    @api.multi
    def imprimir_justificacion(self):
        datas = {
            'req_id': self.id
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'transferencias_devoluciones.formato_desembolso_template',
            'data': datas
        }


class PeridoSaldo(models.Model):
    _name = 'periodo.saldo'

    name = fields.Char(
        string='Periodo',
        required=True,
        size=6
    )
    periodo_saldo = fields.Char(
        string='Periodo Saldo',
        required=True,
        size=6
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today(),
        readonly=True
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user,
        readonly=True
    )

    @api.model
    def create(self, values):
        obj = super(PeridoSaldo, self).create(values)
        if obj.name == obj.periodo_saldo:
            raise ValidationError('No se puede asignar el saldo a un mismo periodo.')
        return obj


class PeridoSaldoComite(models.Model):
    _name = 'periodo.saldocomite'
    _inherit = 'tabla.base'

    saldo = fields.Char(
        string='Periodo Saldo',
        required=True,
        size=6
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        required=True
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=', unidad_territorial_id),"
               "('servicio', '!=', 'scd')]",
        string=u'Comite Gestión',
        required=True
    )

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}/{}'.format(obj.saldo, obj.comite_gestion_id.name)) for obj in self]


class CoeficienteAtencion(models.Model):

    _name = 'coeficiente.atencion'
    _inherit = 'tabla.base'

    coeficiente_lines_ids = fields.One2many(
        comodel_name='coeficiente.atencion.lineas',
        inverse_name='coeficiente_id',
        string='Coeficiente atencion'
    )
    comite_gestion_ids = fields.One2many(
        comodel_name='pncm.comitegestion',
        inverse_name='coeficiente_atencion_id',
        string=u'Comite Gestión'
    )

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.id)) for obj in self]


class CoeficienteAtencionLineas(models.Model):

    _name = 'coeficiente.atencion.lineas'

    tipo_persona = fields.Many2one(
        comodel_name='tipo.persona',
        string='Tipo Persona',
        domain=[('escala_subvencion', '=', True)],
        required=True
    )
    nivel = fields.Many2one(
        comodel_name='nivel.escala',
        string='Nivel',
        domain="[('tipo_persona', '=', tipo_persona), ('fecha_fin', '=', False)]",
    )
    nro_ninios = fields.Integer(
        string='N° Niños a atender',
        required=True
    )
    coeficiente_id = fields.Many2one(
        comodel_name='coeficiente.atencion',
        inverse_name='coeficiente_lines_ids',
        string='Coeficiente atencion'
    )


class AfiliacionOrganizacionLineas(models.Model):
    _inherit = 'afiliacion.organizacion.lines'

    nivel_mc = fields.Many2one(
        comodel_name='nivel.escala',
        string='Nivel',
        domain="[('tipo_persona', '=', 'MADRE CUIDADORA'), ('fecha_fin', '=', False)]",
    )

    @api.multi
    def name_get(self):
        return [(
            obj.id, u'{} / {} {}'.format(obj.person_id.name, obj.tipo_guia.abrev or "", obj.nivel_mc.tipo or "")
        )for obj in self]

    @api.onchange('cargo')
    def _onchange_tipo_guia(self):
        if self.cargo:
            if self.cargo != 'MADRE CUIDADORA':
                self.local_id = self.ambiente_id = self.salas_id = self.modulo_id = self.nivel_mc = False
            cargo = [
                'PRESIDENTE DE COMITE DE GESTION',
                'PRIMER VOCAL',
                'SEGUNDO VOCAL',
                'SECRETARIO DE COMITE DE GESTION',
                'TESORERO DE COMITE DE GESTION',
                'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'
            ]
            if self.cargo in cargo:
                rec = self.comite_gestion_id.afiliaciones_ids.filtered(
                    lambda x: x.cargo == self.cargo and not x.fecha_retiro and self.id != x.id
                )
                if rec:
                    self.cargo_activo = True
                else:
                    self.cargo_activo = False
            else:
                self.cargo_activo = False
        else:
            self.cargo_activo = False


class ResPartner(models.Model):

    _inherit = 'res.partner'

    asistencia_ids = fields.One2many(
        comodel_name='asistencia.menor',
        inverse_name='partner_id',
        string='Asistencia',
        readonly=True
    )
