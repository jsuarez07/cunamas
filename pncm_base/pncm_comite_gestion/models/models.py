# -*- coding: utf-8 -*-
from openerp import fields, models, api


class CMCargoActorComunal(models.Model):
    _name = 'pncm.cargo_actor_comunal'

    code = fields.Char(string=u'Código', required=True, size=3)
    name = fields.Char(string=u'Nombre', required=True, size=250)
    scd = fields.Boolean(string=u'SCD', required=True)
    saf = fields.Boolean(string=u'SAF', required=True)

    _sql_constraints = [
        ('name_unique', 'unique(name)', u'El nombre ya existe!'),
        ('code_unique', 'unique(code)', u'El código ya existe!')
    ]


class CMEmpleadoCunamas(models.Model):
    _name = 'pncm.empleado_cunamas'

    name = fields.Char(string=u'Nombres', required=True, size=250)
    apellido_paterno = fields.Char(string=u'Apellido Paterno',
                                   required=True, size=250)
    apellido_materno = fields.Char(string=u'Apellido Materno',
                                   required=True, size=250)
    dni = fields.Char(string=u'DNI', required=True, size=8)
    unidad_id = fields.Many2one('hr.department', 'Departamento',
                                required=True)
    cargo_id = fields.Many2one('hr.job', 'Cargo', required=True)

    @api.multi
    def name_get(self):
        return [
            (obj.id, u'{} - {}, ({})'.
             format(obj.dni, ' '.join([obj.apellido_paterno,
                                       obj.apellido_materno, obj.name]),
                    obj.cargo_id.name)) for obj in self
        ]

    _sql_constraints = [
        ('dni_unique', 'unique(dni)', u'El dni ya existe!')
    ]
