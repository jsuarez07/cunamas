# -*- coding: utf-8 -*-

{
    "name": "Formulario de Registro",
    "version": "1.0",
    "author": "CUNAMAS",
    "website": "http://www.cunamas.gob.pe/",
    "category": "Localization",
    "description":
        u"""
        Cambia la plantilla de registro, agregando el campo de DNI del usuario
        """,
    "depends": [
        "website",
        "res_partner_base_pe",
    ],
    "init_xml": [],
    "demo_xml": [],
    "data": [
        'static/src/xml/templates.xml'
    ],
    'installable': True,
    'active': False,
}
