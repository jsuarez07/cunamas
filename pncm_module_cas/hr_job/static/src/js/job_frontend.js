odoo.define('hr_job.job_frontend', function (require) {
    "use strict";

     const pncm = {
        validityAccepExtension: function(fileInput){
            var strExtensions = $(fileInput).attr('extensions');
            if(strExtensions != undefined && strExtensions != null &&
            strExtensions != ''){
                var accepExtensions = strExtensions.split(',');
                var filePath = fileInput.files[0].name;
                var indexLast = filePath.lastIndexOf('.');
                var extension = filePath.substring(indexLast + 1, filePath.length).trim();
                if(!_.contains(accepExtensions, extension)){
                    $(fileInput).val(null);
                    showAlert('Advertencia:', 'Sole puede elegir archivos con las extensiones: '
                        + accepExtensions.toString(), 'warning', 2000);
                    return false;
                }
            }
            return true;
        }
    }

    const jobs_page = {
        paginar_covocatorias: function(page, callback){
            if(page >= 1){
                var cas = $('#name_search_cas').val().trim();
                var position_id = $('#cbo_position').val();
                var state = $('input[name=state_job]:checked').val();
                $.ajax({
                    url: '/jobs/page',
                    type: 'POST',
                    data: {
                        page: page,
                        cas: cas,
                        position_id: position_id,
                        state: state!=undefined?state: ''
                    },
                    success: function(response){
                        $('#jobs_grid').html(response);
                        if(callback != undefined){
                            callback();
                        }
                    },
                    error: function(request){
                        alert('Ocurrio un error en la aplicación, contacte a la unidad de Gestión del Talento del PNCM')
                    }
                });
            }
        }
    };

    const remove_modals = function(){
        var modals = $('.modalForm');
        $.each(modals, function (index, element) {
            $(element).remove();
        });
    }

    const view_modal_confirm = function(width, title, message, callback){
        remove_modals();
        var model_content =
            '<div class="modal fade modalForm" role="dialog">' +
            '<div class="modal-dialog" style="width:'+width+'px; max-height: 700px;" role="document">' +
            '<div class="modal-content" style="width: auto;">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '<div class="modal-title">' +
            '<h4>'+title+'</h4>' +
            '</div></div><div class="modal-body app-modal" style="width: auto;">'+message+'</div>' +
            '<div class="modal-footer" id="button-confirm-modal" style="display: block;"><button type="button" class="btn btn-primary btn_pncm_ok" >Si</button>' +
            '<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>' +
            '</div></div></div></div>';

         var modal = $(model_content);
         modal.find('.btn_pncm_ok').click(function(){
            callback();
         });
         modal.modal({
            cache: false
         });
    }

    const open_report_window = function(url, title){
        var left = (screen.width/2)-(850/2);
        var top = (screen.height/2)-(650/2);
        window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=850, height=650, top='+top+', left='+left);
    }

    var showAlert = function (title,message, type, closeDelay) {

        if ($("#alerts-container").length == 0) {
            $("body")
                .append( $('<div id="alerts-container" style="position: fixed; width: 20%; right: 5%; top: 10%; z-index: 999">'
                 ));
        }

        type = type || "info";

        var alert = $('<div class="alert alert-' + type + ' fade in" style="z-index: 999">')
            .append(
                $('<button type="button" class="close" data-dismiss="alert" aria-label="Close">')
                .append("&times;")
            )
            .append('<h4 class="alert-heading">'+ title +'</h4>')
            .append(message);

        $("#alerts-container").prepend(alert);

        if (closeDelay)
            window.setTimeout(function() { alert.alert("close") }, closeDelay);
    }

    $(document).ready( function(){
        $('.imageFileJob').change(function(evt) {
            if(pncm.validityAccepExtension(this)){
                if (this.files[0].size > 6000000){
                    var message= 'El archivo supera el tamaño maximo permitido de 6MB';
                    showAlert('Advertencia:',message,'warning', 2000);
                    $(this).val(null)
                    evt.preventDefault();
                }
            }
        });

        $('.imageDocJob').change(function(evt) {
            if (this.files[0].size > 4000000){
                var message= 'El archivo supera el tamaño maximo permitido de 4MB';
                showAlert('Advertencia:',message,'warning', 2000);
                $(this).val(null)
                evt.preventDefault();
            }
        });

        var event_resultado_hoja_vida = function(){
            var job_id = $(this).attr('job_id');
            var url = '/jobs/report/hojavida/'+ job_id;
  			open_report_window(url, 'Resultados Evaluación Hoja de Vida');
        };

        var event_resultado_curricular = function(){
            var job_id = $(this).attr('job_id');
            var url = '/jobs/report/curricular/'+ job_id;
  			open_report_window(url, 'Resultados de Evaluación Curricular');
        };

        var event_resultado_final = function(){
            var job_id = $(this).attr('job_id');
            var url = '/jobs/report/final/'+ job_id;
            open_report_window(url, 'Resultados Finales');
        };

        var event_resultado_escrita = function(){
            var job_id = $(this).attr('job_id');
            var url = '/jobs/report/escrita/'+ job_id;
  			open_report_window(url, 'Resultados Evaluación Escrita');
        };

        var handler_page = function(){
            var page = $(this).attr('page');
            jobs_page.paginar_covocatorias(page, function(){
                $('.link-page').click(handler_page);
                $('.btnResultadoHojaVida').click(event_resultado_hoja_vida);
                $('.btnResultadoCurricular').click(event_resultado_curricular);
                $('.btnResultadoFinal').click(event_resultado_final);
                $('.btnResultaEscrita').click(event_resultado_escrita);
            });
        };

        $('.link-page').click(handler_page);
        $('#aceptar').click(function(){
            var page = $('li.page-number.active').find('a').attr('page');
            jobs_page.paginar_covocatorias(page!=undefined?page:1, function(){
                $('.link-page').click(handler_page);
                $('.btnResultadoHojaVida').click(event_resultado_hoja_vida);
                $('.btnResultadoCurricular').click(event_resultado_curricular);
                $('.btnResultadoFinal').click(event_resultado_final);
                $('.btnResultaEscrita').click(event_resultado_escrita);
            });
        })
        $('#limpiar').click(function(){
            $('#name_search_cas').val('');
            $('#cbo_position').val(-1);
            $('input[name=state_job]:checked').prop('checked', false);

            var page = $('li.page-number.active').find('a').attr('page');
            jobs_page.paginar_covocatorias(page!=undefined?page:1, function(){
                $('.link-page').click(handler_page);
                $('.btnResultadoHojaVida').click(event_resultado_hoja_vida);
                $('.btnResultadoCurricular').click(event_resultado_curricular);
                $('.btnResultadoFinal').click(event_resultado_final);
                $('.btnResultaEscrita').click(event_resultado_escrita);
            });
        });

        $('#btn-pncm-postular').click(function(evt){
            var filename = $('#fileInput').val();
            if(filename != undefined && filename != null && filename != ''){
                view_modal_confirm(550, 'Confirmación', '<center><b>¿Está Ud. seguro que desea postular?</b><br/>Tenga en cuenta que solo podrá postular una vez por convocatoria. <br/>'+
                'Se recomienda verificar datos y documentos adjuntos. <br/> <b>Después de confirmar no tendra opción a reclamo.</b></center>', function(){
                    $('#btn-success-pncm-cas').click();
                });
            }
            else{
                showAlert('Advertencia:',
                'Adjunte el archivo de anexos'
                ,'warning',2000);
            }
        });
        $('.btnResultadoHojaVida').click(event_resultado_hoja_vida);
        $('.btnResultadoCurricular').click(event_resultado_curricular);
        $('.btnResultadoFinal').click(event_resultado_final);
        $('.btnResultaEscrita').click(event_resultado_escrita);
    });
});