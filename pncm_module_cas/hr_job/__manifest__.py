# -*- coding: utf-8 -*-
# flake8: noqa
{
    "name": "Proceso de Convocatoria CAS",
    "version": "1.0",
    "author": "CUNAMAS",
    "website": "http://www.cunamas.gob.pe/",
    "category": "Localization",
    "description": u"""
    Crear un objeto: views.job.process
    para configurar los datos del proceso/convocatoria
    """,
    "depends": [
        "hr",
        'hr_recruitment',
        'hr_user',
        'website_portal',
        'website_hr_recruitment',
        'l10n_toponyms_pe',
        'pncm_comite_gestion',
        'web'
    ],
    "init_xml": [],
    "demo_xml": [],
    "data": [
        'views/template_list_view.xml',
        'views/hr_job_base.xml',
        'views/hr_job_process.xml',
        'views/hr_job_base.xml',
        'views/template_cunamas.xml',
        'views/website_portal.xml',
        'views/report_cv.xml',
        'views/report_curriculum_evaluation.xml',
        'views/nivel_experiencia.xml',
        'views/nivel_capacitacion.xml',
        'views/tabla_puntuacion.xml',
        'views/report_evaluacion_hoja_vida.xml',
        'views/report_resultado_final.xml',
        'views/report_entrevista.xml',
        'views/report_resultado_escrita.xml',
        'views/report_resultado_curricular.xml',
        'views/report_evaluation_curricular.xml',
        'security/ir.model.access.csv',
        'data/hr.pncm.nivel_experiencia.csv',
        'data/hr.pncm.nivel_capacitacion.csv',
        'data/hr.pncm.tipo_bonificacion.csv',
        'data/hr.employee.education.level.csv',
        'views/messages.xml'
    ],
    'qweb': [
           'static/src/xml/base.xml',
    ],
    'installable': True,
    'active': False,
}
