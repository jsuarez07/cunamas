# -*- coding: utf-8 -*-
from openerp import models, api
from odoo.addons.hr_job.models.domain import ApplicantUtils
from odoo.addons.hr_job.models.domain import JobUtils
import domain


class CMReportResultadoCurricular(models.AbstractModel):
    _name = 'report.hr_job.resultado_curricular'

    @api.model
    def render_html(self, job_id, data=None):
        report_obj = self.env['report']
        report = report_obj.\
            _get_report_from_name('hr_job.resultado_curricular')
        job = self.env['hr.job'].browse(job_id)

        if job.cronograma_postulacion and \
                job.cronograma_postulacion.directiva == \
                domain.JobUtils.get_directiva_vigente():
            valid_sequences = self.env['hr.recruitment.stage'].search(
                [('sequence', '>=', 2)]
            )
            applicants = self.env['hr.applicant'].search([
                ('job_id', '=', job_id),
                ('stage_id', 'in', [obj.id for obj in valid_sequences]),
            ])
        else:
            applicants = self.env['hr.applicant'].search([
                ('job_id', '=', job_id)
            ])

        applicants = sorted(applicants, key=ApplicantUtils.order_by_fullname)
        docargs = {
            'doc_model': report.model,
            'applicants': applicants,
            'job': job,
            'docs': self
        }
        return report_obj.render('hr_job.resultado_curricular', docargs)


class CMReportEvaluationCurricular(models.AbstractModel):
    _name = 'report.hr_job.report_evaluation_curricular'

    @api.model
    def render_html(self, job_id, data=None):
        report_obj = self.env['report']
        report = report_obj.\
            _get_report_from_name('hr_job.report_evaluation_curricular')
        job = self.env['hr.job'].browse(job_id)
        applicants = self.env['hr.applicant'].search([
            ('job_id', '=', job_id)
        ])
        applicants = sorted(applicants, key=ApplicantUtils.order_by_fullname)
        docargs = {
            'doc_model': report.model,
            'applicants': applicants,
            'job': job,
            'docs': self
        }
        return report_obj.render('hr_job.report_evaluation_curricular', docargs)


class CMReportResultadoFinal(models.AbstractModel):
    _name = 'report.hr_job.resultado_final'

    @api.model
    def render_html(self, job_id, data=None):
        report_obj = self.env['report']
        report = report_obj.\
            _get_report_from_name('hr_job.resultado_final')

        job = self.env['hr.job'].browse(job_id)

        if job.cronograma_postulacion and \
                job.cronograma_postulacion.directiva == \
                domain.JobUtils.get_directiva_vigente():
            print 'directiva vigente'
            tabla_puntuacion = self.env['hr.pncm.tabla_puntuacion'].search([
                ('state', '!=', 'borrador'),
                ('requiere_evaluacion_escrita', '=', job.exam),
                ('directiva', '=', job.cronograma_postulacion.directiva)
            ])
        else:
            print 'directiva anterior'
            tabla_puntuacion = self.env['hr.pncm.tabla_puntuacion'].search([
                ('state', '=', 'activo'),
                ('requiere_evaluacion_escrita', '=', job.exam)
            ])

        if job.exam :
            applicants = self.env['hr.applicant'].search([
                ('job_id', 'in', job_id),
                ('hora_entrevista_escrita', '!=', False)
            ])
        else :
            applicants = self.env['hr.applicant'].search([
                ('job_id', 'in', job_id),
                ('hora_entrevista', '!=', False)
            ])

        applicants = sorted(
            applicants,
            key= JobUtils.attr_sort([
                'puntuacion_total',
                'puntuacion_entrevista']),
            reverse=True
        )


        docargs = {
            'doc_model': report.model,
            'applicants': applicants,
            'job': job,
            'tabla_puntuacion': tabla_puntuacion,
            'docs': self
        }
        return report_obj.render('hr_job.resultado_final', docargs)


class CMReportEvaluacionCurricular(models.AbstractModel):
    _name = 'report.hr_job.report_curriculum_evaluation'

    @api.model
    def render_html(self, job_id, data=None):
        report_obj = self.env['report']
        report = report_obj.\
            _get_report_from_name('hr_job.report_curriculum_evaluation')
        job = self.env['hr.job'].browse(job_id)

        if job.cronograma_postulacion and \
                job.cronograma_postulacion.directiva == \
                domain.JobUtils.get_directiva_vigente():
            valid_sequences = self.env['hr.recruitment.stage'].search(
                [('sequence', '>=', 2)]
            )
            applicants = self.env['hr.applicant'].search([
                ('job_id', '=', job_id),
                ('stage_id', 'in', [obj.id for obj in valid_sequences]),
            ])
        else:
            applicants = self.env['hr.applicant'].search([
                ('job_id', '=', job_id)
            ])

        applicants = sorted(applicants, key=ApplicantUtils.order_by_fullname)
        docargs = {
            'doc_model': report.model,
            'applicants': applicants,
            'job': job,
            'docs': self
        }
        return report_obj.render('hr_job.report_curriculum_evaluation',
                                 docargs)


class CMReportEvaluacionHojaVida(models.AbstractModel):
    _name = 'report.hr_job.report_evaluacion_hoja_vida'

    @api.model
    def render_html(self, job_id, data=None):
        report_obj = self.env['report']
        report = report_obj. \
            _get_report_from_name('hr_job.report_evaluacion_hoja_vida')
        job = self.env['hr.job'].browse(job_id)
        applicants = self.env['hr.applicant'].search([
            ('job_id', '=', job_id)
        ])
        applicants = sorted(applicants, key=ApplicantUtils.order_by_fullname)
        docargs = {
            'doc_model': report.model,
            'applicants': applicants,
            'job': job,
            'docs': self
        }
        return report_obj.render('hr_job.report_evaluacion_hoja_vida', docargs)


class CMReportEntrevista(models.AbstractModel):
    _name = 'report.hr_job.entrevista'

    @api.model
    def render_html(self, job_id, data=None):
        report_obj = self.env['report']
        report = report_obj.\
            _get_report_from_name('hr_job.entrevista')
        job = self.env['hr.job'].browse(job_id)
        if job.exam :
            applicants = self.env['hr.applicant'].search([
                ('job_id', '=', job_id),
                ('hora_entrevista_escrita', '!=', False)
            ])
        else:
            applicants = self.env['hr.applicant'].search([
                ('job_id', '=', job_id),
                ('hora_entrevista', '!=', False)
            ])
        applicants = sorted(applicants, key=ApplicantUtils.order_by_fullname)
        docargs = {
            'doc_model': report.model,
            'applicants': applicants,
            'job': job,
            'docs': self
        }
        return report_obj.render('hr_job.entrevista', docargs)


class CMReportResultadoEscrita(models.AbstractModel):
    _name = 'report.hr_job.resultado_escrita'

    @api.model
    def render_html(self, job_id, data=None):
        report_obj = self.env['report']
        report = report_obj.\
            _get_report_from_name('hr_job.resultado_escrita')
        job = self.env['hr.job'].browse(job_id)
        if job.cronograma_postulacion and \
                job.cronograma_postulacion.directiva == \
                domain.JobUtils.get_directiva_vigente():
            valid_sequences = self.env['hr.recruitment.stage'].search(
                [('sequence', '>=', 3)]
            )
            applicants = self.env['hr.applicant'].search([
                ('job_id', '=', job_id),
                ('stage_id', 'in', [obj.id for obj in valid_sequences]),
            ])
        else:
            applicants = self.env['hr.applicant'].search([
                ('job_id', '=', job_id)
            ])

        applicants = sorted(applicants, key=ApplicantUtils.order_by_fullname)
        docargs = {
            'doc_model': report.model,
            'applicants': applicants,
            'job': job,
            'docs': self
        }
        return report_obj.render('hr_job.resultado_escrita', docargs)
