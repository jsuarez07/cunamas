# -*- coding: utf-8 -*-
from openerp import fields, models, api, _

class MessageConfirm(models.TransientModel):
    _name='hr.job.messages.wizard'

    title = fields.Char(u'')
    body =fields.Text(u'')
    istitlesub = fields.Boolean(
        string=u'',
        default=False
    )
    isfooter = fields.Boolean(
        string=u'',
        default=False
    )