# coding: utf-8

from odoo import http, fields
from odoo.http import request
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.addons.hr_job.models.domain import ApplicantUtils
from odoo.addons.hr_job.models.domain import JobUtils
from datetime import datetime
import json
import pytz
import math


class ControllerApplyJob(http.Controller):
    _limit = 10
    _number_show_pages = 5

    @http.route('/jobs/apply/<model("hr.job"):job>',
                type='http',
                auth="public",
                website=True)
    def jobs_apply(self, job):
        error = {}
        default = {}
        user_id = request.env.user
        profile_complete = False
        custom_validation = False

        if user_id._compute_get_profle():
            profile_complete = user_id._compute_get_profle()[0]
        user = http.request.env['res.users'].browse(user_id.id)
        if 'website_hr_recruitment_error' in request.session:
            error = request.session.pop(
                'website_hr_recruitment_error'
            )
            default = request.session.pop(
                'website_hr_recruitment_default'
            )
        if (datetime.strptime(
            job.date_begin+' '+'00:00:00', '%Y-%m-%d %H:%M:%S') >
                datetime.now(tz=pytz.timezone('America/Lima')).replace(
                    tzinfo=None) or
                datetime.now(tz=pytz.timezone('America/Lima')).replace(
                    tzinfo=None) >
                datetime.strptime(
                    job.date_end+' '+job.date_end_hour, '%Y-%m-%d %H:%M:%S')):
            return request.redirect('/jobs')

        if request.website.user_id.id == request.env.user.id:
            return request.redirect('/jobs')

        if job.state != 'recruit':
            return request.redirect('/jobs')

        job_applicant = request.env['hr.applicant'].sudo(). \
            search([('profile_user', '=', user.id)], limit=1)

        if job.cronograma_postulacion and job_applicant \
                and job_applicant.job_id.\
                cronograma_postulacion == job.cronograma_postulacion:

            exist_applicant = request.env['hr.applicant'].sudo(). \
                search(
                [('job_id', '=', job.id), ('profile_user', '=', user.id)])

            if exist_applicant:
                custom_validation = 'applicant_duplicity'
            else:
                custom_validation = 'time_line_duplicity'

        return request.render(
            "hr_job.apply", {
                'user': user,
                'profile_complete': profile_complete,
                'job': job,
                'error': error,
                'default': default,
                'customValidation': custom_validation
            }
        )

    @http.route('/jobs/report/hojavida/<int:job_id>',
                type='http',
                auth="public",
                website=True)
    def jobs_reporte_hoja_vida(self, job_id):
        error = {}
        default = {}
        job = request.env['hr.job'].sudo().browse(job_id)
        applicants = request.env['hr.applicant'].sudo().search([
            ('job_id', '=', job_id)
        ])
        applicants = sorted(applicants, key=ApplicantUtils.order_by_fullname)

        return request.render(
            "hr_job.report_evaluacion_hoja_vida_user", {
                'job': job,
                'error': error,
                'default': default,
                'applicants': applicants
            }
        )

    @http.route('/jobs/report/curricular/<int:job_id>',
                type='http',
                auth="public",
                website=True)
    def jobs_reporte_curricular(self, job_id):
        error = {}
        default = {}
        job = request.env['hr.job'].sudo().browse(job_id)

        if job.cronograma_postulacion and \
                job.cronograma_postulacion.directiva == \
                JobUtils.get_directiva_vigente():
            valid_sequences = request.env['hr.recruitment.stage'].\
                sudo().search([('sequence', '>=', 2)])

            applicants = request.env['hr.applicant'].sudo().search([
                ('job_id', '=', job_id),
                ('stage_id', 'in', [obj.id for obj in valid_sequences]),
            ])
        else:
            applicants = request.env['hr.applicant'].sudo().search([
                ('job_id', '=', job_id)
            ])

        applicants = sorted(applicants, key=ApplicantUtils.order_by_fullname)

        return request.render(
            "hr_job.reporte_resultado_curricular", {
                'job': job,
                'error': error,
                'default': default,
                'applicants': applicants
            }
        )

    @http.route('/jobs/report/escrita/<int:job_id>',
                type='http',
                auth="public",
                website=True)
    def jobs_reporte_escrita(self, job_id):
        error = {}
        default = {}
        job = request.env['hr.job'].sudo().browse(job_id)

        if job.cronograma_postulacion and \
                job.cronograma_postulacion.directiva == \
                JobUtils.get_directiva_vigente():
            valid_sequences = request.env['hr.recruitment.stage']. \
                sudo().search([('sequence', '>=', 3)])

            applicants = request.env['hr.applicant'].sudo().search([
                ('job_id', '=', job_id),
                ('stage_id', 'in', [obj.id for obj in valid_sequences]),
            ])
        else:
            applicants = request.env['hr.applicant'].sudo().search([
                ('job_id', '=', job_id)
            ])

        applicants = sorted(applicants, key=ApplicantUtils.order_by_fullname)

        return request.render(
            "hr_job.reporte_resultado_escrita", {
                'job': job,
                'error': error,
                'default': default,
                'applicants': applicants
            }
        )

    @http.route('/jobs/report/final/<int:job_id>',
                type='http',
                auth="public",
                website=True)
    def jobs_reporte_final(self, job_id):
        error = {}
        default = {}
        job = request.env['hr.job'].sudo().browse(job_id)

        if job.exam :
            applicants = request.env['hr.applicant'].sudo().search([
                ('job_id', '=', job_id),
                ('hora_entrevista_escrita', '!=', False)
            ])
        else :
            applicants = request.env['hr.applicant'].sudo().search([
                ('job_id', '=', job_id),
                ('hora_entrevista', '!=', False)
            ])

        applicants = sorted(
            applicants,
            key=JobUtils.attr_sort([
                'puntuacion_total',
                'puntuacion_entrevista']),
            reverse=True
        )

        if job.cronograma_postulacion and \
                job.cronograma_postulacion.directiva == \
                JobUtils.get_directiva_vigente():
            tabla_puntuacion = request.env['hr.pncm.tabla_puntuacion'].\
                sudo().search([
                    ('state', '!=', 'borrador'),
                    ('requiere_evaluacion_escrita', '=', job.exam),
                    ('directiva', '=', job.cronograma_postulacion.directiva)
            ])
        else:
            tabla_puntuacion = request.env['hr.pncm.tabla_puntuacion'].\
                sudo().search([
                    ('state', '=', 'activo'),
                    ('requiere_evaluacion_escrita', '=', job.exam)
            ])

        return request.render(
            "hr_job.reporte_resultado_final", {
                'job': job,
                'error': error,
                'default': default,
                'applicants': applicants,
                'tabla_puntuacion': tabla_puntuacion
            }
        )

    @http.route('/jobs/validar/<int:job_id>',
                type='http',
                auth="public",
                website=True)
    def jobs_validar_duplicidad(self, job_id):
        user = request.env.user
        user = http.request.env['res.users'].browse(user.id)

        if user:
            exist_applicant = request.env['hr.applicant'].sudo(). \
                search(
                [('job_id', '=', job_id), ('user_id', '=', user.id)])

            if exist_applicant:
                return json.dumps({'success': False})

        return json.dumps({'success': True})

    @http.route(
        '/jobs',
        type='http',
        auth="public",
        website=True
    )
    def jobs(self):
        limit = self._limit
        page = 1
        result = self._get_jobs(page, limit,
                                [('website_published', '=', True)])
        offset = ((page - 1) * limit) + 1
        total_pages = int(math.ceil(float(result[1]) / limit))
        return request.render("hr_job.index", {
            'jobs': result[0],
            'limit': limit,
            'offset': offset,
            'page_select': page,
            'endset': offset + len(result[0]) - 1,
            'total_pages': total_pages,
            'range_pages': self._get_range_pages(total_pages, page),
            'positions': http.request.env['hr.positions'].sudo().search([]),
            'total_jobs_count': result[1],
            'current_date': datetime.now().date(),
            'web_site_user_id': request.website.user_id.id,
            'date_time_now': datetime.now(tz=pytz.timezone('America/Lima')),
            'login_user_id': request.env.user.id
        })

    @http.route(
        '/jobs/page',
        type='http',
        auth="public",
        website=True,
        csrf=False
    )
    def page_jobs(self, **post):
        limit = self._limit
        page = int(post.get('page'))
        cas = post.get('cas')
        position_id = int(post.get('position_id'))
        state = post.get('state')

        domain = [('website_published', '=', True)]
        if cas:
            domain.append(('name', 'ilike', cas))
        if position_id != -1:
            domain.append(('puesto_trabajo', '=', position_id))
        if state:
            domain.append(('state', '=', state))

        result = self._get_jobs(page, limit, domain)
        offset = ((page - 1) * limit) + 1
        total_pages = int(math.ceil(float(result[1]) / limit))

        return request.render("hr_job.parcial_paginado", {
            'jobs': result[0],
            'limit': limit,
            'offset': offset,
            'page_select': page,
            'endset': offset + len(result[0]) - 1,
            'total_pages': total_pages,
            'range_pages': self._get_range_pages(total_pages, page),
            'total_jobs_count': result[1],
            'current_date': datetime.now().date(),
            'date_time_now': datetime.now(tz=pytz.timezone('America/Lima')),
            'web_site_user_id': request.website.user_id.id,
            'login_user_id': request.env.user.id
        })

    def _get_jobs(self, page, limit, domain):
        env = request.env(context=dict(request.env.context, show_address=True,
                                       no_tag_br=True))
        jobs = env['hr.job']. \
            sudo().search(domain, offset=((page - 1) * limit),
                          limit=limit, order="date_publish desc, number desc")
        total_jobs_count = request.env['hr.job']. \
            sudo().search_count(domain)

        result = (jobs, total_jobs_count)
        return result

    def _get_range_pages(self, total_pages, current_page):
        if total_pages <= self._number_show_pages:
            return range(total_pages)
        else:
            ls = 0
            le = self._number_show_pages

            if current_page + 2 < total_pages:
                if current_page - 2 > 0:
                    ls = current_page - 2 - 1
                if current_page + 2 > self._number_show_pages:
                    le = current_page + 2
            else:
                ls = total_pages - self._number_show_pages
                le = total_pages
            return range(ls, le)


class WebsiteFormInherited(WebsiteForm):

    def insert_record(self, request, model, values, custom, meta=None):
        if model.model in ['hr.applicant']:
            user = request.env.user
            user = http.request.env['res.users'].browse(user.id)
            exist_applicant = False
            if user:
                job_id = values.get('job_id')
                exist_applicant = request.env['hr.applicant'].sudo(). \
                    search(
                    [('job_id', '=', job_id), ('profile_user', '=', user.id)])

            if request.env.user.id != request.website.user_id.id \
                    and not exist_applicant:
                obj_study = []
                for study in user.study_ids:
                    obj_study.append((0, 0, {
                        'user_study_id': study.id,
                        'level_study': study.level_study.id,
                        'career': study.career,
                        'specialty': study.specialty,
                        'date': study.date,
                        'university': study.university,
                        'country_id': study.country_id.id,
                        'state_id': study.state_id.id,
                        # 'file_url': study.file_url,
                        # 'filename': study.filename,
                        'is_career': study.is_career
                    }))

                obj_traning = []
                for traning in user.training_ids:
                    obj_traning.append((0, 0, {
                        'user_training_id': traning.id,
                        'name': traning.name,
                        'date_start': traning.date_start,
                        'date_end': traning.date_end,
                        'institution': traning.institution,
                        'country_id': traning.country_id.id,
                        'state_id': traning.state_id.id,
                        'time': traning.time
                        # 'filename': traning.filename,
                        # 'file_url': traning.file_url,
                    }))

                obj_work = []
                for work in user.work_experience_ids:
                    obj_work.append((0, 0, {
                        'user_work_id': work.id,
                        'name': work.name,
                        'date_start': work.date_start,
                        'date_end': work.date_end,
                        'type_experience': work.type_experience,
                        'charge': work.charge,
                        'description': work.description
                        # 'filename': work.filename,
                        # 'file_url': work.file_url,
                    }))

                values.update({
                    'profile_user': user.id,
                    'name': '{} {} {}'.format(user.partner_name,
                                              user.partner_firstname,
                                              user.partner_secondname),
                    'partner_birthdate': user.partner_birthdate,
                    'partner_document_number': user.partner_document_number,
                    'partner_vat': user.partner_vat,
                    'email_from': user.partner_email,
                    'state_id': user.state_id.id,
                    'province_id': user.province_id.id,
                    'district_id': user.district_id.id,
                    'partner_professional': user.partner_professional,
                    'enlable_number': user.enlable_number,
                    'partner_civil': user.partner_civil,
                    'partner_street': user.partner_street,
                    'partner_sex': user.partner_sex,
                    'partner_phone': user.partner_phone,
                    'partner_email': user.partner_email,
                    'register_date_application': fields.Date().today(),
                    'task_revision_curricular': 'open',
                    'study_ids': obj_study,
                    'training_ids': obj_traning,
                    'work_experience_ids': obj_work
                })
                res = super(WebsiteFormInherited, self). \
                    insert_record(request, model, values, '', meta=meta)

                return res
