# -*- coding: utf-8 -*-

{
    "name": "Relación cliente-usario",
    "version": "1.0",
    "author": "CUNAMAS",
    "website": "http://www.cunamas.gob.pe/",
    "category": "Localization",
    "description": "Modulo de perfil de usuario para CAS",
    "depends": [
        "website",
        "website_portal",
        "l10n_toponyms_pe",
        "hr_employee_pe",
        "res_partner_base_pe"
    ],
    "init_xml": [],
    "demo_xml": [],
    "data": [
        'views/trees.xml',
        'views/forms.xml',
        'security/ir.model.access.csv',
        'views/theme.xml',
        'static/src/xml/templates.xml'
    ],
    'qweb': ['static/src/xml/popup.xml'],
    'installable': True,
    'active': False,
}
