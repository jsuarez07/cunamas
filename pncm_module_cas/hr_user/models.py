# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime
import os

_opttipodocumento = [
    ('dni', 'DNI'),
    ('ext', u'Carnet de Extranjería'),
    ('pas', 'Pasaporte')
]


class ResPartner(models.Model):
    _inherit = 'res.partner'

    firstname = fields.Char(string='Apellido paterno')
    secondname = fields.Char(string='Apellido materno')
    birthdate = fields.Date(string='Fecha de nacimiento')
    type_sex = fields.Selection(
        selection=[
            ('m', 'Masculino'), ('f', 'Femenino')
        ],
        string='Sexo',
        default='m'
    )
    document_number = fields.Char(string=u'Documento de identidad')
    enlable_number = fields.Boolean(string=u'Colegiatura habilitada')
    professional_number = fields.Char(
        string='Colegio profesional'
    )
    country_id = fields.Many2one(
        'res.country',
        'País',
        default=lambda self: self.env.user.company_id.country_id
    )
    state_id = fields.Many2one(
        'res.country.state', 'departamento',
        domain="[('country_id', '=', country_id),"
               " ('state_id', '=', false), ('province_id', '=', false)]")
    province_id = fields.Many2one(
        'res.country.state', 'provincia',
        domain="[('state_id', '=', state_id), ('province_id', '=', false)]")
    district_id = fields.Many2one(
        'res.country.state', 'distrito',
        domain="[('state_id', '=', state_id),"
               " ('province_id', '=', province_id)]")


class ResUsers(models.Model):
    _inherit = 'res.users'

    partner_name = fields.Char(string='Nombres')
    partner_firstname = fields.Char(string='Apellido paterno')
    partner_secondname = fields.Char(string='Apellido materno')
    partner_birthdate = fields.Date(string='Fecha de nacimiento')
    partner_phone = fields.Char(string=u'Teléfono')
    partner_mobile = fields.Char(string='Celular')
    partner_email = fields.Char(string='Email')
    partner_sex = fields.Selection(
        string='Sexo',
        selection=[
            ('m', 'Masculino'),
            ('f', 'Femenino')
        ]
    )
    partner_type_document = fields.Selection(
        selection=_opttipodocumento,
        string="Tipo de Documento"
    )
    file_url_colegiatura = fields.Char('Certificado de Habilidad URL')
    file_name_colegiatura = fields.Char(string=u'Certificado de Habilidad')

    partner_document_number = fields.Char(string=u'Documento de identidad')
    partner_image_dni = fields.Binary(string=u'Adjunto DNI')
    file_partner_image_dni = fields.Char(string=u'')
    file_dni = fields.Binary(string=u'Adjunto DNI')
    file_name_dni = fields.Char(string=u'')
    file_url_dni = fields.Char('Documento DNI Adjunto URL')
    es_de_fuerzas_armadas = fields.Boolean(string='Es de Fuerzas armadas')
    file_fuerzas_armadas = fields.Binary(string=u'Adjunto Certificado FFAA')
    file_name_fuerzas_armadas = fields.Char(string=u'')
    file_name_fuerzas_armadas_new = fields.Char(string=u'')
    file_url_fuerzas_armadas = fields.Char('Certificado de Fuerzas Armadas  URL')
    es_discapacitado = fields.Boolean(string='Es Discapacitado')
    file_discapacitado = fields.Binary(string=u'Certificado Discapacidad')
    file_name_discapacitado = fields.Char(string=u'')
    file_name_discapacitado_new = fields.Char(string=u'')
    file_url_discapacitado = fields.Char('Certificado de Discapacidad URL')
    partner_vat = fields.Char(
        string='RUC',
        related='partner_id.vat'
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state', string='Departamento',
        domain="[('country_id', '=', country_id), "
               "('state_id', '=', false), "
               "('province_id', '=', false)]"
    )
    province_id = fields.Many2one(
        comodel_name='res.country.state', string='Provincia',
        domain="[('state_id', '=', state_id), "
               "('province_id', '=', false)]"
    )
    district_id = fields.Many2one(
        comodel_name='res.country.state', string='Distrito',
        domain="[('state_id', '=', state_id), "
               "('province_id', '=', province_id)]"
    )
    partner_professional = fields.Char(string='Colegio profesional')
    enlable_number = fields.Boolean(string=u'Colegiatura habilitada')
    study_ids = fields.One2many(
        comodel_name='res.partner.study',
        inverse_name='user_id',
        string=u'Estudios'
    )
    training_ids = fields.One2many(
        comodel_name='res.partner.training',
        inverse_name='user_id',
        string=u'Capacitaciones'
    )
    work_experience_ids = fields.One2many(
        comodel_name='res.partner.work.experience',
        inverse_name='user_id',
        string=u'Experiencia laboral'
    )
    partner_civil = fields.Selection(
        selection=[
            ('s', 'Soltero'),
            ('c', 'Casado'),
            ('v', 'Viudo'),
            ('d', 'Divorciado')
        ],
        string=u'Estado civil'
    )
    partner_street = fields.Char(string=u'Dirección')
    profile_complete = fields.Boolean(
        string=u'Perfil Completado',
        compute='_compute_get_profle',
        store=True
    )

    @api.one
    def _compute_get_profle(self):
        if (
                self.partner_name and
                self.partner_firstname and
                self.partner_secondname and
                self.partner_birthdate and
                self.partner_email and
                self.partner_document_number and
                self.file_name_dni and
                self.state_id and
                self.province_id and
                self.district_id
        ):
            self.profile_complete = True
            return True
        return False

    @api.multi
    def write(self, values):
        res = super(ResUsers, self).write(values)
        for obj in self:
            if obj.partner_name:
                obj.partner_id.write({
                    'name':
                        obj.partner_name.upper()
                        if obj.partner_name is not False
                        else obj.partner_name,
                    'firstname': obj.partner_firstname.upper()
                        if obj.partner_firstname is not False
                        else obj.partner_firstname,
                    'secondname': obj.partner_secondname.upper()
                        if obj.partner_secondname is not False
                        else obj.partner_secondname,
                    'birthdate': obj.partner_birthdate,
                    'type_sex': obj.partner_sex,
                    'document_number': obj.partner_document_number,
                    'state_id': obj.state_id.id,
                    'province_id': obj.province_id.id,
                    'vat': obj.vat,
                    'professional_number': obj.partner_professional,
                    'enlable_number': obj.enlable_number,
                    'phone': obj.partner_phone,
                    'district_id': obj.district_id.id,
                    'mobile': obj.partner_mobile
                })
        return res

    _sql_constraints = [
        ('partner_document_number_unique',
         'unique(partner_type_document,partner_document_number)',
         'SOLO debe de haber un numero de DNI!')
    ]


class ResPartnerStudy(models.Model):
    _name = 'res.partner.study'
    _description = 'Estudios'
    _order = "date desc"

    career = fields.Char(string=u'Título o grado')
    level_study = fields.Many2one(
        'hr.employee.education.level',
        string='Nivel de Estudios'
    )
    specialty = fields.Char(string='Especialidad')
    date = fields.Date(
        string=u'Fecha de expedición del título'
    )
    university = fields.Char(string='Universidad')
    country_id = fields.Many2one(
        comodel_name='res.country',
        string=u'País'
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        domain="[('country_id', '=', country_id)]",
        string='Cuidad'
    )
    file = fields.Binary(
        string='Archivo',
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png'
    )
    filename = fields.Char(string='Nombre del archivo')
    is_career = fields.Boolean(string=u'No tiene título')
    is_studing = fields.Boolean(string=u'Estudio actualmente')
    user_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario',
        required=True
    )
    file_url = fields.Char('URL')

    @api.model
    def get_record(self, id):
        domain = [
            ('user_id', '=', self.env.uid)
        ] if not id else [('id', '=', id)]
        return self.search(domain).mapped(lambda x: {
            'id': x.id,
            'level_study': x.level_study.id or '',
            'specialty': x.specialty or '',
            'date':
                datetime.strptime(x.date,
                                  "%Y-%m-%d").date().strftime("%d/%m/%Y")
                if x.date else '',
            'university': x.university or '',
            'country': x.country_id.name or '',
            'state': x.state_id.name or '',
            'is_career': x.is_career
        })

    @api.model
    def delete_record(self, id):
        record_study = self.browse(id)
        record = self.env['applicant.study'].search(
            [('user_study_id', '=', id)], limit=1)
        if not record:
            if record_study.filename:
                path_os = self.env[
                    'ir.config_parameter'].get_param(
                    'path.filestatic')
                path_url = '/' + str(
                    record_study.user_id.id) + '/' + record_study.filename
                file_study_delete = path_os + path_url
                if file_study_delete:
                    if os.path.isfile(file_study_delete):
                        try:
                            os.remove(file_study_delete)
                        except OSError:
                            raise
        record_study.sudo().unlink()
        return True


class ResPartnerTraining(models.Model):
    _name = 'res.partner.training'
    _description = 'Capacitación'
    _order = "date_start desc"

    name = fields.Char(
        string=u'Capacitación',
        required=True
    )
    institution = fields.Char(
        string=u'Institución'
    )
    date_start = fields.Date(
        string='Fecha de inicio',
        default=fields.Date().today()
    )
    date_end = fields.Date(
        string=u'Fecha de término'
    )
    country_id = fields.Many2one(
        comodel_name='res.country',
        string=u'País'
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        domain="[('country_id', '=', country_id)]",
        string='Ciudad'
    )
    time = fields.Float(
        string='Horas lectivas'
    )
    file = fields.Binary(
        string='Archivo',
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png'
    )
    filename = fields.Char(string='Nombre del archivo')
    user_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario',
        required=True
    )
    file_url = fields.Char('URL')

    def date_format(date):
        return datetime.strptime(
            date, "%Y-%m-%d"). \
            date(). \
            strftime("%d/%m/%Y") if date else ''

    @api.model
    def get_record(self, id):
        domain = [
            ('user_id', '=', self.env.uid)
        ] if not id else [('id', '=', id)]
        return self.search(domain).mapped(lambda x: {
            'id': x.id,
            'name': x.name or '',
            'date_start':  self.date_format(x.date_start),
            'date_end':  self.date_format(x.date_end),
            'institution': x.institution or '',
            'country': x.country_id.name or '',
            'state': x.state_id.name or '',
            'time': x.time or ''
        })

    @api.model
    def delete_record(self, id):
        record_training = self.browse(id)
        record = self.env['applicant.training'].search(
            [('user_training_id', '=', id)], limit=1)
        if not record:
            if record_training.filename:
                path_os = self.env[
                    'ir.config_parameter'].get_param(
                    'path.filestatic')
                path_url = '/' + str(record_training.user_id.id) + '/' \
                           + record_training.filename
                file_training_delete = path_os + path_url
                if file_training_delete:
                    if os.path.isfile(file_training_delete):
                        try:
                            os.remove(file_training_delete)
                        except OSError:
                            raise
        record_training.sudo().unlink()
        return True


class ResPartnerWorkExperience(models.Model):
    _name = 'res.partner.work.experience'
    _description = 'Experiencia laboral'
    _order = "date_start desc"

    name = fields.Char(
        string='Nombre de la entidad o  empresa',
        required=True
    )
    date_start = fields.Date(
        string='Fecha de inicio',
        default=fields.Date().today()
    )
    date_end = fields.Date(
        string=u'Fecha de término'
    )
    charge = fields.Char(
        string=u'Cargo desempeñado'
    )
    type_experience = fields.Selection(
        string=u'Tipo de Experiencia',
        selection=[
            ('general', 'LABORAL GENERAL'),
            ('especifica', 'LABORAL ESPECÍFICA')
        ]
    )
    description = fields.Text(
        string=u'Descripción del trabajo realizado'
    )
    country_id = fields.Many2one(
        comodel_name='res.country', string='Departamento',
    )

    state_id = fields.Many2one(
        comodel_name='res.country.state', string='Departamento',
        domain="[ "
               "('state_id', '=', false), "
               "('province_id', '=', false)]"
    )

    filename = fields.Char(
        string='Nombre del archivo'
    )
    file = fields.Binary(
        string='Archivo',
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png'
    )
    file_url = fields.Char('URL')
    user_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario',
        required=True
    )

    @api.model
    def get_record(self, id):
        def date_format(date):
            return datetime.strptime(date, "%Y-%m-%d").\
                date().strftime("%d/%m/%Y") if date else ''

        domain = [
            ('user_id', '=', self.env.uid)
        ] if not id else [('id', '=', id)]
        return self.search(domain).mapped(lambda x: {
            'id': x.id,
            'name': x.name or '',
            'date_start':  date_format(x.date_start),
            'date_end':  date_format(x.date_end),
            'charge': x.charge or '',
            'description': x.description or '',
        })

    @api.model
    def delete_record(self, id):
        record_work = self.browse(id)
        record = self.env['applicant.work.experience'].search(
            [('user_work_id', '=', id)], limit=1)
        if not record:
            if record_work.filename:
                path_os = self.env[
                    'ir.config_parameter'].get_param(
                    'path.filestatic')
                path_url = '/' + str(record_work.user_id.id) + '/' \
                           + record_work.filename
                file_experience_delete = path_os + path_url
                if file_experience_delete:
                    if os.path.isfile(file_experience_delete):
                        try:
                            os.remove(file_experience_delete)
                        except OSError:
                            raise
        record_work.sudo().unlink()
        return True


class ResCountryState(models.Model):
    _inherit = 'res.country.state'

    @api.model
    def get_state(self, country_id):
        country_obj = self.env['res.country'].\
            search([('name', 'ilike', 'Peru')], limit=1)
        domain = [] if not country_obj else [
            ('country_id', '=', country_id or country_obj.id),
            ('state_id', '=', False),
            ('province_id', '=', False), ('district_id', '=', False)
        ]
        return self.search(domain).\
            mapped(lambda x: {'id': x.id, 'name': x.name})

    @api.model
    def get_province(self, state_id):
        domain = [
            ('state_id', '=', state_id),
            ('district_id', '=', False),
            ('province_id', '=', False)
        ]
        return self.search(domain).\
            mapped(lambda x: {'id': x.id, 'name': x.name})

    @api.model
    def get_district(self, province_id):
        domain = [
            ('state_id', '!=', False),
            ('district_id', '=', False),
            ('province_id', '=', province_id)
        ]
        return self.search(domain).\
            mapped(lambda x: {'id': x.id, 'name': x.name})


class ResCountry(models.Model):
    _inherit = 'res.country'

    @api.model
    def get_record(self):
        return self.search([]).\
            mapped(lambda x: {'id': x.id, 'name': x.name})
