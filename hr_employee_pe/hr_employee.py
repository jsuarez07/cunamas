# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014 Consultoria YarosLab SAC(<http://www.yaroslab.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api
from datetime import datetime

class EmployeeEducationLevel(models.Model):

    _name = 'hr.employee.education.level'

    name = fields.Char(u'Nivel Educación', size=100, required=True)
    code = fields.Char(u'Código', size=5, required=True)
    abbreviation = fields.Char('Abreviatura', size=20, required=True)


class EmployeeSituation(models.Model):

    _name = 'hr.employee.situation'

    name = fields.Char(u'Situación Trabajador', size=100, required=True)
    code = fields.Char(u'Código', size=5, required=True)
    abbreviation = fields.Char('Abreviatura', size=20, required=True)

class EmployeeFamilyRelationship(models.Model):

    _name = 'hr.employee.family.relationship'

    name = fields.Char('Vinculo Familiar', size=100, required=True)
    code = fields.Char(u'Código', size=3, required=True)


class EmployeeFamilyRelationshipDocument(models.Model):

    _name = 'hr.employee.family.relationship.document'

    name = fields.Char('Documento Vinculo Familiar', size=100, required=True)
    code = fields.Char(u'Código', size=3, required=True)


class EmployeeRelatedPerson(models.Model):

    _name = 'hr.employee.related.person'

    name = fields.Char('Nombres y Apellidos', size=100, required=True)
    employee_id = fields.Many2one('hr.employee', 'Empleado')
    dni = fields.Char('DNI', size=8, required=True)
    birthday = fields.Date('Fecha de Nacimiento')
    sex = fields.Selection(selection=[
        ('M', 'Masculino'),
        ('F', 'Femenino')
    ], string='Sexo')
    relationship_id = fields.Many2one('hr.employee.family.relationship', 'Vinculo Familiar', required=True)
    relationship_document_id = fields.Many2one('hr.employee.family.relationship.document', 'Documento Acredita')
    comment = fields.Text('Comentario')
    age = fields.Integer('Edad', compute='_compute_age')

    @api.one
    @api.depends('birthday')
    def _compute_age(self):
        if self.birthday:
            birthday = self.birthday
            today = datetime.today()
            birth = datetime.strptime(birthday, '%Y-%m-%d')
            diff = today - birth
            self.age = int(diff.days / 365.2425)


class Employee(models.Model):

    _inherit = 'hr.employee'

    first_name = fields.Char('Nombres(s)', size=250)
    last_name = fields.Char('Apellido Paterno', size=250)
    last_name_2 = fields.Char('Apellido Materno', size=250)
    #document_type_id = fields.Many2one('pe.base.document.type', 'Tipo de Documento')
    document_number = fields.Char('Numero de Documento', size=15)
    person_ruc = fields.Char('RUC', size=11)
    person_cellphone = fields.Char('Celular', size=9)
    person_phone = fields.Char('Teléfono', size=9)
    person_email = fields.Char('Correo Personal', size=250)
    person_education_level_id = fields.Many2one('hr.employee.education.level', u'Nivel Educación')
    person_egress_date = fields.Date('Fecha de Egreso')
    person_specialty = fields.Char('Especialidad/Prog. Académico', size=250)
    person_study_center = fields.Char('Centro de Estudios', size=250)
    person_blood_group = fields.Selection(selection=[
        ('O+', 'O+'),
        ('O-', 'O-'),
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-')
    ], string='Grupo sanguineo')
    person_situation_id = fields.Many2one('hr.employee.situation', u'Situación Trabajador')
    person_payment_type = fields.Selection(selection=[
        ('1', 'EFECTIVO'),
        ('2', 'DEPOSITO EN CUENTA'),
        ('3', 'OTROS')
    ], string='Tipo de Pago')
    home_zone_id = fields.Many2one('res.country.zone', 'Zona')
    home_via_id = fields.Many2one('res.country.via', u'Vía')
    home_state_id = fields.Many2one('res.country.state', 'Departamento')
    home_province_id = fields.Many2one('res.country.province', 'Provincia')
    home_district_id = fields.Many2one('res.country.district', 'Distritos')
    birth_state_id = fields.Many2one('res.country.state', 'Departamento')
    birth_province_id = fields.Many2one('res.country.province', 'Provincia')
    birth_district_id = fields.Many2one('res.country.district', 'Distritos')
    related_person_id = fields.One2many('hr.employee.related.person', 'employee_id', 'Familia')
    age = fields.Integer('Edad', compute='_compute_age')
    nationality1 = fields.Many2one('res.country', string=u'Nacionalidad')
    nationality2 = fields.Many2one('res.country', string=u'Nacionalidad')

    cts_bank_account_id = fields.Many2one('res.partner.bank', u'número de cuenta de CTS')
    disability = fields.Selection(selection=[
        ('1', '[1] SI'),
        ('2', '[2] NO'),
    ], string='¿Discapacidad?')

    cussp = fields.Char('CUSSP')

    @api.one
    @api.depends('birthday')
    def _compute_age(self):
        if self.birthday:
            birthday = self.birthday
            today = datetime.today()
            birth = datetime.strptime(birthday, '%Y-%m-%d')
            diff = today - birth
            self.age = int(diff.days / 365.2425)

    @api.model
    def action_permitology(self, name_group):
        bool_group = self.env['res.users'].has_group(name_group)
        return bool_group


