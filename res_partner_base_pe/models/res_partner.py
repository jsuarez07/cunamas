# -*- coding: utf-8 -*-

from dateutil.relativedelta import relativedelta
from openerp import fields, models, api
from openerp.exceptions import ValidationError
from datetime import datetime

ESTADO_CIVIL = [
    ('Soltero', 'Solter@'),
    ('Casado', 'Casad@'),
    ('Viudo', 'Viud@'),
    ('Divorciado', 'Divorciad@'),
    ('Separado', 'Separad@'),
]
_opttipodocumento = [
    ('dni', 'DNI'),
    ('ruc', 'RUC'),
    ('ext', u'Carnet de Extranjería'),
    ('pas', 'Pasaporte')
]

class ParnertInherit(models.Model):
    _inherit = 'res.partner'

    _varmayoredad = 18

    _varjsonlongituddocumento = {'dni': 8, 'ext': 12, 'pas': 11, 'ruc': 11}
    _varespeciales = u'ÑÁÉÍÓÚ'

    company_type = fields.Selection(
        selection=[
            ('person', 'Persona'),
            ('company', u'Jurídica'),
        ],
        default='person'
    )
    firstname = fields.Char(string='Apellido Paterno')
    secondname = fields.Char(string='Apellido Materno')
    birthdate = fields.Date(string='Fecha de nacimiento')
    gender = fields.Selection(
        selection=[
            ('m', 'Masculino'),
            ('f', 'Femenino')],
        string='Sexo',
        default='m'
    )
    type_document = fields.Selection(
        selection=_opttipodocumento,
        string="Tipo de Documento"
    )
    marital_status = fields.Selection(
        ESTADO_CIVIL,
        string='Estado Civil'
    )
    document_number = fields.Char(string=u'Numero de Documento ')
    age = fields.Char(
        string='Años',
        compute='_compute_age',
    )

    @api.multi
    def _compute_age(self):
        """
        Age computed depending based on the birth date in the
        """
        now = datetime.now()
        for record in self:
            if record.birthdate:
                birthdate = fields.Datetime.from_string(
                    record.birthdate,
                )
                delta = relativedelta(now, birthdate)
                is_deceased = ''
                years_months_days = '%d%s %d%s %d%s%s' % (
                    delta.years, 'Año(s)', delta.months, 'mes(es)',
                    delta.days, 'dia(s)', is_deceased
                )
                years = delta.years
            else:
                years_months_days = 'No FdN'
                years = False
            record.age = years_months_days
            if years:
                record.age_years = years

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            str_name = record.name or ('id. %d' % record.id) or 0
            if record.name and record.secondname and record.firstname:
                str_name = "[%s] %s %s, %s" % (
                record.document_number, record.firstname, record.secondname, record.name)
            elif record.firstname and record.name:
                str_name = "[%s] %s, %s" % (record.document_number, record.firstname, record.name)
            elif record.company_type=='company':
                str_name = "[%s] %s" % (record.document_number, record.business_name)
            result.append((record.id, str_name))
        return result

    @api.depends(
        'name',
        'secondname',
        'firstname',
        'business_name',
        'company_type',
        'document_number'
    )
    @api.onchange(
        'name',
        'secondname',
        'firstname',
        'business_name',
        'company_type',
        'document_number'
    )
    def onchange_person_information(self):
        if self.name:
            self.name = self.name.upper()
        if self.secondname:
            self.secondname = self.secondname.upper()
        if self.firstname:
            self.firstname = self.firstname.upper()
        if self.business_name:
            self.business_name = self.business_name.upper()

        if self.company_type=='company':
            self.name = str(self.business_name).strip().upper()
            self.secondname = ''
            self.firstname = ''
            self.type_document = 'ext'
        elif self.company_type == 'person':
            self.business_name = ''
            self.type_document = 'dni'

    _sql_constraints = [
        ('document_number_unique',
         'UNIQUE(document_number)',
         "Ya existe el numero de documento de documento")
    ]
