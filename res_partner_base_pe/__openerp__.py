# -*- coding: utf-8 -*-

{
    "name": "Agregando las variables para Parntner",
    "version": "1.0",
    "author": "CUNAMAS",
    "website": "http://www.cunamas.gob.pe/",
    "category": "Localization",
    "description": u"""
    Extendiendo el Partner para uso del Programa
    """,
    "depends": [
        "base",
        "l10n_toponyms_pe",

    ],
    "init_xml": [],
    "data": [
    ],
    "demo_xml": [],
    "update_xml": [
        'views/partner_view.xml',
        'views/partner_view_local_propietario.xml',
    ],
    'installable': True,
    'active': False,
}
